<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->lang->admin_load('products', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('products_model');
        $this->lang->admin_load('settings', $this->Settings->user_language);
        $this->digital_upload_path = 'files/';
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '1024';
        $this->popup_attributes = array('width' => '900', 'height' => '600', 'window_name' => 'sma_popup', 'menubar' => 'yes', 'scrollbars' => 'yes', 'status' => 'no', 'resizable' => 'yes', 'screenx' => '0', 'screeny' => '0');
    }

    function index($warehouse_id = NULL)
    {
        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        } else {
            $this->data['warehouses'] = NULL;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : NULL;
        }

        $this->data['supplier'] = $this->input->get('supplier') ? $this->site->getCompanyByID($this->input->get('supplier')) : NULL;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('products')));
        $meta = array('page_title' => lang('products'), 'bc' => $bc);
        $this->page_construct('products/index', $meta, $this->data);
    }

    function variableimages(){
        if($this->input->post('fileSubmit')){
            $variables = $_POST['variable'];
            $product_id= $_POST['product_id'];
            $files= $_POST['file'];
            $vimage_alt= $_POST['vimage_alt'];

            /* echo "<pre>";
             print_r($variables);
             print_r($vimage_alt);
             print_r($files);
             exit('sad');*/

            foreach ($variables as $key => $variable){
                $photo_variable='';
                $photo_alt='';
                if (count($files) > 0) {
                    $photo_variable  = $files[$variable];
                    $photo_alt  = $vimage_alt[$variable];
                    if($photo_variable !=''){
                        $this->products_model->updateVariableImage($product_id,$variable,$photo_variable,$photo_alt);
                    }

                }

                if(isset($_POST[$variable])){
                    $this->products_model->RemoveVariableImage($product_id,$variable);
                }

            }

            $this->session->set_flashdata('message', 'Variables Images has been updated successfully');
            admin_redirect('products/edit/'.$product_id);
        }
    }

    function getProducts($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('index', TRUE);
        $supplier = $this->input->get('supplier') ? $this->input->get('supplier') : NULL;

        if ((! $this->Owner || ! $this->Admin) && ! $warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }
        $detail_link = anchor('admin/products/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('product_details'));
        $delete_link = "<a href='#' class='tip po' title='<b>" . $this->lang->line("delete_product") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete1' id='a__$1' href='" . admin_url('products/delete/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
            . lang('delete_product') . "</a>";
        $single_barcode = anchor('admin/products/print_barcodes/$1', '<i class="fa fa-print"></i> ' . lang('print_barcode_label'));
        // $single_label = anchor_popup('products/single_label/$1/' . ($warehouse_id ? $warehouse_id : ''), '<i class="fa fa-print"></i> ' . lang('print_label'), $this->popup_attributes);
        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li>' . $detail_link . '</li>
			<li><a href="' . admin_url('products/add/$1') . '"><i class="fa fa-plus-square"></i> ' . lang('duplicate_product') . '</a></li>
			<li><a href="' . admin_url('products/edit/$1') . '"><i class="fa fa-edit"></i> ' . lang('edit_product') . '</a></li>';
        if ($warehouse_id) {
            $action .= '<li><a href="' . admin_url('products/set_rack/$1/' . $warehouse_id) . '" data-toggle="modal" data-target="#myModal"><i class="fa fa-bars"></i> '
                . lang('set_rack') . '</a></li>';
        }
        $action .= '<li><a href="' . base_url() . 'ucloud/plugins/filepreviewer/site/resize_image_inline.php?f=$2&w=1100&h=900&m=medium&uh=&o=jpg" data-type="image" data-toggle="lightbox"><i class="fa fa-file-photo-o"></i> '
            . lang('view_image') . '</a></li>
			<li>' . $single_barcode . '</li>
			<li class="divider"></li>
			<li>' . $delete_link . '</li>
			</ul>
		</div></div>';
        $this->load->library('datatables');
        if ($warehouse_id) {
            $this->datatables
                ->select($this->db->dbprefix('products') . ".id as productid, {$this->db->dbprefix('products')}.image as image, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.name as name, {$this->db->dbprefix('brands')}.name as brand, {$this->db->dbprefix('categories')}.name as cname, cost as cost, price as price, COALESCE(wp.quantity, 0) as quantity, {$this->db->dbprefix('units')}.code as unit, wp.rack as rack, alert_quantity, IF(hide>0,'Hidden','Active') ", FALSE)
                ->from('products');
            if ($this->Settings->display_all_products) {
                $this->datatables->join("( SELECT product_id, quantity, rack from {$this->db->dbprefix('warehouses_products')} WHERE warehouse_id = {$warehouse_id}) wp", 'products.id=wp.product_id', 'left');
            } else {
                $this->datatables->join('warehouses_products wp', 'products.id=wp.product_id', 'left')
                    ->where('wp.warehouse_id', $warehouse_id)
                    ->where('wp.quantity !=', 0);
            }
            $this->datatables->join('categories', 'products.category_id=categories.id', 'left')
                ->join('units', 'products.unit=units.id', 'left')
                ->join('brands', 'products.brand=brands.id', 'left');
            // ->group_by("products.id");
        } else {
            $this->datatables
                ->select($this->db->dbprefix('products') . ".id as productid, {$this->db->dbprefix('products')}.image as image, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.name as name, {$this->db->dbprefix('brands')}.name as brand, {$this->db->dbprefix('categories')}.name as cname, cost as cost, price as price, COALESCE(quantity, 0) as quantity, {$this->db->dbprefix('units')}.code as unit, '' as rack, alert_quantity, IF(hide>0,'Hidden','Active') ", FALSE)
                ->from('products')
                ->join('categories', 'products.category_id=categories.id', 'left')
                ->join('units', 'products.unit=units.id', 'left')
                ->join('brands', 'products.brand=brands.id', 'left')
                ->group_by("products.id");
        }
        if (!$this->Owner && !$this->Admin) {
            if (!$this->session->userdata('show_cost')) {
                $this->datatables->unset_column("cost");
            }
            if (!$this->session->userdata('show_price')) {
                $this->datatables->unset_column("price");
            }
        }
        if ($supplier) {
            $this->datatables->where('supplier1', $supplier)
                ->or_where('supplier2', $supplier)
                ->or_where('supplier3', $supplier)
                ->or_where('supplier4', $supplier)
                ->or_where('supplier5', $supplier);
        }
        $this->datatables->add_column("Actions", $action, "productid, image, code, name");
        echo $this->datatables->generate();
    }

    function set_rack($product_id = NULL, $warehouse_id = NULL)
    {
        $this->sma->checkPermissions('edit', true);

        $this->form_validation->set_rules('rack', lang("rack_location"), 'trim|required');

        if ($this->form_validation->run() == true) {
            $data = array('rack' => $this->input->post('rack'),
                'product_id' => $product_id,
                'warehouse_id' => $warehouse_id,
            );
        } elseif ($this->input->post('set_rack')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products");
        }

        if ($this->form_validation->run() == true && $this->products_model->setRack($data)) {
            $this->session->set_flashdata('message', lang("rack_set"));
            admin_redirect("products/" . $warehouse_id);
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['product'] = $this->site->getProductByID($product_id);
            $wh_pr = $this->products_model->getProductQuantity($product_id, $warehouse_id);
            $this->data['rack'] = $wh_pr['rack'];
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/set_rack', $this->data);

        }
    }

    function barcode($product_code = NULL, $bcs = 'code128', $height = 40)
    {
        if ($this->Settings->barcode_img) {
            header('Content-Type: image/png');
        } else {
            header('Content-type: image/svg+xml');
        }
        echo $this->sma->barcode($product_code, $bcs, $height, true, false, true);
    }

    function print_barcodes($product_id = NULL)
    {
        $this->sma->checkPermissions('barcode', true);

        $this->form_validation->set_rules('style', lang("style"), 'required');

        if ($this->form_validation->run() == true) {

            $style = $this->input->post('style');
            $bci_size = ($style == 10 || $style == 12 ? 50 : ($style == 14 || $style == 18 ? 30 : 20));
            $currencies = $this->site->getAllCurrencies();
            $s = isset($_POST['product']) ? sizeof($_POST['product']) : 0;
            if ($s < 1) {
                $this->session->set_flashdata('error', lang('no_product_selected'));
                admin_redirect("products/print_barcodes");
            }
            for ($m = 0; $m < $s; $m++) {
                $pid = $_POST['product'][$m];
                $quantity = $_POST['quantity'][$m];
                $product = $this->products_model->getProductWithCategory($pid);
                $product->price = $this->input->post('check_promo') ? ($product->promotion ? $product->promo_price : $product->price) : $product->price;

                $tax=0;
                $price = 0;
                $tax_rate = $this->site->getTaxRateByID($product->tax_rate);
                $ctax = $this->site->calculateTax($product, $tax_rate, $price);
                $tax = $this->sma->formatDecimal($ctax['amount']);

                if ($variants = $this->products_model->getProductOptions($pid)) {
                    foreach ($variants as $option) {
                        if ($this->input->post('vt_'.$product->id.'_'.$option->id)) {

                            if($product->tax_method == 1){
                                $calucluate_option_tax = $this->site->calculateTax($product, $tax_rate, $option->price);
                                $option_tax_amount = $this->sma->formatDecimal($calucluate_option_tax['amount']);
                            }else{
                                $option_tax_amount = 0;
                                $tax = 0;
                            }

                            $barcodes[] = array(
                                'site' => $this->input->post('site_name') ? $this->Settings->site_name : FALSE,
                                'name' => $this->input->post('product_name') ? $product->name : FALSE,
                                'translated_name' =>  $this->input->post('translated_product_name') ? $product->translated_name : FALSE,
                                'image' => $this->input->post('product_image') ? $product->image : FALSE,
                                //'barcode' => $this->product_barcode($product->code . $this->Settings->barcode_separator . $option->id, 'code128', $bci_size),
                                'barcode' => $product->code . $this->Settings->barcode_separator . $option->id,
                                'bcs' => 'code128',
                                'bcis' => $bci_size,
                                'price' => $this->input->post('price') ? $option->price != 0 ? ($product->price + $option->price) : $product->price : FALSE,
                                'price_with_tax' => $this->input->post('price_with_tax') ? $tax + $option_tax_amount : FALSE,
                                'unit' => $this->input->post('unit') ? $product->unit : FALSE,
                                'category' => $this->input->post('category') ? $product->category.' - '.$option->name : FALSE,
                                'translated_category' => $this->input->post('translated_category') ? $product->translated_category_name : FALSE,
                                'currencies' => $this->input->post('currencies'),
                                'variants' => $this->input->post('variants') ? $variants : FALSE,
                                'quantity' => $quantity
                            );
                        }
                    }
                } else {
                    $barcodes[] = array(
                        'site' => $this->input->post('site_name') ? $this->Settings->site_name : FALSE,
                        'name' => $this->input->post('product_name') ? $product->name : FALSE,
                        'translated_name' =>  $this->input->post('translated_product_name') ? $product->translated_name : FALSE,
                        'image' => $this->input->post('product_image') ? $product->image : FALSE,
                        'barcode' => $this->product_barcode($product->code, $product->barcode_symbology, $bci_size),
                        'price' => $this->input->post('price') ?  $product->price : FALSE,
                        'price_with_tax' => $this->input->post('price_with_tax') ? $tax : FALSE,
                        'unit' => $this->input->post('unit') ? $product->unit : FALSE,
                        'category' => $this->input->post('category') ? $product->category : FALSE,
                        'translated_category' => $this->input->post('translated_category') ? $product->translated_category_name : FALSE,
                        'currencies' => $this->input->post('currencies'),
                        'variants' => FALSE,
                        'quantity' => $quantity
                    );
                }

            }
            $this->data['barcodes'] = $barcodes;
            $this->data['currencies'] = $currencies;
            $this->data['style'] = $style;
            $this->data['items'] = false;
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('print_barcodes')));
            $meta = array('page_title' => lang('print_barcodes'), 'bc' => $bc);
            $this->page_construct('products/print_barcodes', $meta, $this->data);

        } else {

            if ($this->input->get('purchase') || $this->input->get('transfer')) {
                if ($this->input->get('purchase')) {
                    $purchase_id = $this->input->get('purchase', TRUE);
                    $items = $this->products_model->getPurchaseItems($purchase_id);
                } elseif ($this->input->get('transfer')) {
                    $transfer_id = $this->input->get('transfer', TRUE);
                    $items = $this->products_model->getTransferItems($transfer_id);
                }
                if ($items) {
                    foreach ($items as $item) {
                        if ($row = $this->products_model->getProductByID($item->product_id)) {
                            $selected_variants = false;
                            if ($variants = $this->products_model->getProductOptions($row->id)) {
                                foreach ($variants as $variant) {
                                    $selected_variants[$variant->id] = isset($pr[$row->id]['selected_variants'][$variant->id]) && !empty($pr[$row->id]['selected_variants'][$variant->id]) ? 1 : ($variant->id == $item->option_id ? 1 : 0);
                                }
                            }
                            $pr[$row->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $item->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);
                        }
                    }
                    $this->data['message'] = lang('products_added_to_list');
                }
            }

            if ($product_id) {
                if ($row = $this->site->getProductByID($product_id)) {

                    $selected_variants = false;
                    if ($variants = $this->products_model->getProductOptions($row->id)) {
                        foreach ($variants as $variant) {
                            $selected_variants[$variant->id] = $variant->quantity > 0 ? 1 : 0;
                        }
                    }
                    $pr[$row->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $row->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);

                    $this->data['message'] = lang('product_added_to_list');
                }
            }

            if ($this->input->get('category')) {
                if ($products = $this->products_model->getCategoryProducts($this->input->get('category'))) {
                    foreach ($products as $row) {
                        $selected_variants = false;
                        if ($variants = $this->products_model->getProductOptions($row->id)) {
                            foreach ($variants as $variant) {
                                $selected_variants[$variant->id] = $variant->quantity > 0 ? 1 : 0;
                            }
                        }
                        $pr[$row->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $row->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);
                    }
                    $this->data['message'] = lang('products_added_to_list');
                } else {
                    $pr = array();
                    $this->session->set_flashdata('error', lang('no_product_found'));
                }
            }

            if ($this->input->get('subcategory')) {
                if ($products = $this->products_model->getSubCategoryProducts($this->input->get('subcategory'))) {
                    foreach ($products as $row) {
                        $selected_variants = false;
                        if ($variants = $this->products_model->getProductOptions($row->id)) {
                            foreach ($variants as $variant) {
                                $selected_variants[$variant->id] = $variant->quantity > 0 ? 1 : 0;
                            }
                        }
                        $pr[$row->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $row->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);
                    }
                    $this->data['message'] = lang('products_added_to_list');
                } else {
                    $pr = array();
                    $this->session->set_flashdata('error', lang('no_product_found'));
                }
            }

            $this->data['items'] = isset($pr) ? json_encode($pr) : false;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('print_barcodes')));
            $meta = array('page_title' => lang('print_barcodes'), 'bc' => $bc);
            $this->page_construct('products/print_barcodes', $meta, $this->data);

        }
    }
    function addGalleryImages(){

        $gallaerycode = $this->input->post('gallary_unique_code');
        if ($_FILES['file'] > 0) {

            $this->load->library('upload');
            $config['upload_path'] = $this->upload_path;
            $config['allowed_types'] = $this->image_types;
            //$config['max_size'] = $this->allowed_file_size;
            //$config['max_width'] = $this->Settings->iwidth;
            //$config['max_height'] = $this->Settings->iheight;
            $config['overwrite'] = FALSE;
            $config['max_filename'] = 25;
            $config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('file')) {
                $error = $this->upload->display_errors();
                return false;
                // echo $error;
                $this->session->set_flashdata('error', $error);
                admin_redirect("products/add");
            }

            $photo = $this->upload->file_name;
            $data['image'] = $photo;
            $this->load->library('image_lib');
            $config['image_library'] = 'gd2';
            $config['source_image'] = $this->upload_path . $photo;
            $config['new_image'] = $this->thumbs_path . $photo;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $this->Settings->twidth;
            $config['height'] = $this->Settings->theight;
            $this->image_lib->clear();
            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }

            // Large Image resizing
            $config['new_image'] = $this->upload_path . $photo;
            $config['width'] = $this->Settings->iwidth;
            $config['height'] = $this->Settings->iheight;

            $this->image_lib->clear();
            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }

            if ($this->Settings->watermark) {
                $this->image_lib->clear();
                $wm['source_image'] = $this->upload_path . $photo;
                $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                $wm['wm_type'] = 'text';
                $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                $wm['quality'] = '100';
                $wm['wm_font_size'] = '16';
                $wm['wm_font_color'] = '999999';
                $wm['wm_shadow_color'] = 'CCCCCC';
                $wm['wm_vrt_alignment'] = 'top';
                $wm['wm_hor_alignment'] = 'left';
                $wm['wm_padding'] = '10';
                $this->image_lib->initialize($wm);
                $this->image_lib->watermark();
            }
            $this->image_lib->clear();
            $config = NULL;
            $this->products_model->addGalleryImages($gallaerycode, $photo);
        }
    }
    function editGalleryImages(){

        $product_saved_id = $this->input->post('product_saved_id');
        if ($_FILES['file'] > 0) {

            $this->load->library('upload');
            $config['upload_path'] = $this->upload_path;
            $config['allowed_types'] = $this->image_types;
            /*$config['max_size'] = $this->allowed_file_size;
            $config['max_width'] = $this->Settings->iwidth;
            $config['max_height'] = $this->Settings->iheight;*/
            $config['overwrite'] = FALSE;
            $config['max_filename'] = 25;
            $config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('file')) {
                $error = $this->upload->display_errors();
                return false;
                // echo $error;
                $this->session->set_flashdata('error', $error);
                admin_redirect("products/add");
            }

            $photo = $this->upload->file_name;
            $data['image'] = $photo;
            $this->load->library('image_lib');
            $config['image_library'] = 'gd2';
            $config['source_image'] = $this->upload_path . $photo;
            $config['new_image'] = $this->thumbs_path . $photo;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $this->Settings->twidth;
            $config['height'] = $this->Settings->theight;
            $this->image_lib->clear();
            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }
            // Large Image resizing
            $config['new_image'] = $this->upload_path . $photo ;
            $config['width'] = 'auto';
            $config['height'] = $this->Settings->iheight;

            $this->image_lib->clear();
            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }
            if ($this->Settings->watermark) {
                $this->image_lib->clear();
                $wm['source_image'] = $this->upload_path . $photo;
                $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                $wm['wm_type'] = 'text';
                $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                $wm['quality'] = '100';
                $wm['wm_font_size'] = '16';
                $wm['wm_font_color'] = '999999';
                $wm['wm_shadow_color'] = 'CCCCCC';
                $wm['wm_vrt_alignment'] = 'top';
                $wm['wm_hor_alignment'] = 'left';
                $wm['wm_padding'] = '10';
                $this->image_lib->initialize($wm);
                $this->image_lib->watermark();
            }
            $this->image_lib->clear();
            $config = NULL;
            $this->products_model->editGalleryImages($product_saved_id, $photo);
        }
    }
    function getGalleryimagesByUniquId($uniqueId){

        $gallaerycode = $this->input->post('gallary_unique_code');
        $photos = $this->products_model->getGalleryimagesByUniquId($uniqueId);
        echo json_encode($photos);

    }
    function getGalleryimagesById($uniqueId){
        $photos = $this->products_model->getGalleryimagesById($uniqueId);
        echo json_encode($photos);
        exit();

    }

    function removeGalleryImage(){
        $name = $this->input->post('name');
        $photos = $this->products_model->removeGalleryImage($name);
    }


    /* ------------------------------------------------------- */

    function add($id = NULL)
    {
        /* echo "<pre>";
         print_r($_POST);
         exit();*/
        $this->sma->checkPermissions();
        $this->load->helper('security');
        $warehouses = $this->site->getAllWarehouses();
        $this->form_validation->set_rules('category', lang("category"), 'required|is_natural_no_zero');
        if ($this->input->post('type') == 'standard') {
            $this->form_validation->set_rules('cost', lang("product_cost"), 'required');
            //$this->form_validation->set_rules('unit', lang("product_unit"), 'required');
        }

        $variant_flag = 1;
        if( $_POST['attr_price'][0] == ''){
            $variant_flag = 0;
        }
        if($this->input->post('show_variable_price_on_shop') == 1 && $variant_flag == 0){
            $this->form_validation->set_rules('attributesInput2', 'Product Variants', 'required');
        }else if($this->input->post('cost') == 0 && $variant_flag == 0){
            $this->form_validation->set_rules('attributesInput2', 'Product Variants', 'required');
        }else if($this->input->post('price') == 0 && $variant_flag == 0 ){
            $this->form_validation->set_rules('attributesInput2', 'Product Variants', 'required');
        }

        if ($this->input->post('barcode_symbology') == 'ean13') {
            $this->form_validation->set_rules('code', lang("product_code"), 'min_length[13]|max_length[13]');
        }
        $this->form_validation->set_rules('code', lang("product_code"), 'is_unique[products.code]|alpha_dash');
        $this->form_validation->set_rules('slug', lang("slug"), 'required|is_unique[products.slug]|alpha_dash');
        $this->form_validation->set_rules('alert_quantity', lang("alert_quantity"), 'numeric');
        $this->form_validation->set_rules('product_image', lang("product_image"), 'xss_clean');
        $this->form_validation->set_rules('digital_file', lang("digital_file"), 'xss_clean');
        $this->form_validation->set_rules('userfile', lang("product_gallery_images"), 'xss_clean');
        if ($this->form_validation->run() == true) {
            $tax_rate = $this->input->post('tax_rate') ? $this->site->getTaxRateByID($this->input->post('tax_rate')) : NULL;
            $data = array(
                'code' => $this->input->post('code'),
                'barcode_symbology' => $this->input->post('barcode_symbology'),
                'name' => $this->input->post('name'),
                'translated_name' => $this->input->post('traslate_name'),
                'type' => $this->input->post('type'),
                'brand' => $this->input->post('brand'),
                'category_id' => $this->input->post('category'),
                'subcategory_id' => $this->input->post('subcategory') ? $this->input->post('subcategory') : NULL,
                'cost' => $this->sma->formatDecimal($this->input->post('cost')),
                'price' => $this->sma->formatDecimal($this->input->post('price')),
                'unit' => ($this->input->post('unit')) ? $this->input->post('unit')  : 2,
                'sale_unit' => ($this->input->post('default_sale_unit')) ? $this->input->post('default_sale_unit') : 2,
                'purchase_unit' => ($this->input->post('default_purchase_unit')) ? $this->input->post('default_purchase_unit') : 2 ,
                'tax_rate' => $this->input->post('tax_rate'),
                'tax_method' => $this->input->post('tax_method'),
                'alert_quantity' => $this->input->post('alert_quantity'),
                'track_quantity' => $this->input->post('track_quantity') ? $this->input->post('track_quantity') : '0',
                'details' => $this->input->post('details'),
                'product_details' => $this->input->post('product_details'),
                'supplier1' => $this->input->post('supplier'),
                'supplier1price' => $this->sma->formatDecimal($this->input->post('supplier_price')),
                'supplier2' => $this->input->post('supplier_2'),
                'supplier2price' => $this->sma->formatDecimal($this->input->post('supplier_2_price')),
                'supplier3' => $this->input->post('supplier_3'),
                'supplier3price' => $this->sma->formatDecimal($this->input->post('supplier_3_price')),
                'supplier4' => $this->input->post('supplier_4'),
                'supplier4price' => $this->sma->formatDecimal($this->input->post('supplier_4_price')),
                'supplier5' => $this->input->post('supplier_5'),
                'supplier5price' => $this->sma->formatDecimal($this->input->post('supplier_5_price')),
                'cf1' => $this->input->post('cf1'),
                'cf2' => $this->input->post('cf2'),
                'cf3' => $this->input->post('cf3'),
                'cf4' => $this->input->post('cf4'),
                'cf5' => $this->input->post('cf5'),
                'cf6' => $this->input->post('cf6'),
                'promotion' => $this->input->post('promotion'),
                'promo_price' => $this->sma->formatDecimal($this->input->post('promo_price')),
                'start_date' => $this->input->post('start_date') ? $this->sma->fsd($this->input->post('start_date')) : NULL,
                'end_date' => $this->input->post('end_date') ? $this->sma->fsd($this->input->post('end_date')) : NULL,
                'supplier1_part_no' => $this->input->post('supplier_part_no'),
                'supplier2_part_no' => $this->input->post('supplier_2_part_no'),
                'supplier3_part_no' => $this->input->post('supplier_3_part_no'),
                'supplier4_part_no' => $this->input->post('supplier_4_part_no'),
                'supplier5_part_no' => $this->input->post('supplier_5_part_no'),
                'file' => $this->input->post('file_link'),
                'slug' => $this->input->post('slug'),
                'featured' => $this->input->post('featured'),
                'free_shipment' => $this->input->post('product_freeship') == 1 ? 1 : 0,
                'hsn_code' => $this->input->post('hsn_code'),
                'hide' => $this->input->post('hide') ? $this->input->post('hide') : 0,
                'second_name' => $this->input->post('second_name'),
                'gallary_unique_code' => $this->input->post('gallary_unique_code'),
                'weight' => $this->input->post('weight'),
                'width' => $this->input->post('width'),
                'height' => $this->input->post('height'),
                'image' => $this->input->post('image'),
                'back_image' => $this->input->post('back_image'),
                'gimage' => $this->input->post('gimage'),
                'gimage_alt' => $this->input->post('gimage_alt'),
                'image_alt' => $this->input->post('image_alt'),
                'back_image_alt' => $this->input->post('back_image_alt'),
            );
            $warehouse_qty = NULL;
            $product_attributes = NULL;
            $this->load->library('upload');
            if ($this->input->post('type') == 'standard') {
                $wh_total_quantity = 0;
                $pv_total_quantity = 0;
                for ($s = 2; $s > 5; $s++) {
                    $data['suppliers' . $s] = $this->input->post('supplier_' . $s);
                    $data['suppliers' . $s . 'price'] = $this->input->post('supplier_' . $s . '_price');
                }
                foreach ($warehouses as $warehouse) {
                    if ($this->input->post('wh_qty_' . $warehouse->id)) {
                        $warehouse_qty[] = array(
                            'warehouse_id' => $this->input->post('wh_' . $warehouse->id),
                            'quantity' => $this->input->post('wh_qty_' . $warehouse->id),
                            'rack' => $this->input->post('rack_' . $warehouse->id) ? $this->input->post('rack_' . $warehouse->id) : NULL
                        );
                        $wh_total_quantity += $this->input->post('wh_qty_' . $warehouse->id);
                    }
                }

                if ($this->input->post('attributesInput2')) {
                    $a = sizeof($_POST['attr_name']);
                    for ($r = 0; $r <= $a; $r++) {
                        if (isset($_POST['attr_name'][$r])) {
                            $product_attributes[] = array(
                                'name' => $_POST['attr_name'][$r],
                                'supplier_code' => $_POST['attr_supplier_code'][$r],
                                'cost' => $_POST['attr_supplier_price'][$r],
                                'price' => $_POST['attr_price'][$r],
                                'weight' => $_POST['attr_weight'][$r],
                                'website_status' => $_POST['website_status'][$r],
                            );
                            $pv_total_quantity += $_POST['attr_quantity'][$r];
                        }
                    }

                    $data['variants'] = $this->input->post('variants');
                    $data['additional_guide'] = $this->input->post('additional_guide');
                    $data['attributesInput2'] = $this->input->post('attributesInput2');
                } else {
                    $product_attributes = NULL;
                }

                /*                if ($wh_total_quantity != $pv_total_quantity && $pv_total_quantity != 0) {
                                    $this->form_validation->set_rules('wh_pr_qty_issue', 'wh_pr_qty_issue', 'required');
                                    $this->form_validation->set_message('required', lang('wh_pr_qty_issue'));
                                }*/
            }

            if ($this->input->post('type') == 'service') {
                $data['track_quantity'] = 0;
            } elseif ($this->input->post('type') == 'combo') {
                $total_price = 0;
                $c = sizeof($_POST['combo_item_code']) - 1;
                for ($r = 0; $r <= $c; $r++) {
                    if (isset($_POST['combo_item_code'][$r]) && isset($_POST['combo_item_quantity'][$r]) && isset($_POST['combo_item_price'][$r])) {
                        $items[] = array(
                            'item_code' => $_POST['combo_item_code'][$r],
                            'quantity' => $_POST['combo_item_quantity'][$r],
                            'unit_price' => $_POST['combo_item_price'][$r],
                        );
                    }
                    $total_price += $_POST['combo_item_price'][$r] * $_POST['combo_item_quantity'][$r];
                }
                if ($this->sma->formatDecimal($total_price) != $this->sma->formatDecimal($this->input->post('price'))) {
                    $this->form_validation->set_rules('combo_price', 'combo_price', 'required');
                    $this->form_validation->set_message('required', lang('pprice_not_match_ciprice'));
                }
                $data['track_quantity'] = 0;
            } elseif ($this->input->post('type') == 'digital') {
                if ($_FILES['digital_file']['size'] > 0) {
                    $config['upload_path'] = $this->digital_upload_path;
                    $config['allowed_types'] = $this->digital_file_types;
                    $config['max_size'] = $this->allowed_file_size;
                    $config['overwrite'] = FALSE;
                    $config['encrypt_name'] = TRUE;
                    $config['max_filename'] = 25;
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('digital_file')) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        admin_redirect("products/add");
                    }
                    $file = $this->upload->file_name;
                    $data['file'] = $file;
                } else {
                    if (!$this->input->post('file_link')) {
                        $this->form_validation->set_rules('digital_file', lang("digital_file"), 'required');
                    }
                }
                $config = NULL;
                $data['track_quantity'] = 0;
            }
            if (!isset($items)) {
                $items = NULL;
            }


            if ($_FILES['product_image']['size'] > 0) {

                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                //$config['max_size'] = $this->allowed_file_size;
                //$config['max_width'] = $this->Settings->iwidth;
                //$config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['max_filename'] = 25;
                $config['encrypt_name'] = TRUE;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('product_image')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products/add");
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                // Large Image resizing
                $config['new_image'] = $this->upload_path . $photo;
                $config['width'] = $this->Settings->iwidth;
                $config['height'] = $this->Settings->iheight;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'left';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config = NULL;
            }

            if ($_FILES['back_image']['size'] > 0) {


                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                //$config['max_size'] = $this->allowed_file_size;
                //$config['max_width'] = $this->Settings->iwidth;
                //$config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('back_image')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products/edit/" . $id);
                }
                $photo_back = $this->upload->file_name;
                $data['back_image'] = $photo_back;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo_back;
                $config['new_image'] = $this->thumbs_path . $photo_back;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                // Large Image resizing
                $config['new_image'] = $this->upload_path . $photo_back ;
                $config['width'] = $this->Settings->iwidth;
                $config['height'] = $this->Settings->iheight;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }


                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo_back;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'left';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config = NULL;
            }

            if ($_FILES['userfile']['name'][0] != "") {

                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $files = $_FILES;
                $cpt = count($_FILES['userfile']['name']);
                for ($i = 0; $i < $cpt; $i++) {

                    $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
                    $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
                    $_FILES['userfile']['size'] = $files['userfile']['size'][$i];

                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload()) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        admin_redirect("products/add");
                    } else {

                        $pho = $this->upload->file_name;

                        $photos[] = $pho;

                        $this->load->library('image_lib');
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $this->upload_path . $pho;
                        $config['new_image'] = $this->thumbs_path . $pho;
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = $this->Settings->twidth;
                        $config['height'] = $this->Settings->theight;

                        $this->image_lib->initialize($config);

                        if (!$this->image_lib->resize()) {
                            echo $this->image_lib->display_errors();
                        }

                        if ($this->Settings->watermark) {
                            $this->image_lib->clear();
                            $wm['source_image'] = $this->upload_path . $pho;
                            $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                            $wm['wm_type'] = 'text';
                            $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                            $wm['quality'] = '100';
                            $wm['wm_font_size'] = '16';
                            $wm['wm_font_color'] = '999999';
                            $wm['wm_shadow_color'] = 'CCCCCC';
                            $wm['wm_vrt_alignment'] = 'top';
                            $wm['wm_hor_alignment'] = 'left';
                            $wm['wm_padding'] = '10';
                            $this->image_lib->initialize($wm);
                            $this->image_lib->watermark();
                        }

                        $this->image_lib->clear();
                    }
                }
                $config = NULL;
            } else {
                $photos = NULL;
            }
            $data['quantity'] = isset($wh_total_quantity) ? $wh_total_quantity : 0;
            // $this->sma->print_arrays($data, $warehouse_qty, $product_attributes);
        }

        if ($this->form_validation->run() == true && $this->products_model->addProduct($data, $items, $warehouse_qty, $product_attributes, $photos )) {
            $this->session->set_flashdata('message', lang("product_added"));
            admin_redirect('products');
        } else {


            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['brands'] = $this->site->getAllBrands();
            $this->data['authors'] = $this->site->getAllAuthors();
            $this->data['publishers'] = $this->site->getAllPublishers();
            $this->data['base_units'] = $this->site->getAllBaseUnits();
            $this->data['warehouses'] = $warehouses;
            $this->data['warehouses_products'] = $id ? $this->products_model->getAllWarehousesWithPQ($id) : NULL;
            $this->data['product'] = $id ? $this->products_model->getProductByID($id) : NULL;
            $this->data['variants'] = $this->products_model->getAllVariants();
            $this->data['additional_guide'] = $this->products_model->getAllAdditionalGuide();
            $this->data['combo_items'] = ($id && $this->data['product']->type == 'combo') ? $this->products_model->getProductComboItems($id) : NULL;
            $this->data['product_options'] = $id ? $this->products_model->getProductOptionsWithWH($id) : NULL;
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('add_product')));
            $meta = array('page_title' => lang('add_product'), 'bc' => $bc);
            $this->page_construct('products/add', $meta, $this->data);
        }
    }



    function language_html_by_ajax($lang_id)
    {
        $cateories = $this->site->getAllCategories();
        $author = $this->site->getAllAuthors();;
        $publisher = $this->site->getAllPublishers();

        $html_category = '<option></option>';
        $html_author = '<option></option>';
        $html_publisher = '<option></option>';

        //category options
        if(!empty($cateories)){
            foreach($cateories as $key => $data){
                if($lang_id == 2) {
                    $html_category .= '<option value="' . $data->id . '">' . $data->translated_category_name . '</option>';
                }else{
                    $html_category .= '<option value="' . $data->id . '">' . $data->name . '</option>';
                }
            }
        }
        //author options
        if(!empty($author)){
            foreach($author as $key2 => $data2){
                if($lang_id == 2) {
                    $html_author .= '<option value="' . $data2->id . '">' . $data2->translated_name . '</option>';
                }else{
                    $html_author .= '<option value="' . $data2->id . '">' . $data2->name . '</option>';
                }
            }
        }
        //publisher options
        if(!empty($publisher)){
            foreach($publisher as $key3 => $data3){
                if($lang_id == 2) {
                    $html_publisher .= '<option value="' . $data3->id . '">' . $data3->translated_name . '</option>';
                }else{
                    $html_publisher .= '<option value="' . $data3->id . '">' . $data3->name . '</option>';
                }
            }
        }

        echo json_encode(array('category' => $html_category,'author' => $html_author,'publisher' => $html_publisher));

    }



    // ********************************************************************************
    //Category Section
    function categories()
    {

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('categories')));
        $meta = array('page_title' => lang('categories'), 'bc' => $bc);
        $this->page_construct('products/categories', $meta, $this->data);
    }

    function getCategories()
    {

        $print_barcode = anchor('admin/products/print_barcodes/?category=$1', '<i class="fa fa-print"></i>', 'title="'.lang('print_barcodes').'" class="tip"');

        $this->load->library('datatables');
        $this->datatables
            ->select("{$this->db->dbprefix('categories')}.id as id, {$this->db->dbprefix('categories')}.image, {$this->db->dbprefix('categories')}.code, {$this->db->dbprefix('categories')}.name,{$this->db->dbprefix('categories')}.translated_category_name, {$this->db->dbprefix('categories')}.slug, c.name as parent", FALSE)
            ->from("categories")
            ->join("categories c", 'c.id=categories.parent_id', 'left')
            ->group_by('categories.id')
            ->add_column("Actions", "<div class=\"text-center\">".$print_barcode." <a href='" . admin_url('products/edit_category/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_category") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_category") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('products/delete_category/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");

        echo $this->datatables->generate();
    }

    function add_category()
    {
        $this->load->helper('security');
        $this->form_validation->set_rules('code', lang("category_code"), 'trim|is_unique[categories.code]|required');
        $this->form_validation->set_rules('name', lang("name"), 'required|is_unique[categories.name]|min_length[3]');
        $this->form_validation->set_rules('translated_category_name', lang("translated_name"), 'required|is_unique[categories.translated_category_name]|min_length[3]');
        $this->form_validation->set_rules('slug', lang("slug"), 'required|is_unique[categories.slug]|alpha_dash');
        $this->form_validation->set_rules('userfile', lang("category_image"), 'xss_clean');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'translated_category_name' => $this->input->post('translated_category_name'),
                'code' => $this->input->post('code'),
                'slug' => $this->input->post('slug'),
                'category_carousel' => $this->input->post('category_carousel'),
                'parent_id' => $this->input->post('parent'),
                'category_carousel' => 0,
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                //$config['max_size'] = $this->allowed_file_size;
                //$config['max_width'] = $this->Settings->iwidth;
                //$config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                // Large Image resizing
                $config['new_image'] = $this->upload_path . $photo;
                $config['width'] = $this->Settings->iwidth;
                $config['height'] = $this->Settings->iheight;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'left';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config = NULL;
            }

        } elseif ($this->input->post('add_category')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/categories");
        }

        if ($this->form_validation->run() == true && $this->products_model->addCategory($data)) {
            $this->session->set_flashdata('message', lang("category_added"));
            admin_redirect("products/categories");
        } else {

            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['categories'] = $this->products_model->getParentCategories();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/add_category', $this->data);

        }
    }

    function edit_category($id = NULL)
    {
        $this->load->helper('security');
        $this->form_validation->set_rules('code', lang("category_code"), 'trim|required');
        $pr_details = $this->products_model->getCategoryByID($id);
        if ($this->input->post('code') != $pr_details->code) {
            $this->form_validation->set_rules('code', lang("category_code"), 'required|is_unique[categories.code]');
        }

        if ($this->input->post('name') != $pr_details->name) {
            $this->form_validation->set_rules('name', lang("category_name"), 'required|is_unique[categories.name]');
        }
        if ($this->input->post('translated_category_name') != $pr_details->translated_category_name) {
            $this->form_validation->set_rules('translated_category_name', lang("translated_name"), 'required|is_unique[categories.translated_category_name]');
        }


        $this->form_validation->set_rules('slug', lang("slug"), 'required|alpha_dash');
        if ($this->input->post('slug') != $pr_details->slug) {
            $this->form_validation->set_rules('slug', lang("slug"), 'required|alpha_dash|is_unique[categories.slug]');
        }

        $this->form_validation->set_rules('name', lang("category_name"), 'required|min_length[3]');
        $this->form_validation->set_rules('userfile', lang("category_image"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            $data = array(
                'name' => $this->input->post('name'),
                'translated_category_name' => $this->input->post('translated_category_name'),
                'code' => $this->input->post('code'),
                'slug' => $this->input->post('slug'),
                'category_carousel' => $this->input->post('category_carousel'),
                'parent_id' => $this->input->post('parent'),
                'is_updated' => 1,
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                //$config['max_size'] = $this->allowed_file_size;
                //$config['max_width'] = $this->Settings->iwidth;
                //$config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                // Large Image resizing
                $config['new_image'] = $this->upload_path . $photo ;
                $config['width'] = $this->Settings->iwidth;
                $config['height'] = $this->Settings->iheight;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'left';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config = NULL;
            }

        } elseif ($this->input->post('edit_category')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/categories");
        }

        if ($this->form_validation->run() == true && $this->products_model->updateCategory($id, $data)) {
            $this->session->set_flashdata('message', lang("category_updated"));
            admin_redirect("products/categories");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['category'] = $this->products_model->getCategoryByID($id);
            $this->data['categories'] = $this->products_model->getParentCategories();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/edit_category', $this->data);

        }
    }

    function delete_category($id = NULL)
    {

        if ($this->site->getSubCategories($id)) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("category_has_subcategory")));
        }

        if ($this->products_model->deleteCategory($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("category_deleted")));
        }
    }

    function import_categories()
    {

        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');
                $config['upload_path'] = 'files/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products/categories");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen('files/' . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $keys = array('code', 'name', 'image', 'pcode');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }

                foreach ($final as $csv_ct) {
                    if ( ! $this->products_model->getCategoryByCode(trim($csv_ct['code']))) {
                        $pcat = NULL;
                        $pcode = trim($csv_ct['pcode']);
                        if (!empty($pcode)) {
                            if ($pcategory = $this->products_model->getCategoryByCode(trim($csv_ct['pcode']))) {
                                $data[] = array(
                                    'code' => trim($csv_ct['code']),
                                    'name' => trim($csv_ct['name']),
                                    'image' => trim($csv_ct['image']),
                                    'parent_id' => $pcategory->id,
                                );
                            }
                        } else {
                            $data[] = array(
                                'code' => trim($csv_ct['code']),
                                'name' => trim($csv_ct['name']),
                                'image' => trim($csv_ct['image']),
                            );
                        }
                    }
                }
            }

            // $this->sma->print_arrays($data);
        }

        if ($this->form_validation->run() == true && $this->products_model->addCategories($data)) {
            $this->session->set_flashdata('message', lang("categories_added"));
            admin_redirect('products/categories');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['userfile'] = array('name' => 'userfile',
                'id' => 'userfile',
                'type' => 'text',
                'value' => $this->form_validation->set_value('userfile')
            );
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme.'products/import_categories', $this->data);

        }
    }

    function import_subcategories()
    {

        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');
                $config['upload_path'] = 'files/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products/categories");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen('files/' . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $keys = array('code', 'name', 'category_code', 'image');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }

                $rw = 2;
                foreach ($final as $csv_ct) {
                    if ( ! $this->products_model->getSubcategoryByCode(trim($csv_ct['code']))) {
                        if ($parent_actegory = $this->products_model->getCategoryByCode(trim($csv_ct['category_code']))) {
                            $data[] = array(
                                'code' => trim($csv_ct['code']),
                                'name' => trim($csv_ct['name']),
                                'image' => trim($csv_ct['image']),
                                'category_id' => $parent_actegory->id,
                            );
                        } else {
                            $this->session->set_flashdata('error', lang("check_category_code") . " (" . $csv_ct['category_code'] . "). " . lang("category_code_x_exist") . " " . lang("line_no") . " " . $rw);
                            admin_redirect("products/categories");
                        }
                    }
                    $rw++;
                }
            }

            // $this->sma->print_arrays($data);
        }

        if ($this->form_validation->run() == true && $this->products_model->addSubCategories($data)) {
            $this->session->set_flashdata('message', lang("subcategories_added"));
            admin_redirect('products/categories');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['userfile'] = array('name' => 'userfile',
                'id' => 'userfile',
                'type' => 'text',
                'value' => $this->form_validation->set_value('userfile')
            );
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme.'proudcts/import_subcategories', $this->data);

        }
    }

    function category_actions()
    {

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        $this->products_model->deleteCategory($id);
                    }
                    $this->session->set_flashdata('message', lang("categories_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('categories'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('code'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('image'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('parent_actegory'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $sc = $this->products_model->getCategoryByID($id);
                        $parent_actegory = '';
                        if ($sc->parent_id) {
                            $pc = $this->products_model->getCategoryByID($sc->parent_id);
                            $parent_actegory = $pc->code;
                        }
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $sc->code);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $sc->name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $sc->image);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $parent_actegory);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'categories_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    return create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_record_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    //End of category section
    // ********************************************************************************

    // ********************************************************************************
    //Unit Sections
    function units()
    {

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('units')));
        $meta = array('page_title' => lang('units'), 'bc' => $bc);
        $this->page_construct('products/units', $meta, $this->data);
    }

    function getUnits()
    {

        $this->load->library('datatables');
        $this->datatables
            ->select("{$this->db->dbprefix('units')}.id as id, {$this->db->dbprefix('units')}.code, {$this->db->dbprefix('units')}.name, b.name as base_unit, {$this->db->dbprefix('units')}.operator, {$this->db->dbprefix('units')}.operation_value", FALSE)
            ->from("units")
            ->join("units b", 'b.id=units.base_unit', 'left')
            ->group_by('units.id')
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . admin_url('products/edit_unit/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_unit") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_unit") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('products/delete_unit/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");

        echo $this->datatables->generate();
    }

    function add_unit()
    {

        $this->form_validation->set_rules('code', lang("unit_code"), 'trim|is_unique[units.code]|required');
        $this->form_validation->set_rules('name', lang("unit_name"), 'trim|required');
        if ($this->input->post('base_unit')) {
            $this->form_validation->set_rules('operator', lang("operator"), 'required');
            $this->form_validation->set_rules('operation_value', lang("operation_value"), 'trim|required');
        }

        if ($this->form_validation->run() == true) {

            $data = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'base_unit' => $this->input->post('base_unit') ? $this->input->post('base_unit') : NULL,
                'operator' => $this->input->post('base_unit') ? $this->input->post('operator') : NULL,
                'operation_value' => $this->input->post('operation_value') ? $this->input->post('operation_value') : NULL,
            );

        } elseif ($this->input->post('add_unit')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/units");
        }

        if ($this->form_validation->run() == true && $this->products_model->addUnit($data)) {
            $this->session->set_flashdata('message', lang("unit_added"));
            admin_redirect("products/units");
        } else {

            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['base_units'] = $this->site->getAllBaseUnits();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/add_unit', $this->data);

        }
    }

    function edit_unit($id = NULL)
    {

        $this->form_validation->set_rules('code', lang("code"), 'trim|required');
        $unit_details = $this->site->getUnitByID($id);
        if ($this->input->post('code') != $unit_details->code) {
            $this->form_validation->set_rules('code', lang("code"), 'required|is_unique[units.code]');
        }
        $this->form_validation->set_rules('name', lang("name"), 'trim|required');
        if ($this->input->post('base_unit')) {
            $this->form_validation->set_rules('operator', lang("operator"), 'required');
            $this->form_validation->set_rules('operation_value', lang("operation_value"), 'trim|required');
        }

        if ($this->form_validation->run() == true) {

            $data = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'base_unit' => $this->input->post('base_unit') ? $this->input->post('base_unit') : NULL,
                'operator' => $this->input->post('base_unit') ? $this->input->post('operator') : NULL,
                'operation_value' => $this->input->post('operation_value') ? $this->input->post('operation_value') : NULL,
            );

        } elseif ($this->input->post('edit_unit')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/units");
        }

        if ($this->form_validation->run() == true && $this->products_model->updateUnit($id, $data)) {
            $this->session->set_flashdata('message', lang("unit_updated"));
            admin_redirect("products/units");
        } else {

            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['unit'] = $unit_details;
            $this->data['base_units'] = $this->site->getAllBaseUnits();
            $this->load->view($this->theme . 'products/edit_unit', $this->data);

        }
    }

    function delete_unit($id = NULL)
    {

        if ($this->products_model->getUnitChildren($id)) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("unit_has_subunit")));
        }

        if ($this->products_model->deleteUnit($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("unit_deleted")));
        }
    }

    function unit_actions()
    {

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        $this->products_model->deleteUnit($id);
                    }
                    $this->session->set_flashdata('message', lang("units_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('categories'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('code'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('base_unit'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('operator'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('operation_value'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $unit = $this->site->getUnitByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $unit->code);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $unit->name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $unit->base_unit);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $unit->operator);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $unit->operation_value);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'units_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    return create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_record_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
    //End of Unit section
    // ********************************************************************************

    // ********************************************************************************
    //Brand Section
    function brands()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('brands')));
        $meta = array('page_title' => lang('brands'), 'bc' => $bc);
        $this->page_construct('products/brands', $meta, $this->data);
    }

    function getBrands()
    {

        $this->load->library('datatables');
        $this->datatables
            ->select("id, image, code, name, slug")
            ->from("brands")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . admin_url('products/edit_brand/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_brand") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_brand") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('products/delete_brand/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");

        echo $this->datatables->generate();
    }

    function add_brand()
    {

        $this->form_validation->set_rules('name', lang("brand_name"), 'trim|required|is_unique[brands.name]|alpha_numeric_spaces');
        $this->form_validation->set_rules('slug', lang("slug"), 'trim|required|is_unique[brands.slug]|alpha_dash');

        if ($this->form_validation->run() == true) {

            $data = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'slug' => $this->input->post('slug'),
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                //$config['max_size'] = $this->allowed_file_size;
                //$config['max_width'] = $this->Settings->iwidth;
                //$config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                $this->image_lib->clear();
            }

            // Large Image resizing
            $config['new_image'] = $this->upload_path . $photo ;
            $config['width'] = $this->Settings->iwidth;
            $config['height'] = $this->Settings->iheight;

            $this->image_lib->clear();
            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }


        } elseif ($this->input->post('add_brand')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/brands");
        }

        if ($this->form_validation->run() == true && $this->products_model->addBrand($data)) {
            $this->session->set_flashdata('message', lang("brand_added"));
            admin_redirect("products/brands");
        } else {

            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/add_brand', $this->data);

        }
    }

    function edit_brand($id = NULL)
    {

        $this->form_validation->set_rules('name', lang("brand_name"), 'trim|required|alpha_numeric_spaces');
        $brand_details = $this->site->getBrandByID($id);
        if ($this->input->post('name') != $brand_details->name) {
            $this->form_validation->set_rules('name', lang("brand_name"), 'required|is_unique[brands.name]');
        }
        $this->form_validation->set_rules('slug', lang("slug"), 'required|alpha_dash');
        if ($this->input->post('slug') != $brand_details->slug) {
            $this->form_validation->set_rules('slug', lang("slug"), 'required|alpha_dash|is_unique[brands.slug]');
        }

        if ($this->form_validation->run() == true) {

            $data = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'slug' => $this->input->post('slug'),
                'is_updated' => 1,
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                //$config['max_size'] = $this->allowed_file_size;
                //$config['max_width'] = $this->Settings->iwidth;
                //$config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                // Large Image resizing
                $config['new_image'] = $this->upload_path . $photo ;
                $config['width'] = $this->Settings->iwidth;
                $config['height'] = $this->Settings->iheight;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }


                $this->image_lib->clear();
            }

        } elseif ($this->input->post('edit_brand')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/brands");
        }

        if ($this->form_validation->run() == true && $this->products_model->updateBrand($id, $data)) {
            $this->session->set_flashdata('message', lang("brand_updated"));
            admin_redirect("products/brands");
        } else {

            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['brand'] = $brand_details;
            $this->load->view($this->theme . 'products/edit_brand', $this->data);

        }
    }

    function delete_brand($id = NULL)
    {

        if ($this->products_model->brandHasProducts($id)) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("brand_has_products")));
        }

        if ($this->products_model->deleteBrand($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("brand_deleted")));
        }
    }

    function import_brands()
    {

        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');
                $config['upload_path'] = 'files/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("system_settings/brands");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen('files/' . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $keys = array('name', 'code', 'image');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }

                foreach ($final as $csv_ct) {
                    if ( ! $this->products_model->getBrandByName(trim($csv_ct['name']))) {
                        $data[] = array(
                            'code' => trim($csv_ct['code']),
                            'name' => trim($csv_ct['name']),
                            'image' => trim($csv_ct['image']),
                        );
                    }
                }
            }

            // $this->sma->print_arrays($data);
        }

        if ($this->form_validation->run() == true && !empty($data) && $this->products_model->addBrands($data)) {
            $this->session->set_flashdata('message', lang("brands_added"));
            admin_redirect('products/brands');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['userfile'] = array('name' => 'userfile',
                'id' => 'userfile',
                'type' => 'text',
                'value' => $this->form_validation->set_value('userfile')
            );
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme.'products/import_brands', $this->data);

        }
    }

    function brand_actions()
    {

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        $this->products_model->deleteBrand($id);
                    }
                    $this->session->set_flashdata('message', lang("brands_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('brands'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('code'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('image'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $brand = $this->site->getBrandByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $brand->name);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $brand->code);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $brand->image);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'brands_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    return create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_record_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
    //End of brand section
    // ********************************************************************************




    // ********************************************************************************
    //Authors Section
    function authors()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('brands')));
        $meta = array('page_title' => lang('authors'), 'bc' => $bc);
        $this->page_construct('products/authors', $meta, $this->data);
    }

    function getAuthors()
    {

        $this->load->library('datatables');
        $this->datatables
            ->select("id, image, code, name, translated_name , slug")
            ->from("authors")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . admin_url('products/edit_author/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_author") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_author") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('products/delete_author/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");

        echo $this->datatables->generate();
    }

    function add_author()
    {

        $this->form_validation->set_rules('name', lang("author_name"), 'trim|required|is_unique[authors.name]|alpha_numeric_spaces');
        $this->form_validation->set_rules('translated_name', lang("translated_name"), 'trim|required|is_unique[authors.translated_name]|alpha_numeric_spaces');
        $this->form_validation->set_rules('code', lang("author_code"), 'trim|required|is_unique[authors.code]|alpha_numeric_spaces');
        $this->form_validation->set_rules('slug', lang("slug"), 'trim|required|is_unique[authors.slug]|alpha_dash');

        if ($this->form_validation->run() == true) {

            $data = array(
                'name' => $this->input->post('name'),
                'translated_name' => $this->input->post('translated_name'),
                'code' => $this->input->post('code'),
                'slug' => $this->input->post('slug'),
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                //$config['max_size'] = $this->allowed_file_size;
                //$config['max_width'] = $this->Settings->iwidth;
                //$config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                // Large Image resizing
                $config['new_image'] = $this->upload_path . $photo ;
                $config['width'] = $this->Settings->iwidth;
                $config['height'] = $this->Settings->iheight;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                $this->image_lib->clear();
            }

        } elseif ($this->input->post('add_author')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/authors");
        }

        if ($this->form_validation->run() == true && $this->products_model->addAuthor($data)) {
            $this->session->set_flashdata('message', lang("author_added"));
            admin_redirect("products/authors");
        } else {

            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/add_author', $this->data);

        }
    }

    function edit_author($id = NULL)
    {

        $this->form_validation->set_rules('name', lang("author_name"), 'trim|required|alpha_numeric_spaces');
        $author_details = $this->site->getAuthorByID($id);
        if ($this->input->post('name') != $author_details->name) {
            $this->form_validation->set_rules('name', lang("author_name"), 'required|is_unique[authors.name]');
        }

        if ($this->input->post('translated_name') != $author_details->translated_name) {
            $this->form_validation->set_rules('translated_name', lang("translated_name"), 'required|is_unique[authors.translated_name]');
        }

        $this->form_validation->set_rules('slug', lang("slug"), 'required|alpha_dash');
        if ($this->input->post('slug') != $author_details->slug) {
            $this->form_validation->set_rules('slug', lang("slug"), 'required|alpha_dash|is_unique[authors.slug]');
        }

        if ($this->form_validation->run() == true) {

            $data = array(
                'name' => $this->input->post('name'),
                'translated_name' => $this->input->post('translated_name'),
                'code' => $this->input->post('code'),
                'slug' => $this->input->post('slug'),
                'is_updated' => 1,
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                //$config['max_size'] = $this->allowed_file_size;
                // $config['max_width'] = $this->Settings->iwidth;
                //$config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                // Large Image resizing
                $config['new_image'] = $this->upload_path . $photo ;
                $config['width'] = $this->Settings->iwidth;
                $config['height'] = $this->Settings->iheight;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                $this->image_lib->clear();
            }

        } elseif ($this->input->post('edit_author')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/authors");
        }

        if ($this->form_validation->run() == true && $this->products_model->updateAuthor($id, $data)) {
            $this->session->set_flashdata('message', lang("author_updated"));
            admin_redirect("products/authors");
        } else {

            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['author'] = $author_details;
            $this->load->view($this->theme . 'products/edit_author', $this->data);

        }
    }

    function delete_author($id = NULL)
    {

        if ($this->products_model->authorHasProducts($id)) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("author_has_products")));
        }

        if ($this->products_model->deleteAuthor($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("author_deleted")));
        }
    }

    /*function import_brands()
    {

        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');
                $config['upload_path'] = 'files/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("system_settings/brands");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen('files/' . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $keys = array('name', 'code', 'image');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }

                foreach ($final as $csv_ct) {
                    if ( ! $this->products_model->getBrandByName(trim($csv_ct['name']))) {
                        $data[] = array(
                            'code' => trim($csv_ct['code']),
                            'name' => trim($csv_ct['name']),
                            'image' => trim($csv_ct['image']),
                        );
                    }
                }
            }

            // $this->sma->print_arrays($data);
        }

        if ($this->form_validation->run() == true && !empty($data) && $this->products_model->addBrands($data)) {
            $this->session->set_flashdata('message', lang("brands_added"));
            admin_redirect('products/brands');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['userfile'] = array('name' => 'userfile',
                'id' => 'userfile',
                'type' => 'text',
                'value' => $this->form_validation->set_value('userfile')
            );
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme.'products/import_brands', $this->data);

        }
    }*/

    function author_actions()
    {

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        $this->products_model->deleteAuthor($id);
                    }
                    $this->session->set_flashdata('message', lang("authors_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('authors'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('code'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('image'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $author = $this->site->getAuthorByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $author->name);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $author->code);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $author->image);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'authors_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    return create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_record_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
    //End of Authors section
    // ********************************************************************************


    // ********************************************************************************
    //Publisher Section
    function publishers()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('system_settings'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('brands')));
        $meta = array('page_title' => lang('publishers'), 'bc' => $bc);
        $this->page_construct('products/publishers', $meta, $this->data);
    }

    function getPublishers()
    {

        $this->load->library('datatables');
        $this->datatables
            ->select("id, image, code, name, translated_name , slug")
            ->from("publishers")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . admin_url('products/edit_publisher/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_publisher") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_publisher") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('products/delete_publisher/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");

        echo $this->datatables->generate();
    }

    function add_publisher()
    {

        $this->form_validation->set_rules('name', lang("publisher_name"), 'trim|required|is_unique[publishers.name]|alpha_numeric_spaces');
        $this->form_validation->set_rules('translated_name', lang("translated_name"), 'trim|required|is_unique[publishers.translated_name]|alpha_numeric_spaces');
        $this->form_validation->set_rules('code', lang("publisher_code"), 'trim|required|is_unique[publishers.code]|alpha_numeric_spaces');
        $this->form_validation->set_rules('slug', lang("slug"), 'trim|required|is_unique[publishers.slug]|alpha_dash');

        if ($this->form_validation->run() == true) {

            $data = array(
                'name' => $this->input->post('name'),
                'translated_name' => $this->input->post('translated_name'),
                'code' => $this->input->post('code'),
                'slug' => $this->input->post('slug'),
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                //$config['max_size'] = $this->allowed_file_size;
                //$config['max_width'] = $this->Settings->iwidth;
                //$config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                // Large Image resizing
                $config['new_image'] = $this->upload_path . $photo ;
                $config['width'] = $this->Settings->iwidth;
                $config['height'] = $this->Settings->iheight;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                $this->image_lib->clear();
            }

        } elseif ($this->input->post('add_publisher')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/publishers");
        }

        if ($this->form_validation->run() == true && $this->products_model->addPublisher($data)) {
            $this->session->set_flashdata('message', lang("publisher_added"));
            admin_redirect("products/publishers");
        } else {

            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/add_publisher', $this->data);

        }
    }

    function edit_publisher($id = NULL)
    {

        $this->form_validation->set_rules('name', lang("publisher_name"), 'trim|required|alpha_numeric_spaces');
        $publisher_details = $this->site->getPublisherByID($id);
        if ($this->input->post('name') != $publisher_details->name) {
            $this->form_validation->set_rules('name', lang("publisher_name"), 'required|is_unique[publishers.name]');
        }
        if ($this->input->post('translated_name') != $publisher_details->translated_name) {
            $this->form_validation->set_rules('translated_name', lang("translated_name"), 'required|is_unique[publishers.translated_name]');
        }

        $this->form_validation->set_rules('slug', lang("slug"), 'required|alpha_dash');
        if ($this->input->post('slug') != $publisher_details->slug) {
            $this->form_validation->set_rules('slug', lang("slug"), 'required|alpha_dash|is_unique[publishers.slug]');
        }

        if ($this->form_validation->run() == true) {

            $data = array(
                'name' => $this->input->post('name'),
                'translated_name' => $this->input->post('translated_name'),
                'code' => $this->input->post('code'),
                'slug' => $this->input->post('slug'),
                'is_updated' => 1,
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                //$config['max_size'] = $this->allowed_file_size;
                //$config['max_width'] = $this->Settings->iwidth;
                //$config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                // Large Image resizing
                $config['new_image'] = $this->upload_path . $photo ;
                $config['width'] = $this->Settings->iwidth;
                $config['height'] = $this->Settings->iheight;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                $this->image_lib->clear();
            }

        } elseif ($this->input->post('edit_publisher')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/publishers");
        }

        if ($this->form_validation->run() == true && $this->products_model->updatePublisher($id, $data)) {
            $this->session->set_flashdata('message', lang("author_updated"));
            admin_redirect("products/publishers");
        } else {

            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['publisher'] = $publisher_details;
            $this->load->view($this->theme . 'products/edit_publisher', $this->data);

        }
    }

    function delete_publisher($id = NULL)
    {

        if ($this->products_model->publisherHasProducts($id)) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("publisher_has_products")));
        }

        if ($this->products_model->deletePublisher($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("publisher_deleted")));
        }
    }

    /*function import_brands()
    {

        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');
                $config['upload_path'] = 'files/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("system_settings/brands");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen('files/' . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $keys = array('name', 'code', 'image');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }

                foreach ($final as $csv_ct) {
                    if ( ! $this->products_model->getBrandByName(trim($csv_ct['name']))) {
                        $data[] = array(
                            'code' => trim($csv_ct['code']),
                            'name' => trim($csv_ct['name']),
                            'image' => trim($csv_ct['image']),
                        );
                    }
                }
            }

            // $this->sma->print_arrays($data);
        }

        if ($this->form_validation->run() == true && !empty($data) && $this->products_model->addBrands($data)) {
            $this->session->set_flashdata('message', lang("brands_added"));
            admin_redirect('products/brands');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['userfile'] = array('name' => 'userfile',
                'id' => 'userfile',
                'type' => 'text',
                'value' => $this->form_validation->set_value('userfile')
            );
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme.'products/import_brands', $this->data);

        }
    }*/

    function publisher_actions()
    {

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        $this->products_model->deletePublisher($id);
                    }
                    $this->session->set_flashdata('message', lang("publishers_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('publishers'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('code'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('image'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $author = $this->site->getPublisherByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $author->name);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $author->code);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $author->image);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'publishers_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    return create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_record_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
    //End of Publisher section
    // ********************************************************************************


    // Varient Section
    function variants()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('variants')));
        $meta = array('page_title' => lang('variants'), 'bc' => $bc);
        $this->page_construct('products/variants', $meta, $this->data);
    }

    function getVariants()
    {

        $this->load->library('datatables');
        $this->datatables
            ->select("id, name")
            ->from("variants")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . admin_url('products/edit_variant/$1') . "' class='tip' title='" . lang("edit_variant") . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_variant") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('products/delete_variant/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        //->unset_column('id');

        echo $this->datatables->generate();
    }

    function add_variant()
    {

        $this->form_validation->set_rules('name', lang("name"), 'trim|is_unique[variants.name]|required');

        if ($this->form_validation->run() == true) {
            $data = array('name' => $this->input->post('name'));
        } elseif ($this->input->post('add_variant')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/variants");
        }

        if ($this->form_validation->run() == true && $this->products_model->addVariant($data)) {
            $this->session->set_flashdata('message', lang("variant_added"));
            admin_redirect("products/variants");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/add_variant', $this->data);
        }
    }

    function edit_variant($id = NULL)
    {

        $this->form_validation->set_rules('name', lang("name"), 'trim|required');
        $tax_details = $this->products_model->getVariantByID($id);
        if ($this->input->post('name') != $tax_details->name) {
            $this->form_validation->set_rules('name', lang("name"), 'required|is_unique[variants.name]');
        }

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'is_updated' => 1,
            );
        } elseif ($this->input->post('edit_variant')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/variants");
        }

        if ($this->form_validation->run() == true && $this->products_model->updateVariant($id, $data)) {
            $this->session->set_flashdata('message', lang("variant_updated"));
            admin_redirect("products/variants");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['variant'] = $tax_details;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/edit_variant', $this->data);
        }
    }

    function delete_variant($id = NULL)
    {
        if ($this->products_model->deleteVariant($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("variant_deleted")));
        }
    }
    //End of Varient Section


    //Varient Group section start

    function variants_group()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('variants')));
        $meta = array('page_title' => lang('variants_group'), 'bc' => $bc);
        $this->page_construct('products/variants_group', $meta, $this->data);
    }

    function getVariants_group()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("variants_group.id as id,  CONCAT(sma_variants.name, ' > ' , sma_variants_group.name ) AS name , code")
            ->join('variants', 'variants.id=variants_group.variant_id', 'left')
            ->from("variants_group")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . admin_url('products/edit_variant_group/$1') . "' class='tip' title='" . lang("edit_variant") . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_variant") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('products/delete_variant_group/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        //->unset_column('id');
        //echo $this->db->get_compiled_select();

        echo $this->datatables->generate();
    }
    function getSingleVariants_group()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("variants_group.id as id,  CONCAT(sma_variants.name, ' > ' , sma_variants_group.name ) AS name , code")
            ->join('variants', 'variants.id=variants_group.variant_id', 'left')
            ->from("variants_group")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . admin_url('products/edit_variant_group/$1') . "' class='tip' title='" . lang("edit_variant") . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_variant") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('products/delete_variant_group/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        //->unset_column('id');
        //echo $this->db->get_compiled_select();

        echo $this->datatables->generate();
    }

    function add_variant_group()
    {

        $this->form_validation->set_rules('code', lang("code"), 'trim|required|is_unique[variants_group.code]');
        $this->form_validation->set_rules('name', lang("name"), 'trim|required');
        $this->form_validation->set_rules('variant_group_id', lang("variant_group_id"), 'trim|required');
        $this->form_validation->set_rules('userfile', lang("category_image"), 'xss_clean');

        $is_variant_repeated = $this->products_model->checkVariantGroupDuplication($this->input->post('variant_group_id'),$this->input->post('name') );
        if($is_variant_repeated){
            $this->form_validation->set_rules('name', lang("name"), 'is_unique[variants_group.name]');
        }

        if ($this->form_validation->run() == true) {
            $data = array(
                'variant_id' => $this->input->post('variant_group_id'),
                'code' => $this->input->post('code'),
                'name' => $this->input->post('name'),
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                //$config['max_size'] = $this->allowed_file_size;
                //$config['max_width'] = $this->Settings->iwidth;
                //$config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                // Large Image resizing
                $config['new_image'] = $this->upload_path . $photo;
                $config['width'] = $this->Settings->iwidth;
                $config['height'] = $this->Settings->iheight;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'left';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config = NULL;
            }


        } elseif ($this->input->post('add_variant_group')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/variants_group");
        }

        if ($this->form_validation->run() == true && $this->products_model->addVariantGroup($data)) {
            $this->session->set_flashdata('message', lang("variant_added"));
            admin_redirect("products/variants_group");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/add_variant_group', $this->data);
        }
    }

    function edit_variant_group($id = NULL)
    {

        $this->form_validation->set_rules('name', lang("name"), 'trim|required');
        $this->form_validation->set_rules('code', lang("code"), 'trim|required');
        $tax_details = $this->products_model->getVariantGroupByID($id);
        if ($this->input->post('name') != $tax_details->name) {
            $this->form_validation->set_rules('name', lang("name"), 'required|is_unique[variants.name]');
        }

        if ($this->form_validation->run() == true) {
            $data = array(
                'variant_id' => $this->input->post('variant_group_id'),
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                //$config['max_size'] = $this->allowed_file_size;
                //$config['max_width'] = $this->Settings->iwidth;
                //$config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                // Large Image resizing
                $config['new_image'] = $this->upload_path . $photo;
                $config['width'] = $this->Settings->iwidth;
                $config['height'] = $this->Settings->iheight;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'left';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config = NULL;
            }

        } elseif ($this->input->post('edit_variant')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/variants_group");
        }

        if ($this->form_validation->run() == true && $this->products_model->updateVariant_group($id, $data)) {
            $this->session->set_flashdata('message', lang("variant_updated"));
            admin_redirect("products/variants_group");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['variant'] = $tax_details;
            $this->data['modal_js'] = $this->site->modal_js();

            $this->load->view($this->theme . 'products/edit_variant_group', $this->data);
        }
    }

    function delete_variant_group($id = NULL)
    {
        if ($this->products_model->deleteVariantGroup($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("variant_deleted")));
        }
    }




    //End of Varient Group Section


    function suggestions()
    {
        $term = $this->input->get('term', TRUE);
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }

        $rows = $this->products_model->getProductNames($term);
        if ($rows) {
            foreach ($rows as $row) {
                $pr[] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => 1);
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    function get_suggestions()
    {
        $term = $this->input->get('term', TRUE);
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }

        $rows = $this->products_model->getProductsForPrinting($term);
        if ($rows) {
            foreach ($rows as $row) {
                $variants = $this->products_model->getProductOptions($row->id);
                $pr[] = array('id' => $row->id, 'translated_name' => $row->translated_name ,  'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => 1, 'variants' => $variants);
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }


    function get_suggestions_promotion()
    {
        $term = $this->input->get('term', TRUE);
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }

        $rows = $this->products_model->getProductsForPrinting($term);
        if ($rows) {
            foreach ($rows as $row) {
                $variants = $this->products_model->getProductOptions($row->id);
                $pr[] = array('id' => $row->id, 'translated_name' => $row->translated_name ,  'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => 1, 'variants' => '');
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    function get_suggestions_enabled()
    {
        $term = $this->input->get('term', TRUE);
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }

        $rows = $this->products_model->getProductsForPrintingEnabled($term);
        if ($rows) {
            foreach ($rows as $row) {
                $variants = $this->products_model->getProductOptions($row->id);
                $pr[] = array('id' => $row->id, 'translated_name' => $row->translated_name ,  'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => 1, 'variants' => $variants);
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    function addByAjax()
    {
        if (!$this->mPermissions('add')) {
            exit(json_encode(array('msg' => lang('access_denied'))));
        }
        if ($this->input->get('token') && $this->input->get('token') == $this->session->userdata('user_csrf') && $this->input->is_ajax_request()) {
            $product = $this->input->get('product');
            if (!isset($product['code']) || empty($product['code'])) {
                exit(json_encode(array('msg' => lang('product_code_is_required'))));
            }
            if (!isset($product['name']) || empty($product['name'])) {
                exit(json_encode(array('msg' => lang('product_name_is_required'))));
            }
            if (!isset($product['category_id']) || empty($product['category_id'])) {
                exit(json_encode(array('msg' => lang('product_category_is_required'))));
            }
            if (!isset($product['unit']) || empty($product['unit'])) {
                exit(json_encode(array('msg' => lang('product_unit_is_required'))));
            }
            if (!isset($product['price']) || empty($product['price'])) {
                exit(json_encode(array('msg' => lang('product_price_is_required'))));
            }
            if (!isset($product['cost']) || empty($product['cost'])) {
                exit(json_encode(array('msg' => lang('product_cost_is_required'))));
            }
            if ($this->products_model->getProductByCode($product['code'])) {
                exit(json_encode(array('msg' => lang('product_code_already_exist'))));
            }
            if ($row = $this->products_model->addAjaxProduct($product)) {
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $pr = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'qty' => 1, 'cost' => $row->cost, 'name' => $row->name, 'tax_method' => $row->tax_method, 'tax_rate' => $tax_rate, 'discount' => '0');
                $this->sma->send_json(array('msg' => 'success', 'result' => $pr));
            } else {
                exit(json_encode(array('msg' => lang('failed_to_add_product'))));
            }
        } else {
            json_encode(array('msg' => 'Invalid token'));
        }

    }


    /* -------------------------------------------------------- */

    function edit($id = NULL)
    {
        /*echo '<pre>';
        print_r($_POST);
        die;*/
        $this->sma->checkPermissions();
        $this->load->helper('security');
        if ($this->input->post('id')) {
            $id = $this->input->post('id');
        }
        $warehouses = $this->site->getAllWarehouses();
        $warehouses_products = $this->products_model->getAllWarehousesWithPQ($id);
        $product = $this->site->getProductByID($id);
        if (!$id || !$product) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->form_validation->set_rules('category', lang("category"), 'required|is_natural_no_zero');
        if ($this->input->post('type') == 'standard') {
            $this->form_validation->set_rules('cost', lang("product_cost"), 'required');
            //$this->form_validation->set_rules('unit', lang("product_unit"), 'required');
        }
        $this->form_validation->set_rules('code', lang("product_code"), 'alpha_dash');
        if ($this->input->post('code') !== $product->code) {
            $this->form_validation->set_rules('code', lang("product_code"), 'is_unique[products.code]');
        }
        if ($this->input->post('barcode_symbology') == 'ean13') {
            $this->form_validation->set_rules('code', lang("product_code"), 'min_length[13]|max_length[13]');
        }
        $this->form_validation->set_rules('slug', lang("slug"), 'required|alpha_dash');
        if ($this->input->post('slug') !== $product->slug) {
            $this->form_validation->set_rules('slug', lang("slug"), 'required|is_unique[products.slug]|alpha_dash');
        }

        $variant_flag = 1;
        if( $_POST['attr_price'][0] == ''){
            $variant_flag = 0;
        }
        if($this->input->post('show_variable_price_on_shop') == 1 && $variant_flag == 0){
            $this->form_validation->set_rules('attributesInput2', 'Product Variants', 'required');
        }else if($this->input->post('cost') == 0 && $variant_flag == 0){
           // $this->form_validation->set_rules('attributesInput2', 'Product Variants', 'required');
        }else if($this->input->post('price') == 0 && $variant_flag == 0 ){
            $this->form_validation->set_rules('attributesInput2', 'Product Variants', 'required');
        }


        $this->form_validation->set_rules('alert_quantity', lang("alert_quantity"), 'numeric');
        $this->form_validation->set_rules('product_image', lang("product_image"), 'xss_clean');
        $this->form_validation->set_rules('digital_file', lang("digital_file"), 'xss_clean');
        $this->form_validation->set_rules('userfile', lang("product_gallery_images"), 'xss_clean');

        if ($this->form_validation->run('products/add') == true) {


            $data = array('code' => $this->input->post('code'),
                'barcode_symbology' => $this->input->post('barcode_symbology'),
                'name' => $this->input->post('name'),
                'translated_name' => $this->input->post('translated_name'),
                'type' => $this->input->post('type'),
                'brand' => $this->input->post('brand'),
                'category_id' => $this->input->post('category'),
                'subcategory_id' => $this->input->post('subcategory') ? $this->input->post('subcategory') : NULL,
                'cost' => $this->sma->formatDecimal($this->input->post('cost')),
                'price' => $this->sma->formatDecimal($this->input->post('price')),
                'unit' => $this->input->post('unit'),
                'sale_unit' => $this->input->post('default_sale_unit'),
                'purchase_unit' => $this->input->post('default_purchase_unit'),
                'tax_rate' => $this->input->post('tax_rate'),
                'tax_method' => $this->input->post('tax_method'),
                'alert_quantity' => $this->input->post('alert_quantity'),
                'track_quantity' => $this->input->post('track_quantity') ? $this->input->post('track_quantity') : '0',
                'details' => $this->input->post('details'),
                'product_details' => $this->input->post('product_details'),
                'supplier1' => $this->input->post('supplier'),
                'supplier1price' => $this->sma->formatDecimal($this->input->post('supplier_price')),
                'supplier2' => $this->input->post('supplier_2'),
                'supplier2price' => $this->sma->formatDecimal($this->input->post('supplier_2_price')),
                'supplier3' => $this->input->post('supplier_3'),
                'supplier3price' => $this->sma->formatDecimal($this->input->post('supplier_3_price')),
                'supplier4' => $this->input->post('supplier_4'),
                'supplier4price' => $this->sma->formatDecimal($this->input->post('supplier_4_price')),
                'supplier5' => $this->input->post('supplier_5'),
                'supplier5price' => $this->sma->formatDecimal($this->input->post('supplier_5_price')),
                'cf1' => $this->input->post('cf1'),
                'cf2' => $this->input->post('cf2'),
                'cf3' => $this->input->post('cf3'),
                'cf4' => $this->input->post('cf4'),
                'cf5' => $this->input->post('cf5'),
                'cf6' => $this->input->post('cf6'),
                'promotion' => $this->input->post('promotion'),
                'promo_price' => $this->sma->formatDecimal($this->input->post('promo_price')),
                'start_date' => $this->input->post('start_date') ? $this->sma->fsd($this->input->post('start_date')) : NULL,
                'end_date' => $this->input->post('end_date') ? $this->sma->fsd($this->input->post('end_date')) : NULL,
                'supplier1_part_no' => $this->input->post('supplier_part_no'),
                'supplier2_part_no' => $this->input->post('supplier_2_part_no'),
                'supplier3_part_no' => $this->input->post('supplier_3_part_no'),
                'supplier4_part_no' => $this->input->post('supplier_4_part_no'),
                'supplier5_part_no' => $this->input->post('supplier_5_part_no'),
                'slug' => $this->input->post('slug'),
                'featured' => $this->input->post('featured'),
                'free_shipment' => $this->input->post('product_freeship') == 1 ? 1 : 0,
                'show_variable_price_on_shop' => $this->input->post('show_variable_price_on_shop') == 1 ? 1 : 0,
                'is_updated' => $this->input->post('product_freeship') == 1 ? 1 : 0,
                'hsn_code' => $this->input->post('hsn_code'),
                'hide' => $this->input->post('hide') ? $this->input->post('hide') : 0,
                'second_name' => $this->input->post('second_name'),
                'weight' => $this->input->post('weight'),
                'width' => $this->input->post('width'),
                'height' => $this->input->post('height'),
                'image' => $this->input->post('image'),
                'back_image' => $this->input->post('back_image'),
                'gimage' => $this->input->post('gimage'),
                'gimage_alt' => $this->input->post('gimage_alt'),
                'image_alt' => $this->input->post('image_alt'),
                'back_image_alt' => $this->input->post('back_image_alt'),
            );
            $data['variants'] = $this->input->post('variants');
            $data['additional_guide'] = $this->input->post('additional_guide');
            $data['attributesInput2'] = $this->input->post('attributesInput2');
            $warehouse_qty = NULL;
            $product_attributes = NULL;
            $update_variants = array();
            $this->load->library('upload');
            if ($this->input->post('type') == 'standard') {
                if ($product_variants = $this->products_model->getProductOptions($id)) {
                    foreach ($product_variants as $pv) {
                        $update_variants[] = array(
                            'id' => $this->input->post('variant_id_'.$pv->id),
                            'name' => $this->input->post('variant_name_'.$pv->id),
                            'cost' => $this->input->post('variant_cost_'.$pv->id),
                            'price' => $this->input->post('variant_price_'.$pv->id),
                        );
                    }
                }
                for ($s = 2; $s > 5; $s++) {
                    $data['suppliers' . $s] = $this->input->post('supplier_' . $s);
                    $data['suppliers' . $s . 'price'] = $this->input->post('supplier_' . $s . '_price');
                }
                foreach ($warehouses as $warehouse) {
                    $warehouse_qty[] = array(
                        'warehouse_id' => $this->input->post('wh_' . $warehouse->id),
                        'rack' => $this->input->post('rack_' . $warehouse->id) ? $this->input->post('rack_' . $warehouse->id) : NULL
                    );
                }

                if ($this->input->post('attributesInput2')) {
                    $a = sizeof($_POST['attr_name']);
                    for ($r = 0; $r <= $a; $r++) {
                        if (isset($_POST['attr_name'][$r])) {
                            /*  if ($product_variatnt = $this->products_model->getPrductVariantByPIDandName($id, trim($_POST['attr_name'][$r]))) {
                                  $this->form_validation->set_message('required', lang("product_already_has_variant").' ('.$_POST['attr_name'][$r].')');
                                  $this->form_validation->set_rules('new_product_variant', lang("new_product_variant"), 'required');
                              } else {*/
                            $product_attributes[] = array(
                                'name' => $_POST['attr_name'][$r],
                                'supplier_code' => $_POST['attr_supplier_code'][$r],
                                'cost' => $_POST['attr_supplier_price'][$r],
                                'price' => $_POST['attr_price'][$r],
                                'weight' => $_POST['attr_weight'][$r],
                                'website_status' => $_POST['website_status'][$r],
                            );
                            /*}*/
                        }
                    }


                } else {
                    $product_attributes = NULL;
                }

            }

            if($this->input->post('multiple_del')){
                $multiple_img = $this->input->post('multiple_del');
                foreach ($multiple_img as $record) {
                    $insertData[] = array(
                        'id'      => $record,
                        'status'  => 0,
                        'is_updated' => 1
                    );
                }
                $this->products_model->updateProductPhots($insertData);
            }
            if($this->input->post('front_image_del')){
                $this->products_model->updateDelFrontImage($id);
            }
            if($this->input->post('back_image_del')){
                $this->products_model->updateDelBackImage($id);
            }



            if ($this->input->post('type') == 'service') {
                $data['track_quantity'] = 0;
            } elseif ($this->input->post('type') == 'combo') {
                $total_price = 0;
                $c = sizeof($_POST['combo_item_code']) - 1;
                for ($r = 0; $r <= $c; $r++) {
                    if (isset($_POST['combo_item_code'][$r]) && isset($_POST['combo_item_quantity'][$r]) && isset($_POST['combo_item_price'][$r])) {
                        $items[] = array(
                            'item_code' => $_POST['combo_item_code'][$r],
                            'quantity' => $_POST['combo_item_quantity'][$r],
                            'unit_price' => $_POST['combo_item_price'][$r],
                        );
                    }
                    $total_price += $_POST['combo_item_price'][$r] * $_POST['combo_item_quantity'][$r];
                }
                if ($this->sma->formatDecimal($total_price) != $this->sma->formatDecimal($this->input->post('price'))) {
                    $this->form_validation->set_rules('combo_price', 'combo_price', 'required');
                    $this->form_validation->set_message('required', lang('pprice_not_match_ciprice'));
                }
                $data['track_quantity'] = 0;
            } elseif ($this->input->post('type') == 'digital') {
                if ($this->input->post('file_link')) {
                    $data['file'] = $this->input->post('file_link');
                }
                if ($_FILES['digital_file']['size'] > 0) {
                    $config['upload_path'] = $this->digital_upload_path;
                    $config['allowed_types'] = $this->digital_file_types;
                    $config['max_size'] = $this->allowed_file_size;
                    $config['overwrite'] = FALSE;
                    $config['encrypt_name'] = TRUE;
                    $config['max_filename'] = 25;
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('digital_file')) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        admin_redirect("products/add");
                    }
                    $file = $this->upload->file_name;
                    $data['file'] = $file;
                }
                $config = NULL;
                $data['track_quantity'] = 0;
            }
            if (!isset($items)) {
                $items = NULL;
            }

            if ($_FILES['product_image']['size'] > 0) {

                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                //$config['max_size'] = $this->allowed_file_size;
                //$config['max_width'] = $this->Settings->iwidth;
                //$config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('product_image')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products/edit/" . $id);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                // Large Image resizing
                $config['new_image'] = $this->upload_path . $photo ;
                $config['width'] = $this->Settings->iwidth;
                $config['height'] = $this->Settings->iheight;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'left';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config = NULL;
            }

            if ($_FILES['back_image']['size'] > 0) {

                $config1 = array();
                $config1['upload_path'] = $this->upload_path;
                $config1['allowed_types'] = $this->image_types;
                //$config['max_size'] = $this->allowed_file_size;
                //$config['max_width'] = $this->Settings->iwidth;
                //$config['max_height'] = $this->Settings->iheight;
                $config1['overwrite'] = FALSE;
                $config1['encrypt_name'] = TRUE;
                $config1['max_filename'] = 25;
                $this->upload->initialize($config1);
                if (!$this->upload->do_upload('back_image')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products/edit/" . $id);
                }
                $photo_back = $this->upload->file_name;
                $data['back_image'] = $photo_back;
                $this->load->library('image_lib');
                $config1['image_library'] = 'gd2';
                $config1['source_image'] = $this->upload_path . $photo_back;
                $config1['new_image'] = $this->thumbs_path . $photo_back;
                $config1['maintain_ratio'] = TRUE;
                $config1['width'] = $this->Settings->twidth;
                $config1['height'] = $this->Settings->theight;

                $this->image_lib->clear();
                $this->image_lib->initialize($config1);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                // Large Image resizing
                $config1['new_image'] = $this->upload_path . $photo_back ;
                $config1['width'] = $this->Settings->iwidth;
                $config1['height'] = $this->Settings->iheight;

                $this->image_lib->clear();
                $this->image_lib->initialize($config1);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }


                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo_back;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'left';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config1 = NULL;
            }

            if ($_FILES['userfile']['name'][0] != "") {

                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $files = $_FILES;
                $cpt = count($_FILES['userfile']['name']);
                for ($i = 0; $i < $cpt; $i++) {

                    $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
                    $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
                    $_FILES['userfile']['size'] = $files['userfile']['size'][$i];

                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload()) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        admin_redirect("products/edit/" . $id);
                    } else {

                        $pho = $this->upload->file_name;

                        $photos[] = $pho;

                        $this->load->library('image_lib');
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $this->upload_path . $pho;
                        $config['new_image'] = $this->thumbs_path . $pho;
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = $this->Settings->twidth;
                        $config['height'] = $this->Settings->theight;

                        $this->image_lib->initialize($config);

                        if (!$this->image_lib->resize()) {
                            echo $this->image_lib->display_errors();
                        }

                        if ($this->Settings->watermark) {
                            $this->image_lib->clear();
                            $wm['source_image'] = $this->upload_path . $pho;
                            $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                            $wm['wm_type'] = 'text';
                            $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                            $wm['quality'] = '100';
                            $wm['wm_font_size'] = '16';
                            $wm['wm_font_color'] = '999999';
                            $wm['wm_shadow_color'] = 'CCCCCC';
                            $wm['wm_vrt_alignment'] = 'top';
                            $wm['wm_hor_alignment'] = 'left';
                            $wm['wm_padding'] = '10';
                            $this->image_lib->initialize($wm);
                            $this->image_lib->watermark();
                        }

                        $this->image_lib->clear();
                    }
                }
                $config = NULL;
            } else {
                $photos = NULL;
            }
            $data['quantity'] = isset($wh_total_quantity) ? $wh_total_quantity : 0;
            // $this->sma->print_arrays($data, $warehouse_qty, $update_variants, $product_attributes, $photos, $items);
        }

        if ($this->form_validation->run() == true && $this->products_model->updateProduct($id, $data, $items, $warehouse_qty, $product_attributes, $photos, $update_variants)) {
            $this->session->set_flashdata('message', lang("product_updated"));
            admin_redirect('products');
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['images'] = $this->products_model->getProductPhotos($id);
            $this->data['brands'] = $this->site->getAllBrands();
            $this->data['authors'] = $this->site->getAllAuthors();
            $this->data['publishers'] = $this->site->getAllPublishers();
            $this->data['base_units'] = $this->site->getAllBaseUnits();
            $this->data['warehouses'] = $warehouses;
            $this->data['warehouses_products'] = $warehouses_products;
            $this->data['product'] = $product;
            $this->data['variants'] = $this->products_model->getAllVariants();
            $this->data['subunits'] = $this->site->getUnitsByBUID($product->unit);
            $this->data['variableimages']= $this->products_model->getVariableImages($id);
            $this->data['galleryimages']= $this->products_model->getGalleryImages($id);
            $this->data['galleryimagesCount']= (count($this->products_model->getGalleryImages($id)) + 1);
            //$this->data['product_variants'] = $this->products_model->getProductOptions($id);
            $product_variants = array();
            $product_variants =  $this->products_model->getProductOptions($id);
            $reserved_variants = array();
            foreach ($product_variants as $key => $value){
                $variant = $this->products_model->VariantSavedGetByNamePid($value->name,$value->product_id);
                $variant_selected = $this->products_model->getReserveVariant($variant,$value->product_id);
                if($variant_selected){
                    $reserved_variants[]= $variant_selected;
                    $product_variants[$key]->del_check = 0;
                }else{
                    $product_variants[$key]->del_check = 1;
                }
            }
            $this->data['product_variants'] = $product_variants;
            $allsavedvariants = $this->products_model->getAllSavedVariants($id);
            $allsavedvariants_name = array();
            if(!empty($allsavedvariants)){
                foreach ($allsavedvariants as $key => $dat){
                    $allsavedvariants_name[] = $dat['variant'];
                }
            }
            $this->data['saved_variants'] = $this->products_model->getAllSavedVariants($id);
            $this->data['allsavedvariants_name'] = $allsavedvariants_name;
            $this->data['additional_guide'] = $this->products_model->getAllAdditionalGuide();
            $this->data['combo_items'] = $product->type == 'combo' ? $this->products_model->getProductComboItems($product->id) : NULL;
            $this->data['product_options'] = $id ? $this->products_model->getProductOptionsWithWH($id) : NULL;
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('edit_product')));
            $meta = array('page_title' => lang('edit_product'), 'bc' => $bc);
            $this->page_construct('products/edit', $meta, $this->data);
        }
    }

    /* ---------------------------------------------------------------- */

    function import_csv()
    {
        //$this->sma->checkPermissions('csv');
        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products/import_csv");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $keys = array('name', 'code', 'barcode_symbology', 'brand', 'category_code', 'cost', 'price', 'alert_quantity','tax_rate', 'tax_method', 'image', 'subcategory_code', 'variants', 'cf1', 'cf2', 'cf3', 'cf4', 'cf5', 'cf6' ,'featured','hide','free_shipment', 'product_details');

                $final = array();
                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }
                //$this->sma->print_arrays($final);
                // echo '<pre>';
                // print_r($final);
                //  die;
                $rw = 2; $items = array();
                foreach ($final as $csv_pr) {
                    if ( ! $this->products_model->getProductByCode(trim($csv_pr['code']))) {
                        if ($catd = $this->products_model->getCategoryByCode(trim($csv_pr['category_code']))) {
                            $brand = $this->products_model->getBrandByName(trim($csv_pr['brand']));

                            if($csv_pr['image']) {
                                $img_temp_name = mt_rand();
                                $ext_img_url = trim($csv_pr['image']);
                                copy($ext_img_url, FCPATH . '/assets/uploads/thumbs/' . $img_temp_name . '.jpg');
                                copy($ext_img_url, FCPATH . '/assets/uploads/' . $img_temp_name . '.jpg');

                                $full_temp_img_name = $img_temp_name . '.jpg';
                                $this->load->library('image_lib');
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $this->upload_path . $full_temp_img_name;
                                $config['new_image'] = $this->thumbs_path . $full_temp_img_name;
                                $config['maintain_ratio'] = TRUE;
                                $config['width'] = $this->Settings->twidth;
                                $config['height'] = $this->Settings->theight;
                                $this->image_lib->clear();
                                $this->image_lib->initialize($config);
                            }else{
                                $full_temp_img_name = 'no_image.png';
                            }

                            if (!$this->image_lib->resize()) {
                                echo $this->image_lib->display_errors();
                            }

                            $tax_details = $this->products_model->getTaxRateByName(trim($csv_pr['tax_rate']));
                            $prsubcat = $this->products_model->getCategoryByCode(trim($csv_pr['subcategory_code']));
                            $items[] = array (
                                'code' => trim($csv_pr['code']),
                                'name' => trim($csv_pr['name']),
                                'category_id' => $catd->id,
                                'barcode_symbology' => mb_strtolower(trim($csv_pr['barcode_symbology']), 'UTF-8'),
                                'brand' => ($brand ? $brand->id : NULL),
                                'product_details' => trim($csv_pr['product_details']),
                                'cost' => trim($csv_pr['cost']),
                                'price' => trim($csv_pr['price']),
                                'alert_quantity' => trim($csv_pr['alert_quantity']),
                                'tax_rate' => ($tax_details ? $tax_details->id : NULL),
                                'tax_method' => ($csv_pr['tax_method'] == 'exclusive' ? 1 : 0),
                                'subcategory_id' => ($prsubcat ? $prsubcat->id : NULL),
                                'variants' => trim($csv_pr['variants']),
                                'image' => trim($full_temp_img_name),
                                'cf1' => trim($csv_pr['cf1']),
                                'cf2' => trim($csv_pr['cf2']),
                                'cf3' => trim($csv_pr['cf3']),
                                'cf4' => trim($csv_pr['cf4']),
                                'cf5' => trim($csv_pr['cf5']),
                                'cf6' => trim($csv_pr['cf6']),
                                'featured' => trim($csv_pr['featured'] ? $csv_pr['featured'] : 0 ),
                                'hide' => trim($csv_pr['hide'] == 1 ? 0 : 1),
                                'free_shipment' => trim($csv_pr['free_shipment'] ? $csv_pr['free_shipment'] : 0),
                                'hsn_code' => trim($csv_pr['hsn_code']),
                            );
                        } else {
                            $this->session->set_flashdata('error', lang("check_category_code") . " (" . $csv_pr['category_code'] . "). " . lang("category_code_x_exist") . " " . lang("line_no") . " " . $rw);
                            admin_redirect("products/import_csv");
                        }
                    }else{
                        $this->session->set_flashdata('error', "Product code already exit.Line No ". " " . $rw);
                        admin_redirect("products/import_csv");
                    }

                    $rw++;
                }
            }

            //  $this->sma->print_arrays($items);
            //   die;
        }

        if ($this->form_validation->run() == true && $prs = $this->products_model->add_products($items)) {
            $this->products_model->slugify();
            $this->session->set_flashdata('message', sprintf(lang("products_added"), $prs));
            admin_redirect('products');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['userfile'] = array('name' => 'userfile',
                'id' => 'userfile',
                'type' => 'text',
                'value' => $this->form_validation->set_value('userfile')
            );

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('import_products_by_csv')));
            $meta = array('page_title' => lang('import_products_by_csv'), 'bc' => $bc);
            $this->page_construct('products/import_csv', $meta, $this->data);

        }
    }

    /* ------------------------------------------------------------------ */

    function update_price()
    {
        $this->sma->checkPermissions('csv');
        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (DEMO) {
                $this->session->set_flashdata('message', lang("disabled_in_demo"));
                admin_redirect('welcome');
            }

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);

                $keys = array('code', 'price');

                $final = array();

                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }
                $rw = 2;
                foreach ($final as $csv_pr) {
                    if (!$this->products_model->getProductByCode(trim($csv_pr['code']))) {
                        $this->session->set_flashdata('message', lang("check_product_code") . " (" . $csv_pr['code'] . "). " . lang("code_x_exist") . " " . lang("line_no") . " " . $rw);
                        admin_redirect("products");
                    }
                    $rw++;
                }
            }

        } elseif ($this->input->post('update_price')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/group_product_prices/".$group_id);
        }

        if ($this->form_validation->run() == true && !empty($final)) {
            $this->products_model->updatePrice($final);
            $this->session->set_flashdata('message', lang("price_updated"));
            admin_redirect('products');
        } else {

            $this->data['userfile'] = array('name' => 'userfile',
                'id' => 'userfile',
                'type' => 'text',
                'value' => $this->form_validation->set_value('userfile')
            );
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme.'products/update_price', $this->data);

        }
    }

    /* ------------------------------------------------------------------------------- */

    function delete($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->products_model->deleteProduct($id)) {
            if($this->input->is_ajax_request()) {
                $this->sma->send_json(array('error' => 0, 'msg' => lang("product_deleted")));
            }
            $this->session->set_flashdata('message', lang('product_deleted'));
            admin_redirect('welcome');
        }

    }

    /* ----------------------------------------------------------------------------- */

    function quantity_adjustments($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('adjustments');

        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $this->data['warehouses'] = null;
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('quantity_adjustments')));
        $meta = array('page_title' => lang('quantity_adjustments'), 'bc' => $bc);
        $this->page_construct('products/quantity_adjustments', $meta, $this->data);
    }

    function getadjustments($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('adjustments');

        $delete_link = "<a href='#' class='tip po' title='<b>" . $this->lang->line("delete_adjustment") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('products/delete_adjustment/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a>";

        $this->load->library('datatables');
        $this->datatables
            ->select("{$this->db->dbprefix('adjustments')}.id as id, date, reference_no, warehouses.name as wh_name, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as created_by, note, attachment")
            ->from('adjustments')
            ->join('warehouses', 'warehouses.id=adjustments.warehouse_id', 'left')
            ->join('users', 'users.id=adjustments.created_by', 'left')
            ->group_by("adjustments.id");
        if ($warehouse_id) {
            $this->datatables->where('adjustments.warehouse_id', $warehouse_id);
        }
        $this->datatables->add_column("Actions", "<div class='text-center'><a href='" . admin_url('products/edit_adjustment/$1') . "' class='tip' title='" . lang("edit_adjustment") . "'><i class='fa fa-edit'></i></a> " . $delete_link . "</div>", "id");

        echo $this->datatables->generate();

    }

    public function view_adjustment($id)
    {
        $this->sma->checkPermissions('adjustments', TRUE);

        $adjustment = $this->products_model->getAdjustmentByID($id);
        if (!$id || !$adjustment) {
            $this->session->set_flashdata('error', lang('adjustment_not_found'));
            $this->sma->md();
        }

        $this->data['inv'] = $adjustment;
        $this->data['rows'] = $this->products_model->getAdjustmentItems($id);
        $this->data['created_by'] = $this->site->getUser($adjustment->created_by);
        $this->data['updated_by'] = $this->site->getUser($adjustment->updated_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($adjustment->warehouse_id);
        $this->load->view($this->theme.'products/view_adjustment', $this->data);
    }

    function add_adjustment($count_id = NULL)
    {
        $this->sma->checkPermissions('adjustments', true);
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');

        if ($this->form_validation->run() == true) {

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld($this->input->post('date'));
            } else {
                $date = date('Y-m-d H:s:i');
            }

            $reference_no = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('qa');
            $warehouse_id = $this->input->post('warehouse');
            $note = $this->sma->clear_tags($this->input->post('note'));

            $i = isset($_POST['product_id']) ? sizeof($_POST['product_id']) : 0;
            for ($r = 0; $r < $i; $r++) {

                $product_id = $_POST['product_id'][$r];
                $type = $_POST['type'][$r];
                $quantity = $_POST['quantity'][$r];
                $serial = $_POST['serial'][$r];
                $variant = isset($_POST['variant'][$r]) && !empty($_POST['variant'][$r]) ? $_POST['variant'][$r] : NULL;

                if (!$this->Settings->overselling && $type == 'subtraction') {
                    if ($variant) {
                        if($op_wh_qty = $this->products_model->getProductWarehouseOptionQty($variant, $warehouse_id)) {
                            if ($op_wh_qty->quantity < $quantity) {
                                $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'));
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                        } else {
                            $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    }
                    if($wh_qty = $this->products_model->getProductQuantity($product_id, $warehouse_id)) {
                        if ($wh_qty['quantity'] < $quantity) {
                            $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    } else {
                        $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }

                $products[] = array(
                    'product_id' => $product_id,
                    'type' => $type,
                    'quantity' => $quantity,
                    'warehouse_id' => $warehouse_id,
                    'option_id' => $variant,
                    'serial_no' => $serial,
                );

            }

            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("products"), 'required');
            } else {
                krsort($products);
            }

            $data = array(
                'date' => $date,
                'reference_no' => $reference_no,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'created_by' => $this->session->userdata('user_id'),
                'count_id' => $this->input->post('count_id') ? $this->input->post('count_id') : NULL,
            );

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            // $this->sma->print_arrays($data, $products);

        }

        if ($this->form_validation->run() == true && $this->products_model->addAdjustment($data, $products)) {
            $this->session->set_userdata('remove_qals', 1);
            $this->session->set_flashdata('message', lang("quantity_adjusted"));
            admin_redirect('products/quantity_adjustments');
        } else {

            if ($count_id) {
                $stock_count = $this->products_model->getStouckCountByID($count_id);
                $items = $this->products_model->getStockCountItems($count_id);
                $c = rand(100000, 9999999);
                foreach ($items as $item) {
                    if ($item->counted != $item->expected) {
                        $product = $this->site->getProductByID($item->product_id);
                        $row = json_decode('{}');
                        $row->id = $item->product_id;
                        $row->code = $product->code;
                        $row->name = $product->name;
                        $row->qty = $item->counted-$item->expected;
                        $row->type = $row->qty > 0 ? 'addition' : 'subtraction';
                        $row->qty = $row->qty > 0 ? $row->qty : (0-$row->qty);
                        $options = $this->products_model->getProductOptions($product->id);
                        $row->option = $item->product_variant_id ? $item->product_variant_id : 0;
                        $row->serial = '';
                        $ri = $this->Settings->item_addition ? $product->id : $c;

                        $pr[$ri] = array('id' => str_replace(".", "", microtime(true)), 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                            'row' => $row, 'options' => $options);
                        $c++;
                    }
                }
            }
            $this->data['adjustment_items'] = $count_id ? json_encode($pr) : FALSE;
            $this->data['warehouse_id'] = $count_id ? $stock_count->warehouse_id : FALSE;
            $this->data['count_id'] = $count_id;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('add_adjustment')));
            $meta = array('page_title' => lang('add_adjustment'), 'bc' => $bc);
            $this->page_construct('products/add_adjustment', $meta, $this->data);

        }
    }

    function edit_adjustment($id)
    {
        $this->sma->checkPermissions('adjustments', true);
        $adjustment = $this->products_model->getAdjustmentByID($id);
        if (!$id || !$adjustment) {
            $this->session->set_flashdata('error', lang('adjustment_not_found'));
            $this->sma->md();
        }
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');

        if ($this->form_validation->run() == true) {

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld($this->input->post('date'));
            } else {
                $date = $adjustment->date;
            }

            $reference_no = $this->input->post('reference_no');
            $warehouse_id = $this->input->post('warehouse');
            $note = $this->sma->clear_tags($this->input->post('note'));

            $i = isset($_POST['product_id']) ? sizeof($_POST['product_id']) : 0;
            for ($r = 0; $r < $i; $r++) {

                $product_id = $_POST['product_id'][$r];
                $type = $_POST['type'][$r];
                $quantity = $_POST['quantity'][$r];
                $serial = $_POST['serial'][$r];
                $variant = isset($_POST['variant'][$r]) && !empty($_POST['variant'][$r]) ? $_POST['variant'][$r] : null;

                if (!$this->Settings->overselling && $type == 'subtraction') {
                    if ($variant) {
                        if($op_wh_qty = $this->products_model->getProductWarehouseOptionQty($variant, $warehouse_id)) {
                            if ($op_wh_qty->quantity < $quantity) {
                                $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'));
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                        } else {
                            $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    }
                    if($wh_qty = $this->products_model->getProductQuantity($product_id, $warehouse_id)) {
                        if ($wh_qty['quantity'] < $quantity) {
                            $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    } else {
                        $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }

                $products[] = array(
                    'product_id' => $product_id,
                    'type' => $type,
                    'quantity' => $quantity,
                    'warehouse_id' => $warehouse_id,
                    'option_id' => $variant,
                    'serial_no' => $serial,
                );

            }

            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("products"), 'required');
            } else {
                krsort($products);
            }

            $data = array(
                'date' => $date,
                'reference_no' => $reference_no,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'created_by' => $this->session->userdata('user_id')
            );

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            // $this->sma->print_arrays($data, $products);

        }

        if ($this->form_validation->run() == true && $this->products_model->updateAdjustment($id, $data, $products)) {
            $this->session->set_userdata('remove_qals', 1);
            $this->session->set_flashdata('message', lang("quantity_adjusted"));
            admin_redirect('products/quantity_adjustments');
        } else {

            $inv_items = $this->products_model->getAdjustmentItems($id);
            // krsort($inv_items);
            $c = rand(100000, 9999999);
            foreach ($inv_items as $item) {
                $product = $this->site->getProductByID($item->product_id);
                $row = json_decode('{}');
                $row->id = $item->product_id;
                $row->code = $product->code;
                $row->name = $product->name;
                $row->qty = $item->quantity;
                $row->type = $item->type;
                $options = $this->products_model->getProductOptions($product->id);
                $row->option = $item->option_id ? $item->option_id : 0;
                $row->serial = $item->serial_no ? $item->serial_no : '';
                $ri = $this->Settings->item_addition ? $product->id : $c;

                $pr[$ri] = array('id' => str_replace(".", "", microtime(true)), 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                    'row' => $row, 'options' => $options);
                $c++;
            }

            $this->data['adjustment'] = $adjustment;
            $this->data['adjustment_items'] = json_encode($pr);
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('edit_adjustment')));
            $meta = array('page_title' => lang('edit_adjustment'), 'bc' => $bc);
            $this->page_construct('products/edit_adjustment', $meta, $this->data);

        }
    }

    function add_adjustment_by_csv()
    {
        $this->sma->checkPermissions('adjustments', true);
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');

        if ($this->form_validation->run() == true) {

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld($this->input->post('date'));
            } else {
                $date = date('Y-m-d H:s:i');
            }

            $reference_no = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('qa');
            $warehouse_id = $this->input->post('warehouse');
            $note = $this->sma->clear_tags($this->input->post('note'));
            $data = array(
                'date' => $date,
                'reference_no' => $reference_no,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'created_by' => $this->session->userdata('user_id'),
                'count_id' => NULL,
            );

            if ($_FILES['csv_file']['size'] > 0) {

                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('csv_file')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $csv = $this->upload->file_name;
                $data['attachment'] = $csv;

                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $keys = array('code', 'quantity', 'variant');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }
                // $this->sma->print_arrays($final);
                $rw = 2;
                foreach ($final as $pr) {
                    if ($product = $this->products_model->getProductByCode(trim($pr['code']))) {
                        $csv_variant = trim($pr['variant']);
                        $variant = !empty($csv_variant) ? $this->products_model->getProductVariantID($product->id, $csv_variant) : FALSE;

                        $csv_quantity = trim($pr['quantity']);
                        $type = $csv_quantity > 0 ? 'addition' : 'subtraction';
                        $quantity = $csv_quantity > 0 ? $csv_quantity : (0-$csv_quantity);

                        if (!$this->Settings->overselling && $type == 'subtraction') {
                            if ($variant) {
                                if($op_wh_qty = $this->products_model->getProductWarehouseOptionQty($variant, $warehouse_id)) {
                                    if ($op_wh_qty->quantity < $quantity) {
                                        $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'). ' - ' . lang('line_no') . ' ' . $rw);
                                        redirect($_SERVER["HTTP_REFERER"]);
                                    }
                                } else {
                                    $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'). ' - ' . lang('line_no') . ' ' . $rw);
                                    redirect($_SERVER["HTTP_REFERER"]);
                                }
                            }
                            if($wh_qty = $this->products_model->getProductQuantity($product->id, $warehouse_id)) {
                                if ($wh_qty['quantity'] < $quantity) {
                                    $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'). ' - ' . lang('line_no') . ' ' . $rw);
                                    redirect($_SERVER["HTTP_REFERER"]);
                                }
                            } else {
                                $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'). ' - ' . lang('line_no') . ' ' . $rw);
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                        }

                        $products[] = array(
                            'product_id' => $product->id,
                            'type' => $type,
                            'quantity' => $quantity,
                            'warehouse_id' => $warehouse_id,
                            'option_id' => $variant,
                        );

                    } else {
                        $this->session->set_flashdata('error', lang('check_product_code') . ' (' . $pr['code'] . '). ' . lang('product_code_x_exist') . ' ' . lang('line_no') . ' ' . $rw);
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                    $rw++;
                }

            } else {
                $this->form_validation->set_rules('csv_file', lang("upload_file"), 'required');
            }

            // $this->sma->print_arrays($data, $products);

        }

        if ($this->form_validation->run() == true && $this->products_model->addAdjustment($data, $products)) {
            $this->session->set_flashdata('message', lang("quantity_adjusted"));
            admin_redirect('products/quantity_adjustments');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('add_adjustment')));
            $meta = array('page_title' => lang('add_adjustment_by_csv'), 'bc' => $bc);
            $this->page_construct('products/add_adjustment_by_csv', $meta, $this->data);

        }
    }

    function delete_adjustment($id = NULL)
    {
        $this->sma->checkPermissions('delete', TRUE);

        if ($this->products_model->deleteAdjustment($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("adjustment_deleted")));
        }

    }

    /* --------------------------------------------------------------------------------------------- */

    function modal_view($id = NULL)
    {
        $this->sma->checkPermissions('index', TRUE);

        $pr_details = $this->site->getProductByID($id);
        if (!$id || !$pr_details) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            $this->sma->md();
        }
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $pr_details->code . '/' . $pr_details->barcode_symbology . '/40/0') . "' alt='" . $pr_details->code . "' class='pull-left' />";
        if ($pr_details->type == 'combo') {
            $this->data['combo_items'] = $this->products_model->getProductComboItems($id);
        }
        $this->data['product'] = $pr_details;
        $this->data['unit'] = $this->site->getUnitByID($pr_details->unit);
        $this->data['brand'] = $this->site->getBrandByID($pr_details->brand);
        $this->data['images'] = $this->products_model->getProductPhotos($id);
        $this->data['category'] = $this->site->getCategoryByID($pr_details->category_id);
        $this->data['subcategory'] = $pr_details->subcategory_id ? $this->site->getCategoryByID($pr_details->subcategory_id) : NULL;
        $this->data['tax_rate'] = $pr_details->tax_rate ? $this->site->getTaxRateByID($pr_details->tax_rate) : NULL;
        $this->data['warehouses'] = $this->products_model->getAllWarehousesWithPQ($id);
        $this->data['options'] = $this->products_model->getProductOptionsWithWH($id);
        $this->data['variants'] = $this->products_model->getProductOptions($id);

        $this->load->view($this->theme.'products/modal_view', $this->data);
    }

    function view($id = NULL)
    {
        $this->sma->checkPermissions('index');

        $pr_details = $this->products_model->getProductByID($id);
        if (!$id || !$pr_details) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $pr_details->code . '/' . $pr_details->barcode_symbology . '/40/0') . "' alt='" . $pr_details->code . "' class='pull-left' />";
        if ($pr_details->type == 'combo') {
            $this->data['combo_items'] = $this->products_model->getProductComboItems($id);
        }
        $this->data['product'] = $pr_details;
        $this->data['unit'] = $this->site->getUnitByID($pr_details->unit);
        $this->data['brand'] = $this->site->getBrandByID($pr_details->brand);
        $this->data['images'] = $this->products_model->getProductPhotos($id);
        $this->data['category'] = $this->site->getCategoryByID($pr_details->category_id);
        $this->data['subcategory'] = $pr_details->subcategory_id ? $this->site->getCategoryByID($pr_details->subcategory_id) : NULL;
        $this->data['tax_rate'] = $pr_details->tax_rate ? $this->site->getTaxRateByID($pr_details->tax_rate) : NULL;
        $this->data['popup_attributes'] = $this->popup_attributes;
        $this->data['warehouses'] = $this->products_model->getAllWarehousesWithPQ($id);
        $this->data['options'] = $this->products_model->getProductOptionsWithWH($id);
        $this->data['variants'] = $this->products_model->getProductOptions($id);
        $this->data['sold'] = $this->products_model->getSoldQty($id);
        $this->data['purchased'] = $this->products_model->getPurchasedQty($id);

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => $pr_details->name));
        $meta = array('page_title' => $pr_details->name, 'bc' => $bc);
        $this->page_construct('products/view', $meta, $this->data);
    }

    function pdf($id = NULL, $view = NULL)
    {
        $this->sma->checkPermissions('index');

        $pr_details = $this->products_model->getProductByID($id);
        if (!$id || !$pr_details) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $pr_details->code . '/' . $pr_details->barcode_symbology . '/40/0') . "' alt='" . $pr_details->code . "' class='pull-left' />";
        if ($pr_details->type == 'combo') {
            $this->data['combo_items'] = $this->products_model->getProductComboItems($id);
        }
        $this->data['product'] = $pr_details;
        $this->data['unit'] = $this->site->getUnitByID($pr_details->unit);
        $this->data['brand'] = $this->site->getBrandByID($pr_details->brand);
        $this->data['images'] = $this->products_model->getProductPhotos($id);
        $this->data['category'] = $this->site->getCategoryByID($pr_details->category_id);
        $this->data['subcategory'] = $pr_details->subcategory_id ? $this->site->getCategoryByID($pr_details->subcategory_id) : NULL;
        $this->data['tax_rate'] = $pr_details->tax_rate ? $this->site->getTaxRateByID($pr_details->tax_rate) : NULL;
        $this->data['popup_attributes'] = $this->popup_attributes;
        $this->data['warehouses'] = $this->products_model->getAllWarehousesWithPQ($id);
        $this->data['options'] = $this->products_model->getProductOptionsWithWH($id);
        $this->data['variants'] = $this->products_model->getProductOptions($id);

        $name = $pr_details->code . '_' . str_replace('/', '_', $pr_details->name) . ".pdf";
        if ($view) {
            $this->load->view($this->theme . 'products/pdf', $this->data);
        } else {
            $html = $this->load->view($this->theme . 'products/pdf', $this->data, TRUE);
            if (! $this->Settings->barcode_img) {
                $html = preg_replace("'\<\?xml(.*)\?\>'", '', $html);
            }
            $this->sma->generate_pdf($html, $name);
        }
    }

    function getSubCategories($category_id = NULL)
    {
        if ($rows = $this->products_model->getSubCategories($category_id)) {
            $data = json_encode($rows);
        } else {
            $data = false;
        }
        echo $data;
        exit();
    }


    function quickProductNameUpdate(){
        $pro_id = $this->input->get('product_id');
        $pro_name = $this->input->get('product_name');

        $response = $this->products_model->updateProductName($pro_id,$pro_name);
        if($response == true){
            $data = json_encode('updated');
        }else{
            $data = false;
        }
        echo $data;
    }

    function product_actions($wh = NULL)
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'sync_quantity') {

                    foreach ($_POST['val'] as $id) {
                        $this->site->syncQuantity(NULL, NULL, NULL, $id);
                    }
                    $this->session->set_flashdata('message', $this->lang->line("products_quantity_sync"));
                    redirect($_SERVER["HTTP_REFERER"]);

                } elseif ($this->input->post('form_action') == 'delete') {

                    $this->sma->checkPermissions('delete');
                    foreach ($_POST['val'] as $id) {
                        $this->products_model->deleteProduct($id);
                    }
                    $this->session->set_flashdata('message', $this->lang->line("products_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);

                } elseif ($this->input->post('form_action') == 'labels') {

                    foreach ($_POST['val'] as $id) {
                        $row = $this->products_model->getProductByID($id);
                        $selected_variants = false;
                        if ($variants = $this->products_model->getProductOptions($row->id)) {
                            foreach ($variants as $variant) {
                                $selected_variants[$variant->id] = $variant->quantity > 0 ? 1 : 0;
                            }
                        }
                        $pr[$row->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $row->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);
                    }

                    $this->data['items'] = isset($pr) ? json_encode($pr) : false;
                    $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
                    $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('print_barcodes')));
                    $meta = array('page_title' => lang('print_barcodes'), 'bc' => $bc);
                    $this->page_construct('products/print_barcodes', $meta, $this->data);

                } elseif($this->input->post('form_action') == 'export_to_csv_custom'){

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle('Products');
                    $this->excel->getActiveSheet()->SetCellValue('A1', 'post_title');
                    $this->excel->getActiveSheet()->SetCellValue('B1', 'post_excerpt');
                    $this->excel->getActiveSheet()->SetCellValue('C1', 'post_content');
                    $this->excel->getActiveSheet()->SetCellValue('D1', 'post_status');
                    $this->excel->getActiveSheet()->SetCellValue('E1', 'menu_order');
                    $this->excel->getActiveSheet()->SetCellValue('F1', 'post_date');
                    $this->excel->getActiveSheet()->SetCellValue('G1', 'post_parent');
                    $this->excel->getActiveSheet()->SetCellValue('H1', 'sku');
                    $this->excel->getActiveSheet()->SetCellValue('I1', 'downloadable');
                    $this->excel->getActiveSheet()->SetCellValue('J1', 'virtual');
                    $this->excel->getActiveSheet()->SetCellValue('K1', 'visibility');
                    $this->excel->getActiveSheet()->SetCellValue('L1', 'stock');
                    $this->excel->getActiveSheet()->SetCellValue('M1', 'stock_status');
                    $this->excel->getActiveSheet()->SetCellValue('N1', 'backorders');
                    $this->excel->getActiveSheet()->SetCellValue('O1', 'manage_stock');
                    $this->excel->getActiveSheet()->SetCellValue('P1', 'regular_price');
                    $this->excel->getActiveSheet()->SetCellValue('Q1', 'sale_price');
                    $this->excel->getActiveSheet()->SetCellValue('R1', 'weight');
                    $this->excel->getActiveSheet()->SetCellValue('S1', 'length');
                    $this->excel->getActiveSheet()->SetCellValue('T1', 'width');
                    $this->excel->getActiveSheet()->SetCellValue('U1', 'height');
                    $this->excel->getActiveSheet()->SetCellValue('V1', 'tax_status');
                    $this->excel->getActiveSheet()->SetCellValue('W1', 'tax_class');
                    $this->excel->getActiveSheet()->SetCellValue('X1', 'upsell_ids');
                    $this->excel->getActiveSheet()->SetCellValue('Y1', 'crosssell_ids');
                    $this->excel->getActiveSheet()->SetCellValue('Z1', 'featured');
                    $this->excel->getActiveSheet()->SetCellValue('AA1', 'file_path');
                    $this->excel->getActiveSheet()->SetCellValue('AB1', 'download_limit');
                    $this->excel->getActiveSheet()->SetCellValue('AC1', 'download_expiry');
                    $this->excel->getActiveSheet()->SetCellValue('AD1', 'product_url');
                    $this->excel->getActiveSheet()->SetCellValue('AE1', 'button_text');
                    $this->excel->getActiveSheet()->SetCellValue('AF1', 'images');
                    $this->excel->getActiveSheet()->SetCellValue('AG1', 'tax:product_type');
                    $this->excel->getActiveSheet()->SetCellValue('AH1', 'tax:product_cat');
                    $this->excel->getActiveSheet()->SetCellValue('AI1', 'tax:product_tag');
                    $this->excel->getActiveSheet()->SetCellValue('AJ1', 'tax:product_shipping_class');
                    $this->excel->getActiveSheet()->SetCellValue('AK1', 'meta:woocommerce_sale_flash_type');
                    $this->excel->getActiveSheet()->SetCellValue('AL1', '');
                    $this->excel->getActiveSheet()->SetCellValue('AM1', 'attribute:pa_size');
                    $this->excel->getActiveSheet()->SetCellValue('AN1', 'attribute_data:pa_size');



                    $header = "post_title".",";
                    $header .= "post_excerpt".",";
                    $header .= "post_content".",";
                    $header .= "post_status".",";
                    $header .= "menu_order".",";
                    $header .= "post_date".",";
                    $header .= "post_parent".",";
                    $header .= "sku".",";
                    $header .= "downloadable".",";
                    $header .= "virtual".",";
                    $header .= "visibility".",";
                    $header .= "stock".",";
                    $header .= "stock_status".",";
                    $header .= "backorders".",";
                    $header .= "manage_stock".",";
                    $header .= "regular_price".",";
                    $header .= "sale_price".",";
                    $header .= "weight".",";
                    $header .= "length".",";
                    $header .= "width".",";
                    $header .= "height".",";
                    $header .= "tax_status".",";
                    $header .= "tax_class".",";
                    $header .= "upsell_ids".",";
                    $header .= "crosssell_ids".",";
                    $header .= "featured".",";
                    $header .= "file_path".",";
                    $header .= "download_limit".",";
                    $header .= "download_expiry".",";
                    $header .= "product_url".",";
                    $header .= "button_text".",";
                    $header .= "images".",";
                    $header .= "tax:product_type".",";
                    $header .= "tax:product_cat".",";
                    $header .= "tax:product_tag".",";
                    $header .= "tax:product_shipping_class".",";
                    $header .= "meta:woocommerce_sale_flash_type".",";
                    $header .= "".",";
                    $header .= "attribute:pa_size".",";
                    $header .= "attribute_data:pa_size".",";


                    $row = 2;
                    $cell_content = '';
                    foreach ($_POST['val'] as $id) {

                        $product = $this->products_model->getProductDetail($id);
                        $brand = $this->site->getBrandByID($product->brand);
                        if($units = $this->site->getUnitsByBUID($product->unit)) {
                            foreach($units as $u) {
                                if ($u->id == $product->unit) {
                                    $base_unit = $u->code;
                                }
                                if ($u->id == $product->sale_unit) {
                                    $sale_unit = $u->code;
                                }
                                if ($u->id == $product->purchase_unit) {
                                    $purchase_unit = $u->code;
                                }
                            }
                        } else {
                            $base_unit = '';
                            $sale_unit = '';
                            $purchase_unit = '';
                        }
                        $variants = $this->products_model->getProductOptions($id);
                        $product_variants = '';
                        if ($variants) {
                            foreach ($variants as $variant) {
                                $product_variants .= trim($variant->name) . '|';
                            }
                        }
                        $quantity = $product->quantity;
                        if ($wh) {
                            if($wh_qty = $this->products_model->getProductQuantity($id, $wh)) {
                                $quantity = $wh_qty['quantity'];
                            } else {
                                $quantity = 0;
                            }
                        }
                        $all_images = $this->products_model->getProductAllPhotos($id);

                        $description = strip_tags(trim(preg_replace('/\s\s+/', ' ', $product->product_details)));

                        $cell_content .=  str_replace(',', ' ', $product->name) .",". str_replace(',', ' ', $description) .",".
                            str_replace(',', ' ', $description) .","."publish".","."0".","."".","."0".",".str_replace(',', ' ', $product->code) .","."no".","."no".","."visible".",".$quantity.","."instock".","."no".","."no".",". str_replace(',', ' ', $product->price) .","."".","."".","."".","."".","."".","."taxable".","."reduced-rate".","."".","."".","."".","."".","."".","."".","."".","."".",". '"'.$all_images.'"' .",". str_replace(',', ' ', $brand->name) .",". str_replace(',', ' ', $product->category_name)  ;

                        $cell_content .= "\n";

                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $product->name);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $description);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $description);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, 'publish');
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, '0');
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, '0');
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $product->code);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, 'no');
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, 'no');
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, 'visible');
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, $quantity);
                        $this->excel->getActiveSheet()->SetCellValue('M' . $row, 'instock');
                        $this->excel->getActiveSheet()->SetCellValue('N' . $row, 'no');
                        $this->excel->getActiveSheet()->SetCellValue('O' . $row, 'no');
                        $this->excel->getActiveSheet()->SetCellValue('P' . $row, $product->price);
                        $this->excel->getActiveSheet()->SetCellValue('Q' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('R' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('S' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('T' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('U' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('V' . $row, 'taxable');
                        $this->excel->getActiveSheet()->SetCellValue('W' . $row, 'reduced-rate');
                        $this->excel->getActiveSheet()->SetCellValue('X' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('Y' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('Z' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('AA' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('AB' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('AC' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('AD' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('AE' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('AF' . $row, '"'.$all_images.'"');
                        $this->excel->getActiveSheet()->SetCellValue('AG' . $row, $brand->name);
                        $this->excel->getActiveSheet()->SetCellValue('AH' . $row, $product->category_name);
                        $this->excel->getActiveSheet()->SetCellValue('AI' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('AJ' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('AK' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('AL' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('AM' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('AN' . $row, '');
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                    $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(40);
                    $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(30);
                    $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(30);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'products_' . date('Y_m_d_H_i_s');

                    if ($this->input->post('form_action') == 'export_to_csv_custom') {
                        header("Content-type: application/csv");
                        header('Content-Disposition: attachment;filename="' . $filename . '.csv"');
                        header("Pragma: no-cache");
                        header("Expires: 0");

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
                        //$fd = fopen ("/home/salescutsandfits/public_html/wp/generate_csv/".$filename.".csv", "w");
                        $fd = fopen (FCPATH."/generate_csv/".$filename.".csv", "w");

                        fputs($fd, "$header\n$cell_content");
                        fclose($fd);

                    }

                    redirect($_SERVER["HTTP_REFERER"]);


                } elseif ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle('Products');
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('code'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('barcode_symbology'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('brand'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('category_code'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('unit_code'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('sale').' '.lang('unit_code'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('purchase').' '.lang('unit_code'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('cost'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('price'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('alert_quantity'));
                    $this->excel->getActiveSheet()->SetCellValue('L1', lang('tax_rate'));
                    $this->excel->getActiveSheet()->SetCellValue('M1', lang('tax_method'));
                    $this->excel->getActiveSheet()->SetCellValue('N1', lang('image'));
                    $this->excel->getActiveSheet()->SetCellValue('O1', lang('subcategory_code'));
                    $this->excel->getActiveSheet()->SetCellValue('P1', lang('product_variants'));
                    $this->excel->getActiveSheet()->SetCellValue('Q1', lang('pcf1'));
                    $this->excel->getActiveSheet()->SetCellValue('R1', lang('pcf2'));
                    $this->excel->getActiveSheet()->SetCellValue('S1', lang('pcf3'));
                    $this->excel->getActiveSheet()->SetCellValue('T1', lang('pcf4'));
                    $this->excel->getActiveSheet()->SetCellValue('U1', lang('pcf5'));
                    $this->excel->getActiveSheet()->SetCellValue('V1', lang('pcf6'));
                    $this->excel->getActiveSheet()->SetCellValue('W1', lang('quantity'));
                    $this->excel->getActiveSheet()->SetCellValue('X1', lang('featured'));
                    $this->excel->getActiveSheet()->SetCellValue('Y1', lang('active'));
                    $this->excel->getActiveSheet()->SetCellValue('Z1', lang('free_shipping'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $product = $this->products_model->getProductDetail($id);
                        $brand = $this->site->getBrandByID($product->brand);
                        $base_unit = $sale_unit = $purchase_unit = '';
                        if($units = $this->site->getUnitsByBUID($product->unit)) {
                            foreach($units as $u) {
                                if ($u->id == $product->unit) {
                                    $base_unit = $u->code;
                                }
                                if ($u->id == $product->sale_unit) {
                                    $sale_unit = $u->code;
                                }
                                if ($u->id == $product->purchase_unit) {
                                    $purchase_unit = $u->code;
                                }
                            }
                        }
                        $variants = $this->products_model->getProductOptions($id);
                        $product_variants = '';
                        if ($variants) {
                            foreach ($variants as $variant) {
                                $product_variants .= trim($variant->name) . '|';
                            }
                        }
                        $quantity = $product->quantity;
                        if ($wh) {
                            if($wh_qty = $this->products_model->getProductQuantity($id, $wh)) {
                                $quantity = $wh_qty['quantity'];
                            } else {
                                $quantity = 0;
                            }
                        }
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $product->name);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $product->code);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $product->barcode_symbology);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, ($brand ? $brand->name : ''));
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $product->category_code);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $base_unit);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $sale_unit);
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $purchase_unit);
                        if ($this->Owner || $this->Admin || $this->session->userdata('show_cost')) {
                            $this->excel->getActiveSheet()->SetCellValue('I' . $row, $product->cost);
                        }
                        if ($this->Owner || $this->Admin || $this->session->userdata('show_price')) {
                            $this->excel->getActiveSheet()->SetCellValue('J' . $row, $product->price);
                        }
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $product->alert_quantity);
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, $product->tax_rate_name);
                        $this->excel->getActiveSheet()->SetCellValue('M' . $row, $product->tax_method ? lang('exclusive') : lang('inclusive'));
                        $this->excel->getActiveSheet()->SetCellValue('N' . $row, $product->image);
                        $this->excel->getActiveSheet()->SetCellValue('O' . $row, $product->subcategory_code);
                        $this->excel->getActiveSheet()->SetCellValue('P' . $row, $product_variants);
                        $this->excel->getActiveSheet()->SetCellValue('Q' . $row, $product->cf1);
                        $this->excel->getActiveSheet()->SetCellValue('R' . $row, $product->cf2);
                        $this->excel->getActiveSheet()->SetCellValue('S' . $row, $product->cf3);
                        $this->excel->getActiveSheet()->SetCellValue('T' . $row, $product->cf4);
                        $this->excel->getActiveSheet()->SetCellValue('U' . $row, $product->cf5);
                        $this->excel->getActiveSheet()->SetCellValue('V' . $row, $product->cf6);
                        $this->excel->getActiveSheet()->SetCellValue('W' . $row, $quantity);
                        $this->excel->getActiveSheet()->SetCellValue('X' . $row, $product->featured ? 'yes' : 'no');
                        $this->excel->getActiveSheet()->SetCellValue('Y' . $row, $product->hide ? 'yes' : 'no');
                        $this->excel->getActiveSheet()->SetCellValue('Z' . $row, $product->free_shipment ? 'yes' : 'no');
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                    $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(40);
                    $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(30);
                    $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(30);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'products_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);

                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_product_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'admin/products');
        }
    }

    public function delete_image($id = NULL)
    {
        $this->sma->checkPermissions('edit', true);
        if ($id && $this->input->is_ajax_request()) {
            header('Content-Type: application/json');
            $this->db->delete('product_photos', array('id' => $id));
            $this->sma->send_json(array('error' => 0, 'msg' => lang("image_deleted")));
        }
        $this->sma->send_json(array('error' => 1, 'msg' => lang("ajax_error")));
    }

    public function getSubUnits($unit_id)
    {
        // $unit = $this->site->getUnitByID($unit_id);
        // if ($units = $this->site->getUnitsByBUID($unit_id)) {
        //     array_push($units, $unit);
        // } else {
        //     $units = array($unit);
        // }
        $units = $this->site->getUnitsByBUID($unit_id);
        $this->sma->send_json($units);
    }

    public function qa_suggestions()
    {
        $term = $this->input->get('term', true);

        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }

        $analyzed = $this->sma->analyze_term($term);
        $sr = $analyzed['term'];
        $option_id = $analyzed['option_id'];

        $rows = $this->products_model->getQASuggestions($sr);
        if ($rows) {
            foreach ($rows as $row) {
                $row->qty = 1;
                $options = $this->products_model->getProductOptions($row->id);
                $row->option = $option_id;
                $row->serial = '';

                $pr[] = array('id' => str_replace(".", "", microtime(true)), 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                    'row' => $row, 'options' => $options);

            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    function adjustment_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {

                    $this->sma->checkPermissions('delete');
                    foreach ($_POST['val'] as $id) {
                        $this->products_model->deleteAdjustment($id);
                    }
                    $this->session->set_flashdata('message', $this->lang->line("adjustment_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);

                } elseif ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle('quantity_adjustments');
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('warehouse'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('created_by'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('note'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('items'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $adjustment = $this->products_model->getAdjustmentByID($id);
                        $created_by = $this->site->getUser($adjustment->created_by);
                        $warehouse = $this->site->getWarehouseByID($adjustment->warehouse_id);
                        $items = $this->products_model->getAdjustmentItems($id);
                        $products = '';
                        if ($items) {
                            foreach ($items as $item) {
                                $products .= $item->product_name.'('.$this->sma->formatQuantity($item->type == 'subtraction' ? -$item->quantity : $item->quantity).')'."\n";
                            }
                        }

                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($adjustment->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $adjustment->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $warehouse->name);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $created_by->first_name.' ' .$created_by->last_name);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $this->sma->decode_html($adjustment->note));
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $products);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
                    $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'quantity_adjustments_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_record_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function stock_counts($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('stock_count');

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        } else {
            $this->data['warehouses'] = NULL;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : NULL;
        }

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('stock_counts')));
        $meta = array('page_title' => lang('stock_counts'), 'bc' => $bc);
        $this->page_construct('products/stock_counts', $meta, $this->data);
    }

    function getCounts($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('stock_count', TRUE);

        if ((! $this->Owner || ! $this->Admin) && ! $warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }
        $detail_link = anchor('admin/products/view_count/$1', '<label class="label label-primary pointer">'.lang('details').'</label>', 'class="tip" title="'.lang('details').'" data-toggle="modal" data-target="#myModal"');

        $this->load->library('datatables');
        $this->datatables
            ->select("{$this->db->dbprefix('stock_counts')}.id as id, date, reference_no, {$this->db->dbprefix('warehouses')}.name as wh_name, type, brand_names, category_names, initial_file, final_file")
            ->from('stock_counts')
            ->join('warehouses', 'warehouses.id=stock_counts.warehouse_id', 'left');
        if ($warehouse_id) {
            $this->datatables->where('warehouse_id', $warehouse_id);
        }

        $this->datatables->add_column('Actions', '<div class="text-center">'.$detail_link.'</div>', "id");
        echo $this->datatables->generate();
    }

    function view_count($id)
    {
        $this->sma->checkPermissions('stock_count', TRUE);
        $stock_count = $this->products_model->getStouckCountByID($id);
        if ( ! $stock_count->finalized) {
            $this->sma->md('admin/products/finalize_count/'.$id);
        }

        $this->data['stock_count'] = $stock_count;
        $this->data['stock_count_items'] = $this->products_model->getStockCountItems($id);
        $this->data['warehouse'] = $this->site->getWarehouseByID($stock_count->warehouse_id);
        $this->data['adjustment'] = $this->products_model->getAdjustmentByCountID($id);
        $this->load->view($this->theme.'products/view_count', $this->data);
    }

    function count_stock($page = NULL)
    {
        $this->sma->checkPermissions('stock_count');
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');
        $this->form_validation->set_rules('type', lang("type"), 'required');

        if ($this->form_validation->run() == true) {

            $warehouse_id = $this->input->post('warehouse');
            $type = $this->input->post('type');
            $categories = $this->input->post('category') ? $this->input->post('category') : NULL;
            $brands = $this->input->post('brand') ? $this->input->post('brand') : NULL;
            $this->load->helper('string');
            $name = random_string('md5').'.csv';
            $products = $this->products_model->getStockCountProducts($warehouse_id, $type, $categories, $brands);
            $pr = 0; $rw = 0;
            foreach ($products as $product) {
                if ($variants = $this->products_model->getStockCountProductVariants($warehouse_id, $product->id)) {
                    foreach ($variants as $variant) {
                        $items[] = array(
                            'product_code' => $product->code,
                            'product_name' => $product->name,
                            'variant' => $variant->name,
                            'expected' => $variant->quantity,
                            'counted' => ''
                        );
                        $rw++;
                    }
                } else {
                    $items[] = array(
                        'product_code' => $product->code,
                        'product_name' => $product->name,
                        'variant' => '',
                        'expected' => $product->quantity,
                        'counted' => ''
                    );
                    $rw++;
                }
                $pr++;
            }
            if ( ! empty($items)) {
                $csv_file = fopen('./files/'.$name, 'w');
                fputcsv($csv_file, array(lang('product_code'), lang('product_name'), lang('variant'), lang('expected'), lang('counted')));
                foreach ($items as $item) {
                    fputcsv($csv_file, $item);
                }
                // file_put_contents('./files/'.$name, $csv_file);
                // fwrite($csv_file, $txt);
                fclose($csv_file);
            } else {
                $this->session->set_flashdata('error', lang('no_product_found'));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld($this->input->post('date'));
            } else {
                $date = date('Y-m-d H:s:i');
            }
            $category_ids = '';
            $brand_ids = '';
            $category_names = '';
            $brand_names = '';
            if ($categories) {
                $r = 1; $s = sizeof($categories);
                foreach ($categories as $category_id) {
                    $category = $this->site->getCategoryByID($category_id);
                    if ($r == $s) {
                        $category_names .= $category->name;
                        $category_ids .= $category->id;
                    } else {
                        $category_names .= $category->name.', ';
                        $category_ids .= $category->id.', ';
                    }
                    $r++;
                }
            }
            if ($brands) {
                $r = 1; $s = sizeof($brands);
                foreach ($brands as $brand_id) {
                    $brand = $this->site->getBrandByID($brand_id);
                    if ($r == $s) {
                        $brand_names .= $brand->name;
                        $brand_ids .= $brand->id;
                    } else {
                        $brand_names .= $brand->name.', ';
                        $brand_ids .= $brand->id.', ';
                    }
                    $r++;
                }
            }
            $data = array(
                'date' => $date,
                'warehouse_id' => $warehouse_id,
                'reference_no' => $this->input->post('reference_no'),
                'type' => $type,
                'categories' => $category_ids,
                'category_names' => $category_names,
                'brands' => $brand_ids,
                'brand_names' => $brand_names,
                'initial_file' => $name,
                'products' => $pr,
                'rows' => $rw,
                'created_by' => $this->session->userdata('user_id')
            );

        }

        if ($this->form_validation->run() == true && $this->products_model->addStockCount($data)) {
            $this->session->set_flashdata('message', lang("stock_count_intiated"));
            admin_redirect('products/stock_counts');

        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['brands'] = $this->site->getAllBrands();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('count_stock')));
            $meta = array('page_title' => lang('count_stock'), 'bc' => $bc);
            $this->page_construct('products/count_stock', $meta, $this->data);

        }

    }

    function finalize_count($id)
    {
        $this->sma->checkPermissions('stock_count');
        $stock_count = $this->products_model->getStouckCountByID($id);
        if ( ! $stock_count || $stock_count->finalized) {
            $this->session->set_flashdata('error', lang("stock_count_finalized"));
            admin_redirect('products/stock_counts');
        }

        $this->form_validation->set_rules('count_id', lang("count_stock"), 'required');

        if ($this->form_validation->run() == true) {

            if ($_FILES['csv_file']['size'] > 0) {
                $note = $this->sma->clear_tags($this->input->post('note'));
                $data = array(
                    'updated_by' => $this->session->userdata('user_id'),
                    'updated_at' => date('Y-m-d H:s:i'),
                    'note' => $note
                );

                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('csv_file')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $keys = array('product_code', 'product_name', 'product_variant', 'expected', 'counted');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }
                //$this->sma->print_arrays($final);
                $rw = 2; $differences = 0; $matches = 0;
                foreach ($final as $pr) {
                    if ($product = $this->products_model->getProductByCode(trim($pr['product_code']))) {
                        $pr['counted'] = !empty($pr['counted']) ? $pr['counted'] : 0;
                        if ($pr['expected'] == $pr['counted']) {
                            $matches++;
                        } else {
                            $pr['stock_count_id'] = $id;
                            $pr['product_id'] = $product->id;
                            $pr['cost'] = $product->cost;
                            $pr['product_variant_id'] = empty($pr['product_variant']) ? NULL : $this->products_model->getProductVariantID($pr['product_id'], $pr['product_variant']);
                            $products[] = $pr;
                            $differences++;
                        }
                    } else {
                        $this->session->set_flashdata('error', lang('check_product_code') . ' (' . $pr['product_code'] . '). ' . lang('product_code_x_exist') . ' ' . lang('line_no') . ' ' . $rw);
                        admin_redirect('products/finalize_count/'.$id);
                    }
                    $rw++;
                }

                $data['final_file'] = $csv;
                $data['differences'] = $differences;
                $data['matches'] = $matches;
                $data['missing'] = $stock_count->rows-($rw-2);
                $data['finalized'] = 1;
            }

            // $this->sma->print_arrays($data, $products);
        }

        if ($this->form_validation->run() == true && $this->products_model->finalizeStockCount($id, $data, $products)) {
            $this->session->set_flashdata('message', lang("stock_count_finalized"));
            admin_redirect('products/stock_counts');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['stock_count'] = $stock_count;
            $this->data['warehouse'] = $this->site->getWarehouseByID($stock_count->warehouse_id);
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => admin_url('products/stock_counts'), 'page' => lang('stock_counts')), array('link' => '#', 'page' => lang('finalize_count')));
            $meta = array('page_title' => lang('finalize_count'), 'bc' => $bc);
            $this->page_construct('products/finalize_count', $meta, $this->data);

        }

    }


    function getTranslateMe(){
        if(isset($_GET['translate_it'])){
            $string = $_GET['translate_it'];
            $this->load->helper('language');
            echo json_encode(translateString($string));
        }
    }



    function upselling()
    {
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $this->data['promotions'] = $this->products_model->getPromotions();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('upselling_heading')));
        $meta = array('page_title' => lang('upselling_heading'), 'bc' => $bc);
        $this->page_construct('products/upselling', $meta, $this->data);
    }

    function create_upselling(){

        if($_POST['promotion_type'] == 'buyOneGetOne'){
            $_POST['get_percentage_cat'] = 0;
            $_POST['get_percentage'] = 0;
        }

        if($_POST['apply_type'] == 'cat'){
            $_POST['where_to_apply'] = $_POST['where_to_apply_cat'] ;
        }else{
            $_POST['where_to_apply'] = $_POST['where_to_apply_pro'] ;
        }



        if($this->input->post('get_percentage_cat') > 0){
            $how_much_percentage = $this->input->post('get_percentage_cat');
        }else{
            $how_much_percentage = $this->input->post('get_percentage');
        }


        $this->form_validation->set_rules('offer_name', lang('offer_name'), 'required');
        $this->form_validation->set_rules('offer_title', lang('offer_title'), 'required');
        $this->form_validation->set_rules('offer_start_date', lang('offer_start_date'), 'required');

        if($this->input->post('promotion_type') == 'buyOneGetCategoryFree' || $this->input->post('promotion_type') == 'buyCategoryWithPercentage' ){
            if(!empty($this->input->post('get_cat_name')) && !empty($this->input->post('get_shop_name'))  && $this->form_validation->run() == true ){
                $custom_validation = true;
            }else{
                $this->form_validation->set_rules('get_cat_name', lang('Category'), 'required');
                $this->form_validation->set_rules('get_shop_name', lang('Warehouse'), 'required');
            }
        }


        if($this->input->post('promotion_type') == 'buyOneGetOne' || $this->input->post('promotion_type') == 'buyOneGetOtherProductFree' ){
            if(!empty($this->input->post('get_shop_name'))  && $this->form_validation->run() == true ){
                $custom_validation = true;
            }else{
                $this->form_validation->set_rules('get_shop_name', lang('Warehouse'), 'required');
            }
        }


        if($this->input->post('promotion_type') == 'cartvaluerangeFree' ){
            if(!empty($this->input->post('cart_amount_limit')) && $this->form_validation->run() == true ){
                $custom_validation = true;
            }else{
                $this->form_validation->set_rules('cart_amount_limit', 'Amount For Limit', 'required');
            }
        }

        //To check the category already on promotion or not?
        $running_promtion_cat = $this->products_model->findCategoryAlreadyOnPromotion();
        $checking_cat_alreay_exist = array_intersect($running_promtion_cat, $this->input->post('get_cat_name'));
        if (count($checking_cat_alreay_exist) > 0) {
            $this->form_validation->set_rules('get_cat_name', lang('category_already_on_promotion.'), 'required');
            $custom_validation = false;
        }else{
            $custom_validation = true;
        }

        if ($this->form_validation->run() == true) {
            $data = array(
                'type' => $this->input->post('promotion_type'),
                'name' => $this->input->post('offer_name'),
                'title' => $this->input->post('offer_title'),
                'description' => $this->input->post('offer_description'),
                'status' => $this->input->post('status'),
                'isMoreThanOne' => $this->input->post('more_than_product'),
                'disqualifiedRemoval' => $this->input->post('no_qualify') == 1 ? $this->input->post('no_qualify') : 0 ,
                'redemptionLimit' => $this->input->post('limit_customers') == 1 ? $this->input->post('limit') : 0,
                'minAmount' => $this->input->post('is_min_max_price') == 1 ? $this->input->post('min_price') : 0,
                'maxAmount' => $this->input->post('is_min_max_price') == 1 ? $this->input->post('max_price') : 0,
                'isDateRange' => $this->input->post('available_daterange'),
                'startDate' => $this->input->post('available_daterange') == 1 ?  $this->sma->fsd($this->input->post('offer_start_date')) : NULL,
                'endDate' => $this->input->post('available_daterange') == 1 ? $this->sma->fsd($this->input->post('offer_end_date')) : NULL,
                'inDaysOnly' => $this->input->post('specific_day'),
                'days' => $this->input->post('offer_days'),
                'isTimeRange' => $this->input->post('available_timerange') == 1 ? $this->input->post('available_timerange') : 0,
                'startTime' => $this->input->post('available_timerange') == 1 ? $this->input->post('offer_start_time') : 0,
                'endTime' => $this->input->post('available_timerange') == 1 ? $this->input->post('offer_end_time') : 0,
                'triggerOnItem' => $this->input->post('product_2') > 0 ? 1 : 0,
                'itemQuantity' => $this->input->post('quantity_incart') > 0 ? $this->input->post('quantity_incart') : 0,
                'how_many_buy' => $this->input->post('buy_products') > 0 ?  $this->input->post('buy_products') : 0 ,
                'how_many_free' => $this->input->post('get_products') > 0 ?  $this->input->post('get_products') : 0,
                'how_much_percentage' => $how_much_percentage > 0 ?  $how_much_percentage : 0,
                'cart_amount_limit' => $this->input->post('cart_amount_limit'),
                'pricegroup_include' => $this->input->post('pricegroup_include') ? 1 : 0,
                'where_to_apply' => $this->input->post('where_to_apply'),
                'run_over_other_promotions' => $this->input->post('run_over_other_promotions') == 1 ? 1 : 0,
            );
        }


        if(!empty($this->input->post('product')) && $this->form_validation->run() == true ){
            $custom_validation = true;
        }else{
            if($this->input->post('promotion_type') != 'cartvaluerangeFree' ) {
            $custom_validation = false;
            }
            if($this->input->post('promotion_type') == 'buyProductsWithPercentage' && empty($this->input->post('product'))){
                $this->form_validation->set_rules('product', lang('Products'), 'required');
                $this->form_validation->run() == false;
            }
        }


        if($this->input->post('get_cat_name') && $this->input->post('get_shop_name') && $this->form_validation->run() == true){
            $custom_validation = true;
        }else{
            $this->input->post('get_cat_name') == 0;
            $this->input->post('get_shop_name') == 0;
        }

        if ($custom_validation == true && $this->products_model->addPromotions($data, $this->input->post('product'), $this->input->post('get_cat_name'), $this->input->post('get_shop_name'), $this->input->post('product_2') , $this->input->post('get_percentage') )) {
            $this->session->set_flashdata('message', lang("upselling_added"));
            admin_redirect("products/upselling");
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['groups'] = $this->products_model->getPromotions();


        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('upselling_heading')));
        $meta = array('page_title' => lang('upselling_heading'), 'bc' => $bc);
        $this->page_construct('products/create_upselling', $meta, $this->data);
    }

    function edit_upselling($id = NULL)
    {

        if($_POST['apply_type'] == 'cat'){
            $_POST['where_to_apply'] = $_POST['where_to_apply_cat'] ;
        }else{
            $_POST['where_to_apply'] = $_POST['where_to_apply_pro'] ;
        }

        /*echo '<pre>';
        print_r($_POST);
        die();*/

        if($this->input->post('promotion_type')){
            $this->form_validation->set_rules('offer_name', lang('offer_name'), 'required');
            $this->form_validation->set_rules('offer_title', lang('offer_title'), 'required');
            $this->form_validation->set_rules('offer_start_date', lang('offer_start_date'), 'required');

            if($this->input->post('promotion_type') == 'buyOneGetCategoryFree'){
                if(!empty($this->input->post('get_cat_name_free')) && !empty($this->input->post('get_shop_name'))  && $this->form_validation->run() == true ){
                    $_POST['get_cat_name'] = $_POST['get_cat_name_free'];
                    $custom_validation = true;
                }else{
                    $this->form_validation->set_rules('get_cat_name_free', lang('Category'), 'required');
                    $this->form_validation->set_rules('get_shop_name', lang('Warehouse'), 'required');
                }
            }

            if($this->input->post('promotion_type') == 'buyCategoryWithPercentage' ){
                if(!empty($this->input->post('get_cat_name')) && !empty($this->input->post('get_shop_name'))  && $this->form_validation->run() == true ){
                    $custom_validation = true;
                }else{
                    $this->form_validation->set_rules('get_cat_name', lang('Category'), 'required');
                    $this->form_validation->set_rules('get_shop_name', lang('Warehouse'), 'required');
                }
            }

            if($this->input->post('promotion_type') == 'buyOneGetOne' || $this->input->post('promotion_type') == 'buyOneGetOtherProductFree' ){
                if(!empty($this->input->post('get_shop_name'))  && $this->form_validation->run() == true ){
                    $custom_validation = true;
                }else{
                    $this->form_validation->set_rules('get_shop_name', lang('Warehouse'), 'required');
                }
            }
            if($this->input->post('promotion_type') == 'cartvaluerangeFree' ){
                if(!empty($this->input->post('cart_amount_limit')) && $this->form_validation->run() == true ){
                    $custom_validation = true;
                }else{
                    $this->form_validation->set_rules('cart_amount_limit', 'Amount For Limit', 'required');
                }
            }

            //To check the category already on promotion or not?
            $running_promtion_cat = $this->products_model->findCategoryAlreadyOnPromotion();
            $checking_cat_alreay_exist = array_intersect($running_promtion_cat, $this->input->post('get_cat_name'));
            if (count($checking_cat_alreay_exist) > 0) {
                $this->form_validation->set_rules('get_cat_name', lang('category_already_on_promotion.'));
                $custom_validation = false;
            }else{
                $custom_validation = true;
            }

            if($this->input->post('get_percentage_pro') > 0){
                $how_much_percentage = $this->input->post('get_percentage_pro');
            }else{
                $how_much_percentage = $this->input->post('get_percentage');
            }

            if ($this->form_validation->run() == true) {
                $data = array(
                    'type' => $this->input->post('promotion_type'),
                    'name' => $this->input->post('offer_name'),
                    'title' => $this->input->post('offer_title'),
                    'description' => $this->input->post('offer_description'),
                    'status' => $this->input->post('status'),
                    'isMoreThanOne' => $this->input->post('more_than_product'),
                    'disqualifiedRemoval' => $this->input->post('no_qualify') == 1 ? $this->input->post('no_qualify') : 0 ,
                    'redemptionLimit' => $this->input->post('limit_customers') == 1 ? $this->input->post('limit') : 0,
                    'minAmount' => $this->input->post('is_min_max_price') == 1 ? $this->input->post('min_price') : 0,
                    'maxAmount' => $this->input->post('is_min_max_price') == 1 ? $this->input->post('max_price') : 0,
                    'isDateRange' => $this->input->post('available_daterange'),
                    'startDate' => $this->input->post('available_daterange') == 1 ?  $this->sma->fsd($this->input->post('offer_start_date')) : NULL,
                    'endDate' => $this->input->post('available_daterange') == 1 ? $this->sma->fsd($this->input->post('offer_end_date')) : NULL,
                    'inDaysOnly' => $this->input->post('specific_day'),
                    'days' => $this->input->post('offer_days'),
                    'isTimeRange' => $this->input->post('available_timerange') == 1 ? $this->input->post('available_timerange') : 0,
                    'startTime' => $this->input->post('available_timerange') == 1 ? $this->input->post('offer_start_time') : 0,
                    'endTime' => $this->input->post('available_timerange') == 1 ? $this->input->post('offer_end_time') : 0,
                    'triggerOnItem' => $this->input->post('product_2') > 0 ? 1 : 0,
                    'itemQuantity' => $this->input->post('quantity_incart') > 0 ? $this->input->post('quantity_incart') : 0,
                    'how_many_buy' => $this->input->post('buy_products') > 0 ?  $this->input->post('buy_products') : 0 ,
                    'how_many_free' => $this->input->post('get_products') > 0 ?  $this->input->post('get_products') : 0,
                    'how_much_percentage' => $how_much_percentage > 0 ?  $how_much_percentage : 0,
                    'cart_amount_limit' => $this->input->post('cart_amount_limit'),
                    'pricegroup_include' => $this->input->post('pricegroup_include') == 1 ? $this->input->post('available_timerange') : 0,
                    'where_to_apply' => $this->input->post('where_to_apply'),
                    'run_over_other_promotions' => $this->input->post('run_over_other_promotions') == 1 ? 1 : 0,
                );
            }


            if(!empty($this->input->post('product')) && $this->form_validation->run() == true ){
                $custom_validation = true;
            }else{
                if($this->input->post('promotion_type') != 'cartvaluerangeFree' ) {
                $custom_validation = false;
                }
                if($this->input->post('promotion_type') == 'buyProductsWithPercentage' && empty($this->input->post('product'))){
                    $this->form_validation->set_rules('product', lang('Products'), 'required');
                    $this->form_validation->run() == false;
                }
            }

            if($this->input->post('get_cat_name') && $this->input->post('get_shop_name') && $this->form_validation->run() == true){
                $custom_validation = true;
            }else{
                $this->input->post('get_cat_name') == 0;
                $this->input->post('get_shop_name') == 0;
            }

            if ($custom_validation == true && $this->products_model->addPromotions($data, $this->input->post('product'), $this->input->post('get_cat_name'), $this->input->post('get_shop_name'), $this->input->post('product_2') , $this->input->post('get_percentage') )) {

                $this->products_model->delete_updated_promotion($this->input->post('old_update_upsel_id'));
                $this->session->set_flashdata('message', lang("Upselling_edit_successfully"));
                admin_redirect("products/upselling");
            }

        }
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('upselling_heading')));
        $meta = array('page_title' => lang('upselling_heading'), 'bc' => $bc);
        $pdata = $this->products_model->editUpselling($id);
        if($pdata->type =='cartvaluerangeFree'){
            $pdata->upselling_id = $id;
        }
        //$this->data['upselling'] = $this->products_model->editUpselling($id);
        $this->data['upselling'] = $pdata;
        $this->page_construct('products/edit_upselling', $meta, $this->data);


        //}

    }

    function delete_upselling($id = NULL)
    {

        if (empty($id)) {
            $this->session->set_flashdata('error', lang("We're sorry something going wrong!"));
            admin_redirect("products/upselling");
        }

        if ($this->products_model->deleteUpselling($id)) {
            $this->session->set_flashdata('message', lang("Promotion_Deleted"));
            admin_redirect("products/upselling");
        }
    }




    //Sizing Information Moduel
    function sizing_guide()
    {
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $this->data['sizes'] = $this->products_model->getAllSizeGuide();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('sizing_guide')));
        $meta = array('page_title' => lang('sizing_guide'), 'bc' => $bc);
        $this->page_construct('products/sizing_guide', $meta, $this->data);
    }

    function create_sizing_guide(){
        /*echo '<pre>';
        print_r($_POST);
        die();*/
        $this->form_validation->set_rules('title', lang('title'), 'required');
        $this->form_validation->set_rules('tab1_label', lang('tab1_label'), 'required');
        $this->form_validation->set_rules('tab1_description', lang('tab1_description'), 'required');

        if ($this->form_validation->run() == true) {
            $data = array(
                'title' => $this->input->post('title'),
                'tab1_label' => $this->input->post('tab1_label'),
                'tab1_description' => $this->input->post('tab1_description'),
                'tab2_label' => $this->input->post('tab2_label'),
                'tab2_description' => $this->input->post('tab2_description'),
                'tab3_label' => $this->input->post('tab3_label'),
                'tab3_description' => $this->input->post('tab3_description'),
                'tab4_label' => $this->input->post('tab4_label'),
                'tab4_description' => $this->input->post('tab4_description'),
            );
        }

        if ($this->form_validation->run() == true && $this->products_model->addSizingGuide($data)) {
            $this->session->set_flashdata('message', lang("sizing_guide_added"));
            admin_redirect("products/sizing_guide");
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('create_sizing_guide')));
        $meta = array('page_title' => lang('create_sizing_guide'), 'bc' => $bc);
        $this->page_construct('products/create_sizing_guide', $meta, $this->data);
    }

    function edit_sizing_guide($id = NULL)
    {
        if($this->input->post('title')){

            $this->form_validation->set_rules('title', lang('title'), 'required');
            $this->form_validation->set_rules('tab1_label', lang('tab1_label'), 'required');
            $this->form_validation->set_rules('tab1_description', lang('tab1_description'), 'required');

            if ($this->form_validation->run() == true) {
                $data = array(
                    'title' => $this->input->post('title'),
                    'tab1_label' => $this->input->post('tab1_label'),
                    'tab1_description' => $this->input->post('tab1_description'),
                    'tab2_label' => $this->input->post('tab2_label'),
                    'tab2_description' => $this->input->post('tab2_description'),
                    'tab3_label' => $this->input->post('tab3_label'),
                    'tab3_description' => $this->input->post('tab3_description'),
                    'tab4_label' => $this->input->post('tab4_label'),
                    'tab4_description' => $this->input->post('tab4_description'),
                );
            }

            if ($this->form_validation->run() == true && $this->products_model->updateSizingGuide($data ,$this->input->post('size_id'))) {
                $this->session->set_flashdata('message', lang("sizing_guide_updated"));
                admin_redirect("products/sizing_guide");
            }

        }

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('edit_sizing_guide')));
        $meta = array('page_title' => lang('edit_sizing_guide'), 'bc' => $bc);
        $this->data['sizing_guide'] = $this->products_model->editSizingGuide($id);
        $this->page_construct('products/edit_sizing_guide', $meta, $this->data);

    }

    function delete_sizing_guide($id = NULL)
    {
        if (empty($id)) {
            $this->session->set_flashdata('error', lang("We're sorry something going wrong!"));
            admin_redirect("products/sizing_guide");
        }

        if ($this->products_model->deleteSizingGuide($id)) {
            $this->session->set_flashdata('message', lang("sizing_guide_deleted"));
            admin_redirect("products/sizing_guide");
        }
    }


    function productImageNameChange($file_location, $temp_name){
        $str_array = explode("\\", $temp_name);
        $temp_img_name =  end($str_array);
        $reltive_path = FCPATH.$file_location;
        $new_name = 'img506.jpg';
        rename($reltive_path.$temp_img_name, $reltive_path.$new_name);
        return $new_name;
    }


    function getVarientIDByName($variant_name = NULL){
        $variant_id = $this->products_model->VariantIdGetByName($variant_name);
        $variant_values = $this->products_model->VariantValuesByID($variant_id);

        if($variant_values){
            $data = json_encode($variant_values);
        } else {
            $data = false;
        }
        echo $data;
    }

    function getVarientValuesById(){
        $variant=$this->input->get('variant');
        $pid=$this->input->get('pid');
        $variant_id = $this->products_model->VariantIdGetByName($variant);
        $variant_values = $this->products_model->VariantValuesByIDSelect($variant_id,$pid);

        if($variant_values){
            $data = json_encode($variant_values);
        } else {
            $data = false;
        }
        echo $data;
        exit();
    }
    function getVarientValuesById_add($variant){
        $variant_id = $this->products_model->VariantIdGetByName($variant);
        $variant_values = $this->products_model->VariantValuesByIDSelect_add($variant_id);

        if($variant_values){
            $data = json_encode($variant_values);
        } else {
            $data = false;
        }
        echo $data;
    }



    // ********************************************************************************
    //Payment Method Section
    function payment_methods()
    {
        $this->sma->checkPermissions();
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('payment_methods')));
        $meta = array('page_title' => lang('payment_methods'), 'bc' => $bc);
        $this->page_construct('products/payment_methods', $meta, $this->data);
    }

    function getPayment_methods()
    {
        $this->sma->checkPermissions();
        $this->load->library('datatables');
        $this->datatables
            ->select("{$this->db->dbprefix('payment_methods')}.id as id, {$this->db->dbprefix('payment_methods')}.image, {$this->db->dbprefix('payment_methods')}.code, {$this->db->dbprefix('payment_methods')}.name, IF(status>0,'Enable','Disable') ", FALSE)
            ->from("payment_methods")
            ->add_column("Actions", "<div class=\"text-center\">"." <a href='" . admin_url('products/edit_payment_methods/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_category") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_category") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('products/delete_payment_methods/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");

        echo $this->datatables->generate();
    }

    function add_payment_methods()
    {
        $this->sma->checkPermissions();
        $this->load->helper('security');
        $this->form_validation->set_rules('code', 'Payment Method code', 'trim|is_unique[payment_methods.code]|required');
        $this->form_validation->set_rules('name', 'Payment Method name', 'required|min_length[3]');
        //$this->form_validation->set_rules('name', lang("name"), 'required|is_unique[payment_methods.name]|min_length[3]');
        $this->form_validation->set_rules('userfile', 'Payment Method Image', 'xss_clean');
        $this->form_validation->set_rules('sort_order', 'Sort Order', 'numeric|required');

        if($this->input->post('secret_code') == 'paypal'){
            $this->form_validation->set_rules('paypal_email', 'PayPal Email', 'required|valid_email|is_unique[payment_methods.paypal_email]');
            $this->form_validation->set_rules('paypal_password', 'PayPal Password', 'required|min_length[3]');
        }else if($this->input->post('secret_code') == 'paytabs'){
            $this->form_validation->set_rules('merchant_id', 'PayTabs Merchant Id', 'required|is_unique[payment_methods.merchant_id]');
            $this->form_validation->set_rules('secret_key', 'PayTabs Secret Key', 'required|min_length[3]');
            $this->form_validation->set_rules('url_redirect', 'PayTabs URL Redirect', 'required');
        }

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'secret_code' => $this->input->post('secret_code'),
                'paypal_email' => $this->input->post('secret_code') == 'paypal' ? $this->input->post('paypal_email') : '',
                'paypal_password' => $this->input->post('secret_code') == 'paypal' ? $this->input->post('paypal_password') : '',
                'merchant_id' => $this->input->post('secret_code') == 'paytabs' ? $this->input->post('merchant_id') : '',
                'secret_key' => $this->input->post('secret_code') == 'paytabs' ? $this->input->post('secret_key') : '',
                'url_redirect' => $this->input->post('secret_code') == 'paytabs' ? $this->input->post('url_redirect') : '',
                'secret_key' => $this->input->post('secret_key'),
                'code' => $this->input->post('code'),
                'total' => $this->input->post('total'),
                'order_status' => $this->input->post('order_status'),
                'geo_zone_id' => $this->input->post('geo_zone'),
                'status' => $this->input->post('status'),
                'instruction' => $this->input->post('instruction'),
                'sort_order' => $this->input->post('sort_order'),
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                //$config['max_size'] = $this->allowed_file_size;
                //$config['max_width'] = $this->Settings->iwidth;
                //$config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                // Large Image resizing
                $config['new_image'] = $this->upload_path . $photo;
                $config['width'] = $this->Settings->iwidth;
                $config['height'] = $this->Settings->iheight;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'left';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config = NULL;
            }

        } elseif ($this->input->post('add_payment_method')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/payment_methods");
        }

        if ($this->form_validation->run() == true && $this->products_model->addPaymentMethod($data)) {
            $this->session->set_flashdata('message', lang("payment_method_added"));
            admin_redirect("products/payment_methods");
        } else {

            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/add_payment_methods', $this->data);

        }
    }

    function edit_payment_methods($id = NULL)
    {
        $this->sma->checkPermissions();
        $this->load->helper('security');
        $this->form_validation->set_rules('code', 'Payment Method code', 'trim|required');
        $pr_details = $this->products_model->getPaymentMethodByID($id);
        if ($this->input->post('code') !== $pr_details->code) {
            $this->form_validation->set_rules('code', 'Payment Method code', 'required|is_unique[payment_methods.code]');
        }

        if ($this->input->post('name') != $pr_details->name) {
            $this->form_validation->set_rules('name', 'Payment Method name', 'required|is_unique[payment_methods.name]');
        }

        $this->form_validation->set_rules('name', 'Payment Method name' , 'required|min_length[3]');
        $this->form_validation->set_rules('userfile', 'Payment Method Image', 'xss_clean');
        $this->form_validation->set_rules('sort_order', 'Sort Order', 'numeric|required');


        if($this->input->post('secret_code') == 'paypal'){
            $this->form_validation->set_rules('paypal_email', 'PayPal Email', 'required|valid_email|is_unique[payment_methods.paypal_email]');
            $this->form_validation->set_rules('paypal_password', 'PayPal Password', 'required|min_length[3]');
        }else if($this->input->post('secret_code') == 'paytabs'){
            if ($this->input->post('merchant_id') !== $pr_details->merchant_id) {
                $this->form_validation->set_rules('merchant_id', 'PayTabs Merchant Id', 'is_unique[payment_methods.merchant_id]');
            }
            $this->form_validation->set_rules('merchant_id', 'PayTabs Merchant Id', 'required');
            $this->form_validation->set_rules('secret_key', 'PayTabs Secret Key', 'required|min_length[3]');
            $this->form_validation->set_rules('url_redirect', 'PayTabs URL Redirect', 'required');
        }

        if ($this->form_validation->run() == true) {

            $data = array(
                'name' => $this->input->post('name'),
                'secret_code' => $this->input->post('secret_code'),
                'paypal_email' => $this->input->post('secret_code') == 'paypal' ? $this->input->post('paypal_email') : '',
                'paypal_password' => $this->input->post('secret_code') == 'paypal' ? $this->input->post('paypal_password') : '',
                'merchant_id' => $this->input->post('secret_code') == 'paytabs' ? $this->input->post('merchant_id') : '',
                'secret_key' => $this->input->post('secret_code') == 'paytabs' ? $this->input->post('secret_key') : '',
                'url_redirect' => $this->input->post('secret_code') == 'paytabs' ? $this->input->post('url_redirect') : '',
                'secret_key' => $this->input->post('secret_key'),
                'code' => $this->input->post('code'),
                'total' => $this->input->post('total'),
                'order_status' => $this->input->post('order_status'),
                'geo_zone_id' => $this->input->post('geo_zone'),
                'status' => $this->input->post('status'),
                'instruction' => $this->input->post('instruction'),
                'sort_order' => $this->input->post('sort_order'),
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                //$config['max_size'] = $this->allowed_file_size;
                //$config['max_width'] = $this->Settings->iwidth;
                //$config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                // Large Image resizing
                $config['new_image'] = $this->upload_path . $photo ;
                $config['width'] = $this->Settings->iwidth;
                $config['height'] = $this->Settings->iheight;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'left';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config = NULL;
            }

        } elseif ($this->input->post('edit_payment_method')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/payment_methods");
        }

        if ($this->form_validation->run() == true && $this->products_model->updatePaymentMethod($id, $data)) {
            $this->session->set_flashdata('message', lang("payment_method_updated"));
            admin_redirect("products/payment_methods");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['payment_method'] = $this->products_model->getPaymentMethodByID($id);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/edit_payment_methods', $this->data);

        }
    }

    function delete_payment_methods($id = NULL)
    {
        $this->sma->checkPermissions();
        if ($this->products_model->deletPaymentMethod($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("payment_method_delete")));
        }
    }

    //End of Payment Method Section
    // ********************************************************************************



    // ********************************************************************************
    //Shipment Method Section
    function shipment_methods()
    {
        $this->sma->checkPermissions();
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('system_settings')), array('link' => '#', 'page' => lang('shipment_methods')));
        $meta = array('page_title' => lang('shipment_methods'), 'bc' => $bc);
        $this->page_construct('products/shipment_methods', $meta, $this->data);
    }

    function getShipment_methods()
    {
        $this->sma->checkPermissions();
        $this->load->library('datatables');
        $this->datatables
            ->select("{$this->db->dbprefix('shipment_methods')}.id as id, {$this->db->dbprefix('shipment_methods')}.image, {$this->db->dbprefix('shipment_methods')}.code, {$this->db->dbprefix('shipment_methods')}.name, IF(status>0,'Enable','Disable') ", FALSE)
            ->from("shipment_methods")
            ->add_column("Actions", "<div class=\"text-center\">"." <a href='" . admin_url('products/edit_shipment_methods/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_shipment_method") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_shipment_method") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('products/delete_shipment_methods/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");

        echo $this->datatables->generate();
    }

    function add_shipment_methods()
    {
        $this->sma->checkPermissions();
        $this->load->helper('security');
        $this->form_validation->set_rules('code', 'Shipment code', 'trim|is_unique[shipment_methods.code]|required');
        $this->form_validation->set_rules('name', 'Shipment name', 'required|min_length[3]');
        $this->form_validation->set_rules('userfile', 'Shipment Image', 'xss_clean');

        if($this->input->post('secret_code') == 'aramex'){
            $this->form_validation->set_rules('shipment_type', 'Shipment Type', 'trim|required');
            $this->form_validation->set_rules('account_country_code', 'Account Country Code', 'required');
            $this->form_validation->set_rules('account_entity', 'Account Entity', 'required');
            $this->form_validation->set_rules('account_number', 'Account Number', 'required');
            $this->form_validation->set_rules('account_pin', 'Account Pin', 'required');
            $this->form_validation->set_rules('user_name', 'User Name', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('version', 'Version', 'required');
        }

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'secret_code' => $this->input->post('secret_code'),
                'account_country_code' => $this->input->post('secret_code') == 'aramex' ? $this->input->post('account_country_code') : '',
                'account_entity' => $this->input->post('secret_code') == 'aramex' ? $this->input->post('account_entity') : '',
                'account_number' => $this->input->post('secret_code') == 'aramex' ? $this->input->post('account_number') : '',
                'account_pin' => $this->input->post('secret_code') == 'aramex' ? $this->input->post('account_pin') : '',
                'user_name' => $this->input->post('secret_code') == 'aramex' ? $this->input->post('user_name') : '',
                'password' => $this->input->post('secret_code') == 'aramex' ? $this->input->post('password') : '',
                'version' => $this->input->post('secret_code') == 'aramex' ? $this->input->post('version') : '',
                'shipment_type' => $this->input->post('shipment_type') == 1 ? 'DPX' : 'PPX',
                'geo_zone_id' => $this->input->post('geo_zone'),
                'code' => $this->input->post('code'),
                'fixed_charges' => $this->input->post('fixed_charges'),
                'status' => $this->input->post('status'),
                'instruction' => $this->input->post('instruction'),
                'free_ship_amount' => $this->input->post('free_ship_amount'),
                'free_ship_zone' => $this->input->post('free_ship_zone'),
                'free_amount_status' => $this->input->post('free_amount_status'),
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                //$config['max_size'] = $this->allowed_file_size;
                //$config['max_width'] = $this->Settings->iwidth;
                //$config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                // Large Image resizing
                $config['new_image'] = $this->upload_path . $photo;
                $config['width'] = $this->Settings->iwidth;
                $config['height'] = $this->Settings->iheight;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'left';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config = NULL;
            }

        } elseif ($this->input->post('add_shipment_method')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/shipment_methods");
        }

        if ($this->form_validation->run() == true && $this->products_model->addShipmentMethod($data)) {
            $this->session->set_flashdata('message', lang("shipment_added"));
            admin_redirect("products/shipment_methods");
        } else {

            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/add_shipment_methods', $this->data);

        }
    }

    function edit_shipment_methods($id = NULL)
    {
        $this->sma->checkPermissions();
        $this->load->helper('security');
        $this->form_validation->set_rules('code', 'Shipment code', 'trim|required');
        $pr_details = $this->products_model->getShipmentMethodByID($id);
        if ($this->input->post('code') !== $pr_details->code) {
            $this->form_validation->set_rules('code', 'Shipment code', 'required|is_unique[shipment_methods.code]');
        }

        if ($this->input->post('name') != $pr_details->name) {
            $this->form_validation->set_rules('name', 'Shipment name', 'required|is_unique[shipment_methods.name]');
        }

        $this->form_validation->set_rules('name', 'Shipment name' , 'required|min_length[3]');
        $this->form_validation->set_rules('userfile', 'Shipment Image', 'xss_clean');

        if($this->input->post('secret_code') == 'aramex'){
            $this->form_validation->set_rules('shipment_type', 'Shipment Type', 'trim|required');
            $this->form_validation->set_rules('account_country_code', 'Account Country Code', 'required');
            $this->form_validation->set_rules('account_entity', 'Account Entity', 'required');
            $this->form_validation->set_rules('account_number', 'Account Number', 'required');
            $this->form_validation->set_rules('account_pin', 'Account Pin', 'required');
            $this->form_validation->set_rules('user_name', 'User Name', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('version', 'Version', 'required');
        }



        if ($this->form_validation->run() == true) {

            $data = array(
                'name' => $this->input->post('name'),
                'secret_code' => $this->input->post('secret_code'),
                'account_country_code' => $this->input->post('secret_code') == 'aramex' ? $this->input->post('account_country_code') : '',
                'account_entity' => $this->input->post('secret_code') == 'aramex' ? $this->input->post('account_entity') : '',
                'account_number' => $this->input->post('secret_code') == 'aramex' ? $this->input->post('account_number') : '',
                'account_pin' => $this->input->post('secret_code') == 'aramex' ? $this->input->post('account_pin') : '',
                'user_name' => $this->input->post('secret_code') == 'aramex' ? $this->input->post('user_name') : '',
                'password' => $this->input->post('secret_code') == 'aramex' ? $this->input->post('password') : '',
                'version' => $this->input->post('secret_code') == 'aramex' ? $this->input->post('version') : '',
                'shipment_type' => $this->input->post('shipment_type') == 1 ? 'DPX' : 'PPX',
                'geo_zone_id' => $this->input->post('geo_zone'),
                'code' => $this->input->post('code'),
                'fixed_charges' => $this->input->post('fixed_charges'),
                'status' => $this->input->post('status'),
                'instruction' => $this->input->post('instruction'),
                'free_ship_amount' => $this->input->post('free_ship_amount'),
                'free_ship_zone' => $this->input->post('free_ship_zone'),
                'free_amount_status' => $this->input->post('free_amount_status'),
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                //$config['max_size'] = $this->allowed_file_size;
                //$config['max_width'] = $this->Settings->iwidth;
                //$config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                // Large Image resizing
                $config['new_image'] = $this->upload_path . $photo ;
                $config['width'] = $this->Settings->iwidth;
                $config['height'] = $this->Settings->iheight;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'left';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config = NULL;
            }

        } elseif ($this->input->post('edit_shipment_method')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/shipment_methods");
        }

        if ($this->form_validation->run() == true && $this->products_model->updateShipmentMethod($id, $data)) {
            $this->session->set_flashdata('message', lang("shipment_updated"));
            admin_redirect("products/shipment_methods");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['shipment_method'] = $this->products_model->getShipmentMethodByID($id);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/edit_shipment_methods', $this->data);

        }
    }

    function delete_shipment_methods($id = NULL)
    {
        $this->sma->checkPermissions();
        if ($this->products_model->deletShipmentMethod($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("shipment_method_deleted")));
        }
    }

    //End of Shipment Method Section

}