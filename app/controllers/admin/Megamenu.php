<?php defined('BASEPATH') or exit('No direct script access allowed');

class Megamenu extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->load->admin_model('pos_model');
        $this->load->helper('text');
        $this->pos_settings = $this->pos_model->getSetting();
        $this->pos_settings->pin_code = $this->pos_settings->pin_code ? md5($this->pos_settings->pin_code) : NULL;
        $this->data['pos_settings'] = $this->pos_settings;
        $this->session->set_userdata('last_activity', now());
        $this->lang->admin_load('pos', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->data['selectitem']['repeat'] = array('' => 'Please Select...', 'no-repeat' => 'no-repeat', 'repeat' => 'repeat', 'repeat-x' => 'repeat-x', 'repeat-y' => 'repeat-y');
        $this->data['selectitem']['size'] = array('' => 'Please Select...', 'auto' => 'Auto', 'cover' => 'Cover', 'contain' => 'Contain', '100%' => '100%');
        $this->data['selectitem']['fontweight'] = array('' => 'Please Select...', 'normal' => 'Normal', 'bold' => 'Bold', 'bolder' => 'Bolder');
        $this->data['selectitem']['bg_position'] = array('' => 'Please Select...', 'top left' => 'Top - Left', 'top center' => 'Top - Center', 'top right' => 'Top - Right', 'center left' => 'Center - Left', 'center center' => 'Center - Center', 'center right' => 'Center - Right', 'bottom left' => 'Bottom - Left', 'bottom center' => 'Bottom - Center', 'bottom right' => 'Bottom - Right');
      //  $this->load->model('megamenu_model');
    //    $this->load->library('megamenulib');
    }

    public function index()
    {
        $groups         	  = $this->megamenulib->get_all_groups();
        $this->data['groups'] = array();

        foreach ($groups as $group)
        {
            $items 										 = $this->megamenulib->get_all($group['id']);
            $this->data['groups'][$group['id']]          = $group;
            $this->data['groups'][$group['id']]['items'] = parse_children($items);
        }

        $this->data['title'] = 'Mega Menu';
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('megamenu'), 'page' => 'Menu'), array('link' => '#', 'page' => 'Menu List'));
        $meta = array('page_title' => lang('pos_sales'), 'bc' => $bc);
        $this->page_construct('megamenu/list/index', $meta, $this->data);

       /* _render_page('template/header');
        _render_page('template/login/navbar');
        _render_page('megamenu/list/index');
        _render_page('template/footer');*/
    }

    /**
     * Add new item to navigation group
     *
     * @param string $id Item id
     *
     * @return void
     */
    public function add($id)
    {
        $this->form_validation->set_rules('mname', 'Menu Name', 'required|trim');
        $this->form_validation->set_rules('url', 'URL', 'required|trim');
        $this->form_validation->set_rules('parent_id', 'Parent', 'required|integer');

       // $groups = $this->ion_auth->groups()->result_array();
        $groups = array();
        $this->data['groups'] = $groups;

        if ($this->form_validation->run() === FALSE)
        {
            $this->data['navigation'] = $this->megamenulib->get_group($id);
            $this->data['dropdown']   = $this->megamenulib->get_all_for_dropdown($id);
            $this->data['title']      = 'Add item';
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('megamenu'), 'page' => 'Menu'), array('link' => '#', 'page' => 'Menu List'));
            $meta = array('page_title' => lang('pos_sales'), 'bc' => $bc);
            $this->page_construct('megamenu/list/add', $meta, $this->data);

           // _render_page('template/header');
           // _render_page('template/login/navbar');
           // _render_page('megamenu/list/add',$this->data);
           // _render_page('template/footer');

        }
        else
        {

            $data['mname'] =  $this->input->post('mname');
            $data['url'] =  $this->input->post('url');
            $data['parent_id'] =  $this->input->post('parent_id');
            $data['group_id'] = $id;
            $data['grid'] =  $this->input->post('grid');
            $data['content'] =  $this->input->post('content');
            $data['icon'] =  $this->input->post('icon');
            $data['font'] =  $this->input->post('font');
            $data['fontcolor'] =  $this->input->post('fontcolor');
            $data['fontsize'] =  $this->input->post('fontsize');
            $data['fontweight'] =  $this->input->post('fontweight');
            $data['bg_color'] =  $this->input->post('bg_color');
            $data['bg_hover_color'] =  $this->input->post('bg_hover_color');
            $data['bg_size'] =  $this->input->post('bg_size');
            $data['bg_repeat'] =  $this->input->post('bg_repeat');
            $data['bg_position'] =  $this->input->post('bg_position');
            $data['groups'] =  $this->input->post('groups') != '' ? serialize($this->input->post('groups')) : '';


            $item_id = $this->megamenu_model->add_item($data);


            if(!empty($_FILES['bg_image']['name'][0]))
            {

                $upload = $this->_do_upload($item_id);

                if($upload['status'] === FALSE)
                {
                    $this->session->set_flashdata('message', $upload['error_string']);
                    redirect('admin/megamenu/edit/'.$item_id);

                } else {
                    $data['bg_image'] = $upload['file_name'];
                    $this->megamenu_model->update_item($item_id, $data);
                }


            }

            $this->session->set_flashdata('message', lang('megamenu_item_added'));
            redirect('admin/megamenu');

        }
    }

    /**
     * Edit item from navigation group
     *
     * @param string $id Item id
     *
     * @return void
     */
    public function edit($id)
    {
        $this->form_validation->set_rules('mname', 'Menu Name', 'required|trim');
        $this->form_validation->set_rules('url', 'URL', 'required|trim');

       // $groups = $this->ion_auth->groups()->result_array();
        $groups = array();
        $this->data['groups'] = $groups;


        if ($this->form_validation->run() === FALSE)
        {
            if ($this->data['item'] = $this->megamenu_model->get_item($id))
            {
                $this->data['navigation'] = $this->megamenulib->get_group($this->data['item']['group_id']);
                $this->data['dropdown']   = $this->megamenulib->get_all_for_dropdown($this->data['item']['group_id'], (int) $id);
                $this->data['title']      = 'Edit item';

                $this->data['csrf'] = _get_csrf_nonce();

                $bc = array(array('link' => base_url(), 'page' => lang('home')),array('link' => admin_url('megamenu'), 'page' => 'Menu'), array('link' => '#', 'page' => 'Menu List'));
                $meta = array('page_title' => lang('pos_sales'), 'bc' => $bc);
                $this->page_construct('megamenu/list/edit', $meta, $this->data);
            }
            else
            {
                show_404();
            }
        }
        else
        {

            if($this->input->post('delete_image') == 'true'){
                $this->data['item'] = $this->megamenu_model->get_item($id);
                @unlink(FCPATH.'assets'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'menuimage'.DIRECTORY_SEPARATOR.'bg_images'.DIRECTORY_SEPARATOR.$this->data['item']['bg_image']);
                $data['bg_image'] = '';
            }

            if(!empty($_FILES['bg_image']['name'][0]))
            {

                $upload = $this->_do_upload($id);

                if($upload['status'] === FALSE)
                {
                    $this->session->set_flashdata('message', $upload['error_string']);
                    redirect('admin/megamenu/edit/'.$id);

                } else {
                    $data['bg_image'] = $upload['file_name'];
                }


            }
           /* echo "<pre>";
            print_r($this->input->post());
            exit('sadfs');*/

            $data['mname'] =  $this->input->post('mname');
            $data['url'] =  $this->input->post('url');
            $data['bg_color'] =  $this->input->post('bg_color');
            $data['bg_hover_color'] =  $this->input->post('bg_hover_color');
            $data['icon'] =  $this->input->post('icon');
            $data['font'] =  $this->input->post('font');
            $data['fontcolor'] =  $this->input->post('fontcolor');
            $data['fontsize'] =  $this->input->post('fontsize');
            $data['fontweight'] =  $this->input->post('fontweight');
            $data['grid'] =  $this->input->post('grid');
            $data['content'] =  $this->input->post('content');
            $data['bg_repeat'] =  $this->input->post('bg_repeat');
            $data['bg_size'] =  $this->input->post('bg_size');
            $data['bg_position'] =  $this->input->post('bg_position');
            $data['groups'] =  $this->input->post('groups') != '' ? serialize($this->input->post('groups')) : '';


            $this->megamenu_model->update_item($id, $data);
            $this->session->set_flashdata('message', lang('megamenu_item_saved'));
            redirect('admin/megamenu');

        }
    }

    /**
     * Delete item from navigation group
     *
     * @param string $id Item id
     *
     * @return void
     */
    public function delete($id)
    {
        if ( ! $this->megamenu_model->has_children($id))
        {
            $itemis = $this->megamenu_model->get_item($id);
            $upload_path = FCPATH.'assets'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'menuimage'.DIRECTORY_SEPARATOR;
            @unlink($upload_path.$itemis['bg_image']);
            $this->megamenu_model->delete_item($id);
        }

        $this->session->set_flashdata('message', lang('megamenu_item_deleted'));
        redirect('admin/megamenu');
    }

    /**
     * Reorder items in navigation group
     *
     * @return void
     */
    public function reorder()
    {
        if ($this->input->is_ajax_request())
        {
            $arr   = array();
            $order = $this->input->post('order');

            parse_str($order, $arr);
            $this->megamenu_model->reorder($arr['list']);
        }
        else
        {
            show_404();
        }
    }

    /**
     * Add new navigation group
     *
     * @return void
     */
    public function add_group()
    {

        $this->form_validation->set_error_delimiters('<div class="mt-2 alert alert-danger alert-dismissible fade show" role="alert"><a class="close" data-dismiss="alert" aria-label="Close" href="#">×</a>', '</div>');

        $this->form_validation->set_rules('gname', 'Name', 'required|trim');

        $groups = array();

        $this->data['groups'] = $groups;

        if ($this->form_validation->run() === FALSE)
        {

            $this->data['title'] = 'Add navigation group';

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('megamenu'), 'page' => 'Menu'), array('link' => '#', 'page' => 'Menu List'));
            $meta = array('page_title' => lang('pos_sales'), 'bc' => $bc);
            $this->page_construct('megamenu/group/add', $meta, $this->data);


        }
        else
        {


            $data['gname'] =  $this->input->post('gname');
            $data['bg_color'] =  $this->input->post('bg_color');
            $data['bg_repeat'] =  $this->input->post('bg_repeat');
            $data['bg_size'] =  $this->input->post('bg_size');
            $data['bg_position'] =  $this->input->post('bg_position');
            $data['width'] =  $this->input->post('width');
            $data['groups'] =  $this->input->post('groups') != '' ? serialize($this->input->post('groups')) : '';
            $data['font'] =  $this->input->post('font');
            $data['fontcolor'] =  $this->input->post('fontcolor');
            $data['fontsize'] =  $this->input->post('fontsize');
            $data['fontweight'] =  $this->input->post('fontweight');


            $gid = $this->megamenu_model->add_group($data);
            //$gid=5;


            if(!empty($_FILES['bg_image']['name'][0]))
            {

                $this->data['item'] = $this->megamenulib->get_group($gid);
                $this->load->library('upload');
                $upload = $this->_do_upload($this->data['item']['slug']);

                if($upload['status'] === FALSE)
                {
                    $this->session->set_flashdata('message', $upload['error_string']);
                    redirect('admin/megamenu/edit_group/'.$gid);

                } else {
                    $data['bg_image'] = $upload['file_name'];
                }

                $this->megamenu_model->update_group($gid, $data);
            }

            $this->session->set_flashdata('message', lang('megamenu_item_added'));
            redirect('admin/megamenu');
        }
    }

    /**
     * Edit navigation group
     *
     * @param string $id Item id
     *
     * @return void
     */
    public function edit_group($id)
    {
        $this->form_validation->set_error_delimiters('<div class="mt-2 alert alert-danger alert-dismissible fade show" role="alert"><a class="close" data-dismiss="alert" aria-label="Close" href="#">×</a>', '</div>');
        $this->form_validation->set_rules('gname', 'Name', 'required|trim');
        $this->data['item'] = $this->megamenulib->get_group($id);
        //$groups = $this->ion_auth->groups()->result_array();
        $groups = array();
        $this->data['groups'] = $groups;

        if ($this->form_validation->run() === FALSE)
        {
            if ($this->data['item'])
            {
                $this->data['title'] = 'Edit navigation group';

                $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('megamenu'), 'page' => 'Menu'), array('link' => '#', 'page' => 'Menu List'));
                $meta = array('page_title' => lang('pos_sales'), 'bc' => $bc);
                $this->page_construct('megamenu/group/edit',$meta, $this->data);
            }
            else
            {
                show_404();
            }
        }
        else
        {

            if($this->input->post('delete_image') == 'true'){
                @unlink(FCPATH.'assets'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'menuimage'.DIRECTORY_SEPARATOR.$this->data['item']['bg_image']);
                $data['bg_image'] = '';
            }

            if(!empty($_FILES['bg_image']['name'][0]))
            {

                $upload = $this->_do_upload($this->data['item']['slug']);

                if($upload['status'] === FALSE)
                {
                    $this->session->set_flashdata('message', $upload['error_string']);
                    redirect('admin/megamenu/edit_group/'.$id);

                } else {
                    $data['bg_image'] = $upload['file_name'];
                }


            }

            $data['gname'] =  $this->input->post('gname');
            $data['bg_color'] =  $this->input->post('bg_color');
            $data['bg_repeat'] =  $this->input->post('bg_repeat');
            $data['bg_size'] =  $this->input->post('bg_size');
            $data['bg_position'] =  $this->input->post('bg_position');
            $data['width'] =  $this->input->post('width');
            $data['groups'] =  $this->input->post('groups') != '' ? serialize($this->input->post('groups')) : '';
            $data['font'] =  $this->input->post('font');
            $data['fontcolor'] =  $this->input->post('fontcolor');
            $data['fontsize'] =  $this->input->post('fontsize');
            $data['fontweight'] =  $this->input->post('fontweight');



            $this->megamenu_model->update_group($id, $data);
            $this->session->set_flashdata('message', lang('megamenu_item_saved'));
            redirect('admin/megamenu');
        }
    }

    /**
     * Delete navigation group
     *
     * @param string $id Item id
     *
     * @return void
     */
    public function delete_group($id)
    {

        $groupis = $this->megamenulib->get_group($id);

        $upload_path = FCPATH.'assets'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'menuimage'.DIRECTORY_SEPARATOR;
        @unlink($upload_path.$groupis['bg_image']);

        $this->megamenu_model->delete_group($id);
        $this->session->set_flashdata('message', lang('megamenu_item_deleted'));
        redirect('admin/megamenu');
    }

    private function _do_upload($idis)
    {
        $filesCount = count($_FILES['bg_image']['name']);

        $_FILES['userFile']['name'] 	= $_FILES['bg_image']['name'];
        $_FILES['userFile']['type'] 	= $_FILES['bg_image']['type'];
        $_FILES['userFile']['tmp_name'] = $_FILES['bg_image']['tmp_name'];
        $_FILES['userFile']['error'] 	= $_FILES['bg_image']['error'];
        $_FILES['userFile']['size'] 	= $_FILES['bg_image']['size'];

        $config['upload_path']          = FCPATH.'assets'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'menuimage'.DIRECTORY_SEPARATOR;
        $config['file_ext_tolower']     = true;
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $ext = @split('\.',$_FILES['userFile']['name']);
        $config['max_size']             = 200000000; //set max size allowed in Kilobyte
        $config['max_width']            = 10000; // set max width image allowed
        $config['max_height']           = 10000; // set max height allowed
        $config['file_name']            = $idis.'.'.$ext[1]; //just milisecond timestamp fot unique name

        if(@is_file($config['upload_path'].$config['file_name'])){
            @unlink($config['upload_path'].$config['file_name']);
        }
        //echo $config['file_name'];

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if(!$this->upload->do_upload('userFile')) //upload and validate
        {
            $data['inputerror'] = 'photo';
            $data['error_string'] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
            $data['status'] = FALSE;
            return $data;
        } else {

            $fileData = $this->upload->data();
            $uploadData['file_name'] = $fileData['file_name'];
            $uploadData['status'] = TRUE;
        }

        return $uploadData;
    }


}
