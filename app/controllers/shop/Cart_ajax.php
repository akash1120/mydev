<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cart_ajax extends MY_Shop_Controller
{

    function __construct() {
        parent::__construct();
        if ($this->Settings->mmode) { redirect('notify/offline'); }
    }

    function index() {

        $this->session->set_userdata('requested_page', $this->uri->uri_string());
        if ($this->cart->total_items() < 1) {
            $this->session->set_flashdata('reminder', lang('cart_is_empty'));
            shop_redirect('products');
        }
        $customer_id=0;

        if($this->session->userdata('user_id')){
           $customer_data= $this->site->getCusomerID($this->session->userdata('email'));
            $customer_id = $customer_data->id;
        }


        $this->data['page_title'] = lang('shopping_cart');
        $this->data['customer_id'] =$customer_id;
        $this->page_construct('pages/cart', $this->data);
    }

    function checkout() {
        $this->session->set_userdata('requested_page', $this->uri->uri_string());
        if ($this->cart->total_items() < 1) {
            $this->session->set_flashdata('reminder', lang('cart_is_empty'));
            shop_redirect('products');
        }
        $html='';
        $html_footer='';
        $address_all_data = array();
        $change_new_country = 0;
        if(!empty($this->cart->contents())){
            $customer = $this->site->getCompanyByID($this->session->userdata('company_id'));
            $biller = $this->site->getCompanyByID($this->shop_settings->biller);
            $note = $this->db->escape_str($this->input->post('comment'));
            $product_tax = 0; $total = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            foreach ($this->cart->contents() as $item) {
                $item_option = NULL;
                if ($product_details = $this->site->getProductByID($item['product_id'])) {

                    $product_group_price_data= $this->site->getProductGroupPrice($item['product_id'],$this->Settings->country_price_group);
                    if( $product_group_price_data->price !='') {
                        $price = $product_group_price_data->price;
                    }else {

                    $price = $product_details->promotion ? $product_details->promo_price : $product_details->price;
                    }

                    if ($item['option']) {
                        if ($product_variant = $this->shop_model->getProductVariantByID($item['option'])) {
                            $item_option = $product_variant->id;

                            $product_group_price_variant_data= $this->site->getProductGroupPrice($item['product_id'],$this->Settings->country_price_group,$item_option);
                            if( $product_group_price_variant_data->price !='') {
                                $price = $product_group_price_variant_data->price;
                            }else {
                            $price = $product_variant->price+$price;
                        }


                            //$price = $product_variant->price+$price;
                        }
                    }

                    $item_net_price = $unit_price = $price;
                    $item_quantity = $item_unit_quantity = $item['qty'];
                    $pr_item_tax = $item_tax = 0;
                    $tax = "";
                    if ($this->Settings->country_vat == 1) {
                    if (!empty($product_details->tax_rate)) {

                        $tax_details = $this->site->getTaxRateByID($product_details->tax_rate);
                        $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_price);
                        $item_tax = $ctax['amount'];
                        $tax = $ctax['tax'];
                        if ($product_details->tax_method != 1) {
                            $item_net_price = $unit_price - $item_tax;
                        }
                        $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_unit_quantity), 4);
                        if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller->state == $customer->state), $tax_details)) {
                            $total_cgst += $gst_data['cgst'];
                            $total_sgst += $gst_data['sgst'];
                            $total_igst += $gst_data['igst'];
                        }
                    }
                    }

                    $product_tax += $pr_item_tax;
                    $subtotal = (($item_net_price * $item_unit_quantity) + $pr_item_tax);

                    $unit = $this->site->getUnitByID($product_details->unit);

                    $total += $this->sma->formatDecimal(($item_net_price * $item_unit_quantity), 4);

                }

                $html.='<tr class="cart_item">';
                $html.='<td class="product-name">'. $item['name'].' <strong class="product-quantity"> × '.$item['qty'].'</strong>';
                $html.='</td>';
                $html.='<td class="product-total">';
                $html.='<span class="amount">'. $item['subtotal'].'</span>';
                $html.='</td>';
                $html.='</tr>';
            }





            $shipping = $this->shop_settings->shipping;
            $order_tax = $this->site->calculateOrderTax($this->Settings->default_tax_rate2, ($total + $product_tax));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $shipping), 4);

            $html_footer.='<tr class="cart-subtotal">';
            $html_footer.='<th>Cart Subtotal</th>';
            $html_footer.=' <td><span class="amount">'.$grand_total.'</span></td>';
            $html_footer.='</tr>';
            if($this->session->userdata('gift_cart_amount')){
                $card_number = $this->session->userdata('gift_cart_no');
                $update_balance=0;
                $card_amonut = $this->session->userdata('gift_cart_amount');
                $new_grand_total = $grand_total - $card_amonut;
                if($new_grand_total < 0){
                    $update_balance =   $card_amonut - $grand_total;
                    $new_grand_total =0.00;
                    $card_amonut = $grand_total;
                }
                $grand_total = $new_grand_total;
                $html_footer.='<tr class="cart-subtotal">';
                $html_footer.='<th>Gift Card</th>';
                $html_footer.='<td><strong><span class="amount">'.$card_amonut.'</span></strong>';
                $html_footer.='</td>';
                $html_footer.='</tr>';
            }

            $html_footer.='<tr class="order-total">';
            $html_footer.='<th>Order Total</th>';
            $html_footer.='<td><strong><span class="amount">'.$grand_total.'</span></strong>';
            $html_footer.='</td>';
            $html_footer.='</tr>';
        }
        $address_id=0;

        if($this->input->get('adr')){

            $address_id = $this->input->get('adr');
            if($this->session->userdata('different_country_code') == 1){

                   $address_all_data = $this->shop_model->getAddress( $this->input->get('new_adr'));
                   $change_new_country = 1;
            }

        }

        $this->session->set_userdata('new_address_added',1);
        $this->session->set_userdata('different_country_code',0);

        $this->data['change_new_country'] = $change_new_country;
        $this->data['address_all_data'] = $address_all_data;
        $this->data['html_body'] = $html;
        $this->data['html_footer'] = $html_footer;
        $this->data['customer'] = $this->site->getAwardPointsOfCustomer($this->session->userdata['email']);
        $this->data['addresses'] = $this->loggedIn ? $this->shop_model->getAddresses() : FALSE;
        $this->data['address_id'] =$address_id;
        $this->data['page_title'] = lang('checkout');
        $this->page_construct('pages/checkout', $this->data);
    }

    function add($product_id) {
        if ($this->input->is_ajax_request() || $this->input->post('quantity')) {
            $product = $this->site->getProductByID($product_id);
            $product_img = $product->image;
            if($this->input->post('image_product') != ''){
                $product_img = $this->input->post('image_product');
            }
            /*  echo "<pre>";
              print_r($this->input->post('image_product'));
              exit('ascas');*/
            $pimage = $this->input->post('image_product');

            $options = $this->shop_model->getProductVariants($product_id);

            $price = $product->promotion ? $product->promo_price : $product->price;

            $product_group_price_data= $this->site->getProductGroupPrice($product->id,$this->Settings->country_price_group);
            if( $product_group_price_data->price !='') {
                $price = $product_group_price_data->price;
            }


            $option = FALSE;
            if (!empty($options)) {
                if ($this->input->post('radio')) {
                    $this->load->admin_model('products_model');
                    $option1 = implode(' / ',$this->input->post('radio'));

                    $op_id = $this->products_model->VariantSavedGetByNamepId($option1,$product_id);

                    foreach ($options as $op) {
                        if ($op['id'] == $op_id) {
                            $option = $op;
                        }
                    }
                } else {
                    $option = array_values($options)[0];
                }
                $product_group_price_variant_data= $this->site->getProductGroupPrice($product->id,$this->Settings->country_price_group,$op_id);
                if( $product_group_price_variant_data->price !='') {
                    $price = $product_group_price_variant_data->price;
                 } else {
                $price = $option['price']+$price;
            }
            }

            $selected = $option ? $option['id'] : FALSE;
            //$selected = $option ? $option : FALSE;
            $selected_name = $option ? $option['name'] : FALSE;

            if($this->Settings->country_vat == 1) {
            $tax_rate = $this->site->getTaxRateByID($product->tax_rate);
            $ctax = $this->site->calculateTax($product, $tax_rate, $price);
            $tax = $this->sma->formatDecimal($ctax['amount']);
            $price = $this->sma->formatDecimal($price);
            $unit_price = $this->sma->formatDecimal($product->tax_method ? $price+$tax : $price);
            }else{
                $unit_price = $price;
            }
            $id = $this->Settings->item_addition ? md5($product->id) : md5(microtime());

            //if promotion with percentage is activated then we need to change the price of products.
            $how_much_off_cart = $this->site->getPromotionPercentage('cartvaluerangeFree');
            if(!$how_much_off_cart) {
            $category_id = $this->site->getCategoryIdByProductID($product_id);
            $how_much_off_cat = $this->site->getPromotionPercentage('buyCategoryWithPercentage',$product_id, $category_id);
            $how_much_off_pro = $this->site->getPromotionPercentage('buyProductsWithPercentage',$product_id, $category_id);
            if($how_much_off_cat){
                $unit_price =  $this->site->calCulateProductPriceWithPromotionPercentagePos($unit_price, $how_much_off_cat);
            }else if($how_much_off_pro){
                $unit_price =  $this->site->calCulateProductPriceWithPromotionPercentagePos($unit_price, $how_much_off_pro);
            }
            }else{

            }

            $account_POS_settings = $this->site->getPosSettings();
            if($account_POS_settings->rounding != 0) {
                $unit_price  = $this->sma->roundNumber($unit_price, $account_POS_settings->rounding);
            }
            //buy x get y offer start
            $checkBuyOneGetOne = $this->site->checkBuyOneOnCategoryFree($product->id);
            //buy x get y offer end

            $data = array(
                'id'            => $id,
                'product_id'    => $product->id,
                'qty'           => ($this->input->get('qty') ? $this->input->get('qty') : ($this->input->post('quantity') ? $this->input->post('quantity') : 1)),
                'name'          => $product->name,
                'code'          => $product->code,
                'price'         => $unit_price,
                'tax'           => $tax,
                /*'image'         => $product->image,*/
                'image'         => $product_img,
                'option'        => $selected,
                'option_name'   => $selected_name,
                'options'       => !empty($options) ? $options : NULL,
                'offer_check'      => !empty($checkBuyOneGetOne) ? '1' : '0',
                'offer_type'      => !empty($checkBuyOneGetOne) ? $checkBuyOneGetOne[0]->type : '',
                'beforePromotionPrice'         => $unit_price,
                'beforePromotionTax'           => $tax,
            );

            if ($this->cart->insert($data)) {
                if($data['offer_type'] != ''){
                    $this->site->getXbuyYupdate( $checkBuyOneGetOne[0]->type,  $checkBuyOneGetOne[0]->category_id);
                }
                //Check item on promotion or not --same product
                $this->site->checkBuyOneGetOne($product->id);
                //Check item on promotion or not
                $this->site->checkBuyOneGetOtherOne($product->id);
                //Check item on category promotion or not
                $this->site->checkBuyOneOnCategoryFree($product->id);

                if ($this->input->post('quantity')) {
                    if($this->session->userdata('product_on_promotion_ping')) {
                        $show_alert = 'you can select another product which has same value or less for free (Products With the same Mark)';
                        $this->session->unset_userdata('product_on_promotion_ping');
                    }else if($this->session->userdata('product_on_promotion_get')){
                        $show_alert = 'Congrts.! You get free product.';
                        $this->session->unset_userdata('product_on_promotion_get');
                    }
                    if($show_alert){
                        $this->session->set_flashdata('message', $show_alert);
                    }else{
                        $this->session->set_flashdata('message', lang('item_added_to_cart'));
                    }
                    redirect($_SERVER['HTTP_REFERER']);
                } else {
                    $this->cart->cart_data();
                }
            }
            $this->session->set_flashdata('error', lang('unable_to_add_item_to_cart'));
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function add_ajax($product_id) {
        if ($this->input->is_ajax_request() || $this->input->post('quantity')) {
            $product = $this->site->getProductByID($product_id);
            $product_img = $product->image;
            if($this->input->post('image_product') != ''){
                $product_img = $this->input->post('image_product');
            }
            /*  echo "<pre>";
              print_r($this->input->post('image_product'));
              exit('ascas');*/
            $pimage = $this->input->post('image_product');

            $options = $this->shop_model->getProductVariants($product_id);

            $price = $product->promotion ? $product->promo_price : $product->price;

            $product_group_price_data= $this->site->getProductGroupPrice($product->id,$this->Settings->country_price_group);
            if( $product_group_price_data->price !='') {
                $price = $product_group_price_data->price;
            }


            $option = FALSE;
            if (!empty($options)) {
                if ($this->input->post('radio')) {
                    $this->load->admin_model('products_model');
                    $option1 = implode(' / ',$this->input->post('radio'));

                    $op_id = $this->products_model->VariantSavedGetByNamepId($option1,$product_id);

                    foreach ($options as $op) {
                        if ($op['id'] == $op_id) {
                            $option = $op;
                        }
                    }
                } else {
                    $option = array_values($options)[0];
                }
                $product_group_price_variant_data= $this->site->getProductGroupPrice($product->id,$this->Settings->country_price_group,$op_id);
                if( $product_group_price_variant_data->price !='') {
                    $price = $product_group_price_variant_data->price;
                } else {
                    $price = $option['price']+$price;
                }
            }

            $selected = $option ? $option['id'] : FALSE;
            //$selected = $option ? $option : FALSE;
            $selected_name = $option ? $option['name'] : FALSE;

            if($this->Settings->country_vat == 1) {
                $tax_rate = $this->site->getTaxRateByID($product->tax_rate);
                $ctax = $this->site->calculateTax($product, $tax_rate, $price);
                $tax = $this->sma->formatDecimal($ctax['amount']);
                $price = $this->sma->formatDecimal($price);
                $unit_price = $this->sma->formatDecimal($product->tax_method ? $price+$tax : $price);
            }else{
                $unit_price = $price;
            }
            $id = $this->Settings->item_addition ? md5($product->id) : md5(microtime());

            //if promotion with percentage is activated then we need to change the price of products.
            $how_much_off_cart = $this->site->getPromotionPercentage('cartvaluerangeFree');
            if(!$how_much_off_cart) {
            $category_id = $this->site->getCategoryIdByProductID($product_id);
            $how_much_off_cat = $this->site->getPromotionPercentage('buyCategoryWithPercentage',$product_id, $category_id);
            $how_much_off_pro = $this->site->getPromotionPercentage('buyProductsWithPercentage',$product_id, $category_id);
            if($how_much_off_cat){
                $unit_price =  $this->site->calCulateProductPriceWithPromotionPercentagePos($unit_price, $how_much_off_cat);
            }else if($how_much_off_pro){
                $unit_price =  $this->site->calCulateProductPriceWithPromotionPercentagePos($unit_price, $how_much_off_pro);
            }
            }

            $account_POS_settings = $this->site->getPosSettings();
            if($account_POS_settings->rounding != 0) {
                $unit_price  = $this->sma->roundNumber($unit_price, $account_POS_settings->rounding);
            }
            //buy x get y offer start
            $checkBuyOneGetOne = $this->site->checkBuyOneOnCategoryFree($product->id);
            //buy x get y offer end

            $data = array(
                'id'            => $id,
                'product_id'    => $product->id,
                'qty'           => ($this->input->get('qty') ? $this->input->get('qty') : ($this->input->post('quantity') ? $this->input->post('quantity') : 1)),
                'name'          => $product->name,
                'code'          => $product->code,
                'price'         => $unit_price,
                'tax'           => $tax,
                /*'image'         => $product->image,*/
                'image'         => $product_img,
                'option'        => $selected,
                'option_name'   => $selected_name,
                'options'       => !empty($options) ? $options : NULL,
                'offer_check'      => !empty($checkBuyOneGetOne) ? '1' : '0',
                'offer_type'      => !empty($checkBuyOneGetOne) ? $checkBuyOneGetOne[0]->type : '',
                'beforePromotionPrice'         => $unit_price,
                'beforePromotionTax'           => $tax,
                // 'offer_type' => $checkBuyOneGetOne->type
            );

            if ($this->cart->insert($data)) {
                if($data['offer_type'] != ''){
                    $this->site->getXbuyYupdate( $checkBuyOneGetOne[0]->type,  $checkBuyOneGetOne[0]->category_id);
                }
                //Check item on promotion or not --same product
                $this->site->checkBuyOneGetOne($product->id);
                //Check item on promotion or not
                $this->site->checkBuyOneGetOtherOne($product->id);
                //Check item on category promotion or not
                $this->site->checkBuyOneOnCategoryFree($product->id);



                if ($this->input->post('quantity')) {
                    if($this->session->userdata('product_on_promotion_ping')) {
                        $show_alert = 'you can select another product which has same value or less for free (Products With the same Mark)';
                        $this->session->unset_userdata('product_on_promotion_ping');
                    }else if($this->session->userdata('product_on_promotion_get')){
                        $show_alert = 'Congrts.! You get free product.';
                        $this->session->unset_userdata('product_on_promotion_get');
                    }
                    if($show_alert){
                        $this->session->set_flashdata('message', $show_alert);
                    }else{
                       // $this->session->set_flashdata('message', lang('item_added_to_cart'));
                    }
                    $this->cart->cart_data();
                    exit();
                } else {
                    $this->cart->cart_data();
                    exit();
                }
            }
            //$this->session->set_flashdata('error', lang('unable_to_add_item_to_cart'));
            echo 0;
            exit();
        }
    }

    public function checkProductExistInWarehouse($product_id, $warehouse_id){
        foreach($warehouse_id as $item){
            $this_product_exist_wh[] = $this->site->getWarehouseProduct($item , $product_id);
        }
        if(array_filter($this_product_exist_wh)){
            return true;
        }else{
            return false;
        }
    }


    public function all_item_count(){
        $total = 0;
        if ($this->cart->total_items() > 0){
            foreach ($this->cart->contents() AS $item){
                $total = $item['qty'] + $total;
            }
        }
        return $total;
    }


    function specificProductInCart($product_id){
        $is_on_adding_record = $this->shop_model->getUpcomingPromotions('onAddingProduct', $product_id);

        if(!empty($is_on_adding_record)) {
            foreach ($is_on_adding_record as $row) {
                $item_on_base = $row->item_id;
                $triggerOnItem = $row->triggerOnItem;
                break;
            }
            if($item_on_base == $product_id && $triggerOnItem == 1){
                $get_upselling_product = $this->shop_model->getItemBasePromotion('onAddingProduct', $product_id);
                $img_path = $this->shop_model->getProductImagePath($get_upselling_product[0]->item_id);
                $product_real_price = $this->sma->convertMoney($img_path[0]->price);
                //print_r($get_upselling_product); die;

                $data = array(
                    "id" => $row->id,
                    "popup_product_id" => $get_upselling_product[0]->item_id,
                    "name"  => $row->name,
                    "title" => $row->title,
                    "description" => $row->description,
                    "type" => $row->type,
                    "isMoreThanOne" => $row->isMoreThanOne,
                    "disqualifiedRemoval" => $row->disqualifiedRemoval,
                    "isRedemptionCheck"   => $row->isRedemptionCheck,
                    "redemptionLimit"     => $row->redemptionLimit,
                    "isCartTotal"       => $row->isCartTotal,
                    "minAmount"       => $row->minAmount,
                    "maxAmount"       => $row->maxAmount,
                    "isDateRange"       => $row->isDateRange,
                    "startDate"       => $row->startDate,
                    "endDate"       => $row->endDate,
                    "isTimeRange"       => $row->isTimeRange,
                    "startTime"       => $row->startTime,
                    "endTime"       => $row->endTime,
                    "triggerOnItem"       => $row->triggerOnItem,
                    "itemQuantity"       => $row->itemQuantity,
                    "status" => $row->status,
                    "inDaysOnly" => $row->inDaysOnly,
                    "days" => $row->days,
                    "upselling_id" => $row->upselling_id,
                    "product_image_path" => $img_path[0]->image,
                    "product_real_price" => 'AED '.$product_real_price,
                );

                $data[] = $data;
                echo json_encode($data);
                exit();
            }
        }


    }

    function update($data = NULL) {

        if (is_array($data)) {
            return $this->cart->update($data);
        }
        if ($this->input->is_ajax_request()) {

            $option_id = null;
            $this->load->admin_model('products_model');

            if ($rowid = $this->input->post('rowid', TRUE)) {
                $item = $this->cart->get_item($rowid);
                $op_id = $this->products_model->VariantSavedGetByNamepId($item['option_name'],$item['product_id']);

                $product = $this->site->getProductByID($item['product_id']);
                $options = $this->shop_model->getProductVariants($product->id);
                // $option = $options ? array_values($options)[0] : FALSE;
                $price = $product->promotion ? $product->promo_price : $product->price;
                // $price = $options ? ($option['price']+$price) : $price;
                // $selected = $option ? $option['id'] : FALSE;

                if ($option = $this->input->post('option')) {
                    foreach($options as $op) {
                        if ($op['id'] == $option) {
                            $price = $price + $op['price'];
                        }
                    }
                }

                if (!empty($options)) {
                    foreach ($options as $op) {
                        if ($op['id'] == $op_id) {
                            $option = $op;
                        }
                    }
                    $product_group_price_variant_data= $this->site->getProductGroupPrice($product->id,$this->Settings->country_price_group,$op_id);
                    if( $product_group_price_variant_data->price !='') {
                        $price = $product_group_price_variant_data->price;
                    }else{
                    $price = $option['price']+$price;
                }
                   // $price = $option['price']+$price;
                }
                if($this->Settings->country_vat == 1) {
                $tax_rate = $this->site->getTaxRateByID($product->tax_rate);
                $ctax = $this->site->calculateTax($product, $tax_rate, $price);
                $tax = $this->sma->formatDecimal($ctax['amount']);
                $price = $this->sma->formatDecimal($price);
                $unit_price = $this->sma->formatDecimal($product->tax_method ? $price+$tax : $price);
                }else{
                    $unit_price = $price;
                }

                // $id = $this->Settings->item_addition ? md5($product->id) : md5(microtime());
                $product_id= $item['product_id'];
                //if promotion with percentage is activated then we need to change the price of products.
                $how_much_off_cart = $this->site->getPromotionPercentage('cartvaluerangeFree');
                if(!$how_much_off_cart) {
                $category_id = $this->site->getCategoryIdByProductID($product_id);
                $how_much_off_cat = $this->site->getPromotionPercentage('buyCategoryWithPercentage',$product_id, $category_id);
                $how_much_off_pro = $this->site->getPromotionPercentage('buyProductsWithPercentage',$product_id, $category_id);
                if($how_much_off_cat){
                    $unit_price =  $this->site->calCulateProductPriceWithPromotionPercentagePos($unit_price, $how_much_off_cat);
                }else if($how_much_off_pro){
                    $unit_price =  $this->site->calCulateProductPriceWithPromotionPercentagePos($unit_price, $how_much_off_pro);
                }
                }

                $account_POS_settings = $this->site->getPosSettings();
                if($account_POS_settings->rounding != 0) {
                    $unit_price  = $this->sma->roundNumber($unit_price, $account_POS_settings->rounding);
                }
               /* echo $unit_price;
                exit('sd');*/
                // $options = $this->cart->product_options($rowid);
                // $price = $item['base_price'];
                if($this->input->post('option')){
                    $option_id = $this->input->post('option');
                }else{
                    $option_id = $op_id;

                }
                //buy x get y offer start
                $checkBuyOneGetOne = $this->site->checkBuyOneOnCategoryFree($product->id);
                //buy x get y offer end
                $data = array(
                    'rowid' => $rowid,
                    'product_id' => $product->id,
                    'price' => $unit_price,
                    'tax' => $tax,
                    'qty' => $this->input->post('qty', TRUE),
                    'option' => $option_id? $option_id : FALSE,
                    'options' => $this->cart->product_options($rowid),
                    'offer_check'      => !empty($checkBuyOneGetOne) ? '1' : '0',
                    'offer_type'      => !empty($checkBuyOneGetOne) ? $checkBuyOneGetOne[0]->type : '',
                    'beforePromotionPrice'         => $unit_price,
                    'beforePromotionTax'           => $tax,
                );
                if ($this->cart->update($data)) {
                    if($data['offer_type'] != ''){
                        $this->site->getXbuyYupdate( $checkBuyOneGetOne[0]->type,  $checkBuyOneGetOne[0]->category_id);
                    }
                    $this->sma->send_json(array('cart' => $this->cart->cart_data(true), 'status' => lang('success'), 'message' => lang('cart_updated')));
                }
            }
        }
    }
    function update_shipment($data = NULL) {

        if (is_array($data)) {
            return $this->cart->update($data);
        }
        if ($this->input->is_ajax_request()) {

            $this->shop_settings->shipping = $this->input->post('ship');
            $this->session->set_userdata('shipping_amount',$this->input->post('ship'));
            $this->sma->send_json(array('cart' => $this->cart->cart_data(true), 'status' => lang('success'), 'message' => lang('cart_updated')));
        }
    }

    function remove($rowid = NULL) {
        if ($rowid) {
            return $this->cart->remove($rowid);
        }
        if ($this->input->is_ajax_request()) {
            if ($rowid = $this->input->post('rowid', TRUE)) {

                    $item = $this->cart->get_item($rowid);

                //buy x get y offer start
                $checkBuyOneGetOne = $this->site->checkBuyOneOnCategoryFree($item['product_id']);

                /*  //buy x get y offer end


                    $is_promotion = $this->site->promotionAddToCart($item['product_id']);
                    if(!empty($is_promotion)){
                        foreach ($is_promotion as $row) {
                            $data_how_buy = $row->how_many_buy;
                            $data_how_free = $row->how_many_free;
                            $how_many_this_product_incart = $this->site->specific_item_count($item['product_id']);

                            if($how_many_this_product_incart - 2 < $data_how_buy){
                                $this->site->specific_promotional_item_delete($item['product_id']);
                            }
                        }
                      }*/
                if ($this->cart->remove($rowid)) {
                    if(!empty($checkBuyOneGetOne)){
                        $this->site->getXbuyYupdate( $checkBuyOneGetOne[0]->type,  $checkBuyOneGetOne[0]->category_id);
                    }
                    $this->sma->send_json(array('cart' => $this->cart->cart_data(true), 'status' => lang('success'), 'message' => lang('cart_item_deleted')));

                }
            }
        }
    }

    function destroy() {
        if ($this->input->is_ajax_request()) {
            if ($this->cart->destroy()) {
                $this->session->set_flashdata('message', lang('cart_items_deleted'));
                $this->sma->send_json(array('redirect' => base_url()));
            } else {
                $this->sma->send_json(array('status' => lang('error'), 'message' => lang('error_occured')));
            }
        }
    }

    function add_wishlist($product_id) {
        $this->session->set_userdata('requested_page', $_SERVER['HTTP_REFERER']);
        if (!$this->loggedIn) { $this->sma->send_json(array('redirect' => site_url('login'))); }
        if ($this->shop_model->getWishlist(TRUE) >= 10) {
            $this->sma->send_json(array('status' => lang('warning'), 'message' => lang('max_wishlist'), 'level' => 'warning'));
        }
        if ($this->shop_model->addWishlist($product_id)) {
            $total = $this->shop_model->getWishlist(TRUE);
            $this->sma->send_json(array('status' => lang('success'), 'message' => lang('added_wishlist'), 'total' => $total));
        } else {
            $this->sma->send_json(array('status' => lang('info'), 'message' => lang('product_exists_in_wishlist'), 'level' => 'info'));
        }
    }

    function remove_wishlist($product_id) {
        $this->session->set_userdata('requested_page', $_SERVER['HTTP_REFERER']);
        if (!$this->loggedIn) { $this->sma->send_json(array('redirect' => site_url('login'))); }
        if ($this->shop_model->removeWishlist($product_id)) {
            $total = $this->shop_model->getWishlist(TRUE);
            $this->sma->send_json(array('status' => lang('success'), 'message' => lang('removed_wishlist'), 'total' => $total));
        } else {
            $this->sma->send_json(array('status' => lang('error'), 'message' => lang('error_occured'), 'level' => 'error'));
        }
    }

}