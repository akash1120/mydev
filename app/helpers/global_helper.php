<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Method: rights
 * Params: $right_id
 * Return: True / False
 */
/**
 * Method: getColumns
 * Params: $table
 * Return: Fields of table
 */
if (!function_exists('getColumns')) {

    function getZones($id)
    {
        $result = "";
        $ci = &get_instance();
        $table = $ci->db->dbprefix . 'zone';
        $sql = "SELECT * FROM " . $table.' WHERE country_id='. $id;
        $query = $ci->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $result [] = $row;
            }
        }
        return $result;

    }

}



/**
 * Method: getValArray
 * Params: $cols, $table, $where, $criteria
 * Return: array
 */
function getValArray($cols, $table, $where = '', $criteria = '')
{
    $ci = &get_instance();
    $ci->db->select($cols);
    if ($where != '') {
        $ci->db->where($where, $criteria);
    }
    $ci->db->from($table);
    $ci->db->order_by('id', "desc");
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {

        return $results->result_array();
    }
}

function getValsArray($cols, $table, $where = '', $criteria = '')
{
    $ci = &get_instance();
    $ci->db->select($cols);
    if ($where != '') {
        $ci->db->where($where);
    }
    $ci->db->from($table);
    $ci->db->order_by('id', "desc");
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {

        /*  echo "<pre>";
          print_r($results->result_array());
          exit('asds');*/

        return $results->result_array();
    }
}

function getCheckinOut($where = '')
{
    $ci = &get_instance();
    $ci->db->select('requests.access_number,security_history.*')
        ->from('requests')
        ->join('security_history', 'requests.id = security_history.request_id');
    if ($where != '') {
        $ci->db->where($where);
    }
    $ci->db->order_by('security_history.id',"desc");
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    }
}

function getManagersRequests($where = '')
{
    $ci = &get_instance();
    $ci->db->distinct();
    $ci->db->select('preapprovers_managers.manager_id,requests.*')
        ->from('preapprovers_managers')
        ->join('requests', 'requests.id = preapprovers_managers.request_id');
    if ($where != '') {
        $ci->db->where('preapprovers_managers.manager_id', $where);
    }
    $ci->db->order_by('requests.id', "desc");
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    }
}

function getManagersRequests_Approved($where = '')
{
    $ci = &get_instance();
    $ci->db->distinct();
    $ci->db->select('preapprovers_managers.manager_id,requests.*')
        ->from('preapprovers_managers')
        ->join('requests', 'requests.id = preapprovers_managers.request_id');
    if ($where != '') {
        $ci->db->where('preapprovers_managers.manager_id', $where);
        $ci->db->where('requests.team_signature', 'Approved');
    }
    $ci->db->order_by('requests.id', "desc");
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    }
}

function getContactsRequests_Approved($where = '')
{
    $ci = &get_instance();
    $ci->db->distinct();
    $ci->db->select('preapprovers_contacts.contact_id,requests.*')
        ->from('preapprovers_contacts')
        ->join('requests', 'requests.id = preapprovers_contacts.request_id');
    if ($where != '') {
        $ci->db->where('preapprovers_contacts.contact_id', $where);
        $ci->db->where('requests.team_signature', 'Approved');
    }
    $ci->db->order_by('requests.id', "desc");
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    }
}
function getManagersRequests_pending($where = '')
{
    $ci = &get_instance();
    $ci->db->distinct();
    $ci->db->select('preapprovers_managers.manager_id,requests.*')
        ->from('preapprovers_managers')
        ->join('requests', 'requests.id = preapprovers_managers.request_id');
    if ($where != '') {
        $ci->db->where('preapprovers_managers.manager_id', $where);
        $ci->db->where('requests.team_signature IS NULL', null, false);
    }
    $ci->db->order_by('requests.id', "desc");
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    }
}

function getManagersRequests_reject($where = '')
{
    $ci = &get_instance();
    $ci->db->distinct();
    $ci->db->select('preapprovers_managers.manager_id,requests.*')
        ->from('preapprovers_managers')
        ->join('requests', 'requests.id = preapprovers_managers.request_id');
    if ($where != '') {
        $ci->db->where('preapprovers_managers.manager_id', $where);
        $ci->db->where('requests.team_signature', 'Reject');
    }
    $ci->db->order_by('requests.id', "desc");
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    }
}


function getPreapproverRequests($where = '')
{
    $ci = &get_instance();
    $ci->db->distinct();
    $ci->db->select('preapprovers_requests.*,users.name')
        ->from('preapprovers_requests')
        ->join('users', 'users.id = preapprovers_requests.created_by');
    if ($where != '') {
        $ci->db->where($where);
    }
    //$ci->db->order_by('preapprovers_requests.id',"desc");
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    }
}


function getContactRequests($where = '')
{
    $ci = &get_instance();
    $ci->db->distinct();
    $ci->db->select('preapprovers_contacts.contact_id,requests.*')
        ->from('preapprovers_contacts')
        ->join('requests', 'requests.id = preapprovers_contacts.request_id');
    if ($where != '') {
        $ci->db->where('preapprovers_contacts.contact_id', $where);
    }
    $ci->db->order_by('requests.id', "desc");
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    }
}

function getContactRequests_pending($where = '')
{
    $ci = &get_instance();
    $ci->db->distinct();
    $ci->db->select('preapprovers_contacts.contact_id,requests.*')
        ->from('preapprovers_contacts')
        ->join('requests', 'requests.id = preapprovers_contacts.request_id');
    if ($where != '') {
        $ci->db->where('preapprovers_contacts.contact_id', $where);
        $ci->db->where('requests.team_signature IS NULL', null, false);
    }
    $ci->db->order_by('requests.id', "desc");
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    }
}

function getContactRequests_reject($where = '')
{
    $ci = &get_instance();
    $ci->db->distinct();
    $ci->db->select('preapprovers_contacts.contact_id,requests.*')
        ->from('preapprovers_contacts')
        ->join('requests', 'requests.id = preapprovers_contacts.request_id');
    if ($where != '') {
        $ci->db->where('preapprovers_contacts.contact_id', $where);
        $ci->db->where('requests.team_signature', 'Reject');
    }
    $ci->db->order_by('requests.id', "desc");
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    }
}

function getManagerWoerkpermits($where = '')
{
    $ci = &get_instance();
    $ci->db->distinct();
    $ci->db->select('preapprovers_managers.manager_id,work_permits.*')
        ->from('preapprovers_managers')
        ->join('work_permits', 'work_permits.request_id = preapprovers_managers.request_id');
    if ($where != '') {
        $ci->db->where('preapprovers_managers.manager_id', $where);
    }
    $ci->db->order_by('work_permits.id', "desc");
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    }
}

function getManagerMaterialIn($where = '')
{
    $ci = &get_instance();
    $ci->db->distinct();
    $ci->db->select('preapprovers_managers.manager_id,material_info.*')
        ->from('preapprovers_managers')
        ->join('material_info', 'material_info.request_id = preapprovers_managers.request_id');
    if ($where != '') {
        $ci->db->where('preapprovers_managers.manager_id', $where);
        $ci->db->where('material_info.type', 'in');
    }
    $ci->db->order_by('material_info.id', "desc");
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    }
}

function getManagerMaterialOut($where = '')
{
    $ci = &get_instance();
    $ci->db->distinct();
    $ci->db->select('preapprovers_managers.manager_id,material_info.*')
        ->from('preapprovers_managers')
        ->join('material_info', 'material_info.request_id = preapprovers_managers.request_id');
    if ($where != '') {
        $ci->db->where('preapprovers_managers.manager_id', $where);
        $ci->db->where('material_info.type', 'out');
    }
    $ci->db->order_by('material_info.id', "desc");
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    }
}


function getContactWoerkpermits($where = '')
{
    $ci = &get_instance();
    $ci->db->distinct();
    $ci->db->select('preapprovers_contacts.contact_id,work_permits.*')
        ->from('preapprovers_contacts')
        ->join('work_permits', 'work_permits.request_id = preapprovers_contacts.request_id');
    if ($where != '') {
        $ci->db->where('preapprovers_contacts.contact_id', $where);
    }
    $ci->db->order_by('work_permits.id', "desc");
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    }
}

function getContactMaterialIn($where = '')
{
    $ci = &get_instance();
    $ci->db->distinct();
    $ci->db->select('preapprovers_contacts.contact_id,material_info.*')
        ->from('preapprovers_contacts')
        ->join('material_info', 'material_info.request_id = preapprovers_contacts.request_id');
    if ($where != '') {
        $ci->db->where('preapprovers_contacts.contact_id', $where);
        $ci->db->where('material_info.type', 'in');
    }
    $ci->db->order_by('material_info.id', "desc");
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    }
}

function getContactMaterialOut($where = '')
{
    $ci = &get_instance();
    $ci->db->distinct();
    $ci->db->select('preapprovers_contacts.contact_id,material_info.*')
        ->from('preapprovers_contacts')
        ->join('material_info', 'material_info.request_id = preapprovers_contacts.request_id');
    if ($where != '') {
        $ci->db->where('preapprovers_contacts.contact_id', $where);
        $ci->db->where('material_info.type', 'out');
    }
    $ci->db->order_by('material_info.id', "desc");
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    }
}


function getValObject($cols, $table, $where = '', $criteria = '')
{
    $ci = &get_instance();
    $ci->db->select($cols);
    if ($where != '') {
        $ci->db->where($where, $criteria);
    }
    $ci->db->from($table);
    $ci->db->order_by('id', "desc");
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result();
    }
}

/**
 * Mehtod: init_admin_pagination
 * params: $uri, $total_records,$perpage
 * return: pagination configuration
 */
if (!function_exists('init_admin_pagination')) {

    function init_admin_pagination($uri, $total_records, $perpage, $id = '')
    {
        $ci = &get_instance();
        $config ["base_url"] = base_url() . $uri;
        $prev_link = '&laquo;';
        $next_link = '&raquo;';
        $config ["total_rows"] = $total_records;
        $config ["per_page"] = $perpage;
        if ($id)
            $config ['uri_segment'] = '5';
        else
            $config ['uri_segment'] = '4';
        $config ['first_link'] = 'First';
        $config ['last_link'] = 'Last';
        $config ['num_links'] = '5';
        $config ['prev_link'] = $prev_link;
        $config ['next_link'] = $next_link;
        $config ['num_tag_open'] = '<li>';
        $config ['num_tag_close'] = '</li>';
        $config ['cur_tag_open'] = '<li class="active"><a>';
        $config ['cur_tag_close'] = '</a></li>';
        $config ['prev_tag_open'] = '<li>';
        $config ['prev_tag_close'] = '</li>';
        $config ['next_tag_open'] = '<li>';
        $config ['next_tag_close'] = '</li>';
        $config ['page_query_string'] = FALSE;
        $ci->pagination->initialize($config);
        return $config;
    }

}
/**
 * Mehtod: init_front_pagination
 * params: $uri, $total_records,$perpage
 * return: pagination configuration
 */
if (!function_exists('init_front_pagination')) {

    function init_front_pagination($url, $total_records, $perpage)
    {

        $ci = &get_instance();
        $config ["base_url"] = base_url() . $url;
        $prev_link = '&lsaquo;';
        $next_link = '&rsaquo;';
        $config ["total_rows"] = $total_records;
        $config ["per_page"] = $perpage;
        $config ['uri_segment'] = '4';
        $config ['first_link'] = 'First &laquo;';
        $config ['last_link'] = '&raquo; Last';
        $config ['first_tag_open'] = '<li>';
        $config ['first_tag_close'] = '</li>';
        $config ['last_tag_open'] = '<li>';
        $config ['last_tag_close'] = '</li>';
        $config ['num_links'] = '5';
        $config ['prev_link'] = $prev_link;
        $config ['next_link'] = $next_link;
        $config ['num_tag_open'] = '<li>';
        $config ['num_tag_close'] = '</li>';
        $config ['cur_tag_open'] = '<li class="active"><a>';
        $config ['cur_tag_close'] = '</a></li>';
        $config ['prev_tag_open'] = '<li>';
        $config ['prev_tag_close'] = '</li>';
        $config ['next_tag_open'] = '<li>';
        $config ['next_tag_close'] = '</li>';
        $config ['page_query_string'] = false;
        $ci->pagination->initialize($config);


        return $config;
    }

}

function countTableRecords($table, $where = '')
{
    $ci = &get_instance();
    if ($where != '') {
        $ci->db->where($where);
    }
    $ci->db->from($table);
    $query = $ci->db->get();
    return $query->num_rows();
}

function getRowArrays($cols, $table, $where = '')
{
    $ci = &get_instance();
    $ci->db->select($cols);
    if ($where != '') {
        $ci->db->where($where);
    }
    $ci->db->from($table);
    $ci->db->order_by('id', 'desc');
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    }
}

function getRowArray($cols, $table, $where = '')
{
    $ci = &get_instance();
    $ci->db->select($cols);
    if ($where != '') {
        $ci->db->where($where);
    }
    $ci->db->from($table);
    $ci->db->limit(1);
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->row_array();
    }
}

function getlatestRowArray($cols, $table, $where = '')
{
    $ci = &get_instance();
    $ci->db->select($cols);
    if ($where != '') {
        $ci->db->where($where);
    }
    $ci->db->from($table);
    $ci->db->order_by('id', 'desc');
    $ci->db->limit(1);
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->row_array();
    }
}

function getRowObject($cols, $table, $where = '')
{
    $ci = &get_instance();
    $ci->db->select($cols);
    if ($where != '') {
        $ci->db->where($where);
    }
    $ci->db->from($table);
    $ci->db->limit(1);
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->row();
    }
}

function getTableRecordsOrderLimit($cols, $table, $where = '', $order = '', $limit = '', $start = 0)
{
    $ci = &get_instance();
    $ci->db->select($cols);
    if ($where != '') {
        $ci->db->where($where);
    }
    $ci->db->from($table);
    if ($order != '') {
        $ci->db->order_by($order);
    }
    if ($limit != '') {
        $ci->db->limit($limit, $start);
    }
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    } else {
        return array();
    }
}

function getTableRecordsOrder($cols, $table, $where = '', $order = '')
{
    $ci = &get_instance();
    $ci->db->select($cols);
    if ($where != '') {
        $ci->db->where($where);
    }
    $ci->db->from($table);
    if ($order != '') {
        $ci->db->order_by($order);
    }
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    } else {
        return array();
    }
}

function getTableRecordsLimit($cols, $table, $where = '', $limit = '')
{
    $ci = &get_instance();
    $ci->db->select($cols);
    if ($where != '') {
        $ci->db->where($where);
    }
    $ci->db->from($table);
    if ($limit != '') {
        $ci->db->limit($limit);
    }
    $results = $ci->db->get();
    if ($results->num_rows() > 0) {
        return $results->result_array();
    } else {
        return array();
    }
}

function insert($table, $data)
{
    $ci = &get_instance();
    $ci->db->insert($table, $data);
    return $ci->db->insert_id();
}

function update($table, $data, $where)
{
    $ci = &get_instance();
    $ci->db->where($where);
    return $ci->db->update($table, $data);
}

function delete($table, $where)
{
    $ci = &get_instance();
    $ci->db->where($where);
    return $ci->db->delete($table);
}

/**
 * Get get_all_countries.
 * @access    private
 * @return array
 */
function get_all_countries()
{
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('countries');
    $q = $CI->db->get();
    return $q->result_array();
}

function get_all_jobs_status()
{
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('job_status');
    $q = $CI->db->get();
    return $q->result_array();
}

function get_roles()
{
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('admin_roles');
    $CI->db->where("id !=", '0');
    $q = $CI->db->get();
    return $q->result_array();
}

function get_video_categories()
{
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('video_categories');
    $q = $CI->db->get();
    return $q->result_array();
}

function get_all_clinics($doctorId)
{
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('clinic');
    $CI->db->where('doctor_id', $doctorId);
    $q = $CI->db->get();
    return $q->result_array();
}

function get_all_widgets($doctorId)
{
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('widgets');
    $CI->db->where('doctor_id', $doctorId);
    $q = $CI->db->get();
    return $q->result_array();
}

function get_all_staff($doctorId)
{
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('users');
    $CI->db->where('staff_parent', $doctorId);
    $q = $CI->db->get();
    return $q->result_array();
}

/**
 * Get get_company_types.
 * @access    private
 * @return array
 */
function get_company_types()
{
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('c_users_company_types');
    $CI->db->where('status', 1);
    $q = $CI->db->get();
    return $q->result_array();
}

/* check view port */
if (!function_exists('checkIsTablet')) {

    function checkIsTablet()
    {
        $ci = &get_instance();
        $ci->load->library('Mobile_Detect');
        $detect = new Mobile_Detect();
        if ($detect->isTablet()) {
            return true;
        } else {
            return false;
        }
    }

}

/**
 * Method: getChildren
 * params: $table, $ids
 * Returns: $ids
 */
if (!function_exists('getChilder')) {

    function getChildren($table, $ids)
    {
        $ci = &get_instance();
        $ids = (array)$ids;
        $catid = array_unique($ids);
        sort($ids);
        $array = $ids;
        $implodeArray = implode(',', $array);
        $arrayNew = array();
        for ($i = 0; $i <= count($array); $i++) {
            $query = "SELECT category_id FROM " . $ci->db->dbprefix($table) . " WHERE status=1 AND parent_id IN (" . $implodeArray . ") AND category_id NOT IN (" . $implodeArray . ") ";
            $query = $ci->db->query($query);
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $row) {
                    $arrayNew [] = $row->category_id;
                }
            }
            $ids = array_merge($ids, $arrayNew);
        }
        $ids = array_unique($ids);
        return $ids;
    }

}
if (!function_exists('dump_var')) {

    function dump_var($ar)
    {
        echo '<pre>';
        var_dump($ar);
        echo '</pre>';
        die();
    }

}

/**
 * Method: show_custom_options
 * @param $type , $description,$description_ar
 * @return string
 */
function show_custom_options($type, $description, $description_ar)
{
    $output = '';
    if ($type == 'selectlist') {
        $total_options_arr = explode(',', $description);
        if (is_array($total_options_arr)) {
            $i = 1;
            foreach ($total_options_arr as $k => $v) {
                if ($v != "") {
                    $option_values_arr = explode(':', $v);
                    $option_label = replace_in_view($option_values_arr[0]);
                    $option_default = $option_values_arr[1];
                    $option_value = replace_in_view(strtolower($option_label));

                    if ($option_default == 1) {
                        $yes = 'checked="checked"';
                        $no = '';
                    } else {
                        $no = 'checked="checked"';
                        $yes = '';
                    }
                    $output .= '<ol id="ol_' . $i . '" class="media-list"> <li id="h_' . $i . '"><strong>Option ' . $i . '</strong></li><li><label for="opt_label_' . $i . '">Option Label</label>&nbsp;<input type="text" name="option_label_' . $i . '" id="option_label_' . $i . '" value="' . $option_value . '" />&nbsp;&nbsp;<button type="button" class="btn btn-danger btn-xs" onclick="delete_cf_options(this.id)" id="' . $i . '"><i class="ti-close"></i> Delete</button></li><li><label for="opt_default_' . $i . '">Default</label>&nbsp;&nbsp;<input type="radio" name="option_default_' . $i . '"  value="1" id="yes_option_default_' . $i . '" ' . $yes . '/>&nbsp;Yes&nbsp;&nbsp;<input type="radio" name="option_default_' . $i . '"  value="0" ' . $no . ' id="no_option_default_' . $i . '" />&nbsp;No</li></ol>';
                }
                $i++;
                //}
            }
        }
    } else {
        $total_options_arr = explode(',', $description);
        if (is_array($total_options_arr)) {
            $i = 1;
            foreach ($total_options_arr as $k => $v) {

                if ($v != "") {
                    $option_values_arr = explode(':', $v);
                    $option_label = replace_in_view($option_values_arr[0]);
                    $option_default = $option_values_arr[1];
                    $option_value = replace_in_view(strtolower($option_label));

                    if ($option_default == 1) {
                        $yes = 'checked="checked"';
                        $no = '';
                    } else {
                        $no = 'checked="checked"';
                        $yes = '';
                    }
                    $output .= '<ol id="ol_' . $i . '" class="media-list"> <li id="h_' . $i . '"><strong>Option ' . $i . '</strong></li><li><label for="opt_label_' . $i . '">Option Label</label>&nbsp;<input type="text" name="option_label_' . $i . '" id="option_label_' . $i . '" value="' . $option_value . '" />&nbsp;&nbsp;<button type="button" class="btn btn-danger btn-xs" onclick="delete_cf_options(this.id)" id="' . $i . '"><i class="ti-close"></i> Delete</button></li><li><label for="opt_default_' . $i . '">Default</label>&nbsp;&nbsp;<input type="radio" name="option_default_' . $i . '"  value="1" id="yes_option_default_' . $i . '" ' . $yes . '/>&nbsp;Yes&nbsp;&nbsp;<input type="radio" name="option_default_' . $i . '"  value="0" ' . $no . ' id="no_option_default_' . $i . '" />&nbsp;No</li></ol>';
                }
                $i++;
            }
        }
    }
    return $output;
}

/**
 * Method: getFieldTypes
 * Returns: field types array
 */
if (!function_exists('getFieldTypes')) {

    function getFieldTypes()
    {
        $cutom_field_types = array(
            'text' => 'Text Field',
            'textarea' => 'Text Area',
            'radio' => 'Radio Button',
            'checkbox' => 'Check Box',
            'selectlist' => 'Selection List'
        );
        return $cutom_field_types;
    }

}

/**
 * Method: replace_in_db
 * @param $str
 * @return mixed
 */
function replace_in_db($str)
{
    $arr_replace = array(
        ',',
        ':'
    );
    $arr_replace_with = array(
        '##@@#@##',
        '@@##@#@@'
    );
    $output = str_replace($arr_replace, $arr_replace_with, $str);
    return $output;
}

/**
 * Method: replace_in_view
 * @param $str
 * @return mixed
 */
function replace_in_view($str)
{
    $arr_replace_with = array(
        ',',
        ':'
    );
    $arr_replace = array(
        '##@@#@##',
        '@@##@#@@'
    );
    $output = str_replace($arr_replace, $arr_replace_with, $str);
    return $output;
}

function createForm($category_id, $product_id = '')
{
    $ci = &get_instance();
    $where = 'status = 1  AND ';
    $r = 1;
    foreach ($category_id as $id) {
        if ($r < count($category_id))
            $where .= ' category_id = ' . $id . ' OR ';
        else if ($r == count($category_id))
            $where .= ' category_id = ' . $id;
        $r++;
    }
    $f = $w = '';
    if ($product_id <> 0 && $product_id <> '') {
        $f = ' ,c_extrafieldvalues.values as value ,
        c_extrafieldvalues.id,c_extrafieldvalues.extra_field_id,c_extrafieldvalues.product_id';
        $w = 'INNER  JOIN c_extrafieldvalues ON (c_extrafieldvalues.extra_field_id = c_extrafields.extra_field_id AND c_extrafieldvalues.product_id=' . $product_id . ')';
    }
    $query = 'SELECT c_extrafields.* ' . $f . '
        FROM
        (c_extrafields)
        ' . $w . '

        WHERE
            ' . $where;
    $query = $ci->db->query($query);
    if ($query->num_rows() > 0) {
        $n = 1;
        foreach ($query->result_array() as $row) {
            $required = '';
            $req = '';
            switch ($row['type']) {

                /*                 * Text Box */
                CASE 'text':
                    $txt = explode(',', $row['values']);
                    if ($row['required'] == 1) {
                        $required = 'required';
                        $req = '*';
                    }
                    $maxlength = '';
                    if ($txt [0] <> '') {
                        $maxlength = 'maxlength="' . $txt [0] . '"';
                    }

                    echo '<div class="form-group">
                        <label class="control-label col-xs-12 col-sm-3 no-padding-right">' . $row['label'] . ' ' . $req . '</label>
                        <div class="col-xs-12 col-sm-9"><div class="clearfix">
                    <input type="' . $row['type'] . '" ' . $maxlength . ' class="col-xs-12 col-sm-5 ' . $required . '" id="c_form_' . $n . '" name="c_form_' . $n . '" placeholder="' . $row['label'] . '" value="' . $row['value'] . '">
                        <input type="hidden"name="extra_field_id[]" value="' . $row['extra_field_id'] . '"/></div>
                    </div></div>
';
                    break;

                /*                 * Text Box */
                CASE 'textfield':
                    $txt = explode(',', $row['values']);
                    if ($row['required'] == 1) {
                        $required = 'required';
                        $req = '*';
                    }
                    $maxlength = '';
                    if ($txt [0] <> '') {
                        $maxlength = 'maxlength="' . $txt [0] . '"';
                    }
                    echo '<div class="form-group">
                        <label class="control-label col-xs-12 col-sm-3 no-padding-right">' . $row['label'] . ' ' . $req . '</label>
                        <div class="col-xs-12 col-sm-9"><div class="clearfix">
                    <input type="' . $row['type'] . '" ' . $maxlength . ' class="col-xs-12 col-sm-5 ' . $required . '" id="c_form_' . $n . '" name="c_form_' . $n . '" placeholder="' . $row['label'] . '" value="' . $row['value'] . '">
                        <input type="hidden"name="extra_field_id[]" value="' . $row['extra_field_id'] . '"/></div>
                    </div></div>
';
                    break;


                /*                 * Textarea */
                CASE 'textarea':
                    echo '<div class="form-group">
                        <label class="control-label col-xs-12 col-sm-3 no-padding-right">' . $row['label'] . ' ' . $req . '</label>
                        <div class="col-xs-12 col-sm-9"><div class="clearfix">
                    <textarea  class="col-xs-12 col-sm-5 ' . $required . '" id="c_form_' . $n . '" name="c_form_' . $n . '" placeholder="' . $row['label'] . '">' . $row['value'] . '</textarea>
                        <input type="hidden"name="extra_field_id[]" value="' . $row['extra_field_id'] . '"/></div>
                    </div></div>
';
                    break;

                /*                 * Radio Buttons */
                CASE 'radio':
                    // Radio button
                    $val = rtrim($row['values'], ","); // trim last Comma in values
                    $values = explode(',', $val);
                    if ($row['required'] == 1) {
                        $required = 'required';
                        $req = '*';
                    }

                    echo '<div class="form-group">
                        <label class="control-label col-xs-12 col-sm-3 no-padding-right">' . $row['label'] . ' ' . $req . '</label>
                        <div class="col-xs-12 col-sm-9">';
                    for ($i = 0; $i < count($values); $i++) {
                        $subVal = explode(':', $values[$i]);
                        $r_check = '';
                        // for checked or not

                        if ($row['value'] <> '') {
                            if ($row['value'] == $subVal[0]) {
                                $r_check = 'checked="checked"';
                            }
                        } else {
                            if ($subVal[1] == 1) {
                                $r_check = 'checked="checked"';
                            }
                        }

                        echo '<div><label class="blue"><input type="' . $row['type'] . '" name="c_form_' . $n . '" value="' . $subVal[0] . '" ' . $r_check . ' class="' . $required . '" ><span class="lbl">&nbsp;' . $subVal[0] . '</span></label></div>';
                    }
                    echo '<input type="hidden"name="extra_field_id[]" value="' . $row['extra_field_id'] . '"/></div></div>';
                    break;

                /*                 * Checkboxes */
                CASE 'checkbox':
                    // checkbox
                    $val = rtrim($row['values'], ","); // trim last Comma in values
                    $values = explode(',', $val);
                    if ($row['required'] == 1) {
                        $required = 'required';
                        $req = '*';
                    }

                    echo '<div class="form-group">
                        <label class="control-label col-xs-12 col-sm-3 no-padding-right">' . $row['label'] . ' ' . $req . '</label>
                        <div class="col-xs-12 col-sm-9">';
                    for ($i = 0; $i < count($values); $i++) {
                        $subVal = explode(':', $values[$i]);
                        $checked = '';
                        // for check box is checked or not;

                        if ($row['value'] <> '') {
                            $val = explode(',', $row['value']);
                            if (in_array($subVal[0], $val, true)) {
                                $checked = 'checked="checked"';
                            }
                        } else {
                            if ($subVal[1] == 1) {
                                $checked = 'checked="checked"';
                            }
                        }

                        echo '<div><label class="blue"><input type="' . $row['type'] . '" name="c_form_' . $n . '[]" value="' . $subVal[0] . '" ' . $checked . ' class="' . $required . '" ><span class="lbl">&nbsp;' . $subVal[0] . '</span></label></div>';
                    }
                    echo '<input type="hidden"name="extra_field_id[]" value="' . $row['extra_field_id'] . '"/></div></div>';
                    break;

                /*                 * Select List */
                CASE 'selectlist':
                    // Select List
                    $val = rtrim($row['values'], ","); // trim last Comma in values
                    $values = explode(',', $val);
                    if ($row['required'] == 1) {
                        $required = 'required';
                        $req = '*';
                    }

                    echo '<div class="form-group">
                        <label class="control-label col-xs-12 col-sm-3 no-padding-right">' . $row['label'] . ' ' . $req . '</label>
                        ';
                    echo '<div class="col-xs-12 col-sm-9"><div class="clearfix">
                        <select class="col-xs-12 col-sm-5 ' . $required . '" id="c_form_' . $n . '" name="c_form_' . $n . '">';
                    for ($i = 0; $i < count($values); $i++) {
                        $subVal = explode(':', $values[$i]);
                        $selected = '';
                        // for check box is checked or not;
                        if ($row['value'] <> '') {
                            if ($row['value'] == $subVal[0]) {
                                $selected = 'selected';
                            }
                        } else {
                            if ($subVal[1] == 1) {
                                $selected = 'selected';
                            }
                        }
                        echo '<option value="' . $subVal[0] . '" ' . $selected . '>' . $subVal[0] . '</option>';
                    }
                    echo '</select><input type="hidden"name="extra_field_id[]" value="' . $row['extra_field_id'] . '"/></div></div></div>';
                    break;


                default:
            }
            $n++;
        }
    }
}

if (!function_exists('get_pages_footer')) {

    function get_pages_footer($limit)
    {
        $ci = &get_instance();
        $ci->db->cache_on();
        $ci->db->select('*');
        $ci->db->where('status', 1);
        $ci->db->where('show_footer', 1);
        $ci->db->order_by('title', 'ASC');
        $ci->db->limit($limit);
        $q = $ci->db->get('c_contentmanagement');
        $ci->db->cache_off();
        return $q->result_array();
    }

}
if (!function_exists('get_header_menu')) {

    function get_header_menu($limit)
    {
        $ci = &get_instance();
        $ci->db->cache_on();
        $ci->db->select('*');
        $ci->db->where('status', 1);
        $ci->db->where('show_header', 1);
        $ci->db->where('is_main_page', 1);
        $ci->db->order_by('title', 'ASC');
        $ci->db->limit($limit);
        $q = $ci->db->get('c_contentmanagement');
        $ci->db->cache_off();
        return $q->result_array();
    }

}
if (!function_exists('get_subMenu')) {

    function get_subMenu($id)
    {
        $ci = &get_instance();
        $ci->db->select('*');
        $ci->db->where('status', 1);
        $ci->db->where('page_id', $id);
        $ci->db->order_by('title', 'ASC');
        $q = $ci->db->get('c_contentmanagement');
        return $q->result_array();
    }

}

/**
 * Method: getCompanyData
 * Params: $id
 * Return: data row
 */
function getCompanyData($id)
{
    $ci = get_instance();
    $sql_ = 'SELECT com.* from c_users_companies as com ';
    $sql_ .= ' WHERE com.company_id=' . $id;
    $query = $ci->db->query($sql_);
    if ($query->num_rows() > 0) {
        return $query->row_array();
    }
}

/**
 * Method: get_email_tempData
 * Params: $email_type
 * Return: array
 */
if (!function_exists('get_email_tempData')) {

    function get_email_tempData($email_type)
    {
        $ci = get_instance();
        $ci->db->select("*");
        $ci->db->from('c_email_templates');
        $ci->db->where('email_template_type', $email_type);
        $ci->db->where('status', 1);
        $ci->db->order_by('id', 'desc');
        $ci->db->limit(1);
        $query = $ci->db->get();
        if ($query->num_rows >= 1) {
            return $query->row_array();
        }
    }

}
/**
 * ******For single column value
 * */
if (!function_exists('get_user_col_value')) {

    function get_user_col_value($cols, $where = '', $criteria = '')
    {
        $ci = &get_instance();
        $arr_results = array();
        $ci->db->select($cols);
        $ci->db->where($where, $criteria);
        $ci->db->from('resellers');
        $ci->db->limit(1);
        $results = $ci->db->get();
        if ($results->num_rows() > 0) {
            return $results->row_array();
        }
    }

}
/**
 * ******For complete user data
 * */
if (!function_exists('get_user_data')) {

    function get_user_data($col, $where = '', $criteria = '')
    {
        $ci = &get_instance();
        $ci->db->select($col);
        $ci->db->where($where, $criteria);
        $ci->db->from('resellers');
        $ci->db->limit(1);
        $results = $ci->db->get();
        if ($results->num_rows() > 0) {
            return $results->row_array();
        }
    }

}

/**
 * Method: generateCategories for advertisements
 * Params: $parent,$level,$sel
 * Return: categories
 */
if (!function_exists('generateCategories')) {

    function generateCategories($parent, $level, $sel)
    {
        $ci = &get_instance();
        $ci->db->where('parent_id', $parent);
        $ci->db->select('category_id,category_name');
        $ci->db->where('status', 1);
        $query = $ci->db->get('c_product_categories');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                if (is_array($sel)) {
                    if (in_array($row->category_id, $sel)) {
                        $seletd = 'selected="selected"';
                    } else {
                        $seletd = '';
                    }
                } else {
                    if ($row->category_id == $sel) {
                        $seletd = 'selected="selected"';
                    } else {
                        $seletd = '';
                    }
                }
                echo '<option value="' . $row->category_id . '" ' . $seletd . '>' . str_repeat('-', $level) . ' ' . $row->category_name . '</option>';
                generateCategories($row->category_id, $level + 1, $sel);
            }
        }
    }

}

/* Get Banner Code* */

function get_banner_code($direction, $advertising_id, $parent_categories)
{
    $cat_id = '';
    $uri = explode("/", $_SERVER['REQUEST_URI']);
    $urlType = @$uri[1];
    $urlType1 = @$uri[2];
    if ($urlType == '' || $urlType == 'post') {
        $type = 1;
    } elseif ($urlType == 'equipment-listings' || $urlType == 'parts-listings') {
        $type = 2;
        if ($urlType1 <> '') {
            $cat_id = getVal('category_id', 'c_product_categories', 'category_slug', $urlType1);
        }
    } else {
        $type = 1;
    }
    if ($type <> 0) {
        $whr = '';
        if ($advertising_id <> '' && $advertising_id <> 0) {
            $whr .= ' AND advertising_id <> ' . $advertising_id;
        }
        if ($parent_categories <> '' && $parent_categories <> 0) {
            $whr .= ' AND category_id IN (' . $parent_categories . ')';
        }
        if ($type <> '' && $type <> 0) {
            $whr .= ' AND is_home  = ' . $type;
        }
        if ($cat_id <> '') {
            $whr .= ' AND category_id = ' . $cat_id;
        }
        $CI = &get_instance();
        $query = "SELECT *
                  FROM
                 	c_advertisings
					 WHERE  end_date >= '" . date('Y-m-d') . "' AND advertising_destination_id = '" . $direction . "' and status = 1 " . $whr . "
					 ORDER BY  RAND(), `advertising_id` desc limit 1
                ";
//    $CI->db->cache_on();
//    $CI->db->cache_delete();
        $query = $CI->db->query($query);
//    $CI->db->cache_off();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result;
        }
    }
}

/* Get Banner Code* */

function get_banner_code_array($direction, $advertising_id, $parent_categories)
{
    $cat_id = '';
    $uri = explode("/", $_SERVER['REQUEST_URI']);
    $urlType = @$uri[1];
    $urlType1 = @$uri[2];
    if ($urlType == '' || $urlType == 'post') {
        $type = 1;
    } elseif ($urlType == 'equipment-listings' || $urlType == 'parts-listings') {
        $type = 2;
        if ($urlType1 <> '') {
            $cat_id = getVal('category_id', 'c_product_categories', 'category_slug', $urlType1);
        }
    } else {
        $type = 1;
    }
    if ($type <> 0) {
        $whr = '';
        if ($advertising_id <> '' && $advertising_id <> 0) {
            $whr .= ' AND advertising_id <> ' . $advertising_id;
        }
        if ($parent_categories <> '' && $parent_categories <> 0) {
            $whr .= ' AND category_id IN (' . $parent_categories . ')';
        }
        if ($type <> '' && $type <> 0) {
            $whr .= ' AND is_home  = ' . $type;
        }
        if ($cat_id <> '') {
            $whr .= ' AND category_id = ' . $cat_id;
        }
        $CI = &get_instance();
        $query = "SELECT *
                  FROM
                 	c_advertisings
					 WHERE  end_date >= '" . date('Y-m-d') . "' AND advertising_destination_id = '" . $direction . "' and status = 1 " . $whr . "
					 ORDER BY  RAND(), `advertising_id` desc limit 3
                ";
        $query = $CI->db->query($query);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result;
        }
    }
}

function get_cms_navigation($key)
{
    $CI = &get_instance();
    $table2 = $CI->db->dbprefix . 'contentmanagement';
    $CI->db->select('*');
    $CI->db->where($key, 1);
    $CI->db->where('status', 1);
    $CI->db->order_by('ordering', 'desc');
    $q2 = $CI->db->get($table2);
    return $q2->result_array();
}

function random_string($type = 'alnum', $len = 8)
{
    switch ($type) {
        case 'basic':
            return mt_rand();
        case 'alnum':
        case 'numeric':
        case 'nozero':
        case 'alpha':
            switch ($type) {
                case 'alpha':
                    $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    break;
                case 'alnum':
                    $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    break;
                case 'numeric':
                    $pool = '0123456789';
                    break;
                case 'nozero':
                    $pool = '123456789';
                    break;
            }
            return substr(str_shuffle(str_repeat($pool, ceil($len / strlen($pool)))), 0, $len);
        case 'unique': // todo: remove in 3.1+
        case 'md5':
            return md5(uniqid(mt_rand()));
        case 'encrypt': // todo: remove in 3.1+
        case 'sha1':
            return sha1(uniqid(mt_rand(), TRUE));
    }
}

function get_managers()
{
    $CI = &get_instance();
    $CI->db->select('email');
    $CI->db->from('users');
    $CI->db->where("group_id =", 4);
    $CI->db->order_by('id', "desc");
    $q = $CI->db->get();
    return $q->result_array();
}

function get_datateamOperations()
{
    $CI = &get_instance();
    $CI->db->select('email');
    $CI->db->from('users');
    $CI->db->where("group_id =", 5);
    $CI->db->order_by('id', "desc");
    $q = $CI->db->get();
    return $q->result_array();
}

function get_datateam($array = '')
{
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('users');
    $CI->db->where_in('id', $array);
    $CI->db->order_by('id', "desc");
    // $CI->db->where("group_id =", 5);
    $q = $CI->db->get();
    return $q->result_array();
}

function printr($p, $exit = 1)
{
    echo '<pre>';
    print_r($p);
    echo '</pre>';
    if ($exit == 1) {
        exit;
    }
}


function getLoggedInUser()
{

    $CI = &get_instance();
//or you can load library in the site helper also
    $CI->load->library('session');

    return $active_user = $CI->session->userdata('active_user');
}

function changeDateFormat($date)
{
    return date("Y-m-d", strtotime($date));
}


/* check view port */
if (!function_exists('getDataByColumn')) {

    function getDataByColumn($table = '', $col = '', $val)
    {
        $CI = &get_instance();
        $CI->db->select('*');
        $CI->db->from($table);
        $CI->db->order_by('id', "desc");
        $CI->db->where($col, $val);
        $q = $CI->db->get();
        return $q->result();
    }

}

if (!function_exists('getDataByColumnRow')) {

    function getDataByColumnRow($table = '', $where = '')
    {
        $CI = &get_instance();
        $CI->db->select('*');
        $CI->db->from($table);
        $CI->db->order_by('id', "desc");
        $CI->db->limit(1);
        if ($where != '') {
            $CI->db->where($where);
        }
        $q = $CI->db->get();
        return $q->result();
    }

}


/* check view port */
if (!function_exists('getRequestMaterialData')) {

    function getRequestMaterialData($request_id = '', $type = '')
    {
        $CI = &get_instance();
        $CI->db->select('*');
        $CI->db->from("material_info");
        $CI->db->where("material_info.request_id", $request_id);
        if ($type) {
            $CI->db->where("material_info.type", $type);
        }
        $CI->db->order_by('material_info.id', "desc");
        $q = $CI->db->get();
        return $q->result();
    }
}
if (!function_exists('getRequestMaterialData_temp')) {

    function getRequestMaterialData_temp($request_id = '', $type = '')
    {
        $CI = &get_instance();
        $CI->db->select('*');
        $CI->db->from("material_info_temp");
        $CI->db->where("material_info_temp.request_id", $request_id);
        if ($type) {
            $CI->db->where("material_info_temp.type", $type);
        }
        $CI->db->order_by('material_info_temp.id', "desc");
        $q = $CI->db->get();
        return $q->result();
    }
}


/* check view port */
if (!function_exists('getRequestWorkPermit')) {

    function getRequestWorkPermit($request_id = '')
    {
        $CI = &get_instance();
        $CI->db->select('*');
        $CI->db->from("work_permits");
        $CI->db->where("work_permits.request_id", $request_id);
        $CI->db->order_by('work_permits.id', "desc");
        $q = $CI->db->get();
        return $q->result();
    }

}
if (!function_exists('getRequestWorkPermit_temp')) {

    function getRequestWorkPermit_temp($request_id = '')
    {
        $CI = &get_instance();
        $CI->db->select('*');
        $CI->db->from("work_permits_temp");
        $CI->db->where("work_permits_temp.request_id", $request_id);
        $CI->db->order_by('work_permits_temp.id', "desc");
        $q = $CI->db->get();
        return $q->result();
    }

}

/* check view port */
if (!function_exists('getRequestPersonnels')) {

    function getRequestPersonnels($request_id = '')
    {
        $CI = &get_instance();
        $CI->db->select('*');
        $CI->db->from("personnels");
        $CI->db->where("personnels.request_id", $request_id);
        $CI->db->order_by('personnels.id', "desc");
        $q = $CI->db->get();
        return $q->result_array();
    }

}

/* check view port */
if (!function_exists('getSelectedManagersSecuirity')) {

    function getSelectedManagersSecuirity($request_id = '')
    {

        $CI = &get_instance();
        $CI->db->select('preapprovers_managers.id as ptbl_id,preapprovers_managers.manager_id,preapprovers_managers.request_id,preapprovers_managers.checkin_status,preapprovers_managers.security_history_id,users.*');
        $CI->db->from("preapprovers_managers");
        $CI->db->join('users', 'users.id = preapprovers_managers.manager_id');
        $CI->db->where("preapprovers_managers.request_id", $request_id);
        $q = $CI->db->get();
        $results = $q->result_array();
        return $results;

    }

}
/* check view port */
if (!function_exists('getSelectedManagerSecuirity')) {

    function getSelectedManagerSecuirity($request_id = '')
    {

        $CI = &get_instance();
        $CI->db->select('preapprovers_managers.id as ptbl_id,preapprovers_managers.manager_id,preapprovers_managers.request_id,preapprovers_managers.checkin_status,preapprovers_managers.security_history_id,users.*');
        $CI->db->from("preapprovers_managers");
        $CI->db->join('users', 'users.id = preapprovers_managers.manager_id');
        $CI->db->where("preapprovers_managers.id", $request_id);
        $q = $CI->db->get();
        $results = $q->row_array();
        return $results;

    }

}
/* check view port */
if (!function_exists('getSelectedBackupsSecuirity')) {

    function getSelectedBackupsSecuirity($request_id = '')
    {

        $CI = &get_instance();
        $CI->db->select('preapprovers_contacts.id as ptbl_id,preapprovers_contacts.contact_id,preapprovers_contacts.request_id,preapprovers_contacts.checkin_status,preapprovers_contacts.security_history_id,users.*');
        $CI->db->from("preapprovers_contacts");
        $CI->db->join('users', 'users.id = preapprovers_contacts.contact_id');
        $CI->db->where("preapprovers_contacts.request_id", $request_id);
        $q = $CI->db->get();
        $results = $q->result_array();
        return $results;

    }

}
/* check view port */
if (!function_exists('getSelectedBackupSecuirity')) {

    function getSelectedBackupSecuirity($request_id = '')
    {

        $CI = &get_instance();
        $CI->db->select('preapprovers_contacts.id as ptbl_id,preapprovers_contacts.contact_id,preapprovers_contacts.request_id,preapprovers_contacts.checkin_status,preapprovers_contacts.security_history_id,users.*');
        $CI->db->from("preapprovers_contacts");
        $CI->db->join('users', 'users.id = preapprovers_contacts.contact_id');
        $CI->db->where("preapprovers_contacts.id", $request_id);
        $q = $CI->db->get();
        $results = $q->row_array();
        return $results;

    }

}



/* check view port */
if (!function_exists('getSelectedPersonsSecuirity')) {

    function getSelectedPersonsSecuirity($request_id = '')
    {

        $CI = &get_instance();
        $CI->db->select('preapprovers_personnels.id as ptbl_id,preapprovers_personnels.personnel_id,preapprovers_personnels.request_id,preapprovers_personnels.checkin_status,preapprovers_personnels.security_history_id,users.*');
        $CI->db->from("preapprovers_personnels");
        $CI->db->join('users', 'users.id = preapprovers_personnels.personnel_id');
        $CI->db->where("preapprovers_personnels.request_id", $request_id);
        $q = $CI->db->get();
        $results = $q->result_array();
        return $results;

    }

}
/* check view port */
if (!function_exists('getSelectedPersonSecuirity')) {

    function getSelectedPersonSecuirity($request_id = '')
    {

        $CI = &get_instance();
        $CI->db->select('preapprovers_personnels.id as ptbl_id,preapprovers_personnels.personnel_id,preapprovers_personnels.request_id,preapprovers_personnels.checkin_status,preapprovers_personnels.security_history_id,users.*');
        $CI->db->from("preapprovers_personnels");
        $CI->db->join('users', 'users.id = preapprovers_personnels.personnel_id');
        $CI->db->where("preapprovers_personnels.id", $request_id);
        $q = $CI->db->get();
        $results = $q->row_array();
        return $results;

    }

}

/* check view port */
if (!function_exists('getSelectedManagers')) {

    function getSelectedManagers($request_id = '')
    {
        $CI = &get_instance();
        $CI->db->select('*');
        $CI->db->from("preapprovers_managers");
        $CI->db->where("preapprovers_managers.request_id", $request_id);
        $q = $CI->db->get();
        $results = $q->result_array();
        $ids = '';
        if (isset($results[0]['manager_id'])) {
            foreach ($results as $key => $data) {
                $ids[] = $data['manager_id'];
            }
            $ids = implode(", ", $ids);
        }
        return $ids;

    }

}

if (!function_exists('getSelectedManagers_temp')) {

    function getSelectedManagers_temp($request_id = '')
    {
        $CI = &get_instance();
        $CI->db->select('*');
        $CI->db->from("preapprovers_managers_temp");
        $CI->db->where("preapprovers_managers_temp.request_id", $request_id);
        $q = $CI->db->get();
        $results = $q->result_array();
        $ids = '';
        if (isset($results[0]['manager_id'])) {
            foreach ($results as $key => $data) {
                $ids[] = $data['manager_id'];
            }
            $ids = implode(", ", $ids);
        }
        return $ids;

    }

}


if (!function_exists('getSelectedContacts')) {

    function getSelectedContacts($request_id = '')
    {
        $CI = &get_instance();
        $CI->db->select('*');
        $CI->db->from("preapprovers_contacts");
        $CI->db->where("preapprovers_contacts.request_id", $request_id);
        $q = $CI->db->get();
        $results = $q->result_array();
        $ids = '';
        if (isset($results[0]['contact_id'])) {
            foreach ($results as $key => $data) {
                $ids[] = $data['contact_id'];

            }
            $ids = implode(", ", $ids);
        }
        return $ids;

    }

}

if (!function_exists('getSelectedContacts_temp')) {

    function getSelectedContacts_temp($request_id = '')
    {
        $CI = &get_instance();
        $CI->db->select('*');
        $CI->db->from("preapprovers_contacts_temp");
        $CI->db->where("preapprovers_contacts_temp.request_id", $request_id);
        $q = $CI->db->get();
        $results = $q->result_array();
        $ids = '';
        if (isset($results[0]['contact_id'])) {
            foreach ($results as $key => $data) {
                $ids[] = $data['contact_id'];

            }
            $ids = implode(", ", $ids);
        }
        return $ids;

    }

}
if (!function_exists('getSelectedPersonnels')) {

    function getSelectedPersonnels($request_id = '')
    {
        $CI = &get_instance();
        $CI->db->select('*');
        $CI->db->from("preapprovers_personnels");
        $CI->db->where("preapprovers_personnels.request_id", $request_id);
        $q = $CI->db->get();
        $results = $q->result_array();
        $ids = '';
        if (isset($results[0]['personnel_id'])) {
            foreach ($results as $key => $data) {
                $ids[] = $data['personnel_id'];

            }
            $ids = implode(", ", $ids);
        }
        return $ids;

    }

}

if (!function_exists('getSelectedPersonnels_temp')) {

    function getSelectedPersonnels_temp($request_id = '')
    {
        $CI = &get_instance();
        $CI->db->select('*');
        $CI->db->from("preapprovers_personnels_temp");
        $CI->db->where("preapprovers_personnels_temp.request_id", $request_id);
        $q = $CI->db->get();
        $results = $q->result_array();
        $ids = '';
        if (isset($results[0]['personnel_id'])) {
            foreach ($results as $key => $data) {
                $ids[] = $data['personnel_id'];

            }
            $ids = implode(", ", $ids);
        }
        return $ids;

    }

}

/* check view port */
if (!function_exists('getPreApproverManagersContacts')) {

    function getPreApproverManagersContacts($request_id = '')
    {
        $ids = '';
        $CI = &get_instance();
        $CI->db->select('*');
        $CI->db->from("preapprovers_managers");
        $CI->db->where("preapprovers_managers.request_id", $request_id);
        $q = $CI->db->get();
        $results_manager = $q->result_array();

        if (isset($results_manager[0]['manager_id'])) {
            foreach ($results_manager as $key => $data) {
                $ids[] = $data['manager_id'];
            }
        }


        $CI = &get_instance();
        $CI->db->select('*');
        $CI->db->from("preapprovers_contacts");
        $CI->db->where("preapprovers_contacts.request_id", $request_id);
        $q = $CI->db->get();
        $results_contact = $q->result_array();

        if (isset($results_contact[0]['contact_id'])) {
            foreach ($results_contact as $key => $data) {
                $ids[] = $data['contact_id'];
            }
        }


        return $ids;

    }

}

if (!function_exists('getPreApproverManagersContacts_temp')) {

    function getPreApproverManagersContacts_temp($request_id = '')
    {
        $ids = '';
        $CI = &get_instance();
        $CI->db->select('*');
        $CI->db->from("preapprovers_managers_temp");
        $CI->db->where("preapprovers_managers_temp.request_id", $request_id);
        $q = $CI->db->get();
        $results_manager = $q->result_array();

        if (isset($results_manager[0]['manager_id'])) {
            foreach ($results_manager as $key => $data) {
                $ids[] = $data['manager_id'];
            }
        }


        $CI = &get_instance();
        $CI->db->select('*');
        $CI->db->from("preapprovers_contacts_temp");
        $CI->db->where("preapprovers_contacts_temp.request_id", $request_id);
        $q = $CI->db->get();
        $results_contact = $q->result_array();

        if (isset($results_contact[0]['contact_id'])) {
            foreach ($results_contact as $key => $data) {
                $ids[] = $data['contact_id'];
            }
        }


        return $ids;

    }

}


function count_preappeover_checkin()
{
    $date = date("Y-m-d");
    $CI = &get_instance();
    $query = $CI->db->query("SELECT COUNT(id) as chckin FROM security_history WHERE checkin_status=1 AND DATE(check_in_time) = DATE(NOW())");
    return $query->row()->chckin;
}

function count_other_checkin()
{
    $CI = &get_instance();
    $query = $CI->db->query("SELECT COUNT(id) as chckin FROM security_history  WHERE checkin_status=1 AND DATE(check_in_time) = DATE(NOW())");
    return $query->row()->chckin;

}
function count_other_checkin_old()
{
    $CI = &get_instance();
    $query = $CI->db->query("SELECT COUNT(id) as chckin FROM security_history  WHERE checkin_status=1 AND DATE(check_in_time) = DATE(NOW())");
    return $query->row()->chckin;

}

function count_all_checkinOut()
{
    $CI = &get_instance();
    $query = $CI->db->query("SELECT COUNT(id) as chckin FROM security_history");
    return $query->row()->chckin;

}

function count_preappeover_todayvisiter()
{
    $date = date("Y-m-d");
    $CI = &get_instance();
    $query = $CI->db->query("SELECT COUNT(id) as chckin FROM security_history WHERE  DATE(check_in_time) = DATE(NOW())");
    return $query->row()->chckin;
}

function count_other_todayvisiter()
{
    $CI = &get_instance();
    $query = $CI->db->query("SELECT COUNT(id) as chckin FROM security_history  WHERE DATE(check_in_time) = DATE(NOW())");
    return $query->row()->chckin;

}

function count_preappeover_checkout()
{

    $CI = &get_instance();

    $query = $CI->db->query("SELECT COUNT(id) as checkout FROM security_history WHERE checkin_status=2 AND Date(check_out_time)= CURDATE()");
    return $query->row()->checkout;

}

function count_other_checkout()
{
    $CI = &get_instance();
    $query = $CI->db->query("SELECT COUNT(id) as checkout FROM security_history  WHERE checkin_status=2 AND Date(check_out_time)= CURDATE()");
    return $query->row()->checkout;

}

function count_preappeover_Pending()
{
    $CI = &get_instance();
    $query = $CI->db->query("SELECT COUNT(id) as chckin FROM preapprove_requests WHERE team_signature= ''");
    return $query->row()->chckin;
}

function count_other_Pending($id = '')
{
    $CI = &get_instance();
    if ($id != '') {
        $query = $CI->db->query("SELECT COUNT(id) as chckin FROM requests  WHERE created_by=" . $id . " AND team_signature IS NULL");

    } else {
        $query = $CI->db->query("SELECT COUNT(id) as chckin FROM requests  WHERE  team_signature IS NULL ");
    }
    return $query->row()->chckin;

}


function count_preappeover_Approved()
{
    $CI = &get_instance();
    $query = $CI->db->query("SELECT COUNT(id) as chckin FROM preapprove_requests WHERE team_signature= 'Approved'");
    return $query->row()->chckin;
}

function count_other_Approved($id = '')
{
    $CI = &get_instance();

    if ($id != '') {
        $query = $CI->db->query("SELECT COUNT(id) as chckin FROM requests  WHERE created_by=" . $id . " AND team_signature= 'Approved'");
    } else {
        $query = $CI->db->query("SELECT COUNT(id) as chckin FROM requests  WHERE  team_signature= 'Approved'");
    }

    return $query->row()->chckin;

}

function count_preappeover_Reject()
{
    $CI = &get_instance();
    $query = $CI->db->query("SELECT COUNT(id) as chckin FROM preapprove_requests WHERE team_signature= 'Reject'");
    return $query->row()->chckin;
}

function count_other_Reject($id = '')
{
    $CI = &get_instance();
    if ($id != '') {
        $query = $CI->db->query("SELECT COUNT(id) as chckin FROM requests  WHERE created_by=" . $id . " AND team_signature= 'Reject'");
    } else {
        $query = $CI->db->query("SELECT COUNT(id) as chckin FROM requests  WHERE  team_signature='Reject'");
    }
    return $query->row()->chckin;

}

function getAccessNumber()
{
    $CI = &get_instance();
    $last = $CI->db->order_by('id', "desc")
        ->limit(1)
        ->get('requests')
        ->row();


    /*$number = sprintf("%06d", mt_rand(1, 999999));
    $accessNumber  = $part1 .' '.$number;*/

    if (!empty($last)) {

        /* $number= str_pad($last->id, 6, "0", STR_PAD_LEFT);
         $accessNumber = $part1 .' '.$number;
         $myText = (string)$accessNumber;*/
        return $last->id;

    } else {
        return 1;
    }


}

function get_managers_preapprovers($company_name = '')
{
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('users');
    $CI->db->like('LOWER(company_name)', strtolower($company_name));
    $CI->db->where('group_id', 4);
    $q = $CI->db->get();
    return $q->result_array();
}

function get_contact_preapprovers($company_name = '')
{
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('users');
    $CI->db->like('LOWER(company_name)', strtolower($company_name));
    $CI->db->where('group_id', 6);
    $q = $CI->db->get();
    return $q->result_array();
}

function get_personnels_preapprovers($company_name = '')
{
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from('users');
    $CI->db->like('LOWER(company_name)', strtolower($company_name));
    $CI->db->where('group_id', 7);
    $q = $CI->db->get();
    return $q->result_array();
}
function ForgotPassword($email)
{
    $CI = &get_instance();
    $CI->db->select('email');
    $CI->db->from('users');
    $CI->db->where('email', $email);
    $q=$CI->db->get();
    return $q->row_array();
}


function mainEmailTemplate($company_name = '')
{
    $html = '<div id="email_header" style="text-align: center; background-color: #0f4399;"><img
        style="height: 100px; padding: 5px; width: 100px" src="https://mepure.com/gulfdatahub/assets/images/logo1.jpg">
            </div>

        <div id="email_main" style="padding: 50px;">
          <h3>Dear Visitor,</h3><br>
            <p>Your request for <b>Data Access Request in GDH</b> has been submitted successfully. kindly wait for Approval</p>

           <h3>Visitors</h3>
        <table style="text-align:center;width:50%; border: 1px solid black;border-collapse: collapse;">
        <tr style="">
            <th style="border: 1px solid black;border-collapse: collapse;">Name</th>
            <th style="border: 1px solid black;border-collapse: collapse;">Copmpany</th>
            <th style="border: 1px solid black;border-collapse: collapse;">Contact</th>
        </tr>
        <tr>
            <td style="border: 1px solid black;border-collapse: collapse;">Jill</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Smith</td>
            <td style="border: 1px solid black;border-collapse: collapse;">50</td>
        </tr>
        <tr>
            <td style="border: 1px solid black;border-collapse: collapse;">Eve</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Jackson</td>
            <td style="border: 1px solid black;border-collapse: collapse;">94</td>
        </tr>
        </table>

        <h3>Workers</h3>
        <table style="text-align:center;width:50%; border: 1px solid black;border-collapse: collapse;">
        <tr style="">
            <th style="border: 1px solid black;border-collapse: collapse;">Name</th>
            <th style="border: 1px solid black;border-collapse: collapse;">Copmpany</th>
            <th style="border: 1px solid black;border-collapse: collapse;">Contact</th>
        </tr>
        <tr>
            <td style="border: 1px solid black;border-collapse: collapse;">Eve</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Jackson</td>
            <td style="border: 1px solid black;border-collapse: collapse;">94</td>
        </tr>
        </table>

        <h3>Materia In Items</h3>
        <table style="text-align:center;width:50%; border: 1px solid black;border-collapse: collapse;">
        <tr style="">
            <th style="border: 1px solid black;border-collapse: collapse;">Description</th>
            <th style="border: 1px solid black;border-collapse: collapse;">Quantity</th>
            <th style="border: 1px solid black;border-collapse: collapse;">Serial Number</th>
            <th style="border: 1px solid black;border-collapse: collapse;">Model</th>
        </tr>
        <tr>
            <td style="border: 1px solid black;border-collapse: collapse;">Eve</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Jackson</td>
            <td style="border: 1px solid black;border-collapse: collapse;">94</td>
            <td style="border: 1px solid black;border-collapse: collapse;">94</td>
        </tr>
        </table>

        <h3>Materia Out Items</h3>
        <table style="text-align:center;width:50%; border: 1px solid black;border-collapse: collapse;">
        <tr style="">
            <th style="border: 1px solid black;border-collapse: collapse;">Description</th>
            <th style="border: 1px solid black;border-collapse: collapse;">Quantity</th>
            <th style="border: 1px solid black;border-collapse: collapse;">Serial Number</th>
            <th style="border: 1px solid black;border-collapse: collapse;">Model</th>
        </tr>
        <tr>
            <td style="border: 1px solid black;border-collapse: collapse;">Eve</td>
            <td style="border: 1px solid black;border-collapse: collapse;">Jackson</td>
            <td style="border: 1px solid black;border-collapse: collapse;">94</td>
            <td style="border: 1px solid black;border-collapse: collapse;">94</td>
        </tr>
        </table>
        <p>Regards</p>
        <h3>GDH Management</h3>
        </div>


    <div id="email_footer"
     style=" clear: both; text-align: center; padding: 10px; background-color: #0c0c0c; color: #fff0ff;">
    <p style="">Copyright © 2015 Gulf Data Hub. All rights reserved</p>
    </div>';
    return $html;
}

function checkEmail($email) {
    $CI = &get_instance();
    $sql_ = "SELECT email FROM users WHERE email = '" . $email . "'";
    $query = $CI->db->query($sql_);
    if ($query->num_rows() >= 1) {
        return 1;
    } else {
        return 0;
    }
}

