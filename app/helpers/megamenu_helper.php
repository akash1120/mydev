<?php
/**
 * Adjacency List
 *
 * @package Helper
 * @author  Michał Śniatała <michal@sniatala.pl>
 * @license http://opensource.org/licenses/MIT  (MIT)
 * @since   Version 0.1
 */
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('build_admin_tree'))
{
	function build_admin_tree(&$tree)
	{
		$output = '';

		if ( ! empty($tree))
		{
			foreach ($tree as &$leaf)
			{
				$leaf['mname'] = htmlspecialchars($leaf['mname'], ENT_QUOTES, 'UTF-8');

				if (isset($leaf['children']) && ! empty($leaf['children']))
				{
					$output .= '<li id="list_' . $leaf['id'] . '">
									<div>
										<i class="fa fa-bars icon-move"></i>&nbsp;&nbsp;' . $leaf['mname'] .	 '
											<span>
												<a style="margin-right:15px;" href="' . site_url('admin/megamenu/edit/' . $leaf['id']) . '">
													<i class="fa fa-edit blue-text"></i>
												</a> <a class="delete" data-toggle="modal" data-type="item" data-href="' . site_url('admin/megamenu/delete/' . $leaf['id']) . '" data-name="' . $leaf['mname'] . '" href="javascript:;">
													<i class="fa fa-trash red-text"></i>
												</a>
											</span>
									</div>';
					$output .= '<ol>' . build_admin_tree($leaf['children']) . '</ol>';
					$output .= '</li>';
				}
				else
				{
					$output .= '<li id="list_' . $leaf['id'] . '">
									<div>
										<i class="fa fa-bars icon-move"></i>&nbsp;&nbsp;' . $leaf['mname'] . '
											<span>
												<a style="margin-right:15px;" href="' . site_url('admin/megamenu/edit/'.$leaf['id']) . '">
													<i class="fa fa-edit blue-text"></i>
												</a> <a class="delete" data-toggle="modal" data-type="item" data-href="' . site_url('admin/megamenu/delete/' . $leaf['id']) . '" data-name="' . $leaf['mname'] . '" href="javascript:;">
													<i class="fa fa-trash red-text"></i>
												</a>
											</span>
									</div>
								</li>';
				}
			}
		}

		return $output;
	}
}

//--------------------------------------------------------------------

if ( ! function_exists('build_tree'))
{
	function build_tree($group, &$tree = array())
	{
		$CI =& get_instance();
		if (empty($tree))
		{

			$CI->load->library('megamenulib');
			$tree = $CI->megamenulib->get_all($group);
			$tree = parse_children($tree);
		}

        $group_info = $CI->megamenulib->get_group_with_slug($group);

        if( $CI->megamenulib->check_user_rights( $group_info['groups'] ) ){
            $output = '';
            if($group_info['fontcolor'] != '')
            	$fontcolor = 'color:'.$group_info['fontcolor'].';';
            else
            	$fontcolor = '';

            if($group_info['fontsize'] != '')
            	$fontsize = 'font-size:'.$group_info['fontsize'].'px;';
            else
            	$fontsize = '';

            if($group_info['fontweight'] != '')
            	$fontweight = 'font-weight:'.$group_info['fontweight'].';';
            else
            	$fontweight = '';

			if($group_info['font'] !=''){
                $output .= '<link href="//fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">';
                /*	$output .= '<link href="//fonts.googleapis.com/css?family=Open+Sans'.$group_info['font'].'" rel="stylesheet">';*/
				$output .= "<style>
				.megam-main-nav-wrap, .megam-main-nav-wrap a, .megam-main-nav > li > a{
				    font-family: '".str_replace('+',' ',$group_info['font'])."';
				    ".$fontcolor."
				    ".$fontsize."
				    ".$fontweight."
				}
				</style>";
			}

			$output .= '<nav class="megam-main-nav-wrap justify-content-center navbar fixed-top black">
				<button class="btn '. config_item('button_color') .' '. config_item('button_color_text') .'-text waves-effect waves-light megam-main-nav-trigger"> Menu </button>
				<ul class="megam-main-nav" style="width: '.$group_info['width'].'px;">';

			$output .= '<style>';

			$output .= 'main { padding-top:60px; }';
			$output .= '.side-nav.fixed {padding-top: 60px;}';

			$output .= '.megam-main-nav-wrap {';

	       	if($group_info['bg_color'] != '')
	           	$output .= 'background-color: '.$group_info['bg_color'].'!important;';

	       	if($group_info['bg_image'] != '')
	           	$output .= 'background-image: url('.base_url('assets/uploads/menuimage/'.$group_info['bg_image']).')!important;';

	       	if($group_info['bg_size'] != '')
	           	$output .= 'background-size: '.$group_info['bg_size'].'!important;';

	       	if($group_info['bg_repeat'] != '')
	           	$output .= 'background-repeat: '.$group_info['bg_repeat'].'!important;';

	       	if($group_info['bg_position'] != '')
	           	$output .= 'background-position: '.$group_info['bg_position'].'!important;';

			$output .= '}';
			$output .= '</style>';
			$menusize = 0;

			if ( ! empty($tree))
			{
				foreach ($tree as &$leaf)
				{

					if( $CI->megamenulib->check_user_rights( $leaf['groups'] )){

	                        $output .= '<style>';

	                        	if($leaf['bg_hover_color'] != '')
	                            	$output .= '#mmpi-'.$leaf['id'].'.hover > .megam-has-subnav-link:hover { background-color: '.$leaf['bg_hover_color'].'; }';

	                        $output .= '#mmpi-'.$leaf['id'].' ul.megam-main-dropdown {';

	                        	if($leaf['bg_color'] != '')
	                            	$output .= ' background-color: '.$leaf['bg_color'].';';

	                        	if($leaf['bg_image'] != '')
	                            	$output .= 'background-image: url('.base_url('assets/uploads/menuimage/'.$leaf['bg_image']).');';

	                        	if($leaf['bg_repeat'] != '')
	                            	$output .= 'background-repeat: '.$leaf['bg_repeat'].'; ';

	                        	if($leaf['bg_size'] != '')
	                            	$output .= 'background-size: '.$leaf['bg_size'].'; ';

	                        	if($leaf['bg_position'] != '')
	                            	$output .= 'background-position: '.$leaf['bg_position'].'; ';

	                        $output .= '}';
	                        $output .= '</style>';

						$leaf['mname'] = htmlspecialchars($leaf['mname'], ENT_QUOTES, 'UTF-8');

						if (isset($leaf['children']) && ! empty($leaf['children']))
						{
							$output .= '<li class="megam-has-subnav" id="mmpi-'.$leaf['id'].'"><a href="' . $leaf['url'] . '" class="megam-has-subnav-link">' . $leaf['mname'];
							$output .= $leaf['icon'] ? ' <i class="'.$leaf['icon'].'" style="'.$fontsize.''.$fontcolor.'"></i>' : '';
							$output .='</a>';
							$output .= '<ul class="megam-main-dropdown subnav">'.build_tree_sub($group, $leaf['children']).'</ul>';
							$output .= '</li>';
						}
						else
						{
							$output .= '<li class="megam-has-subnav" id="mmpi-'.$leaf['id'].'"><a href="' . $leaf['url'] . '" class="megam-has-subnav-link">' . $leaf['mname'];
							$output .= $leaf['icon'] ? ' <i class="'.$leaf['icon'].'" style="'.$fontsize.''.$fontcolor.'"></i>' : '';
							$output .= '</a></li>';
						}
						$menusize++;
					} // if end
				} // foreach end

			}
	        $output .= '</ul><div class="clearfix"></div>
	        </nav>';

			$output .= '<style>';
			$widthis = floor(100 / $menusize);
			$output .= '@media (min-width: 990px) {.megam-main-nav > li {float: left;width: '.$widthis.'%;!important}}';
			$output .= '</style>';
			return $output;
		} // check user rights
	}
}

if ( ! function_exists('build_tree_sub'))
{
	function build_tree_sub($group, &$tree = array())
	{
		$CI =& get_instance();
		if (empty($tree))
		{

			$CI->load->library('megamenulib');

			$tree = $CI->megamenulib->get_all($group);
			$tree = parse_children($tree);
		}

		$output = '';

		if ( ! empty($tree))
		{


			$output .= '<div class="row">';
			foreach ($tree as &$leaf)
			{
				if( $CI->megamenulib->check_user_rights( $leaf['groups'] )){
					$leaf['mname'] = htmlspecialchars($leaf['mname'], ENT_QUOTES, 'UTF-8');


					if($leaf['font'] !=''){
						echo  '<link href="//fonts.googleapis.com/css?family='.$leaf['font'].'" rel="stylesheet">';
					}

						echo '<style>';

						echo '.leaf-'.$leaf['id'].'{';
						echo $leaf['bg_color'] ? 'background-color:'.$leaf['bg_color'].';' : '';
						echo $leaf['bg_image'] ? 'background-image:url('.base_url('assets/uploads/menuimage/'.$leaf['bg_image']).');' : '';
						echo $leaf['bg_repeat'] ? 'background-repeat:'.$leaf['bg_repeat'].';' : '';
						echo $leaf['bg_size'] ? 'background-size:'.$leaf['bg_size'].';' : '';
						echo $leaf['bg_position'] ? 'background-position:'.$leaf['bg_position'].';' : '';
                        echo '}';

						echo '.leaffonts-'.$leaf['id'].', .leaffonts-'.$leaf['id'].' > a {';
						if($leaf['font'] !=''){
							echo $leaf['font'] ? 'font-family:'.str_replace('+',' ',$leaf['font']).'!important;' : '';
						}
						echo $leaf['fontsize'] ? 'font-size:'.$leaf['fontsize'].'px!important;' : '';
						echo $leaf['fontweight'] ? 'font-weight:'.$leaf['fontweight'].'!important;' : '';
						echo $leaf['fontcolor'] ? 'color:'.$leaf['fontcolor'].'!important;' : '';
						echo '}';

						echo '.leafcontent-'.$leaf['id'].', .leafcontent-'.$leaf['id'].' a , .leafcontent-'.$leaf['id'].' p {';
						if($leaf['font'] !=''){
							echo $leaf['font'] != '' ? 'font-family:'.str_replace('+',' ',$leaf['font']).'!important;' : '';
						}
						echo $leaf['fontcolor'] ? 'color:'.$leaf['fontcolor'].'!important;' : '';
						echo 'font-weight:normal;';
						echo 'font-size:12pt;';
						echo '}';
						echo '</style>';

					if (isset($leaf['children']) && ! empty($leaf['children']))
					{
						$output .= '<li class="megam-has-subnav"><a href="' . $leaf['url'] . '" class="megam-has-subnav-link leaffonts-'.$leaf['id'].'">' . $leaf['mname'] . '</a>';
						$output .= '<ul class="megam-main-dropdown subnav"><li class="megam-subnav-col">'.build_tree_sub($group, $leaf['children']).'</div></li></ul>';
						$output .= '</li>';
					}
					else
					{


						$output .= '<div class="leaf-'.$leaf['id'].' leaffonts-'.$leaf['id'].' ';

						if($leaf['grid'] != '' && $leaf['grid'] != 0){
							$output .= 'promo-body col-sm-12 col-md-'.$leaf['grid'];
						}
						$output .='">';
						$output .= '<a href="' . $leaf['url'] . '" class="megam-subnav-link leaffonts-'.$leaf['id'].'">';
						$output .= $leaf['icon'] ? '<i class="'.$leaf['icon'].'" class="leaffonts-'.$leaf['id'].'"></i> ' : '';
						$output .= $leaf['mname'];
						$output .= '</a>';

						if($leaf['content'] != '')
							$output .= '<hr class="clearfix w-100 white"><div class="leafcontent-'.$leaf['id'].'" style="word-wrap: break-word;max-width:100%">' . $leaf['content'] . '</div></div>';

					}

				}

			}
			$output .= '</div>';
		}

		return $output;
	}
}

//--------------------------------------------------------------------

if ( ! function_exists('build_breadcrumb'))
{
	function build_breadcrumb($group, $item_id, $attributes = array(), &$tree = array(), &$output_tree = array())
	{
		if (empty($tree))
		{
			$CI =& get_instance();
			$CI->load->library('megamenulib');

			$tree = $CI->megamenulib->get_all($group);
		}

		if ( ! empty($tree))
		{
			foreach ($tree as &$leaf)
			{
				if ($item_id === (int) $leaf['id'])
				{
					array_push($output_tree, $leaf);

					build_breadcrumb($group, (int) $leaf['parent_id'], $attributes, $tree, $output_tree);
				}
			}

			return format_breadcrumb(array_reverse($output_tree), $item_id, $attributes);
		}

		return '';
	}
}

//--------------------------------------------------------------------

if ( ! function_exists('format_breadcrumb'))
{
	function format_breadcrumb($array, $item_id, $attributes = array())
	{
		foreach (array('start_tag' => '<li>', 'end_tag' => '</li>', 'start_tag_active' => '<li class="active">', 'divider' => ' <span class="divider">/</span>') as $key => $val)
		{
			$atts[$key] = ( ! isset($attributes[$key])) ? $val : $attributes[$key];
			unset($attributes[$key]);
		}

		$output = '';

		if ( ! empty($array) && is_array($array))
		{
			foreach ($array as &$item)
			{
				$item['mname'] = htmlspecialchars($item['mname'], ENT_QUOTES, 'UTF-8');

				if ($item_id === (int) $item['id'])
				{
					$output .= $atts['start_tag_active'] . $item['mname'] . $atts['end_tag'];
				}
				else
				{
					$output .= $atts['start_tag'] . '<a href="' . $item['url'] . '">' . $item['mname'] . '</a>' . $atts['divider'] . $atts['end_tag'];
				}
			}
		}

		return $output;
	}
}

//--------------------------------------------------------------------

if ( ! function_exists('build_tree_item'))
{
	function build_tree_item($group, $item_id, $attributes = array(), &$tree = NULL, &$in_array = array())
	{
		if (empty($tree))
		{
			$CI =& get_instance();
			$CI->load->library('megamenulib');

			$tree = $CI->megamenulib->get_all($group);
		}

		if ( ! empty($tree))
		{
			foreach ($tree as &$leaf)
			{
				if ($item_id === (int) $leaf['id'])
				{
					array_push($in_array, $leaf['id']);

					build_tree_item($group, (int) $leaf['parent_id'], $attributes, $tree, $in_array);
				}
			}

			$tree     = parse_children($tree);
			$in_array = array_reverse($in_array);

			return format_tree($tree, $in_array, $attributes);
		}

		return '';
	}
}

//--------------------------------------------------------------------

if ( ! function_exists('format_tree'))
{
	function format_tree(&$tree, &$in_array, $attributes = array())
	{
		foreach (array('start_tag' => '<li>', 'end_tag' => '</li>', 'sub_start_tag' => '<ul>', 'sub_end_tag' => '</ul>') as $key => $val)
		{
			$atts[$key] = ( ! isset($attributes[$key])) ? $val : $attributes[$key];
			unset($attributes[$key]);
		}

		$output = '';

		if ( ! empty($tree))
		{
			foreach ($tree as &$leaf)
			{
				$leaf['mname'] = htmlspecialchars($leaf['mname'], ENT_QUOTES, 'UTF-8');

				if (isset($leaf['children']) && ! empty($leaf['children']) && (in_array($leaf['id'], $in_array)))
				{
					$output .= $atts['start_tag'] . '<a href="' . $leaf['url'] . '">' . $leaf['mname'] . '</a>';
					$output .= $atts['sub_start_tag'] . format_tree($leaf['children'], $in_array, $attributes) . $atts['sub_end_tag'];
					$output .= $atts['end_tag'];
				}
				else
				{
					$output .= $atts['start_tag'] . '<a href="' . $leaf['url'] . '">' . $leaf['mname'] . '</a>' . $atts['end_tag'];
				}
			}
		}

		return $output;
	}
}

//--------------------------------------------------------------------

if ( ! function_exists('parse_children'))
{
	function parse_children(&$query)
	{
		! empty($query) OR FALSE;

		$tree = array();

		foreach ($query as &$row)
		{
			$tree[$row['id']] = $row;
		}

		unset($query);

		$tree_array = array();

		foreach ($tree as &$leaf)
		{
			if (array_key_exists($leaf['parent_id'], $tree))
			{
				$tree[$leaf['parent_id']]['children'][] = &$tree[$leaf['id']];
			}

			if ( ! isset($tree[$leaf['id']]['children']))
			{
				$tree[$leaf['id']]['children'] = array();
			}

			if ( (int) $leaf['parent_id'] === 0)
			{
				$tree_array[] = &$tree[$leaf['id']];
			}
		}

		return $tree_array;
	}
}
/* End of file megamenulib_helper.php */
/* Location: ./helpers/megamenulib_helper.php */
