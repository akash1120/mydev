<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel">Edit Country</h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("system_settings/edit_country/" . $id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <label>Name*</label>
                <?= form_input('name', set_value('code', $country->name), 'class="form-control tip" id="name" required="required"'); ?>
            </div>

            <div class="form-group">
                <label>ISO Code (2)*</label>
                <?= form_input('iso_code_2', set_value('iso_code_2', $country->iso_code_2), 'class="form-control tip" id="iso_code_2" required="required"'); ?>
            </div>

            <div class="form-group">
                <label>ISO Code (3)*</label>
                <?= form_input('iso_code_3', $country->iso_code_3, 'class="form-control tip" id="iso_code_3" required="required"'); ?>
            </div>

            <div class="form-group">
                <label>Currency</label>

                <select name="currency_id" style="width: 100%">
                    <option value="">Select Currency </option>
                    <?php
                    foreach ($all_currencies as $currency) {
                        ?>
                        <option value="<?php echo  $currency->id ?>"  <?php if($country->currency_id == $currency->id ){ echo 'selected';}?>> <?php echo  $currency->name ?> </option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label>Price Group</label>
                <select name="price_group_id" style="width: 100%">
                    <option value="">Select Price Group </option>
                    <?php
                    foreach ($all_pricegroups as $pgroup) {
                        ?>
                        <option value="<?php echo  $pgroup->id ?>" <?php if($country->price_group_id == $pgroup->id ){ echo 'selected';}?>> <?php echo  $pgroup->name ?> </option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <input type="checkbox" value="1" name="vat" id="vat" <?= $country->vat ? 'checked="checked"' : ''; ?>>
                <label class="padding-left-10" for="auto_update">UAE VAT Applicable?</label>
            </div>

            <div class="form-group">
                <label>Address Format </label>
                <textarea name="address_format" class="form-control" placeholder="Address Format"><?=  $country->address_format ?></textarea>
            </div>
            <div class="form-group">
                <label>Postcode Required</label>
                <label class="radio-inline" style="margin-top: 0px;">
                    <input type="radio" name="postcode_required" value="1" <?php if($country->postcode_required == 1) echo 'checked';?>>Yes
                </label>
                <label class="radio-inline">
                    <input type="radio" name="postcode_required" value="0" <?php if($country->postcode_required == 0) echo 'checked';?>>No
                </label>
            </div>

            <div class="form-group">
                <label>Status</label>
                <select name="status" style="width: 100%">
                    <option value="1" <?php if($country->status == 1) echo 'selected';?>> Enabled </option>
                    <option value="0" <?php if($country->status == 0) echo 'selected';?>> Disabled </option>
                </select>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('edit_country', 'Edit Country', 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>
