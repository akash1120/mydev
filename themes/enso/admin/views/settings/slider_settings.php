<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-home"></i><?= $page_title ?></h2>


    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?php echo lang('update_info'); ?></p>
                <?php
                $attrib = array('role' => 'form');
                echo admin_form_open_multipart("system_settings/slider_settings/" . $slider_id, $attrib)
                ?>
                <div class="col-md-12">

                    <?php
                    $conutimag=1;
                    if(!empty($slider_data)){
                      /*  echo "<pre>";
                        print_r($slider_data);
                        exit('fg');*/
                        foreach ($slider_data as $ky => $silde_data){
                            $image_product_gal_saved = 'ucloud/plugins/filepreviewer/site/resize_image_inline.php?f='.$silde_data->image.'&w=160&h=134&m=medium&uh=&o=jpg';

                            ?>

                            <div class="panel panel-default padding10 border_radius3 slider_wrapper" style="padding: 10px 10px !important;">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group1 gallery_wrapper">
                                            <div class="form-group all col-md-8" style="padding-top: 50px; padding-left: 0px;">
                                                <div class="input-group">
                                                    <input type="hidden" id="gimage_id<?= $conutimag;?>" name="image[]" readonly  value="<?= $silde_data->image ?>" class="form-control mediaimage_id" placeholder="Select Image">
                                                    <input type="text" id="gimage<?= $conutimag;?>" name="" readonly  value="<?= $image_product_gal_saved; ?>" class="form-control mediaimage" placeholder="Select Image">
                                                    <span class="input-group-btn ">
                                    <button id="mediagallery" class="btn btn-primary mediagallery width120" type="button">Select Image</button>
                                </span>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <label for="main_image" id="main_image" class="img_gallery_class mainboxmain_image" ><img id="main_image" class="img-thumbnail admin_product_imgsize boxmain_image" alt="" src="<?= base_url($image_product_gal_saved); ?>"></label>
                                            </div>
                                            <input class="alt_text" name="image_alt[]" value="<?= $silde_data->image_alt;?>" type="hidden">
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <?= lang('link', 'link1'); ?>
                                            <?= form_input('link[]', ($silde_data->link), 'class="form-control tip" id="link1"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <?= lang('caption', 'caption1'); ?>
                                            <?= form_textarea('caption[]', (isset($silde_data->caption) ?$silde_data->caption : ($slider_settings ? $silde_data->caption : '')), 'class="form-control border_radius3 tip" id="caption1"'); ?>


                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group pull-right">
                                           <a class="btn btn-success  remVargallery" ><i class="fa fa-trash"></i></a>
                                        </div>
                                       </div>
                                </div>



                            </div>

                            <?php
                            $conutimag++;
                        }
                    }else{
                        ?>

                        <div class="panel panel-default padding10 border_radius3" style="padding: 10px 10px !important;">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group all text-writing">
                                        <?= lang('image', 'image1'); ?>
                                        <div class="input-group">
                                            <input type="hidden" id="gimage1_id" name="image[]" class="form-control mediaimage_id" placeholder="Select Image">
                                            <input type="text" id="gimage1"  class="form-control mediaimage" placeholder="Select Image" value="" required="required">
                                            <span class="input-group-btn ">
                                    <button id="mediagallery" class="btn btn-primary mediagallery width120" type="button">Select Image</button>
                                </span>
                                        </div>
                                        <input class="alt_text" name="image_alt[]" value="" type="hidden">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= lang('link', 'link1'); ?>
                                        <?= form_input('link[]', set_value('link[]', (isset($slider_settings[0]->link) ? $slider_settings[0]->link : '')), 'class="form-control tip" id="link1"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang('caption', 'caption1'); ?>
                                        <textarea name="caption[]" class="form-control border_radius3 tip" id="caption1"></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>



                    <?php } ?>
                    <div id="newgalleryimage"></div>
                    <a class="btn btn-success pull-right" id="add_more_gallery_image"> Add More Slide</a>

                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <?php echo form_submit('edit_slider_setting', 'Submit', 'class="btn btn-primary btn_edit_product_custom"'); ?>
                    </div>

                </div>
                <?= form_close(); ?>

            </div>

        </div>
    </div>
</div>
<style>
    .h_iframe iframe {
        width:100%;
        height:700px;
    }
    .h_iframe {
        height: 100%;
        width:100%;
    }
</style>

<?php

$ukey=base64_encode('admin');
/*$pkey=base64_encode('admin123');*/
$pkey=base64_encode('password');

?>
<script src="<?= $assets ?>js/sweetalert.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('form[data-toggle="validator"]').bootstrapValidator({excluded: [':disabled'],});
        novalidate:false
    });

    var dynamic_id =  1;
    $('#add_more_gallery_image').on('click', function () {

        var html = '';


        html +=' <div class="panel panel-default padding10 border_radius3 slider_wrapper" style="padding: 10px 10px !important;">';
        html +='<div class="row">';
        html +='<div class="col-md-3">';
        html +='<div class="form-group all text-writing">';
        html +='<label for="image1">Image</label>';
        html +='<div class="input-group">';
        html +='<input type="hidden" id="gimage1_id'+dynamic_id+'" name="image[]" class="form-control mediaimage_id" placeholder="Select Image">';
        html +='<input type="text" id="gimage1'+dynamic_id+'"  class="form-control mediaimage" placeholder="Select Image" value="" required="required">';
        html +='<span class="input-group-btn ">';
        html +=' <button id="mediagallery" class="btn btn-primary  mediagallery width120" type="button">Select Image</button>';
        html +='</span>';
        html +='</div>';
        html +='<input class="alt_text" name="image_alt[]" value="" type="hidden">';
        html +='</div>';
        html +='</div>';
        html +='<div class="col-md-3">';
        html +='<div class="form-group">';
        html +='<label for="link1">Link</label>';
        html +='<input type="text" name="link[]" value="" class="form-control tip" id="link1" data-original-title="" title="">';
        html +='</div>';
        html +=' </div>';
        html +='<div class="col-md-6">';
        html +='<div class="form-group">';
        html +='<label for="caption1">Caption</label>';
        html +='<textarea name="caption[]" class="form-control texteditor border_radius3 tip" id="captione1"></textarea>';
        html +=' </div>';
        html +=' </div>';
        html +='<div class="col-md-12">';
        html +='<div class="form-group pull-right">';
        html +='<a class="btn btn-success  remVargallery" ><i class="fa fa-trash"></i></a>';
        html +='</div>';
        html +='</div>';
        html +=' </div>';
        html +='</div>';











        dynamic_id=dynamic_id+1;
        $('#newgalleryimage').append(html);

        $('.remVargallery').on('click', function () {
            $(this).parents('.slider_wrapper').remove();
            return false;
        });
        $('.texteditor').redactor({
            buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
            formattingTags: ['p', 'pre', 'h3', 'h4'],
            minHeight: 100,
            changeCallback: function (e) {
                var v = this.get();
                localStorage.setItem('ponote', v);
            }
        });

        mediaSettings();


    });
    $('.remVargallery').on('click', function () {
        $(this).parents('.slider_wrapper').remove();
        return false;
    });

    /*function mediaSettings() {
        $('.mediagallery').click(function() {
            // console.log($(this).parent().prev('.mediaimage').attr('id'));
            var imageid = $(this).parent().prev('.mediaimage').attr('id');
            $('.myModalee').modal('show');
            $("#myiframe").contents().find("#mypic").val(imageid);

        });
        $('.close').click(function() {
            // console.log($(this).parent().prev('.mediaimage').attr('id'));
            //var imageid = $(this).parent().prev('.mediaimage').attr('id');
            $('.myModalee').modal('hide');
            // $("#myiframe").contents().find("#mypic").val(imageid);

        });
    }*/

</script>
<!-- Modal -->

