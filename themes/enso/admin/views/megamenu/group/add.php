<?php include __DIR__ . '/../header.php'; ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-10">
            <?php
            echo section_header($title);
           // echo infoMessage($message);
            ?>
            <?php echo form_open('admin/megamenu/add_group', array('class'=>'form-horizontal','enctype'=>'multipart/form-data')); ?>

            <div class="md-form">
                <label for="gname" class=""><span class="red-text">*</span> <?php echo lang('megamenu_menu_name'); ?>:</label>
                <input type="text" class="form-control" id="gname" name="gname" value="<?php echo set_value('gname'); ?>">
                <?php echo form_error('gname','<div class="red-text">','</div>'); ?>
            </div>

            <div class="md-form mt-5">
                <?php echo lang('megamenu_menu_width'); ?>: <span class="input-group-addon ml-1 inlinedisplay"><span id="gridSliderVal"><?php echo set_value('width','960'); ?></span></span>
            </div>

            <div class="md-form mb-0 mt-1">
                <input class="w-100" id="width" name="width" data-slider-id="ex1Slider" type="text" data-slider-min="600" data-slider-max="2048" data-slider-step="10" data-slider-value="<?php echo set_value('width','960'); ?>" value="<?php echo set_value('width','960'); ?>"/>
            </div>

            <table class="w-100 mt-3" style="width:100%;">
                <tr><td class="w-50">
                        <?php echo lang('megamenu_font'); ?>:
                    </td><td class="w-50" >
                        <input type="text" class="form-control" id="font" name="font" value="<?php echo set_value('font'); ?>">
                    </td></tr>

                <tr><td class="w-50"></td><td class="w-50">
                        <input class="float-right btn btn-sm <?php echo config_item('button_color') .' '. config_item('button_color_text'); ?>-text waves-effect waves-light normalfontbutton" type="button" value="Use default font" id="usesysfont">
                    </td></tr>

                <tr><td class="w-50 pt-4">
                        <?php echo lang('megamenu_font_color'); ?>:
                    </td><td class="w-50 pt-4">
                        <input type="text" class="form-control" id="fontcolor" name="fontcolor" value="<?php echo set_value('fontcolor'); ?>">
                    </td></tr>

                <tr><td class="w-50 pt-4">
                        <?php echo lang('megamenu_font_size'); ?>: <span class="input-group-addon ml-1 inlinedisplay"><span id="fontsizeSliderVal"><?php echo set_value('fontsize', 14); ?> px</span></span>
                    </td><td class="w-50 pt-4">
                        <input class="w-100" id="fontsize" name="fontsize" data-slider-id="fontsizeSlider" type="text" data-slider-min="10" data-slider-max="40" data-slider-step="1" data-slider-value="<?php echo set_value('fontsize', 14); ?>" value="<?php echo set_value('fontsize', 14); ?>"/>
                    </td></tr>

                <tr><td class="w-50">
                        <?php echo lang('megamenu_font_weight'); ?>:
                    </td><td class="w-50 pt-3">
                        <?php echo form_dropdown('fontweight', $selectitem['fontweight'], set_value('fontweight') ,'class="" id="fontweight"'); ?>
                    </td></tr>

                <tr><td class="w-50 pt-4">
                        <?php echo lang('megamenu_bg_color'); ?>:
                    </td><td class="w-50 pt-4">
                        <input type="text" class="form-control" id="bg_color" name="bg_color" value="<?php echo set_value('bg_color'); ?>">
                    </td></tr>

                <tr><td class="w-100 pt-4" colspan="2">
                        <?php echo lang('megamenu_bg_image'); ?>:
                    </td></tr>

                <tr><td class="w-50">
                    </td><td class="w-50 pl-0 ml-0">
                        <label class="input-group-btn" style="z-index:1000">
				                    <span class="btn btn-sm <?php echo config_item('button_color') .' '. config_item('button_color_text');?>-text waves-effect waves-light normalfontbutton">
				                        Choose image<input id="bg_image" name="bg_image" type="file" class="" style="display: none;" value="<?php echo set_value('bg_image'); ?>">
				                    </span>
                        </label>
                    </td></tr>

                <tr class="forfile" style="display: none;"><td class="w-100" colspan="2">
                        <div class="md-form">
                            <input type="text" id="filename" class="form-control" readonly="" value="<?php echo set_value('bg_image'); ?>">
                        </div>
                    </td></tr>

                <tr><td class="w-50 pt-3">
                        <?php echo lang('megamenu_menu_bg_repeat'); ?>:
                    </td><td class="w-50 pt-3">
                        <?php echo form_dropdown('bg_repeat', $selectitem['repeat'], set_value('bg_repeat') ,'class="custom-select" id="bg_repeat"'); ?>
                    </td></tr>

                <tr><td class="w-50 pt-3">
                        <?php echo lang('megamenu_menu_bg_size'); ?>:
                    </td><td class="w-50 pt-3">
                        <?php echo form_dropdown('bg_size', $selectitem['size'], set_value('bg_size') ,'class="custom-select" id="bg_size"'); ?>
                    </td></tr>

                <tr><td class="w-50">
                        <?php echo lang('megamenu_bg_position'); ?>:
                    </td><td class="w-50 pt-3">
                        <?php echo form_dropdown('bg_position', $selectitem['bg_position'], set_value('bg_position','top left') ,'class="custom-select" id="bg_position"'); ?>
                    </td></tr>

                <tr><td class="w-50 pt-3">
                        <?php echo lang('megamenu_show_only_group'); ?>
                    </td><td class="w-50 pt-3">
                        <?php $menuGroups = set_value('groups'); ?>
                        <?php foreach ($groups as $group):?>
                            <?php
                            $gID=$group['id'];
                            $checked = null;
                            if(is_array($menuGroups)){
                                foreach($menuGroups as $grp) {
                                    if ($gID == $grp) {
                                        $checked= ' checked="checked"';
                                        break;
                                    }
                                }
                            }
                            ?>
                            <input type="checkbox" id="checkbox<?php echo $group['id'];?>" name="groups[]" value="<?php echo $group['id'];?>" <?php echo $checked;?>>
                            <label class="checkbox" for="checkbox<?php echo $group['id'];?>">
                                <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
                            </label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php endforeach?>
                    </td></tr>

            </table>

            <div class="m-5 text-center">
                <?php
                $data = array(
                    'type'          => 'submit',
                    'content'       => '<i class="fa fa-check '. config_item('button_color_text') .'-text left"></i>'.lang('megamenu_save_changes'),
                    'class'			=> 'btn '. config_item('button_color') .' '. config_item('button_color_text') .'-text waves-effect waves-light normalfontbutton'
                );

                echo form_button($data);?>
                <?php
                echo anchor(base_url('admin/megamenu'), '<i class="fa fa-times '. config_item('button_color_text') .'-text left"></i>'.lang('cancel'),'class="btn '. config_item('button_color') .' '. config_item('button_color_text') .'-text waves-effect waves-light normalfontbutton"');
                ?>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function(){
        $("#bg_color").spectrum({
            preferredFormat: "rgb",
            showInput: true,
            showAlpha: true,
            showPalette: true,
            color: "<?php echo set_value('bg_color'); ?>",
            allowEmpty: true,
        });

        $("#fontcolor").spectrum({
            preferredFormat: "hex",
            showInput: true,
            showAlpha: true,
            showPalette: true,
            color: "<?php echo set_value('fontcolor'); ?>",
            allowEmpty: true,
        });

        $('#usesysfont').click(function(){
            $('.font-select').remove();
            $('#font').val('');
            $('#font').fontselect();
        });

        $('#font').fontselect();

        $('#width,#fontsize').slider();

        $("#width").on("slide", function(slideEvt) {
            $("#gridSliderVal").text(slideEvt.value);
        });

        $("#fontsize").on("slide", function(slideEvt) {
            $("#fontsizeSliderVal").text(slideEvt.value + ' px');
        });

        jQuery('#bg_image').on('change', function() {
            $('#filename').val($(this).val());
            if($(this).val() == ''){
                $('#filenametr').hide();
            } else {
                $('#filenametr').show();
            }
        });

    })

</script>
<style>
    .slider{
        width:100%!important;
    }
</style>
<?php include __DIR__ . '/../footer.php'; ?>
