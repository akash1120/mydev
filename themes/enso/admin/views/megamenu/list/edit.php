<?php include __DIR__ . '/../header.php'; ?>
<div class="container" style=" background: #ffffff;">
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-10">
      	<?php
            		echo section_header($title,lang('megamenu_navigation').' '. $navigation['gname']);

      	//	echo infoMessage($message);
      		if($item['fontsize'] == '')
      		$item['fontsize'] = 14;

      		if($item['bg_color'] == '')
      		$item['bg_color'] = 'transparent';
      	?>
	            <?php
            	echo form_open('admin/megamenu/edit/'.$item['id'], array('class'=>'form-horizontal','enctype'=>'multipart/form-data')); ?>

					<div class="md-form">
						<label for="mname" class=""><span class="red-text">*</span> <?php echo lang('megamenu_menu_name'); ?>:</label>
						<input type="text" class="form-control" id="mname" name="mname" value="<?php echo set_value('mname', $item['mname']) ?>">
						<?php echo form_error('mname','<div class="red-text">','</div>'); ?>
					</div>

					<div class="md-form">
						<label for="url" class=""><span class="red-text">*</span> <?php echo lang('megamenu_menu_url'); ?>:</label>
						<input type="text" class="form-control" id="url" name="url" value="<?php echo set_value('url', $item['url']); ?>">
						<?php echo form_error('url','<div class="red-text">','</div>'); ?>
					</div>


					<table class="w-100 mt-4" style="width:100%;">
					<?php if($item['parent_id'] != 0){ ?>
						<tr><td class="w-50">
							<?php echo lang('megamenu_menu_parent'); ?>:
						</td><td class="w-50">
							<?php echo $dropdown[$item['parent_id']]; ?>
						</td></tr>
					<?php } ?>

					<tr><td class="w-50">
						<?php echo lang('megamenu_font'); ?>:
					</td><td class="w-50">
						<input type="text" class="form-control" id="font" name="font" value="<?php echo set_value('font',$item['font']); ?>">
					</td></tr>

					<tr><td class="w-50"></td><td class="w-50">
						<input class="float-right btn btn-sm <?php echo config_item('button_color') .' '. config_item('button_color_text'); ?>-text waves-effect waves-light normalfontbutton" type="button" value="Use default font" id="usesysfont">
					</td></tr>

					<tr><td class="w-50 pt-4">
						<?php echo lang('megamenu_font_color'); ?>:
					</td><td class="w-50 pt-4">
						<input type="text" class="form-control" id="fontcolor" name="fontcolor" value="<?php echo set_value('fontcolor',$item['fontcolor']); ?>">
					</td></tr>

					<tr><td class="w-50 pt-4">
						<?php echo lang('megamenu_font_size'); ?>: <span class="input-group-addon ml-1 inlinedisplay"><span id="fontsizeSliderVal"><?php echo set_value('fontsize', $item['fontsize']); ?> px</span></span>
					</td><td class="w-50 pt-4">
						<input class="w-100" id="fontsize" name="fontsize" data-slider-id="fontsizeSlider" type="text" data-slider-min="10" data-slider-max="40" data-slider-step="1" data-slider-value="<?php echo set_value('fontsize', $item['fontsize']); ?>" value="<?php echo set_value('fontsize', $item['fontsize']); ?>"/>
					</td></tr>

					<tr><td class="w-50">
						<?php echo lang('megamenu_font_weight'); ?>:
					</td><td class="w-50 pt-3">
						<?php echo form_dropdown('fontweight', $selectitem['fontweight'], set_value('fontweight',$item['fontweight']) ,'class="custom-select" id="fontweight"'); ?>
					</td></tr>

					<tr><td class="w-50 pt-4">
						<?php echo lang('megamenu_menu_bg_color'); ?>:
					</td><td class="w-50 pt-4">
						<input type="text" class="form-control" id="bg_color" name="bg_color" value="<?php echo set_value('bg_color',$item['bg_color']); ?>">
					</td></tr>

					<?php if($item['parent_id'] == 0){ ?>
						<tr class="parentmenu"><td class="w-50 pt-3">
							<?php echo lang('megamenu_menu_bg_hover_color'); ?>:
						</td><td class="w-50 pt-3">
							<input type="text" class="form-control" id="bg_hover_color" name="bg_hover_color" value="<?php echo set_value('bg_hover_color',$item['bg_hover_color']); ?>">
						</td></tr>
					<?php } ?>

					<tr><td class="w-50 pt-3">
						<?php echo lang('megamenu_menu_bg_image'); ?>:
					</td><td class="w-50 pt-3">
					<?php if($item['bg_image'] != '') {?>
							<img src="<?php echo base_url('assets/uploads/menuimage/'.$item['bg_image']) ?>" width="100" border="0">
							Delete image ? <input name="delete_image" type="checkbox" value="true">
						<?php }?>
					</td></tr>

					<tr><td class="w-50">
					</td><td class="w-50 pl-0 ml-0">
				                <label class="input-group-btn" style="z-index:1000">
				                    <span class="btn btn-sm <?php echo config_item('button_color') .' '. config_item('button_color_text');?>-text waves-effect waves-light normalfontbutton">
				                        Choose image<input id="bg_image" name="bg_image" type="file" style="display: none;" value="<?php echo set_value('bg_image'); ?>">
				                    </span>
				                </label>
					</td></tr>

					<tr class="forfile" style="display: none;"style="display: none;"><td class="w-100" colspan="2">
				                <div class="md-form">
			                		<input type="text" id="filename" class="form-control" readonly="" value="<?php echo set_value('bg_image'); ?>">
			                	</div>
					</td></tr>

					<tr><td class="w-50">
						<?php echo lang('megamenu_menu_bg_repeat'); ?>:
					</td><td class="w-50 pt-3">
						<?php echo form_dropdown('bg_repeat', $selectitem['repeat'], set_value('bg_repeat',$item['bg_repeat']) ,'class="custom-select" id="bg_repeat"'); ?>
					</td></tr>

					<tr><td class="w-50">
						<?php echo lang('megamenu_menu_bg_size'); ?>:
					</td><td class="w-50 pt-3">
						<?php echo form_dropdown('bg_size', $selectitem['size'], set_value('bg_size',$item['bg_size']) ,'class="custom-select" id="bg_size"'); ?>
					</td></tr>

					<tr><td class="w-50">
						<?php echo lang('megamenu_bg_position'); ?>:
					</td><td class="w-50 pt-3">
						<?php echo form_dropdown('bg_position', $selectitem['bg_position'], set_value('bg_position',$item['bg_position']) ,'class="custom-select" id="bg_position"'); ?>
					</td></tr>

					<tr><td class="w-50 pt-3">
						<?php echo lang('megamenu_menu_icon'); ?>:
					</td><td class="w-50 pt-3">
					<div class="md-form mt-1">
						<input data-placement="bottomRight" name="icon" id="icon" class="icp icp-auto" value="<?php echo set_value('icon',$item['icon']); ?>" type="text"/>
						<span class="input-group-addon iconpickercss"></span>
					</div>
					</td></tr>


					<?php if($item['parent_id'] != 0){ ?>

						<tr><td class="w-50 pt-3">
							<?php echo lang('megamenu_grid_length'); ?>: <span class="input-group-addon ml-1 inlinedisplay"><span id="gridSliderVal"><?php echo set_value('grid',$item['grid']); ?></span></span>
						</td><td class="w-50 pt-3">
							<input class="w-100" id="grid" name="grid" data-slider-id="ex1Slider" type="text" data-slider-min="0" data-slider-max="12" data-slider-step="1" data-slider-value="<?php echo set_value('grid',$item['grid']); ?>" value="<?php echo set_value('grid',$item['grid']); ?>"/>
						</td></tr>

					<?php } ?>

					<tr><td class="w-50 pt-3">
						<?php echo lang('megamenu_show_only_group'); ?>
					</td><td class="w-50 pt-3">
						<?php $menuGroups = unserialize($item['groups']); ?>
		                <?php foreach ($groups as $group):?>
	                    <?php
	                        $gID=$group['id'];
	                        $checked = null;
	                        if(is_array($menuGroups)){
		                        foreach($menuGroups as $grp) {
		                            if ($gID == $grp) {
		                                $checked= ' checked="checked"';
		                            break;
		                            }
		                        }
	                        }
	                    ?>
		                    <input type="checkbox" id="checkbox<?php echo $group['id'];?>" name="groups[]" value="<?php echo $group['id'];?>" <?php echo $checked;?>>
		                    <label class="checkbox" for="checkbox<?php echo $group['id'];?>">
		                    <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
		                    </label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		                <?php endforeach?>
					</td></tr>

						<tr class="submenu"><td class="w-100 mt-3 pt-3" colspan="2">
							Content:
						</td></tr>
						<tr><td class="w-100" colspan="2">
						<div class="md-form">
							<textarea id="content1" name="content" class="md-textarea md-textarea-auto form-control"><?php echo set_value('content',$item['content']); ?></textarea>
						</div>
						</td></tr>
					</table>

		            <div class="m-5 text-center">
		        	<?php
						$data = array(
						        'type'          => 'submit',
						        'content'       => '<i class="fa fa-check '. config_item('button_color_text') .'-text left"></i>'.lang('megamenu_save_changes'),
						        'class'			=> 'btn '. config_item('button_color') .' '. config_item('button_color_text') .'-text waves-effect waves-light normalfontbutton'
						);

		        	echo form_button($data);?>
		        	<?php
		        	    echo anchor(base_url('admin/megamenu'), '<i class="fa fa-times '. config_item('button_color_text') .'-text left"></i>'.lang('cancel'),'class="btn '. config_item('button_color') .' '. config_item('button_color_text') .'-text waves-effect waves-light normalfontbutton"');
		        	    ?>
		            </div>

		         <?php echo form_hidden($csrf); ?>
                <?php echo form_close(); ?>

            </div>
        </div>
    </div>

<script>
jQuery(document).ready(function(){
	$("#bg_color").spectrum({
		preferredFormat: "rgb",
	    showInput: true,
	    showAlpha: true,
	    showPalette: true,
	    color: "<?php echo set_value('bg_color',$item['bg_color']); ?>",
	    allowEmpty: true,
	});

	$("#bg_hover_color").spectrum({
		preferredFormat: "rgb",
	    showInput: true,
	    showAlpha: true,
	    showPalette: true,
	    color: "<?php echo set_value('bg_hover_color',$item['bg_hover_color']); ?>",
	    allowEmpty: true,
	});

	$("#fontcolor").spectrum({
		preferredFormat: "hex",
	    showInput: true,
	    showAlpha: true,
	    showPalette: true,
	    color: "<?php echo set_value('fontcolor', $item['fontcolor']); ?>",
	    allowEmpty: true,
	});

	$('#usesysfont').click(function(){
		$('.font-select').remove();
		$('#font').val('');
		$('#font').fontselect();
	});

    $('#font').fontselect();

	$("#icon").iconpicker({
		animation: false,
		hideOnSelect: true,
	});

	$('#grid, #fontsize').slider();

	$("#grid").on("slide", function(slideEvt) {
		$("#gridSliderVal").text(slideEvt.value);
	});

	$("#fontsize").on("slide", function(slideEvt) {
		$("#fontsizeSliderVal").text(slideEvt.value + ' px');
	});

      jQuery('#bg_image').on('change', function() {
         $('#filename').val($(this).val());
         if($(this).val() == ''){
         	$('.forfile').hide();
         } else {
         	$('.forfile').show();
         }
      });

      jQuery('#bg_image').trigger('change');

	  tinymce.init({
	  	selector:'#content1',
	  	relative_urls: false,
	    plugins : 'textpattern autolink link image lists media paste colorpicker textcolor hr autoresize code table',
	    toolbar: 'styleselect | forecolor backcolor | link image media | paste | bold italic | fontsizeselect | alignleft alignright aligncenter alignjustify | code',
	    paste_data_images: true,
	    automatic_uploads: true,
	    valid_elements : '*[*]',
	    file_picker_types: 'image',
			file_picker_callback: function(cb, value, meta) {
			    var input = document.createElement('input');
			    input.setAttribute('type', 'file');
			    input.setAttribute('accept', 'image/*');

			    // Note: In modern browsers input[type="file"] is functional without
			    // even adding it to the DOM, but that might not be the case in some older
			    // or quirky browsers like IE, so you might want to add it to the DOM
			    // just in case, and visually hide it. And do not forget do remove it
			    // once you do not need it anymore.

			    input.onchange = function() {
			      var file = this.files[0];

			      var reader = new FileReader();
			      reader.onload = function () {
			        // Note: Now we need to register the blob in TinyMCEs image blob
			        // registry. In the next release this part hopefully won't be
			        // necessary, as we are looking to handle it internally.
			        var id = 'blobid' + (new Date()).getTime();
			        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
			        var base64 = reader.result.split(',')[1];
			        var blobInfo = blobCache.create(id, file, base64);
			        blobCache.add(blobInfo);

			        // call the callback and populate the Title field with the file name
			        cb(blobInfo.blobUri(), { title: file.name });
			      };
			      reader.readAsDataURL(file);
			    };

			    input.click();
			  },
	    image_title: true,
	    menubar: "edit format insert",
	    height : 700
	   });


})

</script>
<?php include __DIR__ . '/../footer.php'; ?>