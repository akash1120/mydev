<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script src="<?=$assets.'js/ck/ckeditor/ckeditor.js' ?>"></script>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('create_sizing_guide'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?php echo lang('enter_info'); ?></p>
                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                echo admin_form_open_multipart("products/create_sizing_guide", $attrib)
                ?>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Title:</label><span></span>
                        <?= form_input('title', (isset($_POST['title']) ? $_POST['title'] : ($sizing_guide ? $sizing_guide->title : '')), 'class="form-control" placeholder="Title" id="title" required="required"'); ?>
                    </div>
                </div>


                <!-- start of Tab1 -->
                <div class="col-md-12">
                    <div class="form-group">
                        <h2>- Tab 1 Information</h2>
                        <div class="card-box well well-lg col-md-12">

                            <span class="col-md-8">
                                <?= lang("Label", "label") ?> (Max 20 Character)
                                <?= form_input('tab1_label', (isset($_POST['tab1_label']) ? $_POST['tab1_label'] : ($sizing_guide ? $sizing_guide->tab1_label : '')), 'class="form-control" placeholder="Label for 1st tab" id="tab1_label" required="required"'); ?>
                            </span>

                            <span class="col-md-12">
                                <?= lang("Description", "tab1_description") ?>
                                <?= form_textarea('tab1_description', (isset($_POST['tab1_description']) ? $_POST['tab1_description'] : ($sizing_guide ? $sizing_guide->tab1_description : '')), 'class="form-control" placeholder="Description for 1st tab" id="tab1_description"'); ?>
                            </span>

                        </div>
                    </div>
                </div>
                <!-- End of Tab1 -->

                <!-- start of Tab2 -->
                <div class="col-md-12">
                    <div class="form-group">
                        <h2>- Tab 2 Information</h2>
                        <div class="card-box well well-lg col-md-12">

                            <span class="col-md-8">
                                <?= lang("Label", "label") ?> (Max 20 Character)
                                <?= form_input('tab2_label', (isset($_POST['tab2_label']) ? $_POST['tab2_label'] : ($sizing_guide ? $sizing_guide->tab2_label : '')), 'class="form-control" placeholder="Label for 2nd tab" id="tab2_label"'); ?>
                            </span>

                            <span class="col-md-12">
                                <?= lang("Description", "tab2_description") ?>
                                <?= form_textarea('tab2_description', (isset($_POST['tab2_description']) ? $_POST['tab2_description'] : ($sizing_guide ? $sizing_guide->tab2_description : '')), 'class="form-control" placeholder="Description for 2nd tab" id="tab2_description"'); ?>
                            </span>

                        </div>
                    </div>
                </div>
                <!-- End of Tab2 -->

                <!-- start of Tab3 -->
                <div class="col-md-12">
                    <div class="form-group">
                        <h2>- Tab 3 Information</h2>
                        <div class="card-box well well-lg col-md-12">

                            <span class="col-md-8">
                                <?= lang("Label", "label") ?> (Max 20 Character)
                                <?= form_input('tab3_label', (isset($_POST['tab3_label']) ? $_POST['tab3_label'] : ($sizing_guide ? $sizing_guide->tab3_label : '')), 'class="form-control" placeholder="Label for 3rd tab" id="tab3_label"'); ?>
                            </span>

                            <span class="col-md-12">
                                <?= lang("Description", "tab3_description") ?>
                                <?= form_textarea('tab3_description', (isset($_POST['tab3_description']) ? $_POST['tab3_description'] : ($sizing_guide ? $sizing_guide->tab3_description : '')), 'class="form-control" placeholder="Description for 3rd tab" id="tab3_description"'); ?>
                            </span>

                        </div>
                    </div>
                </div>
                <!-- End of Tab3 -->

                <!-- start of Tab4 -->
                <div class="col-md-12">
                    <div class="form-group">
                        <h2>- Tab 4 Information</h2>
                        <div class="card-box well well-lg col-md-12">

                            <span class="col-md-8">
                                <?= lang("Label", "label") ?> (Max 20 Character)
                                <?= form_input('tab4_label', (isset($_POST['tab4_label']) ? $_POST['tab4_label'] : ($sizing_guide ? $sizing_guide->tab4_label : '')), 'class="form-control" placeholder="Label for 4th tab" id="tab4_label"'); ?>
                            </span>

                            <span class="col-md-12">
                                <?= lang("Description", "tab4_description") ?>
                                <?= form_textarea('tab4_description', (isset($_POST['tab4_description']) ? $_POST['tab4_description'] : ($sizing_guide ? $sizing_guide->tab4_description : '')), 'class="form-control" placeholder="Description for 4th tab" id="tab4_description"'); ?>
                            </span>

                        </div>
                    </div>
                </div>
                <!-- End of Tab3 -->



                <div class="col-md-12">
                    <div class="form-group">
                        <a class="btn btn-success" href="<?= admin_url();?>products/sizing_guide">Cancel</a>
                        <?php echo form_submit('create_sizing', $this->lang->line("create_sizing_guide"), 'class="btn btn-success"'); ?>
                    </div>
                </div>

                <?= form_close(); ?>
            </div>

        </div>
    </div>
</div>



<script type="text/javascript">

    $(window).load(function () {
        //Hide Redactor tool bar
        $("#redactor_toolbar_0").next('div').hide();
        $("#redactor_toolbar_0").hide();

        $("#redactor_toolbar_1").next('div').hide();
        $("#redactor_toolbar_1").hide();

        $("#redactor_toolbar_2").next('div').hide();
        $("#redactor_toolbar_2").hide();

        $("#redactor_toolbar_3").next('div').hide();
        $("#redactor_toolbar_3").hide();
    });


    CKEDITOR.replace('tab1_description',{
        filebrowserBrowseUrl: '/browser/browse.php',
        filebrowserImageBrowseUrl: '/browser/browse.php?type=Images',
        filebrowserUploadUrl: '/uploader/upload.php',
        filebrowserImageUploadUrl: '/uploader/upload.php?type=Images',
        filebrowserWindowWidth: '900',
        filebrowserWindowHeight: '400',
        filebrowserBrowseUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/ckfinder.html?Type=Images',
        filebrowserImageBrowseUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/ckfinder.html?Type=Images',
        filebrowserFlashBrowseUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/ckfinder.html?Type=Flash',
        filebrowserUploadUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/core/connctor/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    });﻿

    CKEDITOR.replace('tab2_description',{
        filebrowserBrowseUrl: '/browser/browse.php',
        filebrowserImageBrowseUrl: '/browser/browse.php?type=Images',
        filebrowserUploadUrl: '/uploader/upload.php',
        filebrowserImageUploadUrl: '/uploader/upload.php?type=Images',
        filebrowserWindowWidth: '900',
        filebrowserWindowHeight: '400',
        filebrowserBrowseUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/ckfinder.html?Type=Images',
        filebrowserImageBrowseUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/ckfinder.html?Type=Images',
        filebrowserFlashBrowseUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/ckfinder.html?Type=Flash',
        filebrowserUploadUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/core/connctor/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    });﻿

    CKEDITOR.replace('tab3_description',{
        filebrowserBrowseUrl: '/browser/browse.php',
        filebrowserImageBrowseUrl: '/browser/browse.php?type=Images',
        filebrowserUploadUrl: '/uploader/upload.php',
        filebrowserImageUploadUrl: '/uploader/upload.php?type=Images',
        filebrowserWindowWidth: '900',
        filebrowserWindowHeight: '400',
        filebrowserBrowseUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/ckfinder.html?Type=Images',
        filebrowserImageBrowseUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/ckfinder.html?Type=Images',
        filebrowserFlashBrowseUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/ckfinder.html?Type=Flash',
        filebrowserUploadUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/core/connctor/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    });﻿

    CKEDITOR.replace('tab4_description',{
        filebrowserBrowseUrl: '/browser/browse.php',
        filebrowserImageBrowseUrl: '/browser/browse.php?type=Images',
        filebrowserUploadUrl: '/uploader/upload.php',
        filebrowserImageUploadUrl: '/uploader/upload.php?type=Images',
        filebrowserWindowWidth: '900',
        filebrowserWindowHeight: '400',
        filebrowserBrowseUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/ckfinder.html?Type=Images',
        filebrowserImageBrowseUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/ckfinder.html?Type=Images',
        filebrowserFlashBrowseUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/ckfinder.html?Type=Flash',
        filebrowserUploadUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/core/connctor/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl: '<?= base_url() ?>themes/default/admin/assets/js/ck/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    });﻿
</script>