<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
    $getAllGeoZones = $this->site->getAllGeoZones();
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_payment_method'); ?></h4>
        </div>

        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open_multipart("products/add_shipment_methods", $attrib); ?>

        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>


            <ul class="nav nav-tabs">
                <li id="flate_rate" class="active tab_payment_method"><a data-toggle="tab" href="#tab1">Flate Rate</a></li>
                <li id="aramex" class="tab_payment_method"><a data-toggle="tab" href="#tab2">Aramex</a></li>
            </ul>

            <div class="tab-content">
                <div id="tab1" class="tab-pane fade in active">
                    <h3>Flate Rate</h3>
                    <p>Flat rates are fixed rates.They are fixed for a set of parameters and for a particular geography. It is the default shipping method of Checkout and it is set by the owner itself.</p>
                </div>
                <div id="tab2" class="tab-pane fade in">
                    <h3>Aramex</h3>
                    <div class="form-group">
                        <label for="account_country_code">Account Country Code</label>
                        <?= form_input('account_country_code', set_value('account_country_code'), 'class="form-control" id="account_country_code" '); ?>
                    </div>
                    <div class="form-group">
                        <label for="account_entity">Account Entity</label>
                        <?= form_input('account_entity', set_value('account_entity'), 'class="form-control" id="account_entity" '); ?>
                    </div>
                    <div class="form-group">
                        <label for="account_number">Account Number</label>
                        <?= form_input('account_number', set_value('account_number'), 'class="form-control" id="account_number" '); ?>
                    </div>
                    <div class="form-group">
                        <label for="account_pin">Account Pin</label>
                        <?= form_input('account_pin', set_value('account_pin'), 'class="form-control" id="account_pin" '); ?>
                    </div>
                    <div class="form-group">
                        <label for="user_name">User Name</label>
                        <?= form_input('user_name', set_value('user_name'), 'class="form-control" id="user_name" '); ?>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <?= form_input('password', set_value('password'), 'class="form-control" id="password" '); ?>
                    </div>
                    <div class="form-group">
                        <label for="version">Version</label>
                        <?= form_input('version', set_value('version'), 'class="form-control" id="version" '); ?>
                    </div>

                    <label class="hide" for="shipment_type">Shipment Type</label>
                    <div class="form-group hide">
                        <input type="radio" checked="checked" class="checkbox" name="shipment_type" value="1" id="normal_delivery">
                        <label for="normal_delivery" class="padding05">Normal Delivery</label>

                        <input type="radio" class="checkbox" name="shipment_type" value="2" id="fast_delivery">
                        <label for="fast_delivery" class="padding05">Fast Delivery</label>
                    </div>

                    <hr/>
                </div>
            </div>


            <div class="form-group">
                <?= lang('shipment_name', 'name'); ?>
                <?= form_input('name', set_value('name'), 'class="form-control gen_slug" id="name" '); ?>
            </div>

            <div class="form-group">
                <?= lang('shipment_code', 'code'); ?>
                <?= form_input('code', set_value('code'), 'class="form-control" id="code" '); ?>
            </div>

            <div class="form-group">
                <label for="geo_zone">Geo Zones</label>
                <select id="geo_zone" name="geo_zone" class="form-control">
                    <option value="">All Zones</option>
                    <?php foreach ($getAllGeoZones as $zone){?>
                        <option value="<?= $zone->geo_zone_id ?>"><?= $zone->name?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-group">
                <label for="total">Fixed Charges<span style="font-size: 10px;">(The checkout fixed charges. if you want to add any fixed charges for using shipment.)</span></label>
                <?= form_input('fixed_charges', set_value('fixed_charges'), 'class="form-control" id="fixed_charges"'); ?>
            </div>



            <div class="form-group">
                <?= lang("shipment_image", "image") ?>
                <input id="image" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false"
                       class="form-control file">
            </div>


            <div class="form-group" style="margin-bottom: 0">
                <label for="free_over">Free Ship Over</label>
            </div>
            <div class="form-group col-md-4" style="padding-left: 0">
                <?= form_input('free_ship_amount', set_value('name',$shipment_method->free_ship_amount), 'class="form-control" id="name" placeholder="Type amount here" '); ?>
            </div>
            <div class="form-group col-md-4" style="padding-right:0; padding-left: 0; ">
                <select id="free_amount_status" name="free_amount_status" class="form-control">
                        <option <?= $shipment_method->free_amount_status == 'above' ? 'selected' : '' ?> value="above">Above</option>
                        <option <?= $shipment_method->free_amount_status == 'below' ? 'selected' : '' ?> value="below">Below</option>
                </select>
            </div>
            <div class="form-group col-md-4" style="padding-right:0 ">
                <select id="free_ship_zone" name="free_ship_zone" class="form-control">
                    <option value="">All Zones</option>
                    <?php foreach ($getAllGeoZones as $zone){?>
                        <option <?= $shipment_method->free_ship_zone == $zone->geo_zone_id ? 'selected' : '' ; ?> value="<?= $zone->geo_zone_id ?>"><?= $zone->name?></option>
                    <?php } ?>
                </select>
            </div>



            <div class="form-group all">
                <?= lang('status', 'status'); ?><br/>
                <input id="status" class="form-control tip" type="checkbox" name="status" value="1" <?= $category->status == 1 ? 'checked' : '' ?>> Check that if you want to enable it
            </div>

            <div class="form-group all">
                <?= lang('shipment_instruction', 'instruction'); ?><br/>
                <textarea name="instruction" id="instruction" class="form-control tip"></textarea>
            </div>

            <input name="secret_code" type="hidden" id="secret_code" value="flate_rate">

        </div>
        <div class="modal-footer">
            <?php echo form_submit('add_shipment_method', lang('add_shipment_method'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>



</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
<script>
    $(document).ready(function() {
        $('.gen_slug').change(function(e) {
            getSlug($(this).val(), 'category');
        });

        $('.tab_payment_method').click(function(e) {
           $("#secret_code").val(this.id);
        });

    });
</script>