<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $get_categories = $this->site->getAllCategories();?>
<?php $get_warehouse = $this->site->getAllWarehouses(); ?>

<?php
    if($upselling->type == 'buyOneGetCategoryFree'){
        $buyOneGetCategoryFree = true;
    }else if($upselling->type == 'buyCategoryWithPercentage'){
        $buyCategoryWithPercentage = true;
    }else if($upselling->type == 'buyProductsWithPercentage'){
        $buyProductsWithPercentage = true;
    }else if($upselling->type == 'cartvaluerangeFree'){
        $cartvaluerangeFree = true;
    }

    $get_selected_warehouse = $this->site->selectedPromotionalWarehouses($upselling->upselling_id);
    $get_selected_category = $this->site->selectedPromotionalCategories($upselling->upselling_id);
    if($upselling->item_id != ''){
        $itemsx = $this->site->selectedPromotionalItems($upselling->upselling_id);
        foreach($itemsx as $record){
            $row = $this->site->getProductByID($record);
            $selected_variants = false;
            if ($variants = $this->site->getProductOptions($record)) {
                foreach ($variants as $variant) {
                    $selected_variants[$variant->id] = isset($pr[$row->id]['selected_variants'][$variant->id]) && !empty($pr[$row->id]['selected_variants'][$variant->id]) ? 1 : ($variant->id == $item->option_id ? 1 : 0);
                }
            }
            $pr[$row->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $item->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);
        }
       $items = json_encode($pr);
    }
?>


<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('Edit_Upselling'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?php echo lang('enter_info'); ?></p>
                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                echo admin_form_open_multipart("products/edit_upselling", $attrib)
                ?>

                <div class="col-md-12">
                    <div class="form-group">

                        <h2>- Select when offer should show</h2>
                        <div class="card-box well well-lg" >
                            <b>Select type of offer</b>
                            <div class="radio hide">
                                <label class="display_offer_checkout" ><input type="radio" value="onCheckout" checked="checked" name="promotion_type">Display offer to customer when clicking the checkout button</label>
                            </div>
                            <div class="radio hide">
                                <label class="display_offer_specific_item"><input type="radio" value="onAddingProduct" name="promotion_type">Display offer to customer when adding product to cart</label>
                            </div>

                            <div class="radio hide">
                                <label class="buy_get_check"><input type="radio" value="buyOneGetOne" name="promotion_type" >By Product Get One Product Free (Same Product will be free)</label>
                            </div>


                            <div class="radio hide">
                                <label class="buy_get_pro_free">
                                    <input type="radio" value="buyOneGetOtherProductFree" name="promotion_type" >Buy X Product Get X Product Free ( Other Product will be free )</label>
                            </div>
                            <div class="radio ">
                                <label class="buy_get_category_free">
                                    <input <?= ($upselling->type == 'buyOneGetCategoryFree') ? 'checked' : ''; ?> type="radio" value="buyOneGetCategoryFree" name="promotion_type" >Buy X Get X Free ( Category )</label>
                            </div>

                            <br/>
                            <div class="radio">
                                <label class="buyCategoryWithPercentage">
                                    <input <?= ($upselling->type == 'buyCategoryWithPercentage') ? 'checked' : ''; ?> type="radio" value="buyCategoryWithPercentage" name="promotion_type" >Buy X With Percentage( Category )</label>
                            </div>
                            <div class="radio">
                                <label class="buyProductsWithPercentage">
                                    <input <?= ($upselling->type == 'buyProductsWithPercentage') ? 'checked' : ''; ?> type="radio" value="buyProductsWithPercentage" name="promotion_type" >Buy X With Percentage( Products )</label>
                            </div>
                            <div class="radio">
                                <label class="cartvaluerangeFree">
                                    <input <?= ($upselling->type == 'cartvaluerangeFree') ? 'checked' : ''; ?> type="radio" value="cartvaluerangeFree" name="promotion_type" >Cart Total Range</label>
                            </div>

                            <div class="col-md-12 ml5 buy_get_div hide">
                                <span class="col-md-12"><label>Select How many get product on buy:</label></span>
                                <span class="col-md-3">
                                    <label>No. of buy Products?</label>
                                    <select class="form-control" name="buy_products">
                                        <?php for($i=0; $i<=10; $i++){ ?>
                                            <option value="<?= $i ?>"><?= $i ?></option>
                                        <?php }?>
                                    </select>
                                </span>
                                <span class="col-md-3">
                                    <label>No. of get free Products?</label>
                                    <select class="form-control" name="get_products">
                                        <?php for($i=0; $i<=10; $i++){ ?>
                                            <option value="<?= $i ?>"><?= $i ?></option>
                                        <?php }?>
                                    </select>
                                </span>

                                <span class="col-md-4">
                                    <label>Select Shop(s)</label>
                                    <select class="form-control" name="get_shop_name[]" multiple="multiple">
                                        <?php foreach($get_warehouse as $record ){ ?>
                                            <option value="<?= $record->id ?>"><?= $record->name; ?></option>
                                        <?php }?>
                                    </select>
                                </span>

                            </div>

                            <div class="col-md-12 ml5 buy_get_other_div hide">
                                <span class="col-md-12"><label>Select How many get product on buy:</label></span>
                                <span class="col-md-3">
                                    <label>No. of buy Products?</label>
                                    <select class="form-control" name="buy_products">
                                        <?php for($i=0; $i<=10; $i++){ ?>
                                            <option value="<?= $i ?>"><?= $i ?></option>
                                        <?php }?>
                                    </select>
                                </span>
                                <span class="col-md-3">
                                    <label>No. of get free Products?</label>
                                    <select class="form-control" name="get_products">
                                        <?php for($i=0; $i<=10; $i++){ ?>
                                            <option value="<?= $i ?>"><?= $i ?></option>
                                        <?php }?>
                                    </select>
                                </span>

                                <span class="col-md-4">
                                    <label>Select Shop(s)</label>
                                    <select class="form-control" name="get_shop_name[]" multiple="multiple">
                                        <?php foreach($get_warehouse as $record ){ ?>
                                            <option value="<?= $record->id ?>"><?= $record->name; ?></option>
                                        <?php }?>
                                    </select>
                                </span>

                            </div>

                            <div class="col-md-12 ml5 buy_get_other_percentage_div <?= ($upselling->type == 'buyCategoryWithPercentage') ? 'show' : 'hide'; ?> ">

                                <span class="col-md-2">
                                    <label>Select percentage</label>
                                    <select class="form-control" name="get_percentage">
                                        <?php for($i=0; $i<=90; $i++){
                                            if(($buyCategoryWithPercentage)){ ?>
                                                <option <?= $upselling->how_much_percentage == $i ? 'selected' : '' ?> value="<?= $i ?>"><?= $i .'%' ?></option>
                                        <?php }else{?>
                                                <option value="<?= $i ?>"><?= $i .'%' ?></option>
                                        <?php } } ?>
                                    </select>
                                </span>

                                <span class="col-md-4">
                                    <label>Select Shop(s)</label>
                                    <select class="form-control" name="get_shop_name[]" multiple="multiple">
                                        <?php foreach($get_warehouse as $record ){ ?>
                                            <option <?= in_array($record->id, $get_selected_warehouse) ? 'selected' : ''?> value="<?= $record->id ?>"><?= $record->name; ?></option>
                                        <?php }?>
                                    </select>
                                </span>

                                <span class="col-md-5">
                                    <label>Select Category for discount?</label>
                                    <select class="form-control" name="get_cat_name[]" multiple="multiple">
                                        <?php foreach($get_categories as $record ){ ?>
                                            <option <?= in_array($record->id, $get_selected_category) ? 'selected' : ''?> value="<?= $record->id ?>"><?= $record->name; ?></option>
                                        <?php }?>
                                    </select>
                                </span>

                                <span class="col-md-4">
                                    <label>Where to apply</label>
                                    <select class="form-control" name="where_to_apply_cat">
                                            <option <?= $upselling->where_to_apply == 'ecommerce' ? 'selected' : ''  ?> value="ecommerce">E-commerce</option>
                                            <option <?= $upselling->where_to_apply == 'pos' ? 'selected' : ''  ?> value="pos">POS</option>
                                            <option <?= $upselling->where_to_apply == 'both' ? 'selected' : ''  ?> value="both">Both of above</option>
                                    </select>
                                </span>

                            </div>

                            <div class="col-md-12 ml5 buy_get_product_other_percentage_div <?= ($upselling->type == 'buyProductsWithPercentage' || $upselling->type == 'cartvaluerangeFree') ? 'show' : 'hide'; ?>">

                                <span class="col-md-2">
                                    <label>Select percentage</label>
                                    <select class="form-control" name="get_percentage_pro">
                                        <?php for($i=0; $i<=90; $i++){
                                            if(($buyProductsWithPercentage || $cartvaluerangeFree)){ ?>
                                                <option <?= $upselling->how_much_percentage == $i ? 'selected' : '' ?> value="<?= $i ?>"><?= $i .'%' ?></option>
                                            <?php }else{?>
                                                <option value="<?= $i ?>"><?= $i .'%' ?></option>
                                            <?php } } ?>
                                    </select>
                                </span>

                                <span class="col-md-6 <?= ($upselling->type == 'buyProductsWithPercentage') ? 'show' : 'hide'; ?>">
                                    <label>Select Shop(s)</label>
                                    <select class="form-control" name="get_shop_name[]" multiple="multiple">
                                        <?php
                                            foreach($get_warehouse as $record ){
                                                if($buyProductsWithPercentage){ ?>
                                                <option <?= in_array($record->id, $get_selected_warehouse) ? 'selected' : ''?> value="<?= $record->id ?>"><?= $record->name; ?></option>
                                        <?php }else { ?>
                                                <option  value="<?= $record->id ?>"><?= $record->name; ?></option>
                                                <?php } }?>
                                    </select>
                                </span>

                                <span class="col-md-4">
                                    <label>Where to apply</label>
                                    <select class="form-control" name="where_to_apply_pro">
                                            <option <?= $upselling->where_to_apply == 'ecommerce' ? 'selected' : ''  ?> value="ecommerce">E-commerce</option>
                                            <option <?= $upselling->where_to_apply == 'pos' ? 'selected' : ''  ?> value="pos">POS</option>
                                            <option <?= $upselling->where_to_apply == 'both' ? 'selected' : ''  ?> value="both">Both of above</option>
                                    </select>
                                </span>

                                <div class="col-md-3 cart_promotion_fields <?= ($upselling->type == 'cartvaluerangeFree') ? 'show' : 'hide'; ?>">
                                    <label>Amonut equal and greater than</label>
                                    <input id="cart_amount_limit" name="cart_amount_limit" class="form-control" type="number" min="0" value="<?= ($upselling->cart_amount_limit) ? $upselling->cart_amount_limit : ''; ?>">
                                </div>

                                <div class="checkbox col-md-7 cart_promotion_fields <?= ($upselling->type == 'cartvaluerangeFree') ? 'show' : 'hide'; ?>" style="padding-top: 20px;">
                                    <label><input id="pricegroup_include" name="pricegroup_include" type="checkbox" value="1" <?= $upselling->pricegroup_include ==1 ? 'checked' : ''; ?>  >Check this box if this promotion also applicable for price group cutomers</label>
                                </div>


                                <div class="checkbox col-md-12 cart_promotion_fields" style="padding-top: 20px;">
                                    <label><input id="run_over_other_promotions" name="run_over_other_promotions" type="checkbox" value="1"  <?= $upselling->run_over_other_promotions == 1 ? 'checked' : ''; ?> >Run over the other Promotions</label>
                                </div>


                            </div>

                            <div class="col-md-12 ml5 buy_get_cat_div <?= ($upselling->type == 'buyOneGetCategoryFree') ? 'show' : 'hide'; ?> ">
                                <span class="col-md-12"><strong>Select How many get product on buy:</strong></span>
                                <span class="col-md-3">
                                    <label>No. of buy Products?</label>
                                    <select class="form-control" name="buy_products">
                                        <?php for($i=0; $i<=10; $i++){ ?>
                                            <option <?= $upselling->how_many_buy == $i ? 'selected' : '' ?> value="<?= $i ?>"><?= $i ?></option>
                                        <?php }?>
                                    </select>
                                </span>

                                <span class="col-md-3">
                                    <label>No. of get free Products?</label>
                                    <select class="form-control" name="get_products">
                                        <?php for($i=0; $i<=10; $i++){ ?>
                                            <option <?= $upselling->how_many_free == $i ? 'selected' : '' ?> value="<?= $i ?>"><?= $i ?></option>
                                        <?php }?>
                                    </select>
                                </span>


                                <span class="col-md-5">
                                    <label>Select Shop(s)</label>
                                    <select class="form-control" name="get_shop_name[]" multiple="multiple">
                                        <?php
                                        foreach($get_warehouse as $record ){
                                            if($buyOneGetCategoryFree){ ?>
                                                <option <?= in_array($record->id, $get_selected_warehouse) ? 'selected' : ''?> value="<?= $record->id ?>"><?= $record->name; ?></option>
                                            <?php }else { ?>
                                                <option  value="<?= $record->id ?>"><?= $record->name; ?></option>
                                            <?php } }?>
                                    </select>
                                </span>

                                <span class="col-md-6">
                                    <label>Select Category for discount?</label>
                                    <select class="form-control" name="get_cat_name_free[]" multiple="multiple">
                                        <?php foreach($get_categories as $record ){ ?>
                                            <option <?= in_array($record->id, $get_selected_category) ? 'selected' : ''?> value="<?= $record->id ?>"><?= $record->name; ?></option>
                                        <?php }?>
                                    </select>
                                </span>

                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <h2>- Setup offer</h2>
                        <div class="card-box well well-lg col-md-12">

                            <input type="hidden" name="old_update_upsel_id" value="<?= $upselling->upselling_id; ?>">

                            <span class="col-md-8">
                                <?= lang("Offer_Name", "offer_name") ?> (Max 27 Character)
                                <?= form_input('offer_name', set_value('offer_name', $upselling->name), 'class="form-control gen_slug" placeholder="Internal User only" id="offer_name" required="required"'); ?>
                            </span>

                            <span class="col-md-8">
                                <?= lang("Offer_Title", "offer_title") ?> (Max 27 Character)
                                <?= form_input('offer_title',  set_value('offer_title', $upselling->title), 'class="form-control gen_slug" placeholder="Bold font that shows up at the top of the offer..." id="offer_title" required="required"'); ?>
                            </span>

                            <span class="col-md-12">
                                <?= lang("Description", "description") ?>
                                <?= form_textarea('offer_description',  set_value('offer_description', strip_tags($upselling->description)), 'class="form-control gen_slug" placeholder="Description" id="offer_description" required="required"'); ?>
                            </span>

                            <span class="col-md-8">
                                <?= lang("status", "status") ?>
                                <?php
                                $opts = array('active' => lang('Active'), 'inactive' => lang('In-active'));
                                echo form_dropdown('status', $opts, (isset($_POST['status']) ? $_POST['status'] : ($product ? $product->status : '')), 'class="form-control" id="status" required="required"');
                                ?>
                            </span>

                        </div>
                    </div>


                    <div class="form-group step-3-div <?php if($upselling->type == 'buyOneGetCategoryFree'){ echo 'hide' ; }else if($upselling->type == 'buyCategoryWithPercentage'){ echo 'hide';}else if($upselling->type == 'cartvaluerangeFree'){ echo 'hide';}else{ echo 'show'; } ?>">
                        <h2> - Select prodcut(s) to upsell</h2>
                        <div class="card-box well well-lg col-md-12">
                            <p>This will be the item(s) that get offered in the popup</p>

                            <div class="checkbox col-md-8 hide">
                                <label><input id="more_than _product" name="more_than_product" type="checkbox" value="1">Allow customers to add more than 1 product to cart</label>
                            </div>

                            <div class="checkbox col-md-8 hide">
                                <label><input id="no_qualify" name="no_qualify" type="checkbox" value="1">Check this box if these offered products should be automatically removed from the customers cart if they no longer qualify to receive this offer</label>
                            </div>

                            <div class="checkbox col-md-8 hide">
                                <label class="customer_limit_sec">
                                    <input id="limit_customers" name="limit_customers" type="checkbox" value="1">Add a limit disclaimer
                                </label><br/>
                                <span class="col-md-3 ml10 limit_span hide">
                                    <input id="limit" name="limit" class="form-control" placeholder="Limit" /> <b>Per Customer</b>
                                </span>
                            </div>


                            <span class="col-md-8">
                                <?= lang("Upsell product(s)", "upsell_products") ?><br/>
                                <?php echo form_input('add_item', '', 'class="form-control" id="add_item" placeholder="' . $this->lang->line("add_item") . '"'); ?>
                            </span>

                        </div>

                        <div class="form-group">
                            <div class="controls table-controls">
                                <table id="bcTable"
                                       class="table items table-striped table-bordered table-condensed table-hover">
                                    <thead>
                                    <tr>
                                        <th class="col-xs-12"><?= lang("product_name") . " (" . $this->lang->line("product_code") . ")"; ?></th>
                                        <!-- <th class="col-xs-1"><?/*= lang("quantity"); */?></th>-->
                                       <!-- <th class="col-xs-7"><?/*= lang("variants"); */?></th>-->
                                        <th class="text-center" style="width:30px;">
                                            <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <h2> - Select Offer Trigger</h2>
                        <div class="card-box well well-lg col-md-12">
                            <p>This is where you set what will trigger that the offer. You can set the offer to trigger when a certain dollar amount
                                is in the customer cart a certain product or group of product is added, or a combination of both.
                            </p>

                            <div class="checkbox col-md-12 specific_cart_reach hide">
                                <label class="min_max_sec">
                                    <input id="is_min_max_price" name="is_min_max_price" type="checkbox" value="1">Only make this offer available when cart total is in a specific price range</label>
                            </div>
                            <div class="col-md-12 ml5 min_max_div hide">
                                <span class="col-md-4">If selected, Please specify range:</span>
                                <span class="col-md-2">
                                    AED: <input type="number" class="form-control" id="min_price" name="min_price" placeholder="Min Price" />
                                </span>
                                <span class="col-md-2">
                                    AED: <input type="number" class="form-control" id="max_price" name="max_price" placeholder="Max Price" />
                                </span>
                            </div>

                            <div class="col-md-12">
                                <hr/>
                            </div>


                            <div class="checkbox col-md-12">
                                <label class="daterange">
                                    <input id="available_daterange" name="available_daterange" checked type="checkbox" value="1">Only make this offer available between a specified date range
                                </label>
                            </div>

                            <div class="col-md-12 ml5 daterange_div">
                                <span class="col-md-4">If selected, Please specify range:</span>
                                <span class="col-md-2">
                                    <?= form_input('offer_start_date',  set_value('offer_start_date', $this->sma->hrsd($upselling->startDate)), 'class="form-control tip date" placeholder="Start date" id="offer_start_date" required="required"'); ?>
                                </span>
                                <span class="col-md-2">
                                    <?= form_input('offer_end_date',  set_value('offer_end_date', $this->sma->hrsd($upselling->endDate)), 'class="form-control tip date" placeholder="End date" id="offer_end_date" required="required"'); ?>
                                </span>
                            </div>

                            <div class="col-md-12">
                                <hr/>
                            </div>

                            <div class="checkbox col-md-12 hide">
                                <label class="specific_day">
                                    <input id="specific_day" name="specific_day" type="checkbox" value="1">Only make this offer available on specified days
                                </label>
                            </div>
                            <div class="col-md-12 ml5 specific_day_div hide">
                                <span class="col-md-4">If selected, Please specify range:</span>
                                <span class="col-md-4">
                                   Days: <input type="text" class="form-control" id="offer_days" name="offer_days" placeholder="Specify Days" />
                                </span>
                            </div>

                            <div class="col-md-12">
                                <hr/>
                            </div>

                            <div class="checkbox col-md-12 hide">
                                <label class="available_timerange"><input id="available_timerange" name="available_timerange" type="checkbox" value="1">Only make this offer available between a specified time range</label>
                            </div>
                            <div class="col-md-12 ml5 available_timerange_div hide">
                                <span class="col-md-4">If selected, Please specify range:</span>
                                <span class="col-md-2">
                                   Start Time: <input type="text" class="form-control  form_datetime" id="offer_start_time" name="offer_start_time" placeholder="Start Time" />
                                </span>
                                <span class="col-md-2">
                                   End Time: <input type="text" class="form-control form_datetime" id="offer_end_time" name="offer_end_time" placeholder="End Time" />
                                </span>
                            </div>


                        </div>
                    </div>


                    <div class="form-group trigger_second_item_div hide">
                        <div class="card-box well well-lg col-md-12">
                            <h3>Trigger on items</h3>

                            <div class="checkbox col-md-8">
                                <label class="product_incart"><input id="selected_product_incart" name="selected_product_incart" type="checkbox" value="1">Trigger offer if any of the selected product(s) are already in the customer's cart</label>
                            </div>

                            <div class="checkbox col-md-3 ml10 product_incart_div hide">
                                <input id="quantity_incart" name="quantity_incart" class="form-control" placeholder="Enter Quantity in cart" />
                            </div>

                            <span class="col-md-8 ml10 product_incart_div hide">
                                <?= lang("Select Product(s)", "select_product") ?><br/>
                                <?php echo form_input('add_item_2', '', 'class="form-control" id="add_item_2" placeholder="' . $this->lang->line("add_item") . '"'); ?>
                            </span><br/>

                        </div>

                        <div class=" product_incart_div hide">
                            <div class="form-group">
                                <div class="controls table-controls">
                                    <table id="bcTable_2"
                                           class="table items table-striped table-bordered table-condensed table-hover" style="margin-top: 13%">
                                        <thead>
                                        <tr>
                                            <th class="col-xs-12"><?= lang("product_name") . " (" . $this->lang->line("product_code") . ")"; ?></th>
                                            <!-- <th class="col-xs-1"><?/*= lang("quantity"); */?></th>-->
                                          <!--  <th class="col-xs-7"><?/*= lang("variants"); */?></th>-->
                                            <th class="text-center" style="width:30px;">
                                                <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                    </div>



                </div>



                <div class="col-md-12">

                    <div class="form-group">
                        <a class="btn btn-info" href="<?= admin_url();?>products/upselling">Cancel</a>
                        <?php echo form_submit('edit_promotion', $this->lang->line("Edit_Promotion"), 'class="btn btn-success"'); ?>

                    </div>

                </div>

                <?php
                    if($upselling->type == 'buyCategoryWithPercentage'){ ?>
                    <input type="hidden" name="apply_type" id="apply_type" value="cat">
                <?php }else{ ?>
                    <input type="hidden" name="apply_type" id="apply_type" value="pro">
                <?php } ?>

                <?= form_close(); ?>
                <button type="button" id="reset" class="btn btn-danger hide">Reset</button>
            </div>

        </div>
    </div>
</div>






<script type="text/javascript">
    var ac_2 = false; bcitemsx_2 = {};
    if (localStorage.getItem('bcitemsx_2')) {
        bcitemsx_2 = JSON.parse(localStorage.getItem('bcitemsx_2'));
    }
    <?php if($items) { ?>
    localStorage.setItem('bcitemsx_2', JSON.stringify(<?= $items; ?>));
    <?php } ?>
    $(document).ready(function() {
        <?php if ($this->input->post('print')) { ?>
        $( window ).load(function() {
            $('html, body').animate({
                scrollTop: ($("#barcode-con").offset().top)-15
            }, 1000);
        });
        <?php } ?>
        if (localStorage.getItem('bcitemsx_2')) {
            loadItems_2();
        }
        $("#add_item_2").autocomplete({
            source: '<?= admin_url('products/get_suggestions_promotion'); ?>',
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item_2').focus();
                    });
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item_2').focus();
                    });
                    $(this).val('');

                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_product_item_2(ui.item);
                    if (row) {
                        $(this).val('');
                    }
                } else {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>');
                }
            }
        });
        check_add_item_val();

        $('#style').change(function (e) {
            localStorage.setItem('bcstyle', $(this).val());
            if ($(this).val() == 50) {
                $('.cf-con').slideDown();
            } else {
                $('.cf-con').slideUp();
            }
        });
        if (style = localStorage.getItem('bcstyle')) {
            $('#style').val(style);
            $('#style').select2("val", style);
            if (style == 50) {
                $('.cf-con').slideDown();
            } else {
                $('.cf-con').slideUp();
            }
        }

        $('#cf_width').change(function (e) {
            localStorage.setItem('cf_width', $(this).val());
        });
        if (cf_width = localStorage.getItem('cf_width')) {
            $('#cf_width').val(cf_width);
        }

        $('#cf_height').change(function (e) {
            localStorage.setItem('cf_height', $(this).val());
        });
        if (cf_height = localStorage.getItem('cf_height')) {
            $('#cf_height').val(cf_height);
        }

        $('#cf_orientation').change(function (e) {
            localStorage.setItem('cf_orientation', $(this).val());
        });
        if (cf_orientation = localStorage.getItem('cf_orientation')) {
            $('#cf_orientation').val(cf_orientation);
        }

        $(document).on('ifChecked', '#site_name', function(event) {
            localStorage.setItem('bcsite_name', 1);
        });
        $(document).on('ifUnchecked', '#site_name', function(event) {
            localStorage.setItem('bcsite_name', 0);
        });
        if (site_name = localStorage.getItem('bcsite_name')) {
            if (site_name == 1)
                $('#site_name').iCheck('check');
            else
                $('#site_name').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#product_name', function(event) {
            localStorage.setItem('bcproduct_name', 1);
        });
        $(document).on('ifUnchecked', '#product_name', function(event) {
            localStorage.setItem('bcproduct_name', 0);
        });
        if (product_name = localStorage.getItem('bcproduct_name')) {
            if (product_name == 1)
                $('#product_name').iCheck('check');
            else
                $('#product_name').iCheck('uncheck');
        }



        $(document).on('ifChecked', '#translated_product_name', function(event) {
            localStorage.setItem('bctranslatedproduct_name', 1);
        });
        $(document).on('ifUnchecked', '#translated_product_name', function(event) {
            localStorage.setItem('bctranslatedproduct_name', 0);
        });
        if (translated_product_name = localStorage.getItem('bctranslatedproduct_name')) {
            if (translated_product_name == 1)
                $('#translated_product_name').iCheck('check');
            else
                $('#translated_product_name').iCheck('uncheck');
        }



        $(document).on('ifChecked', '#price', function(event) {
            localStorage.setItem('bcprice', 1);
        });
        $(document).on('ifUnchecked', '#price', function(event) {
            localStorage.setItem('bcprice', 0);
            $('#currencies').iCheck('uncheck');
        });
        if (price = localStorage.getItem('bcprice')) {
            if (price == 1)
                $('#price').iCheck('check');
            else
                $('#price').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#currencies', function(event) {
            localStorage.setItem('bccurrencies', 1);
        });
        $(document).on('ifUnchecked', '#currencies', function(event) {
            localStorage.setItem('bccurrencies', 0);
        });
        if (currencies = localStorage.getItem('bccurrencies')) {
            if (currencies == 1)
                $('#currencies').iCheck('check');
            else
                $('#currencies').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#unit', function(event) {
            localStorage.setItem('bcunit', 1);
        });
        $(document).on('ifUnchecked', '#unit', function(event) {
            localStorage.setItem('bcunit', 0);
        });
        if (unit = localStorage.getItem('bcunit')) {
            if (unit == 1)
                $('#unit').iCheck('check');
            else
                $('#unit').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#category', function(event) {
            localStorage.setItem('bccategory', 1);
        });
        $(document).on('ifUnchecked', '#category', function(event) {
            localStorage.setItem('bccategory', 0);
        });
        if (category = localStorage.getItem('bccategory')) {
            if (category == 1)
                $('#category').iCheck('check');
            else
                $('#category').iCheck('uncheck');
        }



        $(document).on('ifChecked', '#translated_category', function(event) {
            localStorage.setItem('bctranslated_category', 1);
        });
        $(document).on('ifUnchecked', '#translated_category', function(event) {
            localStorage.setItem('bctranslated_category', 0);
        });
        if (translated_category = localStorage.getItem('bctranslated_category')) {
            if (translated_category == 1)
                $('#translated_category').iCheck('check');
            else
                $('#translated_category').iCheck('uncheck');
        }



        $(document).on('ifChecked', '#check_promo', function(event) {
            localStorage.setItem('bccheck_promo', 1);
        });
        $(document).on('ifUnchecked', '#check_promo', function(event) {
            localStorage.setItem('bccheck_promo', 0);
        });
        if (check_promo = localStorage.getItem('bccheck_promo')) {
            if (check_promo == 1)
                $('#check_promo').iCheck('check');
            else
                $('#check_promo').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#product_image', function(event) {
            localStorage.setItem('bcproduct_image', 1);
        });
        $(document).on('ifUnchecked', '#product_image', function(event) {
            localStorage.setItem('bcproduct_image', 0);
        });
        if (product_image = localStorage.getItem('bcproduct_image')) {
            if (product_image == 1)
                $('#product_image').iCheck('check');
            else
                $('#product_image').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#variants', function(event) {
            localStorage.setItem('bcvariants', 1);
        });
        $(document).on('ifUnchecked', '#variants', function(event) {
            localStorage.setItem('bcvariants', 0);
        });
        if (variants = localStorage.getItem('bcvariants')) {
            if (variants == 1)
                $('#variants').iCheck('check');
            else
                $('#variants').iCheck('uncheck');
        }

        /*        $(document).on('ifChecked', '.checkbox', function(event) {
         var item_id = $(this).attr('data-item-id');
         var vt_id = $(this).attr('id');
         bcitemsx_2[item_id]['selected_variants'][vt_id] = 1;
         localStorage.setItem('bcitemsx_2', JSON.stringify(bcitemsx_2));
         });
         $(document).on('ifUnchecked', '.checkbox', function(event) {
         var item_id = $(this).attr('data-item-id');
         var vt_id = $(this).attr('id');
         bcitemsx_2[item_id]['selected_variants'][vt_id] = 0;
         localStorage.setItem('bcitemsx_2', JSON.stringify(bcitemsx_2));
         });*/

        $(document).on('click', '.del', function () {
            var id = $(this).attr('id');
            delete bcitemsx_2[id];
            localStorage.setItem('bcitemsx_2', JSON.stringify(bcitemsx_2));
            $(this).closest('#row_' + id).remove();
        });

        $('#reset').click(function (e) {

            //bootbox.confirm(lang.r_u_sure, function (result) {
            if (localStorage.getItem('bcitemsx_2')) {
                if (localStorage.getItem('bcitemsx_2')) {
                    localStorage.removeItem('bcitemsx_2');
                }
                if (localStorage.getItem('bcstyle')) {
                    localStorage.removeItem('bcstyle');
                }
                if (localStorage.getItem('bcsite_name')) {
                    localStorage.removeItem('bcsite_name');
                }
                if (localStorage.getItem('bcproduct_name')) {
                    localStorage.removeItem('bcproduct_name');
                }
                if (localStorage.getItem('bctranslatedproduct_name')) {
                    localStorage.removeItem('bctranslatedproduct_name');
                }
                if (localStorage.getItem('bcprice')) {
                    localStorage.removeItem('bcprice');
                }
                if (localStorage.getItem('bccurrencies')) {
                    localStorage.removeItem('bccurrencies');
                }
                if (localStorage.getItem('bcunit')) {
                    localStorage.removeItem('bcunit');
                }
                if (localStorage.getItem('bccategory')) {
                    localStorage.removeItem('bccategory');
                }
                if (localStorage.getItem('bctranslated_category')) {
                    localStorage.removeItem('bctranslated_category');
                }

                // $('#modal-loading').show();
                // window.location.replace("<?//= admin_url('system_settings/create_upselling'); ?>");
            }
            //});
        });

        var old_row_qty;
        $(document).on("focus", '.quantity', function () {
            old_row_qty = $(this).val();
        }).on("change", '.quantity', function () {
            var row = $(this).closest('tr');
            if (!is_numeric($(this).val())) {
                $(this).val(old_row_qty);
                bootbox.alert(lang.unexpected_value);
                return;
            }
            var new_qty = parseFloat($(this).val()),
                item_id = row.attr('data-item-id');
            bcitemsx_2[item_id].qty = new_qty;
            localStorage.setItem('bcitemsx_2', JSON.stringify(bcitemsx_2));
        });

    });

    function add_product_item_2(item) {
        ac_2 = true;
        if (item == null) {
            return false;
        }
        item_id = item.id;
        if (bcitemsx_2[item_id]) {
            bcitemsx_2[item_id].qty = parseFloat(bcitemsx_2[item_id].qty) + 1;
        } else {
            bcitemsx_2[item_id] = item;
            bcitemsx_2[item_id]['selected_variants'] = {};
            $.each(item.variants, function () {
                bcitemsx_2[item_id]['selected_variants'][this.id] = 1;
            });
        }

        localStorage.setItem('bcitemsx_2', JSON.stringify(bcitemsx_2));
        loadItems_2();
        return true;

    }

    function loadItems_2 () {

        if (localStorage.getItem('bcitemsx_2')) {
            $("#bcTable_2 tbody").empty();
            bcitemsx_2 = JSON.parse(localStorage.getItem('bcitemsx_2'));

            $.each(bcitemsx_2, function () {

                var item = this;
                var row_no = item.id;
                var vd = '';
                var newTr = $('<tr id="row_' + row_no + '" class="row_' + item.id + '" data-item-id="' + item.id + '"></tr>');
                tr_html = '<td><input name="product_2[]" type="hidden" value="' + item.id + '"><span id="name_' + row_no + '">' + item.name + ' (' + item.code + ')</span></td>';
                //tr_html += '<td><input class="form-control quantity text-center" name="quantity[]" type="text" value="' + formatDecimal(item.qty) + '" data-id="' + row_no + '" data-item="' + item.id + '" id="quantity_' + row_no + '" onClick="this.select();"></td>';
               /* if(item.variants) {
                    $.each(item.variants, function () {
                        vd += '<input name="vt_'+ item.id +'_'+ this.id +'" type="checkbox" class="checkbox" id="'+this.id+'" data-item-id="'+item.id+'" value="'+this.id+'" '+( item.selected_variants[this.id] == 1 ? 'checked="checked"' : '')+' style="display:inline-block;" /><label for="'+this.id+'" class="padding05">'+this.name+'</label>';
                    });
                }*/
              /*  tr_html += '<td>'+vd+'</td>';*/
                tr_html += '<td class="text-center"><i class="fa fa-times tip del" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
                //tr_html += '<td class="hide"><input class="form-control quantity text-center" name="productid-' + row_no + '" type="hidden"  data-id="' + row_no + '" data-item="' + item.id + '" id="prductidentity_' + row_no + '"></td>';
                newTr.html(tr_html);
                newTr.appendTo("#bcTable_2");
            });
            $('input[type="checkbox"],[type="radio"]').not('.skip').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });
            return true;
        }
    }

</script>



<script type="text/javascript">
    var ac = false; bcitemsx = {};
    if (localStorage.getItem('bcitemsx')) {
        bcitemsx = JSON.parse(localStorage.getItem('bcitemsx'));
    }
    <?php if($items) { ?>
    localStorage.setItem('bcitemsx', JSON.stringify(<?= $items; ?>));
    <?php } ?>
    $(document).ready(function() {
        <?php if ($this->input->post('print')) { ?>
        $( window ).load(function() {
            $('html, body').animate({
                scrollTop: ($("#barcode-con").offset().top)-15
            }, 1000);
        });
        <?php } ?>
        if (localStorage.getItem('bcitemsx')) {
            loadItems();
        }
        $("#add_item").autocomplete({
            source: '<?= admin_url('products/get_suggestions_promotion'); ?>',
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');

                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_product_item(ui.item);
                    if (row) {
                        $(this).val('');
                    }
                } else {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>');
                }
            }
        });
        check_add_item_val();

        $('#style').change(function (e) {
            localStorage.setItem('bcstyle', $(this).val());
            if ($(this).val() == 50) {
                $('.cf-con').slideDown();
            } else {
                $('.cf-con').slideUp();
            }
        });
        if (style = localStorage.getItem('bcstyle')) {
            $('#style').val(style);
            $('#style').select2("val", style);
            if (style == 50) {
                $('.cf-con').slideDown();
            } else {
                $('.cf-con').slideUp();
            }
        }

        $('#cf_width').change(function (e) {
            localStorage.setItem('cf_width', $(this).val());
        });
        if (cf_width = localStorage.getItem('cf_width')) {
            $('#cf_width').val(cf_width);
        }

        $('#cf_height').change(function (e) {
            localStorage.setItem('cf_height', $(this).val());
        });
        if (cf_height = localStorage.getItem('cf_height')) {
            $('#cf_height').val(cf_height);
        }

        $('#cf_orientation').change(function (e) {
            localStorage.setItem('cf_orientation', $(this).val());
        });
        if (cf_orientation = localStorage.getItem('cf_orientation')) {
            $('#cf_orientation').val(cf_orientation);
        }

        $(document).on('ifChecked', '#site_name', function(event) {
            localStorage.setItem('bcsite_name', 1);
        });
        $(document).on('ifUnchecked', '#site_name', function(event) {
            localStorage.setItem('bcsite_name', 0);
        });
        if (site_name = localStorage.getItem('bcsite_name')) {
            if (site_name == 1)
                $('#site_name').iCheck('check');
            else
                $('#site_name').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#product_name', function(event) {
            localStorage.setItem('bcproduct_name', 1);
        });
        $(document).on('ifUnchecked', '#product_name', function(event) {
            localStorage.setItem('bcproduct_name', 0);
        });
        if (product_name = localStorage.getItem('bcproduct_name')) {
            if (product_name == 1)
                $('#product_name').iCheck('check');
            else
                $('#product_name').iCheck('uncheck');
        }



        $(document).on('ifChecked', '#translated_product_name', function(event) {
            localStorage.setItem('bctranslatedproduct_name', 1);
        });
        $(document).on('ifUnchecked', '#translated_product_name', function(event) {
            localStorage.setItem('bctranslatedproduct_name', 0);
        });
        if (translated_product_name = localStorage.getItem('bctranslatedproduct_name')) {
            if (translated_product_name == 1)
                $('#translated_product_name').iCheck('check');
            else
                $('#translated_product_name').iCheck('uncheck');
        }



        $(document).on('ifChecked', '#price', function(event) {
            localStorage.setItem('bcprice', 1);
        });
        $(document).on('ifUnchecked', '#price', function(event) {
            localStorage.setItem('bcprice', 0);
            $('#currencies').iCheck('uncheck');
        });
        if (price = localStorage.getItem('bcprice')) {
            if (price == 1)
                $('#price').iCheck('check');
            else
                $('#price').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#currencies', function(event) {
            localStorage.setItem('bccurrencies', 1);
        });
        $(document).on('ifUnchecked', '#currencies', function(event) {
            localStorage.setItem('bccurrencies', 0);
        });
        if (currencies = localStorage.getItem('bccurrencies')) {
            if (currencies == 1)
                $('#currencies').iCheck('check');
            else
                $('#currencies').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#unit', function(event) {
            localStorage.setItem('bcunit', 1);
        });
        $(document).on('ifUnchecked', '#unit', function(event) {
            localStorage.setItem('bcunit', 0);
        });
        if (unit = localStorage.getItem('bcunit')) {
            if (unit == 1)
                $('#unit').iCheck('check');
            else
                $('#unit').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#category', function(event) {
            localStorage.setItem('bccategory', 1);
        });
        $(document).on('ifUnchecked', '#category', function(event) {
            localStorage.setItem('bccategory', 0);
        });
        if (category = localStorage.getItem('bccategory')) {
            if (category == 1)
                $('#category').iCheck('check');
            else
                $('#category').iCheck('uncheck');
        }



        $(document).on('ifChecked', '#translated_category', function(event) {
            localStorage.setItem('bctranslated_category', 1);
        });
        $(document).on('ifUnchecked', '#translated_category', function(event) {
            localStorage.setItem('bctranslated_category', 0);
        });
        if (translated_category = localStorage.getItem('bctranslated_category')) {
            if (translated_category == 1)
                $('#translated_category').iCheck('check');
            else
                $('#translated_category').iCheck('uncheck');
        }



        $(document).on('ifChecked', '#check_promo', function(event) {
            localStorage.setItem('bccheck_promo', 1);
        });
        $(document).on('ifUnchecked', '#check_promo', function(event) {
            localStorage.setItem('bccheck_promo', 0);
        });
        if (check_promo = localStorage.getItem('bccheck_promo')) {
            if (check_promo == 1)
                $('#check_promo').iCheck('check');
            else
                $('#check_promo').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#product_image', function(event) {
            localStorage.setItem('bcproduct_image', 1);
        });
        $(document).on('ifUnchecked', '#product_image', function(event) {
            localStorage.setItem('bcproduct_image', 0);
        });
        if (product_image = localStorage.getItem('bcproduct_image')) {
            if (product_image == 1)
                $('#product_image').iCheck('check');
            else
                $('#product_image').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#variants', function(event) {
            localStorage.setItem('bcvariants', 1);
        });
        $(document).on('ifUnchecked', '#variants', function(event) {
            localStorage.setItem('bcvariants', 0);
        });
        if (variants = localStorage.getItem('bcvariants')) {
            if (variants == 1)
                $('#variants').iCheck('check');
            else
                $('#variants').iCheck('uncheck');
        }

        /*        $(document).on('ifChecked', '.checkbox', function(event) {
         var item_id = $(this).attr('data-item-id');
         var vt_id = $(this).attr('id');
         bcitemsx[item_id]['selected_variants'][vt_id] = 1;
         localStorage.setItem('bcitemsx', JSON.stringify(bcitemsx));
         });
         $(document).on('ifUnchecked', '.checkbox', function(event) {
         var item_id = $(this).attr('data-item-id');
         var vt_id = $(this).attr('id');
         bcitemsx[item_id]['selected_variants'][vt_id] = 0;
         localStorage.setItem('bcitemsx', JSON.stringify(bcitemsx));
         });*/

        $(document).on('click', '.del', function () {
            var id = $(this).attr('id');
            delete bcitemsx[id];
            localStorage.setItem('bcitemsx', JSON.stringify(bcitemsx));
            $(this).closest('#row_' + id).remove();
        });

        $('#reset').click(function (e) {

            //bootbox.confirm(lang.r_u_sure, function (result) {
            if (localStorage.getItem('bcitemsx')) {
                if (localStorage.getItem('bcitemsx')) {
                    localStorage.removeItem('bcitemsx');
                }
                if (localStorage.getItem('bcstyle')) {
                    localStorage.removeItem('bcstyle');
                }
                if (localStorage.getItem('bcsite_name')) {
                    localStorage.removeItem('bcsite_name');
                }
                if (localStorage.getItem('bcproduct_name')) {
                    localStorage.removeItem('bcproduct_name');
                }
                if (localStorage.getItem('bctranslatedproduct_name')) {
                    localStorage.removeItem('bctranslatedproduct_name');
                }
                if (localStorage.getItem('bcprice')) {
                    localStorage.removeItem('bcprice');
                }
                if (localStorage.getItem('bccurrencies')) {
                    localStorage.removeItem('bccurrencies');
                }
                if (localStorage.getItem('bcunit')) {
                    localStorage.removeItem('bcunit');
                }
                if (localStorage.getItem('bccategory')) {
                    localStorage.removeItem('bccategory');
                }
                if (localStorage.getItem('bctranslated_category')) {
                    localStorage.removeItem('bctranslated_category');
                }

                // $('#modal-loading').show();
                // window.location.replace("<?//= admin_url('system_settings/create_upselling'); ?>");
            }
            //});
        });

        var old_row_qty;
        $(document).on("focus", '.quantity', function () {
            old_row_qty = $(this).val();
        }).on("change", '.quantity', function () {
            var row = $(this).closest('tr');
            if (!is_numeric($(this).val())) {
                $(this).val(old_row_qty);
                bootbox.alert(lang.unexpected_value);
                return;
            }
            var new_qty = parseFloat($(this).val()),
                item_id = row.attr('data-item-id');
            bcitemsx[item_id].qty = new_qty;
            localStorage.setItem('bcitemsx', JSON.stringify(bcitemsx));
        });

    });

    function add_product_item(item) {
        ac = true;
        if (item == null) {
            return false;
        }
        item_id = item.id;
        if (bcitemsx[item_id]) {
            bcitemsx[item_id].qty = parseFloat(bcitemsx[item_id].qty) + 1;
        } else {
            bcitemsx[item_id] = item;
            bcitemsx[item_id]['selected_variants'] = {};
            $.each(item.variants, function () {
                bcitemsx[item_id]['selected_variants'][this.id] = 1;
            });
        }

        localStorage.setItem('bcitemsx', JSON.stringify(bcitemsx));
        loadItems();
        return true;

    }

    function loadItems () {

        if (localStorage.getItem('bcitemsx')) {
            $("#bcTable tbody").empty();
            bcitemsx = JSON.parse(localStorage.getItem('bcitemsx'));

            $.each(bcitemsx, function () {

                var item = this;
                var row_no = item.id;
                var vd = '';
                var newTr = $('<tr id="row_' + row_no + '" class="row_' + item.id + '" data-item-id="' + item.id + '"></tr>');
                tr_html = '<td><input name="product[]" type="hidden" value="' + item.id + '"><span id="name_' + row_no + '">' + item.name + ' (' + item.code + ')</span></td>';
                //tr_html += '<td><input class="form-control quantity text-center" name="quantity[]" type="text" value="' + formatDecimal(item.qty) + '" data-id="' + row_no + '" data-item="' + item.id + '" id="quantity_' + row_no + '" onClick="this.select();"></td>';
               /* if(item.variants) {
                    $.each(item.variants, function () {
                        vd += '<input name="vt_'+ item.id +'_'+ this.id +'" type="checkbox" class="checkbox" id="'+this.id+'" data-item-id="'+item.id+'" value="'+this.id+'" '+( item.selected_variants[this.id] == 1 ? 'checked="checked"' : '')+' style="display:inline-block;" /><label for="'+this.id+'" class="padding05">'+this.name+'</label>';
                    });
                }*/
                /*tr_html += '<td>'+vd+'</td>';*/
                tr_html += '<td class="text-center"><i class="fa fa-times tip del" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
                //tr_html += '<td class="hide"><input class="form-control quantity text-center" name="productid-' + row_no + '" type="hidden"  data-id="' + row_no + '" data-item="' + item.id + '" id="prductidentity_' + row_no + '"></td>';
                newTr.html(tr_html);
                newTr.appendTo("#bcTable");
            });
            $('input[type="checkbox"],[type="radio"]').not('.skip').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });
            return true;
        }
    }

</script>



<script type="text/javascript">
    $(document).ready(function () {

        $('#reset').trigger('click');

        $('.form_datetime').datetimepicker({
            format: 'hh:mm:ss',
            pickDate: false
        });


        $('.customer_limit_sec').click(function () {
            $('.limit_span').toggleClass('show');
        });

        $('.min_max_sec').click(function () {
            $('.min_max_div').toggleClass('show');
        });

        $('.daterange').click(function () {
            $('.daterange_div').toggleClass('show');
        });


        //Buy one Get one free
        $('.buy_get_check').click(function () {
            $('.buy_get_div').addClass('show');
            $('.buy_get_div').removeClass('hide');

            $('.buy_get_cat_div').removeClass('show');
            $('.buy_get_cat_div').addClass('hide');
            // $('.buy_get_div').toggleClass('show');

            //show the 2nd product section
            $('.trigger_second_item_div').addClass('hide');
            $('.trigger_second_item_div').removeClass('show');

            //show the portion when need to add product on promotion
            $('.step-3-div').addClass('show');
            $('.step-3-div').removeClass('hide');

            //hide the portion when need to add product on promotion
            $('.buy_get_other_div').addClass('hide');
            $('.buy_get_other_div').removeClass('show');

            $('.buy_get_other_percentage_div').removeClass('show');
            $('.buy_get_other_percentage_div').addClass('hide');

            //hide the portion when need to add product on promotion
            $('.buy_get_product_other_percentage_div').addClass('hide');
            $('.buy_get_product_other_percentage_div').removeClass('show');
        });

        //Category Free
        $('.buy_get_category_free').click(function () {
            $('.buy_get_cat_div').removeClass('hide');
            $('.buy_get_cat_div').addClass('show');

            $('.buy_get_div').addClass('hide');
            $('.buy_get_div').removeClass('show');

            //Hide the 2nd product section
            $('.trigger_second_item_div').addClass('hide');
            $('.trigger_second_item_div').removeClass('show');

            //hide the portion when need to add product on promotion
            $('.step-3-div').addClass('hide');
            $('.step-3-div').removeClass('show');

            //hide the portion when need to add product on promotion
            $('.specific_cart_reach').addClass('hide');
            $('.specific_cart_reach').removeClass('show');

            //hide the portion when need to add product on promotion
            $('.buy_get_other_div').addClass('hide');
            $('.buy_get_other_div').removeClass('show');

            $('.buy_get_other_percentage_div').removeClass('show');
            $('.buy_get_other_percentage_div').addClass('hide');


            //hide the portion when need to add product on promotion
            $('.buy_get_product_other_percentage_div').addClass('hide');
            $('.buy_get_product_other_percentage_div').removeClass('show');

        });

        //Category Free with percentage
        $('.buyCategoryWithPercentage').click(function () {
            $('.buy_get_other_percentage_div').removeClass('hide');
            $('.buy_get_other_percentage_div').addClass('show');

            $('.buy_get_cat_div').removeClass('show');
            $('.buy_get_cat_div').addClass('hide');

            $('.buy_get_div').addClass('hide');
            $('.buy_get_div').removeClass('show');

            //Hide the 2nd product section
            $('.trigger_second_item_div').addClass('hide');
            $('.trigger_second_item_div').removeClass('show');

            //hide the portion when need to add product on promotion
            $('.step-3-div').addClass('hide');
            $('.step-3-div').removeClass('show');

            //hide the portion when need to add product on promotion
            $('.specific_cart_reach').addClass('hide');
            $('.specific_cart_reach').removeClass('show');

            //hide the portion when need to add product on promotion
            $('.buy_get_other_div').addClass('hide');
            $('.buy_get_other_div').removeClass('show');

            //hide the portion when need to add product on promotion
            $('.buy_get_product_other_percentage_div').addClass('hide');
            $('.buy_get_product_other_percentage_div').removeClass('show');

            //Where to apply change value
            $('#apply_type').val('cat');

        });

        //Products Free with percentage
        $('.buyProductsWithPercentage').click(function () {
            $('.buy_get_other_percentage_div').removeClass('show');
            $('.buy_get_other_percentage_div').addClass('hide');

            $('.buy_get_cat_div').removeClass('show');
            $('.buy_get_cat_div').addClass('hide');

            $('.buy_get_div').addClass('hide');
            $('.buy_get_div').removeClass('show');

            //Hide the 2nd product section
            $('.trigger_second_item_div').addClass('hide');
            $('.trigger_second_item_div').removeClass('show');

            //hide the portion when need to add product on promotion
            $('.step-3-div').addClass('show');
            $('.step-3-div').removeClass('hide');

            //hide the portion when need to add product on promotion
            $('.specific_cart_reach').addClass('hide');
            $('.specific_cart_reach').removeClass('show');

            //hide the portion when need to add product on promotion
            $('.buy_get_other_div').addClass('hide');
            $('.buy_get_other_div').removeClass('show');


            //hide the portion when need to add product on promotion
            $('.buy_get_product_other_percentage_div').addClass('show');
            $('.buy_get_product_other_percentage_div').removeClass('hide');

            //Where to apply change value
            $('#apply_type').val('pro');

        });



        //Other product will be Free
        $('.buy_get_pro_free').click(function () {
            $('.buy_get_other_div').removeClass('hide');
            $('.buy_get_other_div').addClass('show');

            $('.buy_get_cat_div').removeClass('show');
            $('.buy_get_cat_div').addClass('hide');

            $('.buy_get_div').addClass('hide');
            $('.buy_get_div').removeClass('show');

            //Hide the 2nd product section
            $('.trigger_second_item_div').addClass('hide');
            $('.trigger_second_item_div').removeClass('show');

            //hide the portion when need to add product on promotion
            $('.step-3-div').addClass('show');
            $('.step-3-div').removeClass('hide');

            //hide the portion when need to add product on promotion
            $('.specific_cart_reach').addClass('hide');
            $('.specific_cart_reach').removeClass('show');

            $('.buy_get_other_percentage_div').removeClass('show');
            $('.buy_get_other_percentage_div').addClass('hide');


            //hide the portion when need to add product on promotion
            $('.buy_get_product_other_percentage_div').addClass('hide');
            $('.buy_get_product_other_percentage_div').removeClass('show');

        });

        //On checkout button show promotion
        $('.display_offer_checkout').click(function () {
            $('.buy_get_cat_div').removeClass('show');
            $('.buy_get_cat_div').addClass('hide');

            $('.buy_get_div').addClass('hide');
            $('.buy_get_div').removeClass('show');


            //Hide the 2nd product section
            $('.trigger_second_item_div').addClass('hide');
            $('.trigger_second_item_div').removeClass('show');


            //show the portion when need to add product on promotion
            $('.step-3-div').addClass('show');
            $('.step-3-div').removeClass('hide');

            //hide the portion when need to add product on promotion
            $('.buy_get_other_div').addClass('hide');
            $('.buy_get_other_div').removeClass('show');

            $('.buy_get_other_percentage_div').removeClass('show');
            $('.buy_get_other_percentage_div').addClass('hide');

            //hide the portion when need to add product on promotion
            $('.buy_get_product_other_percentage_div').addClass('hide');
            $('.buy_get_product_other_percentage_div').removeClass('show');
        });


        //On specific item add to cart show promotion
        $('.display_offer_specific_item').click(function () {
            $('.buy_get_cat_div').removeClass('show');
            $('.buy_get_cat_div').addClass('hide');

            $('.buy_get_div').addClass('hide');
            $('.buy_get_div').removeClass('show');


            //Hide the 2nd product section
            $('.trigger_second_item_div').addClass('show');
            $('.trigger_second_item_div').removeClass('hide');


            //show the portion when need to add product on promotion
            $('.step-3-div').addClass('show');
            $('.step-3-div').removeClass('hide');

            //hide the portion when need to add product on promotion
            $('.specific_cart_reach').addClass('show');
            $('.specific_cart_reach').removeClass('hide');

            //hide the portion when need to add product on promotion
            $('.buy_get_other_div').addClass('hide');
            $('.buy_get_other_div').removeClass('show');

            $('.buy_get_other_percentage_div').removeClass('show');
            $('.buy_get_other_percentage_div').addClass('hide');

            //hide the portion when need to add product on promotion
            $('.buy_get_product_other_percentage_div').addClass('hide');
            $('.buy_get_product_other_percentage_div').removeClass('show');


        });


        $('.specific_day').click(function () {
            $('.specific_day_div').toggleClass('show');
        });

        $('.available_timerange').click(function () {
            $('.available_timerange_div').toggleClass('show');
        });

        $('.product_incart').click(function () {
            $('.product_incart_div').toggleClass('show');
        });

    });

</script>