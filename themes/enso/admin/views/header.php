<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <base href="<?= admin_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $page_title ?> - <?= $Settings->site_name ?></title>
    <link rel="shortcut icon" href="<?= $assets ?>images/icon.png"/>
    <link href="<?= $assets ?>styles/theme.css" rel="stylesheet"/>
    <link href="<?= $assets ?>styles/style.css" rel="stylesheet"/>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="<?= $assets ?>js/jquery.combinations.1.0.min.js"></script>
    <!--[if lt IE 9]>
    <script src="<?= $assets ?>js/jquery.js"></script>
    <![endif]-->
    <noscript><style type="text/css">#loading { display: none; }</style></noscript>
    <?php if ($Settings->user_rtl) { ?>
        <link href="<?= $assets ?>styles/helpers/bootstrap-rtl.min.css" rel="stylesheet"/>
        <link href="<?= $assets ?>styles/style-rtl.css" rel="stylesheet"/>
        <script type="text/javascript">
            $(document).ready(function () { $('.pull-right, .pull-left').addClass('flip'); });
        </script>
    <?php } ?>
    <script type="text/javascript">
        $(window).load(function () {
            $("#loading").fadeOut("slow");
            var us_lange = JSON.stringify(site.settings.user_language);
                us_lange = us_lange.replace('"' , '');
                us_lange = us_lange.replace('"' , '');
            $('.us_langue').html(us_lange.toUpperCase());
        });
    </script>
</head>

<body>
<noscript>
    <div class="global-site-notice noscript">
        <div class="notice-inner">
            <p><strong>JavaScript seems to be disabled in your browser.</strong><br>You must have JavaScript enabled in
                your browser to utilize the functionality of this website.</p>
        </div>
    </div>
</noscript>
<div id="loading"></div>
<div id="app_wrapper">
    <header id="header" class="navbar">
        <div class="container">
            <a class="navbar-brand" href="<?= admin_url() ?>"><span class="logo"><?= $Settings->site_name ?></span></a>

            <div class="btn-group visible-xs pull-right btn-visible-sm">
                <button class="navbar-toggle btn" type="button" data-toggle="collapse" data-target="#sidebar_menu">
                    <span class="fa fa-bars"></span>
                </button>
                <a href="<?= admin_url('users/profile/' . $this->session->userdata('user_id')); ?>" class="btn">
                    <span class="fa fa-user"></span>
                </a>
                <a href="<?= admin_url('logout'); ?>" class="btn">
                    <span class="fa fa-sign-out"></span>
                </a>
            </div>
            <div class="header-nav">
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown">
                        <a class="btn account dropdown-toggle" data-toggle="dropdown" href="#">
                            <img alt="" src="<?= $this->session->userdata('avatar') ? base_url() . 'assets/uploads/avatars/thumbs/' . $this->session->userdata('avatar') : base_url('assets/images/' . $this->session->userdata('gender') . '.png'); ?>" class="mini_avatar img-rounded">

                            <div class="user">
                                <span><?= lang('welcome') ?> <?= $this->session->userdata('username'); ?></span>
                            </div>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="<?= admin_url('users/profile/' . $this->session->userdata('user_id')); ?>">
                                    <i class="fa fa-user"></i> <?= lang('profile'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?= admin_url('users/profile/' . $this->session->userdata('user_id') . '/#cpassword'); ?>"><i class="fa fa-key"></i> <?= lang('change_password'); ?>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?= admin_url('logout'); ?>">
                                    <i class="fa fa-sign-out"></i> <?= lang('logout'); ?>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav pull-right">

                    <li class="dropdown hidden-xs">
                        <a class="btn bblue tip" title="<?= lang('dashboard') ?>"
                           data-placement="bottom" data-container="body" href="<?= admin_url('') ?>">
                            <i class="fa fa-dashboard"></i> <span class="padding05"><?= lang('dashboard') ?></span>
                        </a>
                    </li>

                    <li class="dropdown hidden-xs">
                        <a class="btn bblue tip" title="<?= lang('pos') ?>"
                           data-placement="bottom" data-container="body" href="<?= admin_url('pos') ?>">
                            <i class="fa fa-shopping-cart"></i> <span class="padding05"><?= lang('pos') ?></span>
                        </a>
                    </li>

                    <li class="dropdown hidden-xs">
                        <a class="btn tip bblue" title="<?= lang('language') ?>" data-placement="bottom" data-toggle="dropdown"
                           href="#"> <i class="fa fa-language"></i>
                            <span class="padding05 us_langue"></span>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <?php $scanned_lang_dir = array_map(function ($path) {
                                return basename($path);
                            }, glob(APPPATH . 'language/*', GLOB_ONLYDIR));
                            foreach ($scanned_lang_dir as $entry) { ?>
                                <li>
                                    <a href="<?= admin_url('welcome/language/' . $entry); ?>">
                                        <img src="<?= base_url('assets/images/'.$entry.'.png'); ?>" class="language-img">
                                        &nbsp;&nbsp;<?= ucwords($entry); ?>
                                    </a>
                                </li>
                            <?php } ?>
                            <li class="divider"></li>
                            <li>
                                <a href="<?= admin_url('welcome/toggle_rtl') ?>">
                                    <i class="fa fa-align-<?=$Settings->user_rtl ? 'right' : 'left';?>"></i>
                                    <?= lang('toggle_alignment') ?>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php if($Owner){ ?>
                        <li class="dropdown hidden-sm">
                        <a class="btn bblue tip" title="<?= lang('Admin Notification') ?>"
                            data-placement="bottom" data-container="body" href="<?= admin_url('notifications') ?>">
                            <i class="fa fa-info"></i>
                        </a>
                    </li>
                    <?php } ?>

                    <?php if (($Owner || $Admin || $GP['reports-quantity_alerts'] || $GP['reports-expiry_alerts']) && ($qty_alert_num > 0 || $exp_alert_num > 0 || $shop_sale_alerts)) { ?>
                        <li class="dropdown hidden-sm">
                            <a class="btn bblue tip" title="<?= lang('alerts') ?>"
                                data-placement="bottom" data-toggle="dropdown" href="#">
                                <i class="fa fa-bell-o"></i>
                                <span class="number bred black"><?= $qty_alert_num+(($Settings->product_expiry) ? $exp_alert_num : 0)+$shop_sale_alerts+$shop_payment_alerts; ?></span>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="<?= admin_url('reports/quantity_alerts') ?>" class="">
                                        <span class="label label-danger pull-right" style="margin-top:3px;"><?= $qty_alert_num; ?></span>
                                        <span style="padding-right: 35px;"><?= lang('quantity_alerts') ?></span>
                                    </a>
                                </li>
                                <?php if ($Settings->product_expiry) { ?>
                                <li>
                                    <a href="<?= admin_url('reports/expiry_alerts') ?>" class="">
                                        <span class="label label-danger pull-right" style="margin-top:3px;"><?= $exp_alert_num; ?></span>
                                        <span style="padding-right: 35px;"><?= lang('expiry_alerts') ?></span>
                                    </a>
                                </li>
                                <?php } ?>
                                <?php if ($shop_sale_alerts) { ?>
                                <li>
                                    <a href="<?= admin_url('sales?shop=yes&delivery=no') ?>" class="">
                                        <span class="label label-danger pull-right" style="margin-top:3px;"><?= $shop_sale_alerts; ?></span>
                                        <span style="padding-right: 35px;"><?= lang('sales_x_delivered') ?></span>
                                    </a>
                                </li>
                                <?php } ?>
                                <?php if ($shop_payment_alerts) { ?>
                                <li>
                                    <a href="<?= admin_url('sales?shop=yes&attachment=yes') ?>" class="">
                                        <span class="label label-danger pull-right" style="margin-top:3px;"><?= $shop_payment_alerts; ?></span>
                                        <span style="padding-right: 35px;"><?= lang('manual_payments') ?></span>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($Owner || $Admin) { ?>
                    <li class="dropdown hidden-xs">
                        <a class="btn bred tip" title="<?= lang('clear_ls') ?>" data-placement="bottom" id="clearLS" href="#">
                            <i class="fa fa-eraser"></i> <span class="padding05"><?= lang('Clear cache') ?></span>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </header>

    <div class="container" id="container">
        <div class="row" id="main-con">
        <table class="lt"><tr><td class="sidebar-con">
            <div id="sidebar-left">
                <div class="sidebar-nav nav-collapse collapse navbar-collapse" id="sidebar_menu">
                    <ul class="nav main-menu">

                        <?php if ($Owner || $Admin || $Manager) { ?>

                            <li class="custom_height <?php if(strtolower($this->router->fetch_method()) == 'payment_methods'){ echo ''; }else if( strtolower($this->router->fetch_method()) == 'shipment_methods' ){ echo ''; } else{ echo 'mm_products'; } ?> ">
                                <a class="dropmenu text-center" href="#">
                                    <div><i class="fa fa-tags color-green"></i></div>
                                    <span class="text"> <?= lang('products'); ?> </span>
                                    <!--<span class="chevron closed"></span>-->
                                </a>
                                <ul class="submenu_left_right submenu-pro">
                                    <li id="products_index">
                                        <a class="submenu" href="<?= admin_url('products'); ?>">
                                            <span class="text"> <?= lang('list_products'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_print_barcodes">
                                        <a class="submenu" href="<?= admin_url('products/print_barcodes'); ?>">
                                            <span class="text"> <?= lang('print_barcode_label'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_quantity_adjustments">
                                        <a class="submenu" href="<?= admin_url('products/quantity_adjustments'); ?>">
                                            <span class="text"> <?= lang('quantity_adjustments'); ?></span>
                                        </a>
                                    </li>

                                    <li id="products_stock_counts">
                                        <a class="submenu" href="<?= admin_url('products/stock_counts'); ?>">
                                            <span class="text"> <?= lang('stock_counts'); ?></span>
                                        </a>
                                    </li>

                                    <li id="products_categories">
                                        <a class="submenu" href="<?= admin_url('products/categories') ?>">
                                            <span class="text"> <?= lang('categories'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_units">
                                        <a class="submenu" href="<?= admin_url('products/units') ?>">
                                            <span class="text"> <?= lang('units'); ?></span>
                                        </a>
                                    </li>
                                    <li  id="products_brands">
                                        <a class="submenu" href="<?= admin_url('products/brands') ?>">
                                            <span class="text"> <?= lang('brands'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_variants">
                                        <a class="submenu" href="<?= admin_url('products/variants') ?>">
                                            <span class="text"> <?= lang('variants'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_variants_group">
                                        <a class="submenu" href="<?= admin_url('products/variants_group') ?>">
                                            <span class="text">Variants Group</span>
                                        </a>
                                    </li>

                                    <li id="products_sizing_guide">
                                        <a class="submenu" href="<?= admin_url('products/sizing_guide') ?>">
                                            <span class="text">Additional Guide</span>
                                        </a>
                                    </li>

                                    <li id="system_settings_user_upselling">
                                        <a class="submenu" href="<?= admin_url('products/upselling') ?>">
                                            <span class="text"> <?= lang('upselling'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="custom_height mm_customers mm_sales <?= strtolower($this->router->fetch_method()) == 'sales' ? 'mm_pos' : '' ?>">
                                <a class="dropmenu text-center" href="#">
                                    <div><i class="fa fa-heart color-blue"></i></div>
                                    <span class="text"> <?= lang('sales'); ?>
                                    <!--</span> <span class="chevron closed"></span>-->
                                </a>
                                <ul class="submenu_left_right submenu-sale-pro">
                                    <li id="sales_index">
                                        <a class="submenu" href="<?= admin_url('sales'); ?>">
                                            <span class="text"> <?= lang('list_sales'); ?></span>
                                        </a>
                                    </li>
                                    <?php if (POS) { ?>
                                    <li id="pos_sales">
                                        <a class="submenu" href="<?= admin_url('pos/sales'); ?>">
                                            <span class="text"> <?= lang('pos_sales'); ?></span>
                                        </a>
                                    </li>
                                    <?php } ?>

                                    <li id="sales_sale_by_csv">
                                        <a class="submenu" href="<?= admin_url('sales/sale_by_csv'); ?>">
                                            <span class="text"> <?= lang('add_sale_by_csv'); ?></span>
                                        </a>
                                    </li>
                                    <li id="sales_deliveries">
                                        <a class="submenu" href="<?= admin_url('sales/deliveries'); ?>">
                                            <span class="text"> <?= lang('deliveries'); ?></span>
                                        </a>
                                    </li>
                                    <li id="sales_gift_cards">
                                        <a class="submenu" href="<?= admin_url('sales/gift_cards'); ?>">
                                            <span class="text"> <?= lang('list_gift_cards'); ?></span>
                                        </a>
                                    </li>

                                    <li id="sales_index">
                                        <a class="submenu" href="<?= admin_url('quotes'); ?>">
                                            <span class="text"> <?= lang('list_quotes'); ?></span>
                                        </a>
                                    </li>

                                    <li id="sales_customer_groups" class="hide">
                                        <a class="submenu" href="<?= admin_url('sales/customer_groups') ?>">
                                            <span class="text"> Discount Group</span>
                                        </a>
                                    </li>

                                    <li id="sales_price_groups">
                                        <a class="submenu" href="<?= admin_url('sales/price_groups') ?>">
                                            <span class="text"> <?= lang('price_groups'); ?></span>
                                        </a>
                                    </li>

                                    <li id="customers_index">
                                        <a class="submenu" href="<?= admin_url('customers'); ?>">
                                            <span class="text"> <?= lang('list_customers'); ?></span>
                                        </a>
                                    </li>

                                </ul>
                            </li>


                            <li class="mm_purchases custom_height ">
                                <a class="dropmenu text-center" href="#">
                                    <div><i class="fa fa-shopping-cart color-maroon"></i></div>
                                    <span class="text"> <?= lang('purchases'); ?></span>
                                </a>
                                <ul class="submenu_left_right submenu-purchase-pro">
                                    <li id="suppliers_index">
                                        <a class="submenu" href="<?= admin_url('suppliers'); ?>">
                                            <span class="text"> <?= lang('list_suppliers'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_index">
                                        <a class="submenu" href="<?= admin_url('purchases'); ?>">
                                            <span class="text"> <?= lang('purchase_order'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_receipt">
                                        <a class="submenu" href="<?= admin_url('purchases/purchases_receipt'); ?>">
                                            <span class="text"> <?= lang('purchase_receipt'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_index">
                                        <a class="submenu" href="#">
                                            <span class="text"> <?= lang('advance_payment'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_index">
                                        <a class="submenu" href="#">
                                            <span class="text"> <?= lang('bill'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_index">
                                        <a class="submenu" href="#">
                                            <span class="text"> <?= lang('pay_bill'); ?></span>
                                        </a>
                                    </li>

                                    <!-- End -->
                                    <li id="purchases_index">
                                        <a class="submenu" href="#">
                                            <span class="text"> <?= lang('list_purchases'); ?></span>
                                        </a>
                                    </li>
                                    <li id="purchases_expenses">
                                        <a class="submenu" href="<?= admin_url('purchases/expenses'); ?>">
                                            <span class="text"> <?= lang('list_expenses'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="mm_transfers custom_height ">
                                <a class="dropmenu text-center" href="#">
                                    <div><i class="fa fa-arrows-alt color-green "></i></div>
                                    <span class="text"> <?= lang('transfers'); ?> </span>
                                </a>
                                <ul class="submenu_left_right submenu-transfer-pro">
                                    <li id="transfers_index">
                                        <a class="submenu" href="<?= admin_url('transfers'); ?>">
                                            <span class="text"> <?= lang('list_transfers'); ?></span>
                                        </a>
                                    </li>
                                    <li id="transfers_add">
                                        <a class="submenu" href="<?= admin_url('transfers/add'); ?>">
                                            <span class="text"> <?= lang('add_transfer'); ?></span>
                                        </a>
                                    </li>
                                    <li id="transfers_purchase_by_csv">
                                        <a class="submenu" href="<?= admin_url('transfers/transfer_by_csv'); ?>">
                                            <span class="text"> <?= lang('add_transfer_by_csv'); ?></span>
                                        </a>
                                    </li>
                                    <li id="system_settings_warehouses">
                                        <a class="submenu" href="<?= admin_url('system_settings/warehouses') ?>">
                                            <span class="text"> <?= lang('warehouses'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <?php if ($Owner) { ?>
                                <li class="custom_height mm_system_settings <?= strtolower($this->router->fetch_method()) == 'sales' ? '' : 'mm_pos' ?> <?= strtolower($this->router->fetch_method()) == 'payment_methods' ? 'mm_products' : '' ?>   <?= strtolower($this->router->fetch_method()) == 'shipment_methods' ? 'mm_products' : '' ?>">
                                    <a class="dropmenu text-center" href="#">
                                        <div><i class="fa fa-cog color-green"></i></div>
                                        <span class="text"> <?= lang('settings'); ?> </span>
                                    </a>
                                    <ul class="submenu_left_right submenu-setting-pro">
                                        <li id="system_settings_index">
                                            <a class="submenu" href="<?= admin_url('system_settings') ?>">
                                                <span class="text"> <?= lang('system_settings'); ?></span>
                                            </a>
                                        </li>
                                        <?php if (POS) { ?>

                                        <li id="pos_settings">
                                            <a class="submenu" href="<?= admin_url('pos/settings') ?>">
                                                <span class="text"> <?= lang('pos_settings'); ?></span>
                                            </a>
                                        </li>
                                            <li id="pos_locations">
                                                <a class="submenu" href="<?= admin_url('system_settings/locations') ?>">
                                                    <span class="text">Locations</span>
                                                </a>
                                            </li>
                                        <li id="products_payment_methods">
                                            <a class="submenu" href="<?= admin_url('products/payment_methods') ?>">
                                                <span class="text">Payment Methods</span>
                                            </a>
                                        </li>
                                        <li id="products_shipment_methods">
                                            <a class="submenu" href="<?= admin_url('products/shipment_methods') ?>">
                                                <span class="text">Shipment Methods</span>
                                            </a>
                                        </li>
                                        <li id="pos_printers">
                                            <a class="submenu" href="<?= admin_url('pos/printers') ?>">
                                                <span class="text"> <?= lang('list_printers'); ?></span>
                                            </a>
                                        </li>

                                        <li id="pos_add_printer">
                                            <a class="submenu" href="<?= admin_url('pos/add_printer') ?>">
                                                <span class="text"> <?= lang('add_printer'); ?></span>
                                            </a>
                                        </li>
                                        <li id="pos_add_expense">
                                            <a class="submenu" id="add_expense" data-placement="bottom" data-html="true" href="<?=admin_url('purchases/add_expense')?>" data-toggle="modal" data-target="#myModal">
                                                <span class="text"> <?= lang('add_expense'); ?></span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                        <li id="system_settings_change_logo">
                                            <a class="submenu" href="<?= admin_url('system_settings/change_logo') ?>" data-toggle="modal" data-target="#myModal">
                                                <span class="text"> <?= lang('change_logo'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_currencies">
                                            <a class="submenu" href="<?= admin_url('system_settings/currencies') ?>">
                                                <span class="text"> <?= lang('currencies'); ?></span>
                                            </a>
                                        </li>


                                        <li id="system_settings_expense_categories">
                                            <a class="submenu" href="<?= admin_url('system_settings/expense_categories') ?>">
                                                <span class="text"> <?= lang('expense_categories'); ?></span>
                                            </a>
                                        </li>

                                        <li id="system_settings_tax_rates">
                                            <a class="submenu" href="<?= admin_url('system_settings/tax_rates') ?>">
                                                <span class="text"> <?= lang('tax_rates'); ?></span>
                                            </a>
                                        </li>

                                        <li id="system_settings_email_templates">
                                            <a class="submenu" href="<?= admin_url('system_settings/email_templates') ?>">
                                                <span class="text"> <?= lang('email_templates'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_user_groups">
                                            <a class="submenu" href="<?= admin_url('system_settings/user_groups') ?>">
                                                <span class="text"> <?= lang('group_permissions'); ?></span>
                                            </a>
                                        </li>


                                        <li class="hide" id="system_settings_backups">
                                            <a class="submenu" href="<?= admin_url('system_settings/backups') ?>">
                                                <span class="text"> <?= lang('backups'); ?></span>
                                            </a>
                                        </li>

                                    </ul>

                                </li>
                            <?php } ?>

                            <li class="mm_auth mm_suppliers mm_billers custom_height ">
                                <a class="dropmenu text-center" href="#">
                                    <div><i class="fa fa-users color-blue"></i></div>
                                    <span class="text"> <?= lang('Users'); ?> </span>
                                </a>
                                <ul class="submenu_left_right submenu-users-pro">
                                    <?php if ($Owner) { ?>
                                        <li id="auth_users">
                                            <a class="submenu" href="<?= admin_url('users'); ?>">
                                                <span class="text"> <?= lang('list_users'); ?></span>
                                            </a>
                                        </li>
                                        <li id="billers_index">
                                            <a class="submenu" href="<?= admin_url('billers'); ?>">
                                                <span class="text"> <?= lang('list_billers'); ?></span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>

                        <?php if ($Owner) { ?>
                            <li class="mm_reports custom_height">
                                <a class="dropmenu text-center" href="#">
                                    <div><i class="fa fa-bar-chart-o color-maroon"></i></div>
                                    <span class="text"> <?= lang('reports'); ?> </span>
                                </a>

                                <ul class="submenu_left_right submenu-chart-pro">
                                    <li id="reports_index">
                                        <a class="submenu" id="today_profit" title="<span><?= lang('today_profit') ?></span>"
                                           data-placement="bottom" data-html="true" href="<?= admin_url('reports/profit') ?>"
                                           data-toggle="modal" data-target="#myModal">
                                            <span class="text"> <?= lang('today_profit'); ?></span>
                                        </a>
                                    </li>

                                    <li id="reports_index">
                                        <a class="submenu" href="<?= admin_url('reports') ?>">
                                            <span class="text"> <?= lang('overview_chart'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_warehouse_stock">
                                        <a class="submenu" href="<?= admin_url('reports/warehouse_stock') ?>">
                                            <span class="text"> <?= lang('warehouse_stock'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_best_sellers">
                                        <a class="submenu" href="<?= admin_url('reports/best_sellers') ?>">
                                            <span class="text"> <?= lang('best_sellers'); ?></span>
                                        </a>
                                    </li>
                                    <?php if (POS) { ?>
                                    <li id="reports_register">
                                        <a class="submenu" href="<?= admin_url('reports/register') ?>">
                                            <span class="text"> <?= lang('register_report'); ?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                    <li id="reports_quantity_alerts">
                                        <a class="submenu" href="<?= admin_url('reports/quantity_alerts') ?>">
                                            <span class="text"> <?= lang('product_quantity_alerts'); ?></span>
                                        </a>
                                    </li>
                                    <?php if ($Settings->product_expiry) { ?>
                                    <li id="reports_expiry_alerts">
                                        <a class="submenu" href="<?= admin_url('reports/expiry_alerts') ?>">
                                            <span class="text"> <?= lang('product_expiry_alerts'); ?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                    <li id="reports_products">
                                        <a class="submenu" href="<?= admin_url('reports/products') ?>">
                                            <span class="text"> <?= lang('products_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_adjustments">
                                        <a class="submenu" href="<?= admin_url('reports/adjustments') ?>">
                                            <span class="text"> <?= lang('adjustments_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_categories">
                                        <a class="submenu" href="<?= admin_url('reports/categories') ?>">
                                            <span class="text"> <?= lang('categories_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_brands">
                                        <a class="submenu" href="<?= admin_url('reports/brands') ?>">
                                            <span class="text"> <?= lang('brands_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_daily_sales">
                                        <a class="submenu" href="<?= admin_url('reports/daily_sales') ?>">
                                            <span class="text"> <?= lang('daily_sales'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_monthly_sales">
                                        <a class="submenu" href="<?= admin_url('reports/monthly_sales') ?>">
                                            <span class="text"> <?= lang('monthly_sales'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_sales">
                                        <a class="submenu" href="<?= admin_url('reports/sales') ?>">
                                            <span class="text"> <?= lang('sales_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_payments">
                                        <a class="submenu" href="<?= admin_url('reports/payments') ?>">
                                            <span class="text"> <?= lang('payments_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_profit_loss">
                                        <a class="submenu" href="<?= admin_url('reports/profit_loss') ?>">
                                            <span class="text"> <?= lang('profit_and_loss'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_daily_purchases">
                                        <a class="submenu" href="<?= admin_url('reports/daily_purchases') ?>">
                                            <span class="text"> <?= lang('daily_purchases'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_monthly_purchases">
                                        <a class="submenu" href="<?= admin_url('reports/monthly_purchases') ?>">
                                            <span class="text"> <?= lang('monthly_purchases'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_purchases">
                                        <a class="submenu" href="<?= admin_url('reports/purchases') ?>">
                                            <span class="text"> <?= lang('purchases_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_expenses">
                                        <a class="submenu" href="<?= admin_url('reports/expenses') ?>">
                                            <span class="text"> <?= lang('expenses_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_customer_report">
                                        <a class="submenu" href="<?= admin_url('reports/customers') ?>">
                                            <span class="text"> <?= lang('customers_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_supplier_report">
                                        <a class="submenu" href="<?= admin_url('reports/suppliers') ?>">
                                            <span class="text"> <?= lang('suppliers_report'); ?></span>
                                        </a>
                                    </li>
                                    <li id="reports_staff_report">
                                        <a class="submenu" href="<?= admin_url('reports/users') ?>">
                                            <span class="text"> <?= lang('staff_report'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>

                            <?php if ($Owner && file_exists(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'shop'.DIRECTORY_SEPARATOR.'Shop.php')) { ?>
                            <li class="mm_shop_settings mm_api_settings custom_height">
                                <a class="dropmenu text-center" href="#">
                                    <div><i class="fa fa-paint-brush color-green"></i></div>
                                    <span class="text"> <?= lang('front_end'); ?> </span>
                                </a>
                                <ul class="submenu_left_right submenu-frontend-pro">
                                    <li id="shop_settings_index">
                                        <a class="submenu" href="<?= admin_url('shop_settings') ?>">
                                            <span class="text"> <?= lang('shop_settings'); ?></span>
                                        </a>
                                    </li>
                                    <li id="shop_settings_index">
                                        <a class="submenu" href="<?= admin_url('megamenu') ?>">
                                            <span class="text"> Click Menu</span>
                                        </a>
                                    </li>
                                    <li id="shop_settings_slider">
                                        <a class="submenu" href="<?= admin_url('shop_settings/slider') ?>">
                                            <span class="text"> <?= lang('slider_settings'); ?></span>
                                        </a>
                                    </li>
                                    <?php if ($this->Settings->apis) { ?>
                                    <li id="api_settings_index">
                                        <a class="submenu" href="<?= admin_url('api_settings') ?>">
                                            <span class="text"> <?= lang('api_keys'); ?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                    <li id="shop_settings_pages">
                                        <a class="submenu" href="<?= admin_url('shop_settings/pages') ?>">
                                            <span class="text"> <?= lang('list_pages'); ?></span>
                                        </a>
                                    </li>
                                    <li id="shop_settings_pages">
                                        <a class="submenu" href="<?= admin_url('shop_settings/add_page') ?>">
                                            <span class="text"> <?= lang('add_page'); ?></span>
                                        </a>
                                    </li>
                                    <li id="shop_settings_landing_page_block">
                                        <a class="submenu" href="<?= admin_url('shop_settings/landing_page_block') ?>">
                                            <span class="text"> <?= lang('Landing_Page_Block'); ?></span>
                                        </a>
                                    </li>
                                    <li id="shop_settings_add_landing_page_block">
                                        <a class="submenu" href="<?= admin_url('shop_settings/add_landing_page_block') ?>">
                                            <span class="text"> <?= lang('add_block'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                                <li class="mm_marketing custom_height">
                                    <a class="dropmenu text-center" href="#">
                                        <div><i class="fa fa-bell color-maroon"></i></div>
                                        <span class="text">Marketing</span>
                                    </a>

                                    <ul class="submenu_left_right submenu-marketing">
                                        <li id="reports_index">
                                            <a class="submenu" href="<?= admin_url('marketing') ?>">
                                                <span class="text">Google Feeds</span>
                                            </a>
                                        </li>
                                        <li id="reports_index">
                                            <a class="submenu" href="<?= admin_url('marketing/g_analytics') ?>">
                                                <span class="text">Google Tag Manager</span>
                                            </a>
                                        </li>
                                        <li id="site_map_index">
                                            <a class="submenu" href="<?= admin_url('marketing/genrate_sitemap') ?>">
                                                <span class="text">Site Map</span>
                                            </a>
                                        </li>
                                        <li id="site_map_index">
                                            <a class="submenu" href="<?= admin_url('marketing/seo_settings') ?>">
                                                <span class="text">Seo Settings</span>
                                            </a>
                                        </li>
                                        <li id="site_map_index">
                                            <a class="submenu" href="<?= admin_url('marketing/open_graph_settings') ?>">
                                                <span class="text">Open Graph Settings</span>
                                            </a>
                                        </li>
                                        <li id="site_map_index">
                                            <a class="submenu" href="<?= admin_url('marketing/subscribers') ?>">
                                                <span class="text">Subscribers</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php } ?>

                        <?php
                        } else { // not owner and not admin
                            ?>
                            <?php if ($GP['products-index'] || $GP['products-add'] || $GP['products-barcode'] || $GP['products-adjustments'] || $GP['products-stock_count']) { ?>
                                <li class="mm_products custom_height">
                                    <a class="dropmenu text-center" href="#">
                                        <div><i class="fa fa-tags color-green"></i></div>
                                        <span class="text"> <?= lang('products'); ?> </span>
                                        <!--<span class="chevron closed"></span>-->
                                    </a>
                                    <ul class="submenu_left_right submenu-pro">
                                        <li id="products_index">
                                            <a class="submenu" href="<?= admin_url('products'); ?>">
                                                <span class="text"> <?= lang('list_products'); ?></span>
                                            </a>
                                        </li>
                                        <li id="products_print_barcodes" >
                                            <a class="submenu" href="<?= admin_url('products/print_barcodes'); ?>">
                                                <span class="text"> <?= lang('print_barcode_label'); ?></span>
                                            </a>
                                        </li>
                                        <li id="products_quantity_adjustments" >
                                            <a class="submenu" href="<?= admin_url('products/quantity_adjustments'); ?>">
                                                <span class="text"> <?= lang('quantity_adjustments'); ?></span>
                                            </a>
                                        </li>

                                        <li id="products_stock_counts">
                                            <a class="submenu" href="<?= admin_url('products/stock_counts'); ?>">
                                                <span class="text"> <?= lang('stock_counts'); ?></span>
                                            </a>
                                        </li>

                                        <li id="products_categories">
                                            <a class="submenu" href="<?= admin_url('products/categories') ?>">
                                                <span class="text"> <?= lang('categories'); ?></span>
                                            </a>
                                        </li>

                                        <li id="products_units" >
                                            <a class="submenu" href="<?= admin_url('products/units') ?>">
                                                <span class="text"> <?= lang('units'); ?></span>
                                            </a>
                                        </li>
                                        <li  id="products_brands">
                                            <a class="submenu" href="<?= admin_url('products/brands') ?>">
                                                <span class="text"> <?= lang('brands'); ?></span>
                                            </a>
                                        </li>
                                        <li id="products_variants">
                                            <a class="submenu" href="<?= admin_url('products/variants') ?>">
                                                <span class="text"> <?= lang('variants'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_user_upselling">
                                            <a class="submenu" href="<?= admin_url('products/upselling') ?>">
                                                <span class="text"> <?= lang('upselling'); ?></span>
                                            </a>
                                        </li>

                                    </ul>
                                </li>
                            <?php } ?>

                            <?php if ($GP['customers-index'] || $GP['customers-add'] || $GP['sales-index'] || $GP['sales-add'] || $GP['sales-deliveries'] || $GP['sales-gift_cards']) { ?>
                                <li class="mm_customers custom_height mm_sales <?= strtolower($this->router->fetch_method()) == 'sales' ? 'mm_pos' : '' ?>">
                                    <a class="dropmenu text-center" href="#">
                                        <div><i class="fa fa-heart color-blue"></i></div>
                                        <span class="text"> <?= lang('sales'); ?>
                                            <!--</span> <span class="chevron closed"></span>-->
                                    </a>
                                    <ul class="submenu_left_right submenu-sale-pro">
                                        <li id="sales_index">
                                            <a class="submenu" href="<?= admin_url('sales'); ?>">
                                                <span class="text"> <?= lang('list_sales'); ?></span>
                                            </a>
                                        </li>
                                        <?php if (POS) { ?>
                                            <li id="pos_sales">
                                                <a class="submenu" href="<?= admin_url('pos/sales'); ?>">
                                                    <span class="text"> <?= lang('pos_sales'); ?></span>
                                                </a>
                                            </li>
                                        <?php } ?>
                                        <li id="sales_add">
                                            <a class="submenu" href="<?= admin_url('sales/add'); ?>">
                                                <span class="text"> <?= lang('add_sale'); ?></span>
                                            </a>
                                        </li>
                                        <li id="sales_sale_by_csv">
                                            <a class="submenu" href="<?= admin_url('sales/sale_by_csv'); ?>">
                                                <span class="text"> <?= lang('add_sale_by_csv'); ?></span>
                                            </a>
                                        </li>
                                        <li id="sales_deliveries">
                                            <a class="submenu" href="<?= admin_url('sales/deliveries'); ?>">
                                                <span class="text"> <?= lang('deliveries'); ?></span>
                                            </a>
                                        </li>
                                        <li id="sales_gift_cards">
                                            <a class="submenu" href="<?= admin_url('sales/gift_cards'); ?>">
                                                <span class="text"> <?= lang('list_gift_cards'); ?></span>
                                            </a>
                                        </li>

                                        <li id="sales_index">
                                            <a class="submenu" href="<?= admin_url('quotes'); ?>">
                                                <span class="text"> <?= lang('list_quotes'); ?></span>
                                            </a>
                                        </li>

                                        <li id="customers_index">
                                            <a class="submenu" href="<?= admin_url('customers'); ?>">
                                                <span class="text"> <?= lang('list_customers'); ?></span>
                                            </a>
                                        </li>

                                    </ul>
                                </li>
                            <?php } ?>

                            <?php if ($GP['purchases-index'] || $GP['purchases-add'] || $GP['purchases-expenses']) { ?>
                                <li class="mm_purchases custom_height">
                                    <a class="dropmenu text-center" href="#">
                                        <div><i class="fa fa-shopping-cart color-maroon"></i></div>
                                        <span class="text"> <?= lang('purchases'); ?></span>
                                    </a>
                                    <ul class="submenu_left_right submenu-purchase-pro">
                                        <li id="purchases_index">
                                            <a class="submenu" href="<?= admin_url('purchases'); ?>">
                                                <span class="text"> <?= lang('list_purchases'); ?></span>
                                            </a>
                                        </li>
                                        <li id="purchases_add">
                                            <a class="submenu" href="<?= admin_url('purchases/add'); ?>">
                                                <span class="text"> <?= lang('add_purchase'); ?></span>
                                            </a>
                                        </li>
                                        <li id="purchases_purchase_by_csv">
                                            <a class="submenu" href="<?= admin_url('purchases/purchase_by_csv'); ?>">
                                                <span class="text"> <?= lang('add_purchase_by_csv'); ?></span>
                                            </a>
                                        </li>
                                        <li id="purchases_expenses">
                                            <a class="submenu" href="<?= admin_url('purchases/expenses'); ?>">
                                                <span class="text"> <?= lang('list_expenses'); ?></span>
                                            </a>
                                        </li>
                                        <li id="purchases_add_expense">
                                            <a class="submenu" href="<?= admin_url('purchases/add_expense'); ?>" data-toggle="modal" data-target="#myModal">
                                                <span class="text"> <?= lang('add_expense'); ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php } ?>

                            <?php if ($GP['transfers-index'] || $GP['transfers-add']) { ?>
                                <li class="mm_transfers custom_height">
                                    <a class="dropmenu text-center" href="#">
                                        <div><i class="fa fa-arrows-alt color-green "></i></div>
                                        <span class="text"> <?= lang('transfers'); ?> </span>
                                    </a>
                                    <ul class="submenu_left_right submenu-transfer-pro">
                                        <li id="transfers_index">
                                            <a class="submenu" href="<?= admin_url('transfers'); ?>">
                                                <span class="text"> <?= lang('list_transfers'); ?></span>
                                            </a>
                                        </li>
                                        <li id="transfers_add">
                                            <a class="submenu" href="<?= admin_url('transfers/add'); ?>">
                                                <span class="text"> <?= lang('add_transfer'); ?></span>
                                            </a>
                                        </li>
                                        <li id="transfers_purchase_by_csv">
                                            <a class="submenu" href="<?= admin_url('transfers/transfer_by_csv'); ?>">
                                                <span class="text"> <?= lang('add_transfer_by_csv'); ?></span>
                                            </a>
                                        </li>
                                        <li id="system_settings_warehouses">
                                            <a class="submenu" href="<?= admin_url('transfers/warehouses') ?>">
                                                <span class="text"> <?= lang('warehouses'); ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php } ?>

                            <?php if ($GP['suppliers-index'] || $GP['suppliers-add']) { ?>
                                <li class="mm_auth mm_suppliers mm_billers custom_height">
                                    <a class="dropmenu text-center" href="#">
                                        <div><i class="fa fa-users color-blue"></i></div>
                                        <span class="text"> <?= lang('Users'); ?> </span>
                                    </a>
                                    <ul class="submenu_left_right submenu-users-pro">
                                        <?php if ($Owner) { ?>
                                            <li id="auth_users">
                                                <a class="submenu" href="<?= admin_url('users'); ?>">
                                                    <span class="text"> <?= lang('list_users'); ?></span>
                                                </a>
                                            </li>
                                            <li id="auth_create_user">
                                                <a class="submenu" href="<?= admin_url('users/create_user'); ?>">
                                                    <span class="text"> <?= lang('new_user'); ?></span>
                                                </a>
                                            </li>
                                            <li id="billers_index">
                                                <a class="submenu" href="<?= admin_url('billers'); ?>">
                                                    <span class="text"> <?= lang('list_billers'); ?></span>
                                                </a>
                                            </li>
                                            <li id="billers_index">
                                                <a class="submenu" href="<?= admin_url('billers/add'); ?>" data-toggle="modal" data-target="#myModal">
                                                    <span class="text"> <?= lang('add_biller'); ?></span>
                                                </a>
                                            </li>
                                        <?php } ?>

                                    </ul>
                                </li>
                            <?php } ?>

                            <?php if ($GP['reports-quantity_alerts'] || $GP['reports-expiry_alerts'] || $GP['reports-products'] || $GP['reports-monthly_sales'] || $GP['reports-sales'] || $GP['reports-payments'] || $GP['reports-purchases'] || $GP['reports-customers'] || $GP['reports-suppliers'] || $GP['reports-expenses']) { ?>
                                <li class="mm_reports custom_height ">
                                    <a class="dropmenu text-center" href="#">
                                        <div><i class="fa fa-bar-chart-o color-maroon"></i></div>
                                        <span class="text"> <?= lang('reports'); ?> </span>
                                    </a>
                                    <ul class="submenu_left_right submenu-chart-pro">
                                        <li id="reports_index">
                                            <a class="submenu" href="<?= admin_url('reports') ?>">
                                                <span class="text"> <?= lang('overview_chart'); ?></span>
                                            </a>
                                        </li>
                                        <li id="reports_warehouse_stock">
                                            <a class="submenu" href="<?= admin_url('reports/warehouse_stock') ?>">
                                                <span class="text"> <?= lang('warehouse_stock'); ?></span>
                                            </a>
                                        </li>
                                        <li id="reports_best_sellers">
                                            <a class="submenu" href="<?= admin_url('reports/best_sellers') ?>">
                                                <span class="text"> <?= lang('best_sellers'); ?></span>
                                            </a>
                                        </li>
                                        <?php if (POS) { ?>
                                            <li id="reports_register">
                                                <a class="submenu" href="<?= admin_url('reports/register') ?>">
                                                    <span class="text"> <?= lang('register_report'); ?></span>
                                                </a>
                                            </li>
                                        <?php } ?>
                                        <li id="reports_quantity_alerts">
                                            <a class="submenu" href="<?= admin_url('reports/quantity_alerts') ?>">
                                                <span class="text"> <?= lang('product_quantity_alerts'); ?></span>
                                            </a>
                                        </li>
                                        <?php if ($Settings->product_expiry) { ?>
                                            <li id="reports_expiry_alerts">
                                                <a class="submenu" href="<?= admin_url('reports/expiry_alerts') ?>">
                                                    <span class="text"> <?= lang('product_expiry_alerts'); ?></span>
                                                </a>
                                            </li>
                                        <?php } ?>
                                        <li id="reports_products">
                                            <a class="submenu" href="<?= admin_url('reports/products') ?>">
                                                <span class="text"> <?= lang('products_report'); ?></span>
                                            </a>
                                        </li>
                                        <li id="reports_adjustments">
                                            <a class="submenu" href="<?= admin_url('reports/adjustments') ?>">
                                                <span class="text"> <?= lang('adjustments_report'); ?></span>
                                            </a>
                                        </li>
                                        <li id="reports_categories">
                                            <a class="submenu" href="<?= admin_url('reports/categories') ?>">
                                                <span class="text"> <?= lang('categories_report'); ?></span>
                                            </a>
                                        </li>
                                        <li id="reports_brands">
                                            <a class="submenu" href="<?= admin_url('reports/brands') ?>">
                                                <span class="text"> <?= lang('brands_report'); ?></span>
                                            </a>
                                        </li>
                                        <li id="reports_daily_sales">
                                            <a class="submenu" href="<?= admin_url('reports/daily_sales') ?>">
                                                <span class="text"> <?= lang('daily_sales'); ?></span>
                                            </a>
                                        </li>
                                        <li id="reports_monthly_sales">
                                            <a class="submenu" href="<?= admin_url('reports/monthly_sales') ?>">
                                                <span class="text"> <?= lang('monthly_sales'); ?></span>
                                            </a>
                                        </li>
                                        <li id="reports_sales">
                                            <a class="submenu" href="<?= admin_url('reports/sales') ?>">
                                                <span class="text"> <?= lang('sales_report'); ?></span>
                                            </a>
                                        </li>
                                        <li id="reports_payments">
                                            <a class="submenu" href="<?= admin_url('reports/payments') ?>">
                                                <span class="text"> <?= lang('payments_report'); ?></span>
                                            </a>
                                        </li>
                                        <li id="reports_profit_loss">
                                            <a class="submenu" href="<?= admin_url('reports/profit_loss') ?>">
                                                <span class="text"> <?= lang('profit_and_loss'); ?></span>
                                            </a>
                                        </li>
                                        <li id="reports_daily_purchases">
                                            <a class="submenu" href="<?= admin_url('reports/daily_purchases') ?>">
                                                <span class="text"> <?= lang('daily_purchases'); ?></span>
                                            </a>
                                        </li>
                                        <li id="reports_monthly_purchases">
                                            <a class="submenu" href="<?= admin_url('reports/monthly_purchases') ?>">
                                                <span class="text"> <?= lang('monthly_purchases'); ?></span>
                                            </a>
                                        </li>
                                        <li id="reports_purchases">
                                            <a class="submenu" href="<?= admin_url('reports/purchases') ?>">
                                                <span class="text"> <?= lang('purchases_report'); ?></span>
                                            </a>
                                        </li>
                                        <li id="reports_expenses">
                                            <a class="submenu" href="<?= admin_url('reports/expenses') ?>">
                                                <span class="text"> <?= lang('expenses_report'); ?></span>
                                            </a>
                                        </li>
                                        <li id="reports_customer_report">
                                            <a class="submenu" href="<?= admin_url('reports/customers') ?>">
                                                <span class="text"> <?= lang('customers_report'); ?></span>
                                            </a>
                                        </li>
                                        <li id="reports_supplier_report">
                                            <a class="submenu" href="<?= admin_url('reports/suppliers') ?>">
                                                <span class="text"> <?= lang('suppliers_report'); ?></span>
                                            </a>
                                        </li>
                                        <li id="reports_staff_report">
                                            <a class="submenu" href="<?= admin_url('reports/users') ?>">
                                                <span class="text"> <?= lang('staff_report'); ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php } ?>

                        <?php } ?>
                    </ul>
                </div>
                <a href="#" id="main-menu-act" class="full visible-md visible-lg">
                    <i class="fa fa-angle-double-left"></i>
                </a>
            </div>
            </td><td class="content-con">
            <div id="content">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <ul class="breadcrumb">
                            <?php
                            foreach ($bc as $b) {
                                if ($b['link'] === '#') {
                                    echo '<li class="active">' . $b['page'] . '</li>';
                                } else {
                                    echo '<li><a href="' . $b['link'] . '">' . $b['page'] . '</a></li>';
                                }
                            }
                            ?>
                            <li class="right_log hidden-xs">
                                <?= lang('your_ip') . ' ' . $ip_address . " <span class='hidden-sm'>( " . lang('last_login_at') . ": " . date($dateFormats['php_ldate'], $this->session->userdata('old_last_login')) . " " . ($this->session->userdata('last_ip') != $ip_address ? lang('ip:') . ' ' . $this->session->userdata('last_ip') : '') . " )</span>" ?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($message) { ?>
                            <div class="alert alert-success">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?= $message; ?>
                            </div>
                        <?php } ?>
                        <?php if ($error) { ?>
                            <div class="alert alert-danger">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?= $error; ?>
                            </div>
                        <?php } ?>
                        <?php if ($warning) { ?>
                            <div class="alert alert-warning">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?= $warning; ?>
                            </div>
                        <?php } ?>
                        <?php
                        if ($info) {
                            foreach ($info as $n) {
                                if (!$this->session->userdata('hidden' . $n->id)) {
                                    ?>
                                    <div class="alert alert-info">
                                        <a href="#" id="<?= $n->id ?>" class="close hideComment external"
                                           data-dismiss="alert">&times;</a>
                                        <?= $n->comment; ?>
                                    </div>
                                <?php }
                            }
                        } ?>
                        <div class="alerts-con"></div>