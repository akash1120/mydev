<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style>
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    .box .box-header {
        height: 50px;
    }
    .img-thumbnail {
        height:80px !important;
    }
</style>
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 48px;
        height: 24px;
    }

    .switch input {display:none;}

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 22px;
        width: 22px;
        left: 0px;
        bottom: 1px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #00BA70;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #00BA70;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
    .padding_top30 {
        padding-top: 30px;
    }
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

<style></style>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-home"></i>Open Graph Settings</h2>

    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext">Open Graph Settings for all pages</p>

                <div class="table-responsive">
<div class="container col-sm-8" style="padding: 50px;">

    <table id="example" class="display" style="width:100%">
        <thead>
        <tr>
            <th>Serial #</th>
            <th>Page Slug</th>
            <th>Title</th>
            <th>Description</th>
            <th>Image</th>
            <th>Update</th>


        </tr>
        </thead>
        <tbody>

        <?php


      /*  echo "<pre>";
        print_r($slug_array_data);
        exit('sdf');*/


        foreach ($slug_array_data as $key => $data){
            $image = $data['image'];
            if(!$data['image']){
                $image = 'no_image.png';
            }?>
            <tr>
                <td><?= $data['id'] ?></td>
                <td><?= $data['slug'] ?></td>
                <td><?= $data['opn_title'] ?></td>
                <td><?= $data['opn_description'] ?></td>
               <!-- <td><?/*= $data['image'] */?></td>-->
                <td><img src="<?= base_url().'ucloud/plugins/filepreviewer/site/resize_image_inline.php?f='.$image.'&w=160&h=134&m=medium&uh=&o=jpg'; ?>" class="img-thumbnail" alt="Cinque Terre" width="80" height="100"> </td>
                <td> <a class="btn btn-success seoeditimage" data_seo_title="<?= $data['seo_title'];?>" data_seo_description="<?= $data['seo_description'];?>" data_main_image="<?= $data['main_image'];?>" data_slug="<?= $data['slug'];?>" data_opn_title="<?= $data['opn_title'];?>" data_opn_description="<?= $data['opn_description'];?>" data_img="<?= $data['image']?>"><i class="fa fa-edit"></i></a> </td>
 


            </tr>
        <?php } ?>

        </tbody>
        <tfoot>
        <tr>
            <th>Serial #</th>
            <th>Page Slug</th>
            <th>Title</th>
            <th>Description</th>
            <th></th>
            <th></th>
        </tr>
        </tfoot>
    </table>
</div>
               </div>

            </div>

        </div>
    </div>
</div>





<script>
    $(document).ready(function() {
        // Setup - add a text input to each footer cell
        var count=1;
        $('#example tfoot th').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" class="btm_search'+count+'" placeholder="Search '+title+'" />' );
            count++;
        } );

        // DataTable
        var table = $('#example').DataTable();

        // Apply the search
        table.columns().every( function () {
            var that = this;

            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );
        } );
        //$(".btm_search:last").hide();
       // $(".btm_search:last").prev().hide();
        $(".btm_search5").hide();
        $(".btm_search6").hide();
        //
       // $('#meta_description').redactor('core.destroy');


    } );
    $('.seoeditimage').click(function() {
        var slug = $(this).attr('data_slug');
        var seo_title = $(this).attr('data_seo_title');
        var seo_description = $(this).attr('data_seo_description');
        var image_default = $(this).attr('data_main_image');
        var meta_title = $(this).attr('data_opn_title');
        var meta_description = $(this).attr('data_opn_description');


        $('#slug').val(slug);
        $('.slug_readonly').val(slug);

        if(meta_title){
            $('#opn_title').val(meta_title);
        }else{
            $('#opn_title').val(seo_title);
        }


        //rawHTML = $('#opn_description').html().replace(//igm,'');
        $('#opn_description').redactor();
        $('#opn_description').redactor('destroy');

        if(meta_description){
            $('#opn_description').val(meta_description);
        }else{
            $('#opn_description').val(seo_description);
        }


        var img = $(this).attr('data_img');
       // var link = $(this).attr('data_link');
        var urlimag = '<?= site_url() . 'ucloud/plugins/filepreviewer/site/resize_image_inline.php?f='; ?>' + image_default + '&w=160&h=134&m=medium&uh=&o=jpg';
        if(img ==''){
            var img_link = '<?= site_url() . 'ucloud/plugins/filepreviewer/site/resize_image_inline.php?f='; ?>' + image_default + '&w=160&h=134&m=medium&uh=&o=jpg';
            $('#old_image').val(image_default);
        }else{
            var img_link = '<?= site_url() . 'ucloud/plugins/filepreviewer/site/resize_image_inline.php?f='; ?>' + img + '&w=160&h=134&m=medium&uh=&o=jpg';
            $('#old_image').val(img);
        }
        $('.main_image_edit').attr('src',img_link);


        $('.editseo').modal('show');

        $('.default_title').on('click',function(){
            $('#opn_title').val(seo_title);
        });
        $('.default_description').on('click',function(){
            $('#opn_description').val(seo_description);
        });
        $('.default_image').on('click',function(){
            $('.main_image_edit').attr('src','<?= site_url() . 'ucloud/plugins/filepreviewer/site/resize_image_inline.php?f=&w=160&h=134&m=medium&uh=&o=jpg'; ?>');
            $('#pimage_id').val('');
        });





    });






</script>



<div class="modal fade editseo" id="myModalee" >
    <div class="modal-dialog modal-md" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <h4 class="modal-title" id="myModalLabel">Open Graph Settings</h4>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                    <p class="introtext">Please update the information below</p>
                    <?php
                    $attrib = array('role' => 'form');
                    echo admin_form_open_multipart("marketing/update_open_graph_settings", $attrib)
                    ?>
                    <div class="col-md-12">



                        <div class="panel panel-default padding10 border_radius3" style="padding: 10px 10px !important;">
                            <div class="row">

                                    <input name="slug" type="hidden" id="slug" value="">
                                    <input name="old_image" type="hidden" id="old_image" value="">

                                <div class="col-md-8">
                                    <div class="form-group">
                                        <?= lang('title', 'link1'); ?>
                                        <?= form_input('opn_title',set_value('opn_title'), 'class="form-control tip" id="opn_title"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4 padding_top30" >
                                <div class="form-group">
                                        <a class="btn btn-success default_title" > Default Title</a>
                                </div>
                               </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang('Slug', 'Slug'); ?>
                                      <input type="text" class="form-control slug_readonly" value="" readonly>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <?= lang('description', 'caption1'); ?>
                                        <textarea name="opn_description" class="form-control border_radius3 tip" id="opn_description"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4 padding_top30" >
                                    <div class="form-group">
                                        <a class="btn btn-success default_description" > Default Description</a>
                                    </div>
                                </div>
                              <!--  <div class="col-md-12">-->

                                <div class="form-group1">

                                    <div class="form-group all col-md-7 image_padding">
                                        <?= lang("product_front_image", "product_image") ?>
                                        <div class="input-group">
                                            <input type="hidden" id="pimage_id" name="image" readonly value="" class="form-control mediaimage_id" placeholder="Select Image">
                                            <input type="text" id="pimage"  readonly value="" class="form-control mediaimage" placeholder="Select Image">
                                            <span class="input-group-btn ">
                                    <button id="mediagallery" class="btn btn-primary mediagallery width120" type="button">Select Image</button>
                                </span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label for="main_image" id="main_image" class="img_gallery_class mainboxmain_image" ><img id="main_image" class="img-thumbnail main_image_edit  admin_product_imgsize boxmain_image" alt="" src=""></label>
                                    </div>
                                    <input class="alt_text" name="image_alt" value="" type="hidden">
                                    <div class="col-md-3 paddingtop55">
                                        <label><input type="checkbox" name="image_remove" > Remove</label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>


                                    <div class="col-md-12 padding_top30 " >
                                    <div class="form-group">
                                        <a class="btn btn-success default_image pull-right" > Default Image</a>
                                    </div>
                                   </div>

                             <!--   </div>-->





                            </div>
                        </div>


                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <?php echo form_submit('edit_meta_setting', 'Submit', 'class="btn btn-primary btn_edit_product_custom"'); ?>
                        </div>

                    </div>
                    <?= form_close(); ?>


                </div>
                <div class="clearfix"></div>
            </div>
            <!--<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

        <!-- /.modal-content -->
        </div>
        <div class="clearfix"></div>
        <!-- /.modal-dialog -->
    </div>
</div>