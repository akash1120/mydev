<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel">Add Subscriber</h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("marketing/add_subscriber", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <label>Email*</label>
                <input type="email" name="email" value="" class="form-control tip" id="email" required="required" data-bv-field="email">

            </div>


            <div class="form-group">
                <label>Status</label>
              <select name="status" style="width: 100%">
                  <option value="1"> Enabled </option>
                  <option value="0"> Disabled </option>
              </select>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('add_subscriber', 'Add Subscriber', 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>
