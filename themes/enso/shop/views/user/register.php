<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style>
  /*  .well{
        background-color: #ffffff;
    }*/
</style>
<!-- Page Breadcrumb Start -->
<div class="main-breadcrumb mb-100">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="breadcrumb-content text-center ptb-70">
                    <ul class="breadcrumb-list breadcrumb">
                        <li><a href="<?php echo base_url(); ?>">home</a></li>
                      <!--  <li><a href="#">account</a></li>-->
                        <li><a href="<?php echo base_url().'main/register_form'; ?>">register</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
<!-- Page Breadcrumb End -->


<section class="page-contents">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php
                if($this->session->flashdata('error_msg')){
                    echo '<div class="alert alert-danger">'. $this->session->flashdata('error_msg').'</div>';
                }
                 ?>
                <div class="row">
                    <div id="content" class="col-sm-12 register-form">

                        <h1 class="text-center">Register Account</h1>
                        <p class="text-center">If you already have an account with us, please login at the <strong><a href="<?= base_url().'login' ?>">login page</a></strong>.</p>
                        <form action="<?= base_url() ?>register" class="validate fv-form fv-form-bootstrap form-horizontal" enctype="multipart/form-data" role="form" method="post" accept-charset="utf-8" novalidate="novalidate">

                            <fieldset id="account">
                                <legend class="text-center">Your Personal Details</legend>
                                <div class="form-group required" style="display: none;">
                                    <label class="col-sm-2 control-label">Customer Group</label>
                                    <div class="col-sm-10">
                                        <div class="radio">
                                            <label><input type="radio" name="customer_group_id" value="1" checked="checked">Default</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="required col-sm-6 col-sm-offset-3">
                                    <label class="control-label " for="f-name"><span class="require">*</span>Full Name</label>
                                    <div class="">
                                        <?= form_input('first_name', '', 'class="form-control" placeholder="Full Name"  id="first_name" required="required" pattern=".{3,10}"'); ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <!--  <div class="form-group required">
                                    <label class="control-label col-sm-2" for="l-name"><span class="require">*</span>Last Name</label>
                                    <div class="col-sm-10">
                                        <?/*= form_input('last_name', '', 'class="form-control" placeholder="Last Name" id="last_name" required="required"'); */?>
                                    </div>
                                </div>-->
                                <div class="form-group required hide">
                                    <label class="col-sm-2 control-label" for="input-email"> <span class="require">*</span>User Name</label>
                                    <div class="col-sm-10">
                                        <?= form_input('username', set_value('username'), 'class="form-control tip" placeholder="User Name" id="username" required="required"'); ?>
                                    </div>
                                </div>


                                <div class="required col-sm-6 col-sm-offset-3">
                                    <label class="control-label " for="email"><span class="require">*</span>Enter you email address</label>
                                    <div class=" ">
                                        <input type="email" id="email" name="email" placeholder="Enter you email address" class="form-control" required="required"/>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="required col-sm-6 col-sm-offset-3">
                                    <label class="control-label " for="pwd"><span class="require">*</span>Password:</label>
                                    <div class=" ">
                                        <?= form_password('password', '', 'class="form-control tip" id="password" placeholder="Password" required="required" '); ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="required col-sm-6 col-sm-offset-3">
                                    <label class="control-label " for="pwd-confirm"><span class="require">*</span>Confirm Password</label>
                                    <div class=" ">
                                        <?= form_password('password_confirm', '', 'class="form-control" placeholder="Password Confirm" id="password_confirm" required="required" data-bv-identical="true" data-bv-identical-field="password" data-bv-identical-message="' . lang('pw_not_same') . '"'); ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>


                                <div class="required col-sm-6 col-sm-offset-3">
                                    <label class="control-label " for="number"><span class="require">*</span>Telephone</label>
                                    <div class=" ">
                                        <?= form_input('phone', '', 'class="form-control" placeholder="Telephone" id="phone"'); ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group hide">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-telephone">Company</label>
                                        <?= form_input('company', 'Enso', 'class="form-control tip" id="company"'); ?>
                                    </div>
                                </div>

                            </fieldset><br/>


                            <div class="buttons newsletter-input text-center">
                                <div class="">I have read and agree to the <a href="#" class="agree"><b><a target="_blank" href="http://ensodubai.com/page/privacy-policy">Privacy Policy</a></b></a>
                                    <input type="checkbox" name="agree" value="1"> &nbsp;
                                    <div class="clearfix"></div>
                                    <input type="submit" value="Continue" class="newsletter-btn checkout" style="margin-top: 5px;">
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="clearfix" style="margin-top: 20px;"></div>
