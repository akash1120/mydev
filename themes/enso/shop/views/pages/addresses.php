<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- Page Breadcrumb Start -->
<div class="main-breadcrumb mb-100">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="breadcrumb-content text-center ptb-70">
                    <ul class="breadcrumb-list breadcrumb">
                        <li><a href="<?php echo base_url(); ?>">home</a></li>
                        <!--   <li><a href="#">account</a></li>-->
                        <li><a href="<?php echo base_url().'shop/addresses'; ?>">Addresses</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
<!-- Page Breadcrumb End -->

<section class="page-contents">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                <div class="row">
                    <div class="col-sm-12 col-md-12">

                        <div class="panel panel-default margin-top-lg">
                            <div class="panel-heading text-bold">
                                <i class="fa fa-map margin-right-sm"></i> <?= lang('my_addresses'); ?>
                            </div>
                            <div class="panel-body">
                                <?php
                                if (!empty($addresses)) {
                                    echo '<div class="row">';
                                    echo '<div class="col-sm-12 text-bold">'.lang('select_address_to_edit').'</div>';
                                    $r = 1;
                                    foreach ($addresses as $address) {
                                        ?>
                                        <div class="col-sm-6">
                                            <div class="col-sm-10">
                                            <a href="#" class="link-address edit-address" data-id="<?= $address->id; ?>">
                                                    <?= $address->line1; ?><br>
                                                    <?= $address->line2; ?><br>
                                                    <?= $address->city; ?> <?= $address->state; ?><br>
                                                    <?= $address->postal_code; ?> <?= $address->country; ?><br>
                                                    <?= lang('phone').': '.$address->phone; ?>
                                                    <span class="count"><i><?= $r; ?></i></span>
                                                    <span class="edit"><i class="fa fa-edit"></i></span>
                                                </a>
                                            </div>
                                                <div class="col-sm-1">
                                            <a href="#" class="remove-address" data-id="<?= $address->id; ?>">
                                                <span class="edit"><i class="fa fa-trash address_remove"></i></span>
                                            </a>
                                                </div>
                                        </div>
                                        <?php
                                        $r++;
                                    }
                                    echo '</div>';
                                }
                                if (count($addresses) < 6) {
                                    echo '<div class="row margin-top-lg">';
                                    echo '<div class="col-sm-12"><a href="#" id="add-address" class="newsletter-btn address-btn-width">'.lang('add_address').'</a></div>';
                                    echo '</div>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
var addresses = <?= !empty($addresses) ? json_encode($addresses) : 'false'; ?>;
</script>
