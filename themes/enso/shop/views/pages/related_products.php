<style>
    @media (min-width: 768px) {
        .owl-carousel .owl-stage-outer, .single-product {
            /*overflow: inherit; */
        }
        .single-product:hover .pro-img .secondary-img {
            opacity: 0;
        }
        .rel .owl-stage-outer{
            overflow: inherit;
        }

        .overflowinhert{
            overflow: inherit;
        }

    }
</style>

<div class="best-seller-products pb-100">
    <div class="container">
        <div class="row">
            <!-- Section Title Start -->
            <div class="col-xs-12">
                <div class="section-title text-center mb-40">
                    <h3 class="section-info">RELATED PRODUCTS</h3>
                </div>
            </div>
            <!-- Section Title End -->
        </div>
        <!-- Row End -->
        <div class="row">
            <!-- Best Seller Product Activation Start -->
            <div class="best-seller new-products owl-carousel rel">
                <!-- Single Product Start -->
                <?php
                $r = 0;
                foreach ($side_featured as $fp) {
                    $promo_text = $this->site->getPromotionPercentage_promotext($fp->id);

                ?>
                <div class="single-product overflowinhert">

                    <div class="box">
                        <div class="ribbon"><span><?= $promo_text ?></span></div>
                    </div>

                    <!-- Product Image Start -->
                    <div class="pro-img">
                        <a href="<?= site_url('product/' . $fp->slug); ?>">
                            <img class="primary-img homeproduct_slider_img" src="<?= base_url('ucloud/plugins/filepreviewer/site/resize_image_inline.php?f=' . $fp->image.'&w=1100&h=900&m=medium&uh=&o=jpg.jpg'); ?>" alt="<?= $fp->image_alt ? $fp->image_alt : 'product-image' ?>">
                            <img class="secondary-img homeproduct_slider_img" src="<?= base_url('ucloud/plugins/filepreviewer/site/resize_image_inline.php?f=' . $fp->image.'&w=1100&h=900&m=medium&uh=&o=jpg.jpg'); ?>" alt="<?= $fp->image_alt ? $fp->image_alt : 'product-image' ?>">
                        </a>
                       
                        <span class="sticker-new"><?= $promo_text ? 'Promo' : 'New'  ; ?></span>
                    </div>
                    <!-- Product Image End -->
                    <!-- Product Content Start -->
                    <div class="pro-content text-center">
                        <h4><a href="<?= site_url('product/' . $fp->slug); ?>"><?= $fp->name; ?></a></h4>
                        <p class="price">
                            <span>
                                <?php if($fp->show_variable_price_on_shop == 1){
                                    $product_variable_prices = $this->site->getProductVariableLowestAndHeighestPrice($fp->id);
                              /*      echo str_replace('.0000','',$product_variable_prices->lowest) .'-'. str_replace('.0000','',$product_variable_prices->highest)  . ' AED';*/
                                    echo$this->sma->convertMoney($product_variable_prices->lowest) .'-'. $this->sma->convertMoney($product_variable_prices->highest);
                                 }else{
                                    echo $this->sma->convertMoney($fp->price);
                                } ?>
                            </span>
                        </p>
                        <div class="action-links2">
                            <a data-toggle="tooltip" title="Add to Cart" href="<?= site_url('product/' . $fp->slug); ?>">add to cart</a>
                        </div>
                    </div>
                    <!-- Product Content End -->
                </div>
                    <?php
                    $r++;
                }
                ?>
                <!-- Single Product End -->
            </div>
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>