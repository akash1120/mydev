<link rel="stylesheet" href="https://www.paytabs.com/theme/express_checkout/css/express.css">
<script src="https://www.paytabs.com/theme/express_checkout/js/jquery-1.11.1.min.js"></script>
<script src="https://www.paytabs.com/express/express_checkout_v3.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Button Code for PayTabs Express Checkout -->
<div class="PT_express_checkout hide"></div>
<script type="text/javascript">
    Paytabs("#express_checkout").expresscheckout({
        settings:{
            merchant_id: "<?= $this->session->userdata('post_merchant_id'); ?>",
            secret_key: "<?= $this->session->userdata('post_secret_key'); ?>",
            amount : "<?=  ($this->session->userdata('total') + $this->session->userdata('shipping_amount')) - $this->session->userdata('promotion_discount') ; ?>",
            currency : "AED",
            title : "<?=  $this->session->userdata('email'); ?>",
            product_names: "<?=  $this->session->userdata('products_names'); ?>",
            order_id: <?= $this->session->userdata('sale_id'); ?>,
            url_redirect: "<?= base_url().'shop/order_payment_status' ?>",
            display_customer_info:1,
            display_billing_fields:1,
            display_shipping_fields:0,
            language: "en",
            redirect_on_reject: 0,
        }
    });
    $(document).ready(function () {
        $('.PT_open_popup')[0].click();
    });
</script>