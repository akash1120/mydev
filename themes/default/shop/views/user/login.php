<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style>
    .well{
        background-color: #ffffff;
    }
</style>


<section class="page-contents">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php echo $this->session->flashdata('error_msg'); ?>
                <div class="row">
                    <div id="content" class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="well">
                                    <h2>New Customer</h2>
                                    <p><strong>Register Account</strong></p>
                                    <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
                                    <a href="<?= base_url(); ?>register" class="btn btn-primary">Continue</a></div>
                            </div>
                            <div class="col-sm-6">
                                <div class="well">
                                    <h2>Returning Customer</h2>
                                    <p><strong>I am a returning customer</strong></p>
                                    <form action="<?= base_url();?>login" class="validate fv-form fv-form-bootstrap" accept-charset="utf-8" novalidate="novalidate" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label class="control-label" for="input-email">E-Mail Address/User name</label>
                                            <input type="text" name="identity" value="" placeholder="E-Mail Address/User name" id="username" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="input-password">Password</label>
                                            <input type="password" name="password" value="" placeholder="Password" id="password" class="form-control" required>
                                            <a class="forgot-password" href="#">Forgotten Password</a></div>
                                        <input type="submit" value="Login" class="btn btn-primary">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>


































                    <div class=" hide col-sm-12 col-md-12">

                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#login" aria-controls="login" role="tab" data-toggle="tab"><?= lang('login'); ?></a></li>
                            <li role="presentation"><a href="#register" aria-controls="register" role="tab" data-toggle="tab"><?= lang('register'); ?></a></li>
                        </ul>

                        <div class="tab-content padding-lg white bordered-light" style="margin-top:-1px;">
                            <div role="tabpanel" class="tab-pane fade in active" id="login">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="well margin-bottom-no">
                                            <?php include('login_form.php'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <h4 class="title"><span><?= lang('register_new_account'); ?></span></h4>
                                        <p>
                                            <?= lang('register_account_info'); ?>
                                        </p>
                                        <a href="register" class="show-tab btn btn-primary pull-right"><?= lang('register'); ?></a>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane fade" id="register">

                                <?php $attrib = array('class' => 'validate', 'role' => 'form');
                                echo form_open("register", $attrib); ?>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= lang('first_name', 'first_name'); ?>
                                            <div class="controls">
                                                <?= form_input('first_name', '', 'class="form-control" id="first_name" required="required" pattern=".{3,10}"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= lang('last_name', 'last_name'); ?>
                                            <div class="controls">
                                                <?= form_input('last_name', '', 'class="form-control" id="last_name" required="required"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 hide">
                                        <div class="form-group">
                                            <?= lang('company', 'company'); ?>
                                            <?= form_input('company', 'wisdom', 'class="form-control tip" id="company"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= lang('phone', 'phone'); ?>
                                            <div class="controls">
                                                <?= form_input('phone', '', 'class="form-control" id="phone"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= lang('email', 'email'); ?>
                                            <div class="controls">
                                                <input type="email" id="email" name="email" class="form-control" required="required"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <?= lang('username', 'username'); ?>
                                            <?= form_input('username', set_value('username'), 'class="form-control tip" id="username" required="required"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= lang('password', 'password'); ?>
                                            <div class="controls">
                                                <?= form_password('password', '', 'class="form-control tip" id="password" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"'); ?>
                                                <span class="help-block"><?= lang('pasword_hint'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= lang('confirm_password', 'password_confirm'); ?>
                                            <div class="controls">
                                                <?= form_password('password_confirm', '', 'class="form-control" id="password_confirm" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-bv-identical="true" data-bv-identical-field="password" data-bv-identical-message="' . lang('pw_not_same') . '"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <?= form_submit('register', lang('register'), 'class="btn btn-primary"'); ?>
                                <?= form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
