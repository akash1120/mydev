<?php
if ($side_featured) {
   /* echo "<pre>";
    print_r($side_featured);
    exit('sd');*/
?>
    <style>
        .owl-prev {
            top: 40%;
        }
        .owl-next {
            top: 40%;
        }
    </style>
<div class="randomproduct-module">
    <div class="module-title">
        <h2>Related Products</h2>
    </div>
    <div class="owl-carousel relgallary owl-theme owl-loaded">
        <div class="owl-stage-outer">
            <div class="owl-stage">
                <?php
                $r = 0;
                foreach ($side_featured as $fp) {
                ?>
                <div class="owl-item">
                    <div class="row_items">
                        <div class="product-layout product-grid">
                            <div class="product-thumb layout1">
                                <div class="image">
                                    <div class="label-product">
                                        <span>New</span>
                                    </div>
                                    <a href="<?= site_url('product/' . $fp->slug); ?>">
                                        <img src="<?= base_url('assets/uploads/' . $fp->image); ?>"
                                             alt="Products Name" title="Products Name" class="img-responsive lazy">
                                    </a>
                                    <a class="btn-wishlist add-to-wishlist" data-toggle="tooltip" title=""
                                       data-id="<?= $fp->id; ?>" data-original-title="Add to Wish List"><i
                                                class="pe-7s-like"></i></a>
                                    <div class="actions-link2">
                                        <div class="more-detail"><a
                                                    href="<?= site_url('product/' . $fp->slug); ?>"
                                                    class="btn-cart"><span class="button">More Detail</span></a>
                                        </div>
                                        <a class="btn-cart add-to-cart" data-toggle="tooltip" title=""
                                           data-id="<?= $fp->id; ?>" data-original-title="Add to Cart"><span
                                                    class="button"><i class="pe-7s-cart"></i>Add to Cart</span></a>
                                    </div>
                                </div>

                                <div class="product-inner">
                                    <div class="product-caption">
                                        <h2 class="product-name"><a
                                                    href="<?= site_url('product/' . $fp->slug); ?>"><?= $fp->name; ?></a></h2>
                                        <p class="product-des"><?= character_limiter(strip_tags($fp->product_details), 200); ?></p>
                                        <div class="ratings">
                                            <div class="rating-box">
                                                <div class="rating0">rating</div>
                                            </div>
                                        </div>
                                        <p class="price">
                                            <?php
                                            if ($fp->promotion) {
                                                 echo '<del class="text-red">' . $this->Settings->default_currency.' '.$this->sma->convertMoney($fp->price) . '</del><br>';
                                                echo $this->Settings->default_currency.' '.$this->sma->convertMoney($fp->promo_price);
                                            } else {
                                                echo  $this->Settings->default_currency.' '.$this->sma->convertMoney($fp->price);
                                            }


                                            ?>
                                        </p>

                                        <div class="product-intro">
                                            <div class="actions-link">

                                                <a class="btn-compare" data-toggle="tooltip" title=""

                                                   data-original-title="Compare this Product"><i
                                                            class="pe-7s-shuffle"></i></a>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <?php
                    $r++;
                }
                ?>




               <!-- <div class="owl-item"> <div class="row_items">
                        <div class="product-layout product-grid">
                            <div class="product-thumb layout1">
                                <div class="image">
                                    <div class="label-product">
                                        <span>New</span>
                                    </div>
                                    <a href="http://localhost/enso/index.php?route=product/product&amp;product_id=121">
                                        <img src="http://localhost/enso/image/cache/catalog/Eg_11164086._egyptian-cotton-double-bed-plain-white-bedsheet-800x865.jpg"
                                             alt="Products Name" title="Products Name" class="img-responsive lazy">
                                    </a>
                                    <a class="btn-wishlist" data-toggle="tooltip" title=""
                                       onclick="wishlist.add('121');" data-original-title="Add to Wish List"><i
                                                class="pe-7s-like"></i></a>
                                    <div class="actions-link2">
                                        <div class="more-detail"><a
                                                    href="http://localhost/enso/index.php?route=product/product&amp;product_id=121"
                                                    class="btn-cart"><span class="button">More Detail</span></a>
                                        </div>
                                        <a class="btn-cart" data-toggle="tooltip" title=""
                                           onclick="cart.add('121');" data-original-title="Add to Cart"><span
                                                    class="button"><i class="pe-7s-cart"></i>Add to Cart</span></a>
                                    </div>
                                </div>

                                <div class="product-inner">
                                    <div class="product-caption">
                                        <h2 class="product-name"><a
                                                    href="http://localhost/enso/index.php?route=product/product&amp;product_id=121">Products
                                                Name</a></h2>
                                        <p class="product-des">Lorem ipsum dolor sit amet, consetetur sadipscing
                                            elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
                                            aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo
                                            dolores ..</p>
                                        <div class="ratings">
                                            <div class="rating-box">
                                                <div class="rating0">rating</div>
                                            </div>
                                        </div>
                                        <p class="price">
                                            AED 1,000 </p>

                                        <div class="product-intro">
                                            <div class="actions-link">

                                                <a class="btn-compare" data-toggle="tooltip" title=""
                                                   onclick="compare.add('121');"
                                                   data-original-title="Compare this Product"><i
                                                            class="pe-7s-shuffle"></i></a>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div></div>
                <div class="owl-item"> <div class="row_items">
                        <div class="product-layout product-grid">
                            <div class="product-thumb layout1">
                                <div class="image">
                                    <div class="label-product">
                                        <span>New</span>
                                    </div>
                                    <a href="http://localhost/enso/index.php?route=product/product&amp;product_id=121">
                                        <img src="http://localhost/enso/image/cache/catalog/Eg_11164086._egyptian-cotton-double-bed-plain-white-bedsheet-800x865.jpg"
                                             alt="Products Name" title="Products Name" class="img-responsive lazy">
                                    </a>
                                    <a class="btn-wishlist" data-toggle="tooltip" title=""
                                       onclick="wishlist.add('121');" data-original-title="Add to Wish List"><i
                                                class="pe-7s-like"></i></a>
                                    <div class="actions-link2">
                                        <div class="more-detail"><a
                                                    href="http://localhost/enso/index.php?route=product/product&amp;product_id=121"
                                                    class="btn-cart"><span class="button">More Detail</span></a>
                                        </div>
                                        <a class="btn-cart" data-toggle="tooltip" title=""
                                           onclick="cart.add('121');" data-original-title="Add to Cart"><span
                                                    class="button"><i class="pe-7s-cart"></i>Add to Cart</span></a>
                                    </div>
                                </div>

                                <div class="product-inner">
                                    <div class="product-caption">
                                        <h2 class="product-name"><a
                                                    href="http://localhost/enso/index.php?route=product/product&amp;product_id=121">Products
                                                Name</a></h2>
                                        <p class="product-des">Lorem ipsum dolor sit amet, consetetur sadipscing
                                            elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
                                            aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo
                                            dolores ..</p>
                                        <div class="ratings">
                                            <div class="rating-box">
                                                <div class="rating0">rating</div>
                                            </div>
                                        </div>
                                        <p class="price">
                                            AED 1,000 </p>

                                        <div class="product-intro">
                                            <div class="actions-link">

                                                <a class="btn-compare" data-toggle="tooltip" title=""
                                                   onclick="compare.add('121');"
                                                   data-original-title="Compare this Product"><i
                                                            class="pe-7s-shuffle"></i></a>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div></div>
                <div class="owl-item"> <div class="row_items">
                        <div class="product-layout product-grid">
                            <div class="product-thumb layout1">
                                <div class="image">
                                    <div class="label-product">
                                        <span>New</span>
                                    </div>
                                    <a href="http://localhost/enso/index.php?route=product/product&amp;product_id=121">
                                        <img src="http://localhost/enso/image/cache/catalog/Eg_11164086._egyptian-cotton-double-bed-plain-white-bedsheet-800x865.jpg"
                                             alt="Products Name" title="Products Name" class="img-responsive lazy">
                                    </a>
                                    <a class="btn-wishlist" data-toggle="tooltip" title=""
                                       onclick="wishlist.add('121');" data-original-title="Add to Wish List"><i
                                                class="pe-7s-like"></i></a>
                                    <div class="actions-link2">
                                        <div class="more-detail"><a
                                                    href="http://localhost/enso/index.php?route=product/product&amp;product_id=121"
                                                    class="btn-cart"><span class="button">More Detail</span></a>
                                        </div>
                                        <a class="btn-cart" data-toggle="tooltip" title=""
                                           onclick="cart.add('121');" data-original-title="Add to Cart"><span
                                                    class="button"><i class="pe-7s-cart"></i>Add to Cart</span></a>
                                    </div>
                                </div>

                                <div class="product-inner">
                                    <div class="product-caption">
                                        <h2 class="product-name"><a
                                                    href="http://localhost/enso/index.php?route=product/product&amp;product_id=121">Products
                                                Name</a></h2>
                                        <p class="product-des">Lorem ipsum dolor sit amet, consetetur sadipscing
                                            elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
                                            aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo
                                            dolores ..</p>
                                        <div class="ratings">
                                            <div class="rating-box">
                                                <div class="rating0">rating</div>
                                            </div>
                                        </div>
                                        <p class="price">
                                            AED 1,000 </p>

                                        <div class="product-intro">
                                            <div class="actions-link">

                                                <a class="btn-compare" data-toggle="tooltip" title=""
                                                   onclick="compare.add('121');"
                                                   data-original-title="Compare this Product"><i
                                                            class="pe-7s-shuffle"></i></a>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div></div>-->
            </div>
        </div>
        <div class="owl-nav">
            <div class="owl-prev">prev</div>
            <div class="owl-next">next</div>
        </div>
    </div>

</div>
    <?php
}
?>