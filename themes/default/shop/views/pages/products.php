<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style>
    .label-product{
        top: 15px;
        left: 30px;
    }
    .product-desc h1,h2,h3,h4,h5,h6{
        font-family: inherit !important;
    }
    .btn-wishlist1 {
       /* background: #312f2f;*/
        position: absolute;
        top: 15px;
        left: 330px;
        padding: 0px 10px;
        z-index: 1;
        color: #312f2f;
        text-transform: uppercase;
        font-size: 20px;
        line-height: 1;
    }
    .btn-wishlist1 i{
        font-size: 35px;
        top:0px;
    }
    @media only screen and (max-device-width: 599px) {
        .btn-wishlist1 {
            left: 270px;
        }

    }
    @media only screen and (max-width: 768px) {
        .btn-wishlist1 {
            left: 230px;
        }

    }
    .featured-products .btn.quickview{
        border-radius: 0px;
    }
    .tablist .fa{
        font-size: 20px;
    }
    .sortdropdown{
        width:200px !important;
    }
    @media (max-width: 480px) {
        #grid-menu {
            display: block;
        }
        .sortdropdown{
            width:160px !important;
        }
    }
    hr{
        border: none;
        height: 1px;
        background: #000;
    }
    .product {
        border:  none !important;
    }
</style>

<section class="page-contents">
    <div class="container layer-category">

        <div class="row">
            <div class="col-xs-12">

                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div id="loading">
                            <div class="wave">
                                <div class="rect rect1"></div>
                                <div class="rect rect2"></div>
                                <div class="rect rect3"></div>
                                <div class="rect rect4"></div>
                                <div class="rect rect5"></div>
                            </div>
                        </div>
                        <div class="category-info">
                            <h2 class="category-name">e-boutique</h2>
                            <div class="category-des"><p><br></p></div>
                        </div>
                        <div class="toolbar" id="grid-selector" style="padding-left: 0px;">
                            <div id="grid-menu" class="tablist">
                                <ul>
                                    <li class="three-col "><i class="fa fa-th-list"></i></li>
                                    <li class="two-col active"><i class="fa fa-th"></i></li>
                                </ul>
                            </div>
                          <!--  <div class="row">-->
                            <select id="grid-sort" class="form-control pull-left sortdropdown">
                                <option value="name-desc"> Sort By : Default </option>
                                <option id="name-asc" class="sorting" value="name-asc"> Sort By : Name(A - Z) </option>
                                <option id="name-desc" class="sorting" value="name-desc"> Sort By : Name(Z - A) </option>
                                <option id="price-asc" class="sorting" value="price-asc"> Sort By : Price(Low > High) </option>
                                <option id="price-desc" class="sorting" value="price-desc"> Sort By : Price(High > Low) </option>
                                <!--<option value=""> Sort By : Model(A - Z) </option>
                                <option value=""> Sort By : Model(Z - A) </option>-->
                            </select>
                          <!--  </div>-->

                        </div>

                        <div class="clearfix"></div>
                       <!-- <div class="row">-->
                            <div id="results" class="product_listing_result"></div>
                   <!--     </div>-->
                        <div class="clearfix"></div>

                        <div class="row">
                            <div class="col-md-6">
                                <span class="page-info line-height-xl hidden-xs hidden-sm"></span>
                            </div>
                            <div class="col-md-6">
                                <div id="pagination" class="pagination-right"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>