<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
if($product->extra_size > 0){
    $chart_size_data = $this->site->getSizeGuideWithID($product->extra_size);
}
?>
<style>
    .owl-prev {
        width: 40px;
        height: 40px;
        position: absolute;
        top: 25%;
        font-size: 18px;
        margin-left: -20px !important;
        display: block!IMPORTANT;
        border:0px solid black;
        background: white !important;
        border-radius: 100% !important;
        box-shadow: 1px 0 5px 1px rgba(0,0,0,.1);
    }
    .owl-prev span{
        font-size: 25px;
    }
    .owl-prev:hover{

        background: black !important;
        opacity: 1;
        box-shadow: 1px 0 5px 1px rgba(0,0,0,.1);
        color: white;;
    }

    .owl-next {
        width: 40px;
        height: 40px;
        position: absolute;
        top: 25%;
        right: -25px !important;
        display: block!IMPORTANT;
        border:0px solid black;
        background: white !important;
        border-radius: 100% !important;
        box-shadow: 1px 0 5px 1px rgba(0,0,0,.1);
    }
    .owl-next span{
        font-size: 25px;
    }
    .owl-next:hover{

        background: black !important;
        opacity: 1;
        box-shadow: 1px 0 5px 1px rgba(0,0,0,.1);
        color: white;;
    }
    .owl-prev i, .owl-next i {transform : scale(1,6); color: #ccc;}
    .owlimage{
        /*  height: 110px;*/
    }
    .owl-nav {
        display: none;
    }

    .owl-carousel:hover .owl-nav {
        display: block;
    }
    .owl-carousel{
        margin-top: 20px;
    }
    .zoomLens {
        width: 150px !important;
        height: 150px !important;
    }
    .owl-carousel .owl-item {
        padding: 0px !important;
    }
    .cus_border_top{
        padding-top: 30px;
        border-top: 1px solid #dadada;
    }
</style>
<div class="clearfix"></div>
<section class="page-contents" style="background: #ffffff;">

    <div class="container">
        <div class="row">
            <!--  <div class="col-xs-12">-->

            <div class="row">
                <div class="col-sm-12 col-md-12">

                    <div class=" margin-top-lg cus_border_top">
                        <!--<div class="panel-heading custom_panel-heading font-size-large">
                                <h1><?/*= $product->name; */?></h1>
                                <a href="<?/*= shop_url('products'); */?>" class="pull-right"><i class="fa fa-share"></i> <?/*= lang('products'); */?></a>
                                <p class="ratio_star_heading">by <?/*= '<a href="'.site_url('category/'.$category->slug).'" class="line-height-lg">'.$category->name.'</a>'; */?>,
                                    <?/*= $brand ? '<a href="'.site_url('brand/'.$brand->slug).'" class="line-height-lg">'.$brand->name.'</a>' : ''; */?> -
                                    <img src="https://hcplteenscene.files.wordpress.com/2011/10/four-stars.png" style="width: 74px; height: 14px; padding: 0px;">
                                    <i class="fa fa-chevron-down"></i>
                                    <a class="review_clrlink" href="javascript:void(0)">6 Reviews</a>

                                </p>
                            </div>-->

                        <div class="panel-body mprint custom_inner_proviewsection">

                            <div class="row">

                                <div class="col-sm-6">
                                    <img id="zoom_01" src='<?= base_url()?>assets/uploads/<?= $product->image ?>'  data-zoom-image='<?= base_url()?>assets/uploads/<?= $product->image ?>' alt='<?= $product->name?>' class="img-responsive"/>
                                    <div class="owl-carousel owl-theme imggallery">
                                        <?php
                                        if (!empty($images)) {
                                            foreach ($images as $ph) {

                                                echo '<div class="item"><a href="#"><img class="img-responsive owlimage"  data-zoom-image="' . base_url('assets/uploads/' . $ph->photo) . '" src="' . base_url('assets/uploads/' . $ph->photo) . '" alt="' . $ph->photo . '" /></a></div>';
                                            }
                                        }
                                        ?>
                                    </div>

                                    <!--<img id="zoom_01"  src="<?/*= base_url()*/?>assets/uploads/<?/*= $product->image */?>" alt="<?/*= $product->name*/?>" class="img-responsive img-thumbnail"/>-->

                                </div>

                                <div class="col-2 col-sm-6">
                                    <h1 class="product-name"><?= $product->name; ?></h1>
                                    <ul class="list-unstyled price-product">
                                        <li>

                                           <?php if ($product->promotion) { ?>

                                                            <span class="">
                                                               <?= $this->Settings->default_currency; ?> <?= $this->sma->convertMoney($product->promo_price); ?>

                                                            </span>

                                            <div class="save_amount_sec">
                                                <span class="was"><?= $this->sma->convertMoney($product->price).' AED'; ?></span>
                                                <span class="saved">&nbsp; - You Save&nbsp;<span class="noWrap"><?= $this->sma->convertMoney($product->price - $product->promo_price).' AED'; ?></span></span>
                                            </div>
                                            <?php
                                            /*echo '<tr><td>' . lang("promotion") . '</td><td>' . $this->sma->convertMoney($product->promo_price) . ' ('.$this->sma->hrsd($product->start_date).' - '.$this->sma->hrsd($product->start_date).')</td></tr>';*/
                                            }else{ ?>
                                               <span><?= $this->Settings->default_currency; ?>   <?= $this->sma->convertMoney($product->price); ?></span>
                                            <?php } ?>

                                        </li>
                                    </ul>
                                    <ul class="list-unstyled">
                                        <li>Product Code:<span class="ex-text"><?= $product->code; ?></span></li>
                                        <?php $in_stock = TRUE; if ($in_stock || $this->Settings->overselling) { ?>
                                        <li>Availability:<span class="ex-text">In Stock</span></li>
                                      <?php }  ?>
                                        <li><?= $product->free_shipment == 1 ? 'FREE Shipping' : '' ?></li>
                                    </ul>
                                    <p class="short-des">
                                        <?= $page_desc; ?>
                                    </p>

                                  <!--  <p>
                                        <strong><?/*= $product->free_shipment == 1 ? 'FREE Shipping' : '' */?></strong>
                                    </p>
                                    <hr class="cus-margin-top-bottom-0">-->

                                    <!--<p>
                                        <strong>Availability:</strong> <a>In Stock</a>
                                    </p>-->

                                    <!--<p>
                                            <strong><?/*= lang("code"); */?>:</strong> <a><?/*= $product->code; */?></a>,
                                            <strong><?/*= lang("type"); */?>:</strong> <?/*= lang($product->type); */?>
                                        </p>-->

                                    <!--<p>
                                            <strong><?/*= lang("category"); */?>:</strong> <?/*= '<a href="'.site_url('category/'.$category->slug).'" class="line-height-lg">'.$category->name.'</a>'; */?>,
                                            <strong><?/*= lang("brand"); */?>:</strong> <?/*= $brand ? '<a href="'.site_url('brand/'.$brand->slug).'" class="line-height-lg">'.$brand->name.'</a>' : ''; */?>

                                        </p>-->



                                    <!--<p>
                                            <?/*= lang("tax_rate"); */?>: <?/*= $tax_rate->name; */?>,
                                            <?/*= lang("tax_method"); */?>: <?/*= $product->tax_method == 0 ? lang('inclusive') : lang('exclusive'); */?>
                                        </p>-->

                                    <!--<p>
                                    <div class="cat_attributes ">
                                        <strong>Color: </strong> &nbsp;(3 More)
                                        <select class="form-control" style="width: 50%;">
                                            <option value="red">Red</option>
                                            <option value="blue">Blue</option>
                                            <option value="pink">Pink</option>
                                            <option value="white">White</option>
                                        </select>
                                    </div>
                                    </p>
-->
                                    <p>
                                        <?php $in_stock = TRUE; if ($in_stock || $this->Settings->overselling) { ?>

                                        <?= form_open('cart/add/'.$product->id, 'class="validate"'); ?>


                                    <div class="form-group  required <?php echo !empty($variants) ? 'show' : 'hide' ?>" style="width: 100%;" >
                                        <label class="control-label" for="input-option264">Select Size</label>
                                        <?php
                                        if ($variants) {
                                            foreach ($variants as $variant) {
                                                $opts[$variant->id] = $variant->name.($variant->price > 0 ? ' (+'.$this->sma->convertMoney($variant->price, TRUE, FALSE).')' : ($variant->price == 0 ? '' : ' (+'.$this->sma->convertMoney($variant->price, TRUE, FALSE).')'));
                                            }
                                            echo form_dropdown('option', $opts, '', 'class="form-control" required="required"');
                                        }
                                        ?>
                                    </div>

                                    <div class="form-group">
                                    <div class="col-md-4" style="margin: 20px 0;">
                                        <label class="control-label" for="input-quantity">Qty</label>
                                        <div class="quantity-box">
                                            <input type="button" id="minus" value="-" class="form-control">
                                            <input type="text" name="quantity" value="1" size="2" id="input-quantity" class="form-control ext-center quantity-input"  required="required">
                                            <input type="button" id="plus" value="+" class="form-control ">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <button class="button" type="submit" id="button-cart" data-loading-text="Loading..."><i class="pe-7s-cart"></i><span>Add to Cart</span></button>
                                    </div>
                                    <div class="col-md-4">
                                        <button class="button add-to-wishlist" type="button" title="Add to Wish List" data-id="<?= $product->id; ?>"><i class="pe-7s-like"></i><span>Add to Wish List</span></button>
                                    </div>
                                 </div>

                                    <div class="form-group">
                                       <!-- <div class="input-group col-xs-6 <?/*= $chart_size_data !='' ? 'pull-left' : '' */?>">
                                            <span class="input-group-addon pointer btn-minus"><span class="fa fa-minus"></span></span>
                                            <input type="text" name="quantity" class="form-control text-center quantity-input" value="1" required="required">
                                            <span class="input-group-addon pointer btn-plus"><span class="fa fa-plus"></span></span>
                                        </div>-->

                                        <?php if($chart_size_data){ ?>
                                          <!--  <div class="input-group col-xs-6">
                                                <a class="pull-right curso_hand" data-toggle="modal" data-target="#myModal">
                                                    <img alt="Scale" width="32" src="<?/*= site_url('assets/images/scale.png'); */?>"> Sizing info
                                                </a>
                                            </div>-->
                                        <?php } ?>
                                    </div>
                                <?= $chart_size_data !='' ? '<br/>' : '' ?>
                                    <!-- <input type="hidden" name="quantity" class="form-control text-center" value="1"> -->
                                    <!--<div class="form-group" >
                                        <strong>Instructions: </strong>
                                        <div class="special_instructions">
                                            <textarea class="form-control special_inc_textarea" placeholder="Write your Instructions here..."></textarea>
                                        </div>
                                    </div>-->

                                   <!-- <div class="form-group">
                                        <div class="btn-group" role="group" aria-label="...">
                                            <button class="btn btn-info btn-lg add-to-wishlist" data-id="<?/*= $product->id; */?>"><i class="fa fa-heart-o"></i></button>
                                            <button type="submit" class="btn btn-theme btn-lg"><i class="fa fa-shopping-cart padding-right-md"></i> <?/*= lang('add_to_cart'); */?></button>
                                        </div>
                                    </div>-->
                                <?= form_close(); ?>
                                <?php } else {
                                    echo '<strong>'.lang('item_out_of_stock').'</strong>';
                                } ?>

                                    </p>

                                </div>



                                <?php if($product->product_details){  ?>
                                    <div class="col-sm-12" style="padding-top: 20px;">

                                        <p><?= $product->product_details; ?></p>
                                    </div>
                                <?php } ?>

                                <!--<div class="col-sm-12">
                                        <div class="clearfix"></div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped dfTable table-right-left">
                                                <tbody>
                                                <tr>
                                                    <td width="50%"><?/*= lang("name"); */?></td>
                                                    <td width="50%"><?/*= $product->name; */?></td>
                                                </tr>
                                                <tr>
                                                    <td><?/*= lang("code"); */?></td>
                                                    <td><?/*= $product->code; */?></td>
                                                </tr>
                                                <tr>
                                                    <td><?/*= lang("type"); */?></td>
                                                    <td><?/*= lang($product->type); */?></td>
                                                </tr>
                                                <tr>
                                                    <td><?/*= lang("brand"); */?></td>
                                                    <td><?/*= $brand ? '<a href="'.site_url('brand/'.$brand->slug).'" class="line-height-lg">'.$brand->name.'</a>' : ''; */?></td>
                                                </tr>
                                                <tr>
                                                    <td><?/*= lang("category"); */?></td>
                                                    <td><?/*= '<a href="'.site_url('category/'.$category->slug).'" class="line-height-lg">'.$category->name.'</a>'; */?></td>
                                                </tr>
                                                <?php /*if ($product->subcategory_id) { */?>
                                                    <tr>
                                                        <td><?/*= lang("subcategory"); */?></td>
                                                        <td><?/*= '<a href="'.site_url('category/'.$category->slug.'/'.$subcategory->slug).'" class="line-height-lg">'.$subcategory->name.'</a>'; */?></td>
                                                    </tr>
                                                <?php /*} */?>

                                                <tr>
                                                    <td><?/*= lang("price"); */?></td>
                                                    <td><?/*= $this->sma->convertMoney($product->price); */?></td>
                                                </tr>

                                                <?php
                                /*                                                if ($product->promotion) {
                                                                                    echo '<tr><td>' . lang("promotion") . '</td><td>' . $this->sma->convertMoney($product->promo_price) . ' ('.$this->sma->hrsd($product->start_date).' - '.$this->sma->hrsd($product->start_date).')</td></tr>';
                                                                                }
                                                                                */?>

                                                <?php /*if ($product->tax_rate) { */?>
                                                    <tr>
                                                        <td><?/*= lang("tax_rate"); */?></td>
                                                        <td><?/*= $tax_rate->name; */?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><?/*= lang("tax_method"); */?></td>
                                                        <td><?/*= $product->tax_method == 0 ? lang('inclusive') : lang('exclusive'); */?></td>
                                                    </tr>
                                                <?php /*} */?>

                                                <tr>
                                                    <td><?/*= lang("unit"); */?></td>
                                                    <td><?/*= $unit ? $unit->name.' ('.$unit->code.')' : ''; */?></td>
                                                </tr>
                                                <?php /*if (!empty($warehouse) && $product->type == 'standard') { */?>
                                                    <tr>
                                                        <td><?/*= lang("in_stock"); */?></td>
                                                        <td><?/*= $this->sma->formatQuantity($warehouse->quantity); */?></td>
                                                    </tr>
                                                <?php /*} */?>

                                                <?php /*if ($variants) { */?>
                                                    <tr>
                                                        <td><?/*= lang("product_variants"); */?></td>
                                                        <td><?php /*foreach ($variants as $variant) {
                                                                echo '<span class="label label-primary">' . $variant->name . '</span> ';
                                                            } */?></td>
                                                    </tr>
                                                <?php /*} */?>

                                                <?php /*if (!empty($options)) {
                                                    foreach ($options as $option) {
                                                        if ($option->wh_qty != 0) {
                                                            echo '<tr><td colspan="2" class="bg-primary">' . $option->name . '</td></tr>';
                                                            echo '<td>' .lang("in_stock").': '. $this->sma->formatQuantity($option->wh_qty) . '</td>';
                                                            echo '<td>' .lang("price").': '.$this->sma->convertMoney($product->price+$option->price). '</td>';
                                                            echo '</tr>';
                                                        }

                                                    }
                                                } */?>

                                                <?php /*if ($product->cf1 || $product->cf2 || $product->cf3 || $product->cf4 || $product->cf5 || $product->cf6) {
                                                    if ($product->cf1) {
                                                        echo '<tr><td>' . lang("pcf1") . '</td><td>' . $product->cf1 . '</td></tr>';
                                                    }
                                                    if ($product->cf2) {
                                                        echo '<tr><td>' . lang("pcf2") . '</td><td>' . $product->cf2 . '</td></tr>';
                                                    }
                                                    if ($product->cf3) {
                                                        echo '<tr><td>' . lang("pcf3") . '</td><td>' . $product->cf3 . '</td></tr>';
                                                    }
                                                    if ($product->cf4) {
                                                        echo '<tr><td>' . lang("pcf4") . '</td><td>' . $product->cf4 . '</td></tr>';
                                                    }
                                                    if ($product->cf5) {
                                                        echo '<tr><td>' . lang("pcf5") . '</td><td>' . $product->cf5 . '</td></tr>';
                                                    }
                                                    if ($product->cf6) {
                                                        echo '<tr><td>' . lang("pcf6") . '</td><td>' . $product->cf6 . '</td></tr>';
                                                    }
                                                } */?>


                                                </tbody>
                                            </table>
                                            <?php /*if ($product->type == 'combo') { */?>
                                                <strong><?/*= lang('combo_items') */?></strong>
                                                <div class="table-responsive">
                                                    <table
                                                            class="table table-bordered table-striped table-condensed dfTable two-columns">
                                                        <thead>
                                                        <tr>
                                                            <th><?/*= lang('product_name') */?></th>
                                                            <th><?/*= lang('quantity') */?></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php /*foreach ($combo_items as $combo_item) {
                                                            echo '<tr><td>' . $combo_item->name . ' (' . $combo_item->code . ') </td><td>' . $this->sma->formatQuantity($combo_item->qty) . '</td></tr>';
                                                        } */?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            <?php /*} */?>
                                        </div>
                                    </div>-->
                                <div class="col-sm-3 col-md-12">

                                    <?php


                                    include('related_products.php'); ?>
                                </div>

                                <div class="clearfix"></div>

                                <!-- <div class="col-xs-12">

                                    <?/*= $product->details ? '<div class="panel panel-info"><div class="panel-heading">' . lang('product_details_for_invoice') . '</div><div class="panel-body">' . $product->details . '</div></div>' : ''; */?>
                                    <?/*= $product->product_details ? '<div class="panel panel-default" style="margin-bottom:0;"><div class="panel-heading">' . lang('product_details') . '</div><div class="panel-body">' . $product->product_details . '</div></div>' : ''; */?>

                                </div>-->
                            </div>

                        </div>
                    </div>
                </div>


            </div>
            <!-- </div>-->
        </div>
    </div>
</section>

<div id="lightbox" class="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-middle">
        <div class="modal-content">
            <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
            <div class="modal-body">
                <img src="" alt="" />
            </div>
        </div>
    </div>
</div>



<!-- Modal For size chart-->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog cus_sizechart_modal_width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>

                </button>
                <h3 class="modal-title" id="myModalLabel">Sizing info</h3>

            </div>
            <div class="modal-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <?php foreach($chart_size_data as $datum){ ?>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#1" aria-controls="sizecChart" role="tab" data-toggle="tab"><?= $datum->tab1_label ?></a>
                            </li>
                            <li role="presentation" class=" <?= $datum->tab2_label == '' ? 'hide' : ''?>">
                                <a href="#2" aria-controls="sizecChart" role="tab" data-toggle="tab"><?= $datum->tab2_label ?></a>
                            </li>
                            <li role="presentation" class=" <?= $datum->tab3_label == '' ? 'hide' : ''?>">
                                <a href="#3" aria-controls="sizecChart" role="tab" data-toggle="tab"><?= $datum->tab3_label ?></a>
                            </li>
                            <li role="presentation" class=" <?= $datum->tab4_label == '' ? 'hide' : ''?>" >
                                <a href="#4" aria-controls="sizecChart" role="tab" data-toggle="tab"><?= $datum->tab4_label ?></a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="1">
                                <br/>
                                <?= $datum->tab1_description; ?>
                            </div>
                            <div role="tabpanel" class="tab-pane " id="2">
                                <br/>
                                <?= $datum->tab2_description; ?>
                            </div>
                            <div role="tabpanel" class="tab-pane " id="3">
                                <br/>
                                <?= $datum->tab3_description; ?>
                            </div>
                            <div role="tabpanel" class="tab-pane " id="4">
                                <br/>
                                <?= $datum->tab4_description; ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    function shareit_f() {
        //$("head").append("<meta property='og:image' content='<?= base_url() ?>assets/uploads/<?= $product->image ?>' />");
        var url = '<?= site_url() . 'product/' . $product->slug; ?>'; //Set desired URL here
        var img = '<?= base_url() ?>assets/uploads/<?= $product->image ?>' //Set Desired Image here
        var totalurl = encodeURIComponent(url + '?img=' + img);
        window.open('http://www.facebook.com/sharer.php?u=' + totalurl, '', 'width=500, height=500, scrollbars=yes, resizable=no');
    }

</script>