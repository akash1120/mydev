<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-cog"></i><?= lang('edit_block'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?= lang('update_info'); ?></p>

                <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                echo admin_form_open("shop_settings/edit_landingpage_block/".$page->id, $attrib);
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= lang('title', 'title'); ?>
                                <?= form_input('title', set_value('title' , $page->title), 'class="form-control" id="title" pattern=".{3,60}" required="" data-fv-notempty-message="'.lang('title_required').'"'); ?>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang('number_of_products', 'order_no'); ?>
                                <?= form_input('number_of_products', set_value('number_of_products' , $page->number_of_products), 'class="form-control" id="number_of_products" required=""'); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang('order_no', 'order_no'); ?>
                                <?= form_input('order_no', set_value('order_number', $page->order_number), 'class="form-control" id="order_no" required=""'); ?>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= lang('select_product', 'products'); ?>
                                <?php echo form_input('add_item', '', 'class="form-control" id="add_item" placeholder="' . $this->lang->line("add_item") . '"'); ?>

                                <div class="form-group">
                                    <div class="controls table-controls">
                                        <table id="bcTable"
                                               class="table items table-striped table-bordered table-condensed table-hover">
                                            <thead>
                                            <tr>
                                                <th class="col-xs-4"><?= lang("product_name") . " (" . $this->lang->line("product_code") . ")"; ?></th>
                                                <th class="col-xs-7"><?= lang("variants"); ?></th>
                                                <th class="text-center" style="width:30px;">
                                                    <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                                <button type="button" id="reset" class="btn btn-danger hide">Reset</button>
                            </div>

                            <label class="checkbox" for="active">
                                <input type="checkbox" name="status" value="1" id="status" <?= $page->status ? 'checked="checked"' : ''; ?>/>
                                <?= lang('status') ?>
                            </label>

                            <?php echo form_submit('edit_page', lang('edit_block'), 'class="btn btn-primary"'); ?>
                        </div>

                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var ac = false; bbcitemsx = {};
    if (localStorage.getItem('bbcitemsx')) {
        bbcitemsx = JSON.parse(localStorage.getItem('bbcitemsx'));
    }
    <?php  if($items) { ?>

    localStorage.setItem('bbcitemsx', JSON.stringify(<?= $items; ?>));
    <?php } ?>
    $(document).ready(function() {
        <?php if ($this->input->post('print')) { ?>
        $( window ).load(function() {
            $('html, body').animate({
                scrollTop: ($("#barcode-con").offset().top)-15
            }, 1000);
        });
        <?php } ?>
        if (localStorage.getItem('bbcitemsx')) {
            loadItems();
        }
        $("#add_item").autocomplete({
            source: '<?= admin_url('products/get_suggestions_enabled'); ?>',
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');

                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_product_item(ui.item);
                    if (row) {
                        $(this).val('');
                    }
                } else {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>');
                }
            }
        });
        check_add_item_val();

        $('#style').change(function (e) {
            localStorage.setItem('bcstyle', $(this).val());
            if ($(this).val() == 50) {
                $('.cf-con').slideDown();
            } else {
                $('.cf-con').slideUp();
            }
        });
        if (style = localStorage.getItem('bcstyle')) {
            $('#style').val(style);
            $('#style').select2("val", style);
            if (style == 50) {
                $('.cf-con').slideDown();
            } else {
                $('.cf-con').slideUp();
            }
        }

        $('#cf_width').change(function (e) {
            localStorage.setItem('cf_width', $(this).val());
        });
        if (cf_width = localStorage.getItem('cf_width')) {
            $('#cf_width').val(cf_width);
        }

        $('#cf_height').change(function (e) {
            localStorage.setItem('cf_height', $(this).val());
        });
        if (cf_height = localStorage.getItem('cf_height')) {
            $('#cf_height').val(cf_height);
        }

        $('#cf_orientation').change(function (e) {
            localStorage.setItem('cf_orientation', $(this).val());
        });
        if (cf_orientation = localStorage.getItem('cf_orientation')) {
            $('#cf_orientation').val(cf_orientation);
        }

        $(document).on('ifChecked', '#site_name', function(event) {
            localStorage.setItem('bcsite_name', 1);
        });
        $(document).on('ifUnchecked', '#site_name', function(event) {
            localStorage.setItem('bcsite_name', 0);
        });
        if (site_name = localStorage.getItem('bcsite_name')) {
            if (site_name == 1)
                $('#site_name').iCheck('check');
            else
                $('#site_name').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#product_name', function(event) {
            localStorage.setItem('bcproduct_name', 1);
        });
        $(document).on('ifUnchecked', '#product_name', function(event) {
            localStorage.setItem('bcproduct_name', 0);
        });
        if (product_name = localStorage.getItem('bcproduct_name')) {
            if (product_name == 1)
                $('#product_name').iCheck('check');
            else
                $('#product_name').iCheck('uncheck');
        }



        $(document).on('ifChecked', '#translated_product_name', function(event) {
            localStorage.setItem('bctranslatedproduct_name', 1);
        });
        $(document).on('ifUnchecked', '#translated_product_name', function(event) {
            localStorage.setItem('bctranslatedproduct_name', 0);
        });
        if (translated_product_name = localStorage.getItem('bctranslatedproduct_name')) {
            if (translated_product_name == 1)
                $('#translated_product_name').iCheck('check');
            else
                $('#translated_product_name').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#price', function(event) {
            localStorage.setItem('bcprice', 1);
        });
        $(document).on('ifUnchecked', '#price', function(event) {
            localStorage.setItem('bcprice', 0);
            $('#currencies').iCheck('uncheck');
        });
        if (price = localStorage.getItem('bcprice')) {
            if (price == 1)
                $('#price').iCheck('check');
            else
                $('#price').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#currencies', function(event) {
            localStorage.setItem('bccurrencies', 1);
        });
        $(document).on('ifUnchecked', '#currencies', function(event) {
            localStorage.setItem('bccurrencies', 0);
        });
        if (currencies = localStorage.getItem('bccurrencies')) {
            if (currencies == 1)
                $('#currencies').iCheck('check');
            else
                $('#currencies').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#unit', function(event) {
            localStorage.setItem('bcunit', 1);
        });
        $(document).on('ifUnchecked', '#unit', function(event) {
            localStorage.setItem('bcunit', 0);
        });
        if (unit = localStorage.getItem('bcunit')) {
            if (unit == 1)
                $('#unit').iCheck('check');
            else
                $('#unit').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#category', function(event) {
            localStorage.setItem('bccategory', 1);
        });
        $(document).on('ifUnchecked', '#category', function(event) {
            localStorage.setItem('bccategory', 0);
        });
        if (category = localStorage.getItem('bccategory')) {
            if (category == 1)
                $('#category').iCheck('check');
            else
                $('#category').iCheck('uncheck');
        }



        $(document).on('ifChecked', '#translated_category', function(event) {
            localStorage.setItem('bctranslated_category', 1);
        });
        $(document).on('ifUnchecked', '#translated_category', function(event) {
            localStorage.setItem('bctranslated_category', 0);
        });
        if (translated_category = localStorage.getItem('bctranslated_category')) {
            if (translated_category == 1)
                $('#translated_category').iCheck('check');
            else
                $('#translated_category').iCheck('uncheck');
        }



        $(document).on('ifChecked', '#check_promo', function(event) {
            localStorage.setItem('bccheck_promo', 1);
        });
        $(document).on('ifUnchecked', '#check_promo', function(event) {
            localStorage.setItem('bccheck_promo', 0);
        });
        if (check_promo = localStorage.getItem('bccheck_promo')) {
            if (check_promo == 1)
                $('#check_promo').iCheck('check');
            else
                $('#check_promo').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#product_image', function(event) {
            localStorage.setItem('bcproduct_image', 1);
        });
        $(document).on('ifUnchecked', '#product_image', function(event) {
            localStorage.setItem('bcproduct_image', 0);
        });
        if (product_image = localStorage.getItem('bcproduct_image')) {
            if (product_image == 1)
                $('#product_image').iCheck('check');
            else
                $('#product_image').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#variants', function(event) {
            localStorage.setItem('bcvariants', 1);
        });
        $(document).on('ifUnchecked', '#variants', function(event) {
            localStorage.setItem('bcvariants', 0);
        });
        if (variants = localStorage.getItem('bcvariants')) {
            if (variants == 1)
                $('#variants').iCheck('check');
            else
                $('#variants').iCheck('uncheck');
        }

        $(document).on('click', '.del', function () {
            var id = $(this).attr('id');
            delete bbcitemsx[id];
            localStorage.setItem('bbcitemsx', JSON.stringify(bbcitemsx));
            $(this).closest('#row_' + id).remove();
        });

        $('#reset').on('click' , function (e) {
            //bootbox.confirm(lang.r_u_sure, function (result) {
            if (localStorage.getItem('bbcitemsx')) {
                if (localStorage.getItem('bbcitemsx')) {
                    localStorage.removeItem('bbcitemsx');
                }
                if (localStorage.getItem('bcstyle')) {
                    localStorage.removeItem('bcstyle');
                }
                if (localStorage.getItem('bcsite_name')) {
                    localStorage.removeItem('bcsite_name');
                }
                if (localStorage.getItem('bcproduct_name')) {
                    localStorage.removeItem('bcproduct_name');
                }
                if (localStorage.getItem('bctranslatedproduct_name')) {
                    localStorage.removeItem('bctranslatedproduct_name');
                }
                if (localStorage.getItem('bcprice')) {
                    localStorage.removeItem('bcprice');
                }
                if (localStorage.getItem('bccurrencies')) {
                    localStorage.removeItem('bccurrencies');
                }
                if (localStorage.getItem('bcunit')) {
                    localStorage.removeItem('bcunit');
                }
                if (localStorage.getItem('bccategory')) {
                    localStorage.removeItem('bccategory');
                }
                if (localStorage.getItem('bctranslated_category')) {
                    localStorage.removeItem('bctranslated_category');
                }
            }
            //});
        });

        var old_row_qty;
        $(document).on("focus", '.quantity', function () {
            old_row_qty = $(this).val();
        }).on("change", '.quantity', function () {
            var row = $(this).closest('tr');
            if (!is_numeric($(this).val())) {
                $(this).val(old_row_qty);
                bootbox.alert(lang.unexpected_value);
                return;
            }
            var new_qty = parseFloat($(this).val()),
                item_id = row.attr('data-item-id');
            bbcitemsx[item_id].qty = new_qty;
            localStorage.setItem('bbcitemsx', JSON.stringify(bbcitemsx));
        });

    });

    function add_product_item(item) {
        ac = true;
        if (item == null) {
            return false;
        }
        item_id = item.id;
        if (bbcitemsx[item_id]) {
            bbcitemsx[item_id].qty = parseFloat(bbcitemsx[item_id].qty) + 1;
        } else {
            bbcitemsx[item_id] = item;
            bbcitemsx[item_id]['selected_variants'] = {};
            $.each(item.variants, function () {
                bbcitemsx[item_id]['selected_variants'][this.id] = 1;
            });
        }

        localStorage.setItem('bbcitemsx', JSON.stringify(bbcitemsx));
        loadItems();
        return true;

    }

    function loadItems () {

        if (localStorage.getItem('bbcitemsx')) {
            $("#bcTable tbody").empty();
            bbcitemsx = JSON.parse(localStorage.getItem('bbcitemsx'));

            $.each(bbcitemsx, function () {

                var item = this;
                var row_no = item.id;
                var vd = '';
                var newTr = $('<tr id="row_' + row_no + '" class="row_' + item.id + '" data-item-id="' + item.id + '"></tr>');
                tr_html = '<td><input name="product[]" type="hidden" value="' + item.id + '"><span id="name_' + row_no + '">' + item.name + ' (' + item.code + ')</span></td>';
                //tr_html += '<td><input class="form-control quantity text-center" name="quantity[]" type="text" value="' + formatDecimal(item.qty) + '" data-id="' + row_no + '" data-item="' + item.id + '" id="quantity_' + row_no + '" onClick="this.select();"></td>';
                if(item.variants) {
                    $.each(item.variants, function () {
                        vd += '<input name="vt_'+ item.id +'_'+ this.id +'" type="checkbox" class="checkbox" id="'+this.id+'" data-item-id="'+item.id+'" value="'+this.id+'" '+( item.selected_variants[this.id] == 1 ? 'checked="checked"' : '')+' style="display:inline-block;" /><label for="'+this.id+'" class="padding05">'+this.name+'</label>';
                    });
                }
                tr_html += '<td>'+vd+'</td>';
                tr_html += '<td class="text-center"><i class="fa fa-times tip del" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
                //tr_html += '<td class="hide"><input class="form-control quantity text-center" name="productid-' + row_no + '" type="hidden"  data-id="' + row_no + '" data-item="' + item.id + '" id="prductidentity_' + row_no + '"></td>';
                newTr.html(tr_html);
                newTr.appendTo("#bcTable");
            });
            $('input[type="checkbox"],[type="radio"]').not('.skip').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });
            return true;
        }
    }
</script>